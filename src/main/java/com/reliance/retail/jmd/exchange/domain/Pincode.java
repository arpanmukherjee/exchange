package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.State;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Pincode.
 */
@Entity
@Table(name = "pincode")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Pincode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "pin", nullable = false)
    private Integer pin;

    @Column(name = "city")
    private String city;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private State state;

    @Column(name = "country")
    private String country;

    @OneToMany(mappedBy = "pincode")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "vendorHub", "pincode" }, allowSetters = true)
    private Set<Address> addresses = new HashSet<>();

    @ManyToMany(mappedBy = "servingPincodes")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "servingPincodes", "approvedBy", "vendorHub" }, allowSetters = true)
    private Set<VendorHubMapping> vendorHubMappings = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Pincode id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPin() {
        return this.pin;
    }

    public Pincode pin(Integer pin) {
        this.setPin(pin);
        return this;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public String getCity() {
        return this.city;
    }

    public Pincode city(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public State getState() {
        return this.state;
    }

    public Pincode state(State state) {
        this.setState(state);
        return this;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getCountry() {
        return this.country;
    }

    public Pincode country(String country) {
        this.setCountry(country);
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Set<Address> getAddresses() {
        return this.addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        if (this.addresses != null) {
            this.addresses.forEach(i -> i.setPincode(null));
        }
        if (addresses != null) {
            addresses.forEach(i -> i.setPincode(this));
        }
        this.addresses = addresses;
    }

    public Pincode addresses(Set<Address> addresses) {
        this.setAddresses(addresses);
        return this;
    }

    public Pincode addAddress(Address address) {
        this.addresses.add(address);
        address.setPincode(this);
        return this;
    }

    public Pincode removeAddress(Address address) {
        this.addresses.remove(address);
        address.setPincode(null);
        return this;
    }

    public Set<VendorHubMapping> getVendorHubMappings() {
        return this.vendorHubMappings;
    }

    public void setVendorHubMappings(Set<VendorHubMapping> vendorHubMappings) {
        if (this.vendorHubMappings != null) {
            this.vendorHubMappings.forEach(i -> i.removeServingPincodes(this));
        }
        if (vendorHubMappings != null) {
            vendorHubMappings.forEach(i -> i.addServingPincodes(this));
        }
        this.vendorHubMappings = vendorHubMappings;
    }

    public Pincode vendorHubMappings(Set<VendorHubMapping> vendorHubMappings) {
        this.setVendorHubMappings(vendorHubMappings);
        return this;
    }

    public Pincode addVendorHubMappings(VendorHubMapping vendorHubMapping) {
        this.vendorHubMappings.add(vendorHubMapping);
        vendorHubMapping.getServingPincodes().add(this);
        return this;
    }

    public Pincode removeVendorHubMappings(VendorHubMapping vendorHubMapping) {
        this.vendorHubMappings.remove(vendorHubMapping);
        vendorHubMapping.getServingPincodes().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pincode)) {
            return false;
        }
        return id != null && id.equals(((Pincode) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pincode{" +
            "id=" + getId() +
            ", pin=" + getPin() +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            "}";
    }
}
