package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswer}.
 */
public interface ExchangeAnswerService {
    /**
     * Save a exchangeAnswer.
     *
     * @param exchangeAnswerDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangeAnswerDTO save(ExchangeAnswerDTO exchangeAnswerDTO);

    /**
     * Updates a exchangeAnswer.
     *
     * @param exchangeAnswerDTO the entity to update.
     * @return the persisted entity.
     */
    ExchangeAnswerDTO update(ExchangeAnswerDTO exchangeAnswerDTO);

    /**
     * Partially updates a exchangeAnswer.
     *
     * @param exchangeAnswerDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExchangeAnswerDTO> partialUpdate(ExchangeAnswerDTO exchangeAnswerDTO);

    /**
     * Get all the exchangeAnswers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangeAnswerDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exchangeAnswer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangeAnswerDTO> findOne(Long id);

    /**
     * Delete the "id" exchangeAnswer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
