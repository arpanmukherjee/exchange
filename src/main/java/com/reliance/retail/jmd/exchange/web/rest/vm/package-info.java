/**
 * View Models used by Spring MVC REST controllers.
 */
package com.reliance.retail.jmd.exchange.web.rest.vm;
