package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The ExchangeOrderStatus enumeration.
 */
public enum ExchangeOrderStatus {
    Created,
    Picked,
    DeliveredToDc,
    PickedFromDc,
    DeliveredToVendor,
    Cancelled,
}
