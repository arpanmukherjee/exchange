package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.Vendor;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vendor} and its DTO {@link VendorDTO}.
 */
@Mapper(componentModel = "spring")
public interface VendorMapper extends EntityMapper<VendorDTO, Vendor> {
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    VendorDTO toDto(Vendor s);

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);
}
