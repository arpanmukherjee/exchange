package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.PincodeDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.Pincode}.
 */
public interface PincodeService {
    /**
     * Save a pincode.
     *
     * @param pincodeDTO the entity to save.
     * @return the persisted entity.
     */
    PincodeDTO save(PincodeDTO pincodeDTO);

    /**
     * Updates a pincode.
     *
     * @param pincodeDTO the entity to update.
     * @return the persisted entity.
     */
    PincodeDTO update(PincodeDTO pincodeDTO);

    /**
     * Partially updates a pincode.
     *
     * @param pincodeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PincodeDTO> partialUpdate(PincodeDTO pincodeDTO);

    /**
     * Get all the pincodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PincodeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" pincode.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PincodeDTO> findOne(Long id);

    /**
     * Delete the "id" pincode.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
