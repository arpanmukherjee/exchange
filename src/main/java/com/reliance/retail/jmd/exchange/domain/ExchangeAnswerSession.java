package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.AnswerSessionType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExchangeAnswerSession.
 */
@Entity
@Table(name = "exchange_answer_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeAnswerSession implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "phone_no")
    private Integer phoneNo;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "browser_session_id")
    private String browserSessionId;

    @Enumerated(EnumType.STRING)
    @Column(name = "session_type")
    private AnswerSessionType sessionType;

    @JsonIgnoreProperties(value = { "set", "session", "approvedBy", "client", "vendorHub" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private ExchangePrice price;

    @OneToMany(mappedBy = "session")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "question", "option", "set", "session" }, allowSetters = true)
    private Set<ExchangeAnswer> exchangeAnswers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "productContexts", "exchangePrices", "exchangeOrders", "exchangeAnswerSessions", "questions", "options", "approvedBy" },
        allowSetters = true
    )
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "customerAddress", "vendorAddress", "dcAddress", "images", "exchangeAnswerSessions", "differentialAmounts", "client", "product",
        },
        allowSetters = true
    )
    private ExchangeOrder order;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExchangeAnswerSession id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPhoneNo() {
        return this.phoneNo;
    }

    public ExchangeAnswerSession phoneNo(Integer phoneNo) {
        this.setPhoneNo(phoneNo);
        return this;
    }

    public void setPhoneNo(Integer phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUserId() {
        return this.userId;
    }

    public ExchangeAnswerSession userId(String userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBrowserSessionId() {
        return this.browserSessionId;
    }

    public ExchangeAnswerSession browserSessionId(String browserSessionId) {
        this.setBrowserSessionId(browserSessionId);
        return this;
    }

    public void setBrowserSessionId(String browserSessionId) {
        this.browserSessionId = browserSessionId;
    }

    public AnswerSessionType getSessionType() {
        return this.sessionType;
    }

    public ExchangeAnswerSession sessionType(AnswerSessionType sessionType) {
        this.setSessionType(sessionType);
        return this;
    }

    public void setSessionType(AnswerSessionType sessionType) {
        this.sessionType = sessionType;
    }

    public ExchangePrice getPrice() {
        return this.price;
    }

    public void setPrice(ExchangePrice exchangePrice) {
        this.price = exchangePrice;
    }

    public ExchangeAnswerSession price(ExchangePrice exchangePrice) {
        this.setPrice(exchangePrice);
        return this;
    }

    public Set<ExchangeAnswer> getExchangeAnswers() {
        return this.exchangeAnswers;
    }

    public void setExchangeAnswers(Set<ExchangeAnswer> exchangeAnswers) {
        if (this.exchangeAnswers != null) {
            this.exchangeAnswers.forEach(i -> i.setSession(null));
        }
        if (exchangeAnswers != null) {
            exchangeAnswers.forEach(i -> i.setSession(this));
        }
        this.exchangeAnswers = exchangeAnswers;
    }

    public ExchangeAnswerSession exchangeAnswers(Set<ExchangeAnswer> exchangeAnswers) {
        this.setExchangeAnswers(exchangeAnswers);
        return this;
    }

    public ExchangeAnswerSession addExchangeAnswer(ExchangeAnswer exchangeAnswer) {
        this.exchangeAnswers.add(exchangeAnswer);
        exchangeAnswer.setSession(this);
        return this;
    }

    public ExchangeAnswerSession removeExchangeAnswer(ExchangeAnswer exchangeAnswer) {
        this.exchangeAnswers.remove(exchangeAnswer);
        exchangeAnswer.setSession(null);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ExchangeAnswerSession client(Client client) {
        this.setClient(client);
        return this;
    }

    public ExchangeOrder getOrder() {
        return this.order;
    }

    public void setOrder(ExchangeOrder exchangeOrder) {
        this.order = exchangeOrder;
    }

    public ExchangeAnswerSession order(ExchangeOrder exchangeOrder) {
        this.setOrder(exchangeOrder);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeAnswerSession)) {
            return false;
        }
        return id != null && id.equals(((ExchangeAnswerSession) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeAnswerSession{" +
            "id=" + getId() +
            ", phoneNo=" + getPhoneNo() +
            ", userId='" + getUserId() + "'" +
            ", browserSessionId='" + getBrowserSessionId() + "'" +
            ", sessionType='" + getSessionType() + "'" +
            "}";
    }
}
