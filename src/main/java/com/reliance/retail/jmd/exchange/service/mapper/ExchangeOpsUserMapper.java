package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangeOpsUser} and its DTO {@link ExchangeOpsUserDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangeOpsUserMapper extends EntityMapper<ExchangeOpsUserDTO, ExchangeOpsUser> {}
