package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.ExchangeOpsUserRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeOpsUserService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser}.
 */
@RestController
@RequestMapping("/api")
public class ExchangeOpsUserResource {

    private final Logger log = LoggerFactory.getLogger(ExchangeOpsUserResource.class);

    private static final String ENTITY_NAME = "exchangeOpsUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangeOpsUserService exchangeOpsUserService;

    private final ExchangeOpsUserRepository exchangeOpsUserRepository;

    public ExchangeOpsUserResource(ExchangeOpsUserService exchangeOpsUserService, ExchangeOpsUserRepository exchangeOpsUserRepository) {
        this.exchangeOpsUserService = exchangeOpsUserService;
        this.exchangeOpsUserRepository = exchangeOpsUserRepository;
    }

    /**
     * {@code POST  /exchange-ops-users} : Create a new exchangeOpsUser.
     *
     * @param exchangeOpsUserDTO the exchangeOpsUserDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangeOpsUserDTO, or with status {@code 400 (Bad Request)} if the exchangeOpsUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-ops-users")
    public ResponseEntity<ExchangeOpsUserDTO> createExchangeOpsUser(@Valid @RequestBody ExchangeOpsUserDTO exchangeOpsUserDTO)
        throws URISyntaxException {
        log.debug("REST request to save ExchangeOpsUser : {}", exchangeOpsUserDTO);
        if (exchangeOpsUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangeOpsUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangeOpsUserDTO result = exchangeOpsUserService.save(exchangeOpsUserDTO);
        return ResponseEntity
            .created(new URI("/api/exchange-ops-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-ops-users/:id} : Updates an existing exchangeOpsUser.
     *
     * @param id the id of the exchangeOpsUserDTO to save.
     * @param exchangeOpsUserDTO the exchangeOpsUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeOpsUserDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeOpsUserDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangeOpsUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-ops-users/{id}")
    public ResponseEntity<ExchangeOpsUserDTO> updateExchangeOpsUser(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ExchangeOpsUserDTO exchangeOpsUserDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExchangeOpsUser : {}, {}", id, exchangeOpsUserDTO);
        if (exchangeOpsUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeOpsUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeOpsUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExchangeOpsUserDTO result = exchangeOpsUserService.update(exchangeOpsUserDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeOpsUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /exchange-ops-users/:id} : Partial updates given fields of an existing exchangeOpsUser, field will ignore if it is null
     *
     * @param id the id of the exchangeOpsUserDTO to save.
     * @param exchangeOpsUserDTO the exchangeOpsUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeOpsUserDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeOpsUserDTO is not valid,
     * or with status {@code 404 (Not Found)} if the exchangeOpsUserDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the exchangeOpsUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/exchange-ops-users/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExchangeOpsUserDTO> partialUpdateExchangeOpsUser(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ExchangeOpsUserDTO exchangeOpsUserDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExchangeOpsUser partially : {}, {}", id, exchangeOpsUserDTO);
        if (exchangeOpsUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeOpsUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeOpsUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExchangeOpsUserDTO> result = exchangeOpsUserService.partialUpdate(exchangeOpsUserDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeOpsUserDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /exchange-ops-users} : get all the exchangeOpsUsers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangeOpsUsers in body.
     */
    @GetMapping("/exchange-ops-users")
    public ResponseEntity<List<ExchangeOpsUserDTO>> getAllExchangeOpsUsers(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of ExchangeOpsUsers");
        Page<ExchangeOpsUserDTO> page = exchangeOpsUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exchange-ops-users/:id} : get the "id" exchangeOpsUser.
     *
     * @param id the id of the exchangeOpsUserDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangeOpsUserDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-ops-users/{id}")
    public ResponseEntity<ExchangeOpsUserDTO> getExchangeOpsUser(@PathVariable Long id) {
        log.debug("REST request to get ExchangeOpsUser : {}", id);
        Optional<ExchangeOpsUserDTO> exchangeOpsUserDTO = exchangeOpsUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangeOpsUserDTO);
    }

    /**
     * {@code DELETE  /exchange-ops-users/:id} : delete the "id" exchangeOpsUser.
     *
     * @param id the id of the exchangeOpsUserDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-ops-users/{id}")
    public ResponseEntity<Void> deleteExchangeOpsUser(@PathVariable Long id) {
        log.debug("REST request to delete ExchangeOpsUser : {}", id);
        exchangeOpsUserService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
