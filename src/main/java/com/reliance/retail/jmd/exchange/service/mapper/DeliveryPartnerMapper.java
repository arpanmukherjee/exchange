package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.DeliveryPartner;
import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.service.dto.DeliveryPartnerDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DeliveryPartner} and its DTO {@link DeliveryPartnerDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeliveryPartnerMapper extends EntityMapper<DeliveryPartnerDTO, DeliveryPartner> {
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    DeliveryPartnerDTO toDto(DeliveryPartner s);

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);
}
