package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Pincode;
import com.reliance.retail.jmd.exchange.service.dto.PincodeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pincode} and its DTO {@link PincodeDTO}.
 */
@Mapper(componentModel = "spring")
public interface PincodeMapper extends EntityMapper<PincodeDTO, Pincode> {}
