package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet;
import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerSetRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeAnswerSetService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSetDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeAnswerSetMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExchangeAnswerSet}.
 */
@Service
@Transactional
public class ExchangeAnswerSetServiceImpl implements ExchangeAnswerSetService {

    private final Logger log = LoggerFactory.getLogger(ExchangeAnswerSetServiceImpl.class);

    private final ExchangeAnswerSetRepository exchangeAnswerSetRepository;

    private final ExchangeAnswerSetMapper exchangeAnswerSetMapper;

    public ExchangeAnswerSetServiceImpl(
        ExchangeAnswerSetRepository exchangeAnswerSetRepository,
        ExchangeAnswerSetMapper exchangeAnswerSetMapper
    ) {
        this.exchangeAnswerSetRepository = exchangeAnswerSetRepository;
        this.exchangeAnswerSetMapper = exchangeAnswerSetMapper;
    }

    @Override
    public ExchangeAnswerSetDTO save(ExchangeAnswerSetDTO exchangeAnswerSetDTO) {
        log.debug("Request to save ExchangeAnswerSet : {}", exchangeAnswerSetDTO);
        ExchangeAnswerSet exchangeAnswerSet = exchangeAnswerSetMapper.toEntity(exchangeAnswerSetDTO);
        exchangeAnswerSet = exchangeAnswerSetRepository.save(exchangeAnswerSet);
        return exchangeAnswerSetMapper.toDto(exchangeAnswerSet);
    }

    @Override
    public ExchangeAnswerSetDTO update(ExchangeAnswerSetDTO exchangeAnswerSetDTO) {
        log.debug("Request to update ExchangeAnswerSet : {}", exchangeAnswerSetDTO);
        ExchangeAnswerSet exchangeAnswerSet = exchangeAnswerSetMapper.toEntity(exchangeAnswerSetDTO);
        exchangeAnswerSet = exchangeAnswerSetRepository.save(exchangeAnswerSet);
        return exchangeAnswerSetMapper.toDto(exchangeAnswerSet);
    }

    @Override
    public Optional<ExchangeAnswerSetDTO> partialUpdate(ExchangeAnswerSetDTO exchangeAnswerSetDTO) {
        log.debug("Request to partially update ExchangeAnswerSet : {}", exchangeAnswerSetDTO);

        return exchangeAnswerSetRepository
            .findById(exchangeAnswerSetDTO.getId())
            .map(existingExchangeAnswerSet -> {
                exchangeAnswerSetMapper.partialUpdate(existingExchangeAnswerSet, exchangeAnswerSetDTO);

                return existingExchangeAnswerSet;
            })
            .map(exchangeAnswerSetRepository::save)
            .map(exchangeAnswerSetMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExchangeAnswerSetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangeAnswerSets");
        return exchangeAnswerSetRepository.findAll(pageable).map(exchangeAnswerSetMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangeAnswerSetDTO> findOne(Long id) {
        log.debug("Request to get ExchangeAnswerSet : {}", id);
        return exchangeAnswerSetRepository.findById(id).map(exchangeAnswerSetMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangeAnswerSet : {}", id);
        exchangeAnswerSetRepository.deleteById(id);
    }
}
