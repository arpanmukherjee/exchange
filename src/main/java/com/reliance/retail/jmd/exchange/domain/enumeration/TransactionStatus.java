package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The TransactionStatus enumeration.
 */
public enum TransactionStatus {
    Blocked,
    Debited,
    Credited,
}
