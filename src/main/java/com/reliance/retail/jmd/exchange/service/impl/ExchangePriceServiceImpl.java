package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ExchangePrice;
import com.reliance.retail.jmd.exchange.repository.ExchangePriceRepository;
import com.reliance.retail.jmd.exchange.service.ExchangePriceService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangePriceMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExchangePrice}.
 */
@Service
@Transactional
public class ExchangePriceServiceImpl implements ExchangePriceService {

    private final Logger log = LoggerFactory.getLogger(ExchangePriceServiceImpl.class);

    private final ExchangePriceRepository exchangePriceRepository;

    private final ExchangePriceMapper exchangePriceMapper;

    public ExchangePriceServiceImpl(ExchangePriceRepository exchangePriceRepository, ExchangePriceMapper exchangePriceMapper) {
        this.exchangePriceRepository = exchangePriceRepository;
        this.exchangePriceMapper = exchangePriceMapper;
    }

    @Override
    public ExchangePriceDTO save(ExchangePriceDTO exchangePriceDTO) {
        log.debug("Request to save ExchangePrice : {}", exchangePriceDTO);
        ExchangePrice exchangePrice = exchangePriceMapper.toEntity(exchangePriceDTO);
        exchangePrice = exchangePriceRepository.save(exchangePrice);
        return exchangePriceMapper.toDto(exchangePrice);
    }

    @Override
    public ExchangePriceDTO update(ExchangePriceDTO exchangePriceDTO) {
        log.debug("Request to update ExchangePrice : {}", exchangePriceDTO);
        ExchangePrice exchangePrice = exchangePriceMapper.toEntity(exchangePriceDTO);
        exchangePrice = exchangePriceRepository.save(exchangePrice);
        return exchangePriceMapper.toDto(exchangePrice);
    }

    @Override
    public Optional<ExchangePriceDTO> partialUpdate(ExchangePriceDTO exchangePriceDTO) {
        log.debug("Request to partially update ExchangePrice : {}", exchangePriceDTO);

        return exchangePriceRepository
            .findById(exchangePriceDTO.getId())
            .map(existingExchangePrice -> {
                exchangePriceMapper.partialUpdate(existingExchangePrice, exchangePriceDTO);

                return existingExchangePrice;
            })
            .map(exchangePriceRepository::save)
            .map(exchangePriceMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExchangePriceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangePrices");
        return exchangePriceRepository.findAll(pageable).map(exchangePriceMapper::toDto);
    }

    /**
     *  Get all the exchangePrices where Set is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ExchangePriceDTO> findAllWhereSetIsNull() {
        log.debug("Request to get all exchangePrices where Set is null");
        return StreamSupport
            .stream(exchangePriceRepository.findAll().spliterator(), false)
            .filter(exchangePrice -> exchangePrice.getSet() == null)
            .map(exchangePriceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get all the exchangePrices where Session is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ExchangePriceDTO> findAllWhereSessionIsNull() {
        log.debug("Request to get all exchangePrices where Session is null");
        return StreamSupport
            .stream(exchangePriceRepository.findAll().spliterator(), false)
            .filter(exchangePrice -> exchangePrice.getSession() == null)
            .map(exchangePriceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangePriceDTO> findOne(Long id) {
        log.debug("Request to get ExchangePrice : {}", id);
        return exchangePriceRepository.findById(id).map(exchangePriceMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangePrice : {}", id);
        exchangePriceRepository.deleteById(id);
    }
}
