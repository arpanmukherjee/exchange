package com.reliance.retail.jmd.exchange.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration =
            Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                    .build()
            );
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.reliance.retail.jmd.exchange.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.reliance.retail.jmd.exchange.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.reliance.retail.jmd.exchange.domain.User.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Authority.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.User.class.getName() + ".authorities");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".clients");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".deliveryPartners");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".vendors");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".vendorHubs");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".vendorHubMappings");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".exchangePrices");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser.class.getName() + ".differentialAmounts");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName() + ".productContexts");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName() + ".exchangePrices");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName() + ".exchangeOrders");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName() + ".exchangeAnswerSessions");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName() + ".questions");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Client.class.getName() + ".options");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.DeliveryPartner.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ProductContext.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ProductContext.class.getName() + ".documents");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ProductContext.class.getName() + ".exchangeOrders");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Vendor.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Vendor.class.getName() + ".documents");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Vendor.class.getName() + ".vendorHubs");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Document.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Pincode.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Pincode.class.getName() + ".addresses");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Pincode.class.getName() + ".vendorHubMappings");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Address.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.VendorHub.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.VendorHub.class.getName() + ".vendorHubMappings");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.VendorHub.class.getName() + ".exchangePrices");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.VendorHubMapping.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.VendorHubMapping.class.getName() + ".servingPincodes");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Question.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Question.class.getName() + ".options");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Question.class.getName() + ".clients");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Option.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Option.class.getName() + ".clients");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeAnswer.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet.class.getName() + ".exchangeAnswers");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangePrice.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Ledger.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.Ledger.class.getName() + ".ledgerTransactions");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.LedgerTransaction.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOrder.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOrder.class.getName() + ".images");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOrder.class.getName() + ".exchangeAnswerSessions");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeOrder.class.getName() + ".differentialAmounts");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession.class.getName());
            createCache(cm, com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession.class.getName() + ".exchangeAnswers");
            createCache(cm, com.reliance.retail.jmd.exchange.domain.DifferentialAmount.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
