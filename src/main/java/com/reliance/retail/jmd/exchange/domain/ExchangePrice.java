package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.PriceMetricStatus;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExchangePrice.
 */
@Entity
@Table(name = "exchange_price")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangePrice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "exchange_amount")
    private Double exchangeAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PriceMetricStatus status;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnoreProperties(value = { "price", "exchangeAnswers" }, allowSetters = true)
    @OneToOne(mappedBy = "price")
    private ExchangeAnswerSet set;

    @JsonIgnoreProperties(value = { "price", "exchangeAnswers", "client", "order" }, allowSetters = true)
    @OneToOne(mappedBy = "price")
    private ExchangeAnswerSession session;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "clients", "deliveryPartners", "vendors", "vendorHubs", "vendorHubMappings", "exchangePrices", "differentialAmounts" },
        allowSetters = true
    )
    private ExchangeOpsUser approvedBy;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "productContexts", "exchangePrices", "exchangeOrders", "exchangeAnswerSessions", "questions", "options", "approvedBy" },
        allowSetters = true
    )
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = { "address", "vendorHubMappings", "exchangePrices", "approvedBy", "vendor" }, allowSetters = true)
    private VendorHub vendorHub;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExchangePrice id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getExchangeAmount() {
        return this.exchangeAmount;
    }

    public ExchangePrice exchangeAmount(Double exchangeAmount) {
        this.setExchangeAmount(exchangeAmount);
        return this;
    }

    public void setExchangeAmount(Double exchangeAmount) {
        this.exchangeAmount = exchangeAmount;
    }

    public PriceMetricStatus getStatus() {
        return this.status;
    }

    public ExchangePrice status(PriceMetricStatus status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(PriceMetricStatus status) {
        this.status = status;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public ExchangePrice createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public ExchangePrice updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public ExchangePrice createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public ExchangePrice updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ExchangeAnswerSet getSet() {
        return this.set;
    }

    public void setSet(ExchangeAnswerSet exchangeAnswerSet) {
        if (this.set != null) {
            this.set.setPrice(null);
        }
        if (exchangeAnswerSet != null) {
            exchangeAnswerSet.setPrice(this);
        }
        this.set = exchangeAnswerSet;
    }

    public ExchangePrice set(ExchangeAnswerSet exchangeAnswerSet) {
        this.setSet(exchangeAnswerSet);
        return this;
    }

    public ExchangeAnswerSession getSession() {
        return this.session;
    }

    public void setSession(ExchangeAnswerSession exchangeAnswerSession) {
        if (this.session != null) {
            this.session.setPrice(null);
        }
        if (exchangeAnswerSession != null) {
            exchangeAnswerSession.setPrice(this);
        }
        this.session = exchangeAnswerSession;
    }

    public ExchangePrice session(ExchangeAnswerSession exchangeAnswerSession) {
        this.setSession(exchangeAnswerSession);
        return this;
    }

    public ExchangeOpsUser getApprovedBy() {
        return this.approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUser exchangeOpsUser) {
        this.approvedBy = exchangeOpsUser;
    }

    public ExchangePrice approvedBy(ExchangeOpsUser exchangeOpsUser) {
        this.setApprovedBy(exchangeOpsUser);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ExchangePrice client(Client client) {
        this.setClient(client);
        return this;
    }

    public VendorHub getVendorHub() {
        return this.vendorHub;
    }

    public void setVendorHub(VendorHub vendorHub) {
        this.vendorHub = vendorHub;
    }

    public ExchangePrice vendorHub(VendorHub vendorHub) {
        this.setVendorHub(vendorHub);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangePrice)) {
            return false;
        }
        return id != null && id.equals(((ExchangePrice) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangePrice{" +
            "id=" + getId() +
            ", exchangeAmount=" + getExchangeAmount() +
            ", status='" + getStatus() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
