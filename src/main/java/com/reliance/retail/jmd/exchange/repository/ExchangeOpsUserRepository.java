package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ExchangeOpsUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExchangeOpsUserRepository extends JpaRepository<ExchangeOpsUser, Long> {}
