package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet;
import com.reliance.retail.jmd.exchange.domain.ExchangePrice;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSetDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangeAnswerSet} and its DTO {@link ExchangeAnswerSetDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangeAnswerSetMapper extends EntityMapper<ExchangeAnswerSetDTO, ExchangeAnswerSet> {
    @Mapping(target = "price", source = "price", qualifiedByName = "exchangePriceId")
    ExchangeAnswerSetDTO toDto(ExchangeAnswerSet s);

    @Named("exchangePriceId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangePriceDTO toDtoExchangePriceId(ExchangePrice exchangePrice);
}
