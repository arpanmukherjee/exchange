package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.AnswerSessionType;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeAnswerSessionDTO implements Serializable {

    private Long id;

    private Integer phoneNo;

    private String userId;

    private String browserSessionId;

    private AnswerSessionType sessionType;

    private ExchangePriceDTO price;

    private ClientDTO client;

    private ExchangeOrderDTO order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(Integer phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBrowserSessionId() {
        return browserSessionId;
    }

    public void setBrowserSessionId(String browserSessionId) {
        this.browserSessionId = browserSessionId;
    }

    public AnswerSessionType getSessionType() {
        return sessionType;
    }

    public void setSessionType(AnswerSessionType sessionType) {
        this.sessionType = sessionType;
    }

    public ExchangePriceDTO getPrice() {
        return price;
    }

    public void setPrice(ExchangePriceDTO price) {
        this.price = price;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public ExchangeOrderDTO getOrder() {
        return order;
    }

    public void setOrder(ExchangeOrderDTO order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeAnswerSessionDTO)) {
            return false;
        }

        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = (ExchangeAnswerSessionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exchangeAnswerSessionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeAnswerSessionDTO{" +
            "id=" + getId() +
            ", phoneNo=" + getPhoneNo() +
            ", userId='" + getUserId() + "'" +
            ", browserSessionId='" + getBrowserSessionId() + "'" +
            ", sessionType='" + getSessionType() + "'" +
            ", price=" + getPrice() +
            ", client=" + getClient() +
            ", order=" + getOrder() +
            "}";
    }
}
