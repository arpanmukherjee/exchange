package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The TransactionType enumeration.
 */
public enum TransactionType {
    Credit,
    Debit,
}
