package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.DeliveryPartner;
import com.reliance.retail.jmd.exchange.repository.DeliveryPartnerRepository;
import com.reliance.retail.jmd.exchange.service.DeliveryPartnerService;
import com.reliance.retail.jmd.exchange.service.dto.DeliveryPartnerDTO;
import com.reliance.retail.jmd.exchange.service.mapper.DeliveryPartnerMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DeliveryPartner}.
 */
@Service
@Transactional
public class DeliveryPartnerServiceImpl implements DeliveryPartnerService {

    private final Logger log = LoggerFactory.getLogger(DeliveryPartnerServiceImpl.class);

    private final DeliveryPartnerRepository deliveryPartnerRepository;

    private final DeliveryPartnerMapper deliveryPartnerMapper;

    public DeliveryPartnerServiceImpl(DeliveryPartnerRepository deliveryPartnerRepository, DeliveryPartnerMapper deliveryPartnerMapper) {
        this.deliveryPartnerRepository = deliveryPartnerRepository;
        this.deliveryPartnerMapper = deliveryPartnerMapper;
    }

    @Override
    public DeliveryPartnerDTO save(DeliveryPartnerDTO deliveryPartnerDTO) {
        log.debug("Request to save DeliveryPartner : {}", deliveryPartnerDTO);
        DeliveryPartner deliveryPartner = deliveryPartnerMapper.toEntity(deliveryPartnerDTO);
        deliveryPartner = deliveryPartnerRepository.save(deliveryPartner);
        return deliveryPartnerMapper.toDto(deliveryPartner);
    }

    @Override
    public DeliveryPartnerDTO update(DeliveryPartnerDTO deliveryPartnerDTO) {
        log.debug("Request to update DeliveryPartner : {}", deliveryPartnerDTO);
        DeliveryPartner deliveryPartner = deliveryPartnerMapper.toEntity(deliveryPartnerDTO);
        deliveryPartner = deliveryPartnerRepository.save(deliveryPartner);
        return deliveryPartnerMapper.toDto(deliveryPartner);
    }

    @Override
    public Optional<DeliveryPartnerDTO> partialUpdate(DeliveryPartnerDTO deliveryPartnerDTO) {
        log.debug("Request to partially update DeliveryPartner : {}", deliveryPartnerDTO);

        return deliveryPartnerRepository
            .findById(deliveryPartnerDTO.getId())
            .map(existingDeliveryPartner -> {
                deliveryPartnerMapper.partialUpdate(existingDeliveryPartner, deliveryPartnerDTO);

                return existingDeliveryPartner;
            })
            .map(deliveryPartnerRepository::save)
            .map(deliveryPartnerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DeliveryPartnerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DeliveryPartners");
        return deliveryPartnerRepository.findAll(pageable).map(deliveryPartnerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DeliveryPartnerDTO> findOne(Long id) {
        log.debug("Request to get DeliveryPartner : {}", id);
        return deliveryPartnerRepository.findById(id).map(deliveryPartnerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DeliveryPartner : {}", id);
        deliveryPartnerRepository.deleteById(id);
    }
}
