package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Client;
import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.Option;
import com.reliance.retail.jmd.exchange.domain.Question;
import com.reliance.retail.jmd.exchange.service.dto.ClientDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.dto.OptionDTO;
import com.reliance.retail.jmd.exchange.service.dto.QuestionDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Client} and its DTO {@link ClientDTO}.
 */
@Mapper(componentModel = "spring")
public interface ClientMapper extends EntityMapper<ClientDTO, Client> {
    @Mapping(target = "questions", source = "questions", qualifiedByName = "questionIdSet")
    @Mapping(target = "options", source = "options", qualifiedByName = "optionIdSet")
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    ClientDTO toDto(Client s);

    @Mapping(target = "removeQuestions", ignore = true)
    @Mapping(target = "removeOptions", ignore = true)
    Client toEntity(ClientDTO clientDTO);

    @Named("questionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    QuestionDTO toDtoQuestionId(Question question);

    @Named("questionIdSet")
    default Set<QuestionDTO> toDtoQuestionIdSet(Set<Question> question) {
        return question.stream().map(this::toDtoQuestionId).collect(Collectors.toSet());
    }

    @Named("optionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OptionDTO toDtoOptionId(Option option);

    @Named("optionIdSet")
    default Set<OptionDTO> toDtoOptionIdSet(Set<Option> option) {
        return option.stream().map(this::toDtoOptionId).collect(Collectors.toSet());
    }

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);
}
