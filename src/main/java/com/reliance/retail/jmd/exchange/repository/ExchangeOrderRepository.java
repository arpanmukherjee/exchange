package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ExchangeOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExchangeOrderRepository extends JpaRepository<ExchangeOrder, Long> {}
