package com.reliance.retail.jmd.exchange.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeAnswerSetDTO implements Serializable {

    private Long id;

    private String tag;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private String createdBy;

    private String updatedBy;

    private ExchangePriceDTO price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ExchangePriceDTO getPrice() {
        return price;
    }

    public void setPrice(ExchangePriceDTO price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeAnswerSetDTO)) {
            return false;
        }

        ExchangeAnswerSetDTO exchangeAnswerSetDTO = (ExchangeAnswerSetDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exchangeAnswerSetDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeAnswerSetDTO{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
