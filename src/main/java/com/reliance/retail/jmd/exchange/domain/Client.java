package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "client_api_key")
    private String clientApiKey;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @OneToMany(mappedBy = "client")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "documents", "exchangeOrders", "client" }, allowSetters = true)
    private Set<ProductContext> productContexts = new HashSet<>();

    @OneToMany(mappedBy = "client")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "set", "session", "approvedBy", "client", "vendorHub" }, allowSetters = true)
    private Set<ExchangePrice> exchangePrices = new HashSet<>();

    @OneToMany(mappedBy = "client")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = {
            "customerAddress", "vendorAddress", "dcAddress", "images", "exchangeAnswerSessions", "differentialAmounts", "client", "product",
        },
        allowSetters = true
    )
    private Set<ExchangeOrder> exchangeOrders = new HashSet<>();

    @OneToMany(mappedBy = "client")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "price", "exchangeAnswers", "client", "order" }, allowSetters = true)
    private Set<ExchangeAnswerSession> exchangeAnswerSessions = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_client__questions",
        joinColumns = @JoinColumn(name = "client_id"),
        inverseJoinColumns = @JoinColumn(name = "questions_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "options", "clients" }, allowSetters = true)
    private Set<Question> questions = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_client__options",
        joinColumns = @JoinColumn(name = "client_id"),
        inverseJoinColumns = @JoinColumn(name = "options_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "question", "clients" }, allowSetters = true)
    private Set<Option> options = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "clients", "deliveryPartners", "vendors", "vendorHubs", "vendorHubMappings", "exchangePrices", "differentialAmounts" },
        allowSetters = true
    )
    private ExchangeOpsUser approvedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Client id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Client name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientApiKey() {
        return this.clientApiKey;
    }

    public Client clientApiKey(String clientApiKey) {
        this.setClientApiKey(clientApiKey);
        return this;
    }

    public void setClientApiKey(String clientApiKey) {
        this.clientApiKey = clientApiKey;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public Client isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public Client createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public Client updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Client createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public Client updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<ProductContext> getProductContexts() {
        return this.productContexts;
    }

    public void setProductContexts(Set<ProductContext> productContexts) {
        if (this.productContexts != null) {
            this.productContexts.forEach(i -> i.setClient(null));
        }
        if (productContexts != null) {
            productContexts.forEach(i -> i.setClient(this));
        }
        this.productContexts = productContexts;
    }

    public Client productContexts(Set<ProductContext> productContexts) {
        this.setProductContexts(productContexts);
        return this;
    }

    public Client addProductContext(ProductContext productContext) {
        this.productContexts.add(productContext);
        productContext.setClient(this);
        return this;
    }

    public Client removeProductContext(ProductContext productContext) {
        this.productContexts.remove(productContext);
        productContext.setClient(null);
        return this;
    }

    public Set<ExchangePrice> getExchangePrices() {
        return this.exchangePrices;
    }

    public void setExchangePrices(Set<ExchangePrice> exchangePrices) {
        if (this.exchangePrices != null) {
            this.exchangePrices.forEach(i -> i.setClient(null));
        }
        if (exchangePrices != null) {
            exchangePrices.forEach(i -> i.setClient(this));
        }
        this.exchangePrices = exchangePrices;
    }

    public Client exchangePrices(Set<ExchangePrice> exchangePrices) {
        this.setExchangePrices(exchangePrices);
        return this;
    }

    public Client addExchangePrice(ExchangePrice exchangePrice) {
        this.exchangePrices.add(exchangePrice);
        exchangePrice.setClient(this);
        return this;
    }

    public Client removeExchangePrice(ExchangePrice exchangePrice) {
        this.exchangePrices.remove(exchangePrice);
        exchangePrice.setClient(null);
        return this;
    }

    public Set<ExchangeOrder> getExchangeOrders() {
        return this.exchangeOrders;
    }

    public void setExchangeOrders(Set<ExchangeOrder> exchangeOrders) {
        if (this.exchangeOrders != null) {
            this.exchangeOrders.forEach(i -> i.setClient(null));
        }
        if (exchangeOrders != null) {
            exchangeOrders.forEach(i -> i.setClient(this));
        }
        this.exchangeOrders = exchangeOrders;
    }

    public Client exchangeOrders(Set<ExchangeOrder> exchangeOrders) {
        this.setExchangeOrders(exchangeOrders);
        return this;
    }

    public Client addExchangeOrder(ExchangeOrder exchangeOrder) {
        this.exchangeOrders.add(exchangeOrder);
        exchangeOrder.setClient(this);
        return this;
    }

    public Client removeExchangeOrder(ExchangeOrder exchangeOrder) {
        this.exchangeOrders.remove(exchangeOrder);
        exchangeOrder.setClient(null);
        return this;
    }

    public Set<ExchangeAnswerSession> getExchangeAnswerSessions() {
        return this.exchangeAnswerSessions;
    }

    public void setExchangeAnswerSessions(Set<ExchangeAnswerSession> exchangeAnswerSessions) {
        if (this.exchangeAnswerSessions != null) {
            this.exchangeAnswerSessions.forEach(i -> i.setClient(null));
        }
        if (exchangeAnswerSessions != null) {
            exchangeAnswerSessions.forEach(i -> i.setClient(this));
        }
        this.exchangeAnswerSessions = exchangeAnswerSessions;
    }

    public Client exchangeAnswerSessions(Set<ExchangeAnswerSession> exchangeAnswerSessions) {
        this.setExchangeAnswerSessions(exchangeAnswerSessions);
        return this;
    }

    public Client addExchangeAnswerSession(ExchangeAnswerSession exchangeAnswerSession) {
        this.exchangeAnswerSessions.add(exchangeAnswerSession);
        exchangeAnswerSession.setClient(this);
        return this;
    }

    public Client removeExchangeAnswerSession(ExchangeAnswerSession exchangeAnswerSession) {
        this.exchangeAnswerSessions.remove(exchangeAnswerSession);
        exchangeAnswerSession.setClient(null);
        return this;
    }

    public Set<Question> getQuestions() {
        return this.questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Client questions(Set<Question> questions) {
        this.setQuestions(questions);
        return this;
    }

    public Client addQuestions(Question question) {
        this.questions.add(question);
        question.getClients().add(this);
        return this;
    }

    public Client removeQuestions(Question question) {
        this.questions.remove(question);
        question.getClients().remove(this);
        return this;
    }

    public Set<Option> getOptions() {
        return this.options;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }

    public Client options(Set<Option> options) {
        this.setOptions(options);
        return this;
    }

    public Client addOptions(Option option) {
        this.options.add(option);
        option.getClients().add(this);
        return this;
    }

    public Client removeOptions(Option option) {
        this.options.remove(option);
        option.getClients().remove(this);
        return this;
    }

    public ExchangeOpsUser getApprovedBy() {
        return this.approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUser exchangeOpsUser) {
        this.approvedBy = exchangeOpsUser;
    }

    public Client approvedBy(ExchangeOpsUser exchangeOpsUser) {
        this.setApprovedBy(exchangeOpsUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", clientApiKey='" + getClientApiKey() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
