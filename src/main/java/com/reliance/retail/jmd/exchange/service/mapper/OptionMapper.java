package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Option;
import com.reliance.retail.jmd.exchange.domain.Question;
import com.reliance.retail.jmd.exchange.service.dto.OptionDTO;
import com.reliance.retail.jmd.exchange.service.dto.QuestionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Option} and its DTO {@link OptionDTO}.
 */
@Mapper(componentModel = "spring")
public interface OptionMapper extends EntityMapper<OptionDTO, Option> {
    @Mapping(target = "question", source = "question", qualifiedByName = "questionId")
    OptionDTO toDto(Option s);

    @Named("questionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    QuestionDTO toDtoQuestionId(Question question);
}
