package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The PriceMetricStatus enumeration.
 */
public enum PriceMetricStatus {
    Approved,
    Rejected,
    Pending,
}
