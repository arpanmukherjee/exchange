package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.VendorHubMappingDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.VendorHubMapping}.
 */
public interface VendorHubMappingService {
    /**
     * Save a vendorHubMapping.
     *
     * @param vendorHubMappingDTO the entity to save.
     * @return the persisted entity.
     */
    VendorHubMappingDTO save(VendorHubMappingDTO vendorHubMappingDTO);

    /**
     * Updates a vendorHubMapping.
     *
     * @param vendorHubMappingDTO the entity to update.
     * @return the persisted entity.
     */
    VendorHubMappingDTO update(VendorHubMappingDTO vendorHubMappingDTO);

    /**
     * Partially updates a vendorHubMapping.
     *
     * @param vendorHubMappingDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<VendorHubMappingDTO> partialUpdate(VendorHubMappingDTO vendorHubMappingDTO);

    /**
     * Get all the vendorHubMappings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VendorHubMappingDTO> findAll(Pageable pageable);

    /**
     * Get all the vendorHubMappings with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VendorHubMappingDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" vendorHubMapping.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VendorHubMappingDTO> findOne(Long id);

    /**
     * Delete the "id" vendorHubMapping.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
