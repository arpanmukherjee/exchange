package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Address;
import com.reliance.retail.jmd.exchange.domain.Pincode;
import com.reliance.retail.jmd.exchange.service.dto.AddressDTO;
import com.reliance.retail.jmd.exchange.service.dto.PincodeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Address} and its DTO {@link AddressDTO}.
 */
@Mapper(componentModel = "spring")
public interface AddressMapper extends EntityMapper<AddressDTO, Address> {
    @Mapping(target = "pincode", source = "pincode", qualifiedByName = "pincodeId")
    AddressDTO toDto(Address s);

    @Named("pincodeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PincodeDTO toDtoPincodeId(Pincode pincode);
}
