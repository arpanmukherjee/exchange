package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Vendor.
 */
@Entity
@Table(name = "vendor")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "pan_no", nullable = false)
    private String panNo;

    @Column(name = "aadhar_no")
    private String aadharNo;

    @Column(name = "gst_no")
    private String gstNo;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @OneToMany(mappedBy = "vendor")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "vendor", "product", "order" }, allowSetters = true)
    private Set<Document> documents = new HashSet<>();

    @OneToMany(mappedBy = "vendor")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "address", "vendorHubMappings", "exchangePrices", "approvedBy", "vendor" }, allowSetters = true)
    private Set<VendorHub> vendorHubs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "clients", "deliveryPartners", "vendors", "vendorHubs", "vendorHubMappings", "exchangePrices", "differentialAmounts" },
        allowSetters = true
    )
    private ExchangeOpsUser approvedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Vendor id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Vendor name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPanNo() {
        return this.panNo;
    }

    public Vendor panNo(String panNo) {
        this.setPanNo(panNo);
        return this;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getAadharNo() {
        return this.aadharNo;
    }

    public Vendor aadharNo(String aadharNo) {
        this.setAadharNo(aadharNo);
        return this;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }

    public String getGstNo() {
        return this.gstNo;
    }

    public Vendor gstNo(String gstNo) {
        this.setGstNo(gstNo);
        return this;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public Vendor isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public Vendor createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public Vendor updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Vendor createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public Vendor updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<Document> getDocuments() {
        return this.documents;
    }

    public void setDocuments(Set<Document> documents) {
        if (this.documents != null) {
            this.documents.forEach(i -> i.setVendor(null));
        }
        if (documents != null) {
            documents.forEach(i -> i.setVendor(this));
        }
        this.documents = documents;
    }

    public Vendor documents(Set<Document> documents) {
        this.setDocuments(documents);
        return this;
    }

    public Vendor addDocument(Document document) {
        this.documents.add(document);
        document.setVendor(this);
        return this;
    }

    public Vendor removeDocument(Document document) {
        this.documents.remove(document);
        document.setVendor(null);
        return this;
    }

    public Set<VendorHub> getVendorHubs() {
        return this.vendorHubs;
    }

    public void setVendorHubs(Set<VendorHub> vendorHubs) {
        if (this.vendorHubs != null) {
            this.vendorHubs.forEach(i -> i.setVendor(null));
        }
        if (vendorHubs != null) {
            vendorHubs.forEach(i -> i.setVendor(this));
        }
        this.vendorHubs = vendorHubs;
    }

    public Vendor vendorHubs(Set<VendorHub> vendorHubs) {
        this.setVendorHubs(vendorHubs);
        return this;
    }

    public Vendor addVendorHub(VendorHub vendorHub) {
        this.vendorHubs.add(vendorHub);
        vendorHub.setVendor(this);
        return this;
    }

    public Vendor removeVendorHub(VendorHub vendorHub) {
        this.vendorHubs.remove(vendorHub);
        vendorHub.setVendor(null);
        return this;
    }

    public ExchangeOpsUser getApprovedBy() {
        return this.approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUser exchangeOpsUser) {
        this.approvedBy = exchangeOpsUser;
    }

    public Vendor approvedBy(ExchangeOpsUser exchangeOpsUser) {
        this.setApprovedBy(exchangeOpsUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vendor)) {
            return false;
        }
        return id != null && id.equals(((Vendor) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vendor{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", panNo='" + getPanNo() + "'" +
            ", aadharNo='" + getAadharNo() + "'" +
            ", gstNo='" + getGstNo() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
