package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.Pincode;
import com.reliance.retail.jmd.exchange.domain.VendorHub;
import com.reliance.retail.jmd.exchange.domain.VendorHubMapping;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.dto.PincodeDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubMappingDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link VendorHubMapping} and its DTO {@link VendorHubMappingDTO}.
 */
@Mapper(componentModel = "spring")
public interface VendorHubMappingMapper extends EntityMapper<VendorHubMappingDTO, VendorHubMapping> {
    @Mapping(target = "servingPincodes", source = "servingPincodes", qualifiedByName = "pincodeIdSet")
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    @Mapping(target = "vendorHub", source = "vendorHub", qualifiedByName = "vendorHubId")
    VendorHubMappingDTO toDto(VendorHubMapping s);

    @Mapping(target = "removeServingPincodes", ignore = true)
    VendorHubMapping toEntity(VendorHubMappingDTO vendorHubMappingDTO);

    @Named("pincodeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PincodeDTO toDtoPincodeId(Pincode pincode);

    @Named("pincodeIdSet")
    default Set<PincodeDTO> toDtoPincodeIdSet(Set<Pincode> pincode) {
        return pincode.stream().map(this::toDtoPincodeId).collect(Collectors.toSet());
    }

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);

    @Named("vendorHubId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    VendorHubDTO toDtoVendorHubId(VendorHub vendorHub);
}
