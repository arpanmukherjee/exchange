package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.DeliveryPartnerRepository;
import com.reliance.retail.jmd.exchange.service.DeliveryPartnerService;
import com.reliance.retail.jmd.exchange.service.dto.DeliveryPartnerDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.DeliveryPartner}.
 */
@RestController
@RequestMapping("/api")
public class DeliveryPartnerResource {

    private final Logger log = LoggerFactory.getLogger(DeliveryPartnerResource.class);

    private static final String ENTITY_NAME = "deliveryPartner";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeliveryPartnerService deliveryPartnerService;

    private final DeliveryPartnerRepository deliveryPartnerRepository;

    public DeliveryPartnerResource(DeliveryPartnerService deliveryPartnerService, DeliveryPartnerRepository deliveryPartnerRepository) {
        this.deliveryPartnerService = deliveryPartnerService;
        this.deliveryPartnerRepository = deliveryPartnerRepository;
    }

    /**
     * {@code POST  /delivery-partners} : Create a new deliveryPartner.
     *
     * @param deliveryPartnerDTO the deliveryPartnerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deliveryPartnerDTO, or with status {@code 400 (Bad Request)} if the deliveryPartner has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delivery-partners")
    public ResponseEntity<DeliveryPartnerDTO> createDeliveryPartner(@Valid @RequestBody DeliveryPartnerDTO deliveryPartnerDTO)
        throws URISyntaxException {
        log.debug("REST request to save DeliveryPartner : {}", deliveryPartnerDTO);
        if (deliveryPartnerDTO.getId() != null) {
            throw new BadRequestAlertException("A new deliveryPartner cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeliveryPartnerDTO result = deliveryPartnerService.save(deliveryPartnerDTO);
        return ResponseEntity
            .created(new URI("/api/delivery-partners/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delivery-partners/:id} : Updates an existing deliveryPartner.
     *
     * @param id the id of the deliveryPartnerDTO to save.
     * @param deliveryPartnerDTO the deliveryPartnerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryPartnerDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryPartnerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deliveryPartnerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delivery-partners/{id}")
    public ResponseEntity<DeliveryPartnerDTO> updateDeliveryPartner(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DeliveryPartnerDTO deliveryPartnerDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DeliveryPartner : {}, {}", id, deliveryPartnerDTO);
        if (deliveryPartnerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryPartnerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!deliveryPartnerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DeliveryPartnerDTO result = deliveryPartnerService.update(deliveryPartnerDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, deliveryPartnerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /delivery-partners/:id} : Partial updates given fields of an existing deliveryPartner, field will ignore if it is null
     *
     * @param id the id of the deliveryPartnerDTO to save.
     * @param deliveryPartnerDTO the deliveryPartnerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryPartnerDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryPartnerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the deliveryPartnerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the deliveryPartnerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/delivery-partners/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DeliveryPartnerDTO> partialUpdateDeliveryPartner(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DeliveryPartnerDTO deliveryPartnerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DeliveryPartner partially : {}, {}", id, deliveryPartnerDTO);
        if (deliveryPartnerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryPartnerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!deliveryPartnerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DeliveryPartnerDTO> result = deliveryPartnerService.partialUpdate(deliveryPartnerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, deliveryPartnerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /delivery-partners} : get all the deliveryPartners.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deliveryPartners in body.
     */
    @GetMapping("/delivery-partners")
    public ResponseEntity<List<DeliveryPartnerDTO>> getAllDeliveryPartners(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of DeliveryPartners");
        Page<DeliveryPartnerDTO> page = deliveryPartnerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /delivery-partners/:id} : get the "id" deliveryPartner.
     *
     * @param id the id of the deliveryPartnerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deliveryPartnerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delivery-partners/{id}")
    public ResponseEntity<DeliveryPartnerDTO> getDeliveryPartner(@PathVariable Long id) {
        log.debug("REST request to get DeliveryPartner : {}", id);
        Optional<DeliveryPartnerDTO> deliveryPartnerDTO = deliveryPartnerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deliveryPartnerDTO);
    }

    /**
     * {@code DELETE  /delivery-partners/:id} : delete the "id" deliveryPartner.
     *
     * @param id the id of the deliveryPartnerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delivery-partners/{id}")
    public ResponseEntity<Void> deleteDeliveryPartner(@PathVariable Long id) {
        log.debug("REST request to delete DeliveryPartner : {}", id);
        deliveryPartnerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
