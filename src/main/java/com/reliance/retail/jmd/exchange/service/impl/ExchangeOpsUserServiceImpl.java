package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.repository.ExchangeOpsUserRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeOpsUserService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeOpsUserMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExchangeOpsUser}.
 */
@Service
@Transactional
public class ExchangeOpsUserServiceImpl implements ExchangeOpsUserService {

    private final Logger log = LoggerFactory.getLogger(ExchangeOpsUserServiceImpl.class);

    private final ExchangeOpsUserRepository exchangeOpsUserRepository;

    private final ExchangeOpsUserMapper exchangeOpsUserMapper;

    public ExchangeOpsUserServiceImpl(ExchangeOpsUserRepository exchangeOpsUserRepository, ExchangeOpsUserMapper exchangeOpsUserMapper) {
        this.exchangeOpsUserRepository = exchangeOpsUserRepository;
        this.exchangeOpsUserMapper = exchangeOpsUserMapper;
    }

    @Override
    public ExchangeOpsUserDTO save(ExchangeOpsUserDTO exchangeOpsUserDTO) {
        log.debug("Request to save ExchangeOpsUser : {}", exchangeOpsUserDTO);
        ExchangeOpsUser exchangeOpsUser = exchangeOpsUserMapper.toEntity(exchangeOpsUserDTO);
        exchangeOpsUser = exchangeOpsUserRepository.save(exchangeOpsUser);
        return exchangeOpsUserMapper.toDto(exchangeOpsUser);
    }

    @Override
    public ExchangeOpsUserDTO update(ExchangeOpsUserDTO exchangeOpsUserDTO) {
        log.debug("Request to update ExchangeOpsUser : {}", exchangeOpsUserDTO);
        ExchangeOpsUser exchangeOpsUser = exchangeOpsUserMapper.toEntity(exchangeOpsUserDTO);
        exchangeOpsUser = exchangeOpsUserRepository.save(exchangeOpsUser);
        return exchangeOpsUserMapper.toDto(exchangeOpsUser);
    }

    @Override
    public Optional<ExchangeOpsUserDTO> partialUpdate(ExchangeOpsUserDTO exchangeOpsUserDTO) {
        log.debug("Request to partially update ExchangeOpsUser : {}", exchangeOpsUserDTO);

        return exchangeOpsUserRepository
            .findById(exchangeOpsUserDTO.getId())
            .map(existingExchangeOpsUser -> {
                exchangeOpsUserMapper.partialUpdate(existingExchangeOpsUser, exchangeOpsUserDTO);

                return existingExchangeOpsUser;
            })
            .map(exchangeOpsUserRepository::save)
            .map(exchangeOpsUserMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExchangeOpsUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangeOpsUsers");
        return exchangeOpsUserRepository.findAll(pageable).map(exchangeOpsUserMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangeOpsUserDTO> findOne(Long id) {
        log.debug("Request to get ExchangeOpsUser : {}", id);
        return exchangeOpsUserRepository.findById(id).map(exchangeOpsUserMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangeOpsUser : {}", id);
        exchangeOpsUserRepository.deleteById(id);
    }
}
