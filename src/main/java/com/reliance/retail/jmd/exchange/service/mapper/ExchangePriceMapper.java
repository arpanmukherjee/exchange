package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Client;
import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.ExchangePrice;
import com.reliance.retail.jmd.exchange.domain.VendorHub;
import com.reliance.retail.jmd.exchange.service.dto.ClientDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangePrice} and its DTO {@link ExchangePriceDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangePriceMapper extends EntityMapper<ExchangePriceDTO, ExchangePrice> {
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    @Mapping(target = "vendorHub", source = "vendorHub", qualifiedByName = "vendorHubId")
    ExchangePriceDTO toDto(ExchangePrice s);

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("vendorHubId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    VendorHubDTO toDtoVendorHubId(VendorHub vendorHub);
}
