package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ExchangeAnswerSession entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExchangeAnswerSessionRepository extends JpaRepository<ExchangeAnswerSession, Long> {}
