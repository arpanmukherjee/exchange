package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.ExchangePrice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ExchangePrice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExchangePriceRepository extends JpaRepository<ExchangePrice, Long> {}
