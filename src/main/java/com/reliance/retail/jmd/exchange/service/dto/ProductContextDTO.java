package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.CategoryConetxtType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.ProductContext} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProductContextDTO implements Serializable {

    private Long id;

    private CategoryConetxtType categoryContextType;

    private String clientProductId;

    private String clientProductName;

    private String clientProductUrl;

    private Boolean isActive;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private String createdBy;

    private String updatedBy;

    private ClientDTO client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CategoryConetxtType getCategoryContextType() {
        return categoryContextType;
    }

    public void setCategoryContextType(CategoryConetxtType categoryContextType) {
        this.categoryContextType = categoryContextType;
    }

    public String getClientProductId() {
        return clientProductId;
    }

    public void setClientProductId(String clientProductId) {
        this.clientProductId = clientProductId;
    }

    public String getClientProductName() {
        return clientProductName;
    }

    public void setClientProductName(String clientProductName) {
        this.clientProductName = clientProductName;
    }

    public String getClientProductUrl() {
        return clientProductUrl;
    }

    public void setClientProductUrl(String clientProductUrl) {
        this.clientProductUrl = clientProductUrl;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductContextDTO)) {
            return false;
        }

        ProductContextDTO productContextDTO = (ProductContextDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, productContextDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductContextDTO{" +
            "id=" + getId() +
            ", categoryContextType='" + getCategoryContextType() + "'" +
            ", clientProductId='" + getClientProductId() + "'" +
            ", clientProductName='" + getClientProductName() + "'" +
            ", clientProductUrl='" + getClientProductUrl() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", client=" + getClient() +
            "}";
    }
}
