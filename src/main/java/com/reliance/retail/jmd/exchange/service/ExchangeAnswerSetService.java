package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSetDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet}.
 */
public interface ExchangeAnswerSetService {
    /**
     * Save a exchangeAnswerSet.
     *
     * @param exchangeAnswerSetDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangeAnswerSetDTO save(ExchangeAnswerSetDTO exchangeAnswerSetDTO);

    /**
     * Updates a exchangeAnswerSet.
     *
     * @param exchangeAnswerSetDTO the entity to update.
     * @return the persisted entity.
     */
    ExchangeAnswerSetDTO update(ExchangeAnswerSetDTO exchangeAnswerSetDTO);

    /**
     * Partially updates a exchangeAnswerSet.
     *
     * @param exchangeAnswerSetDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExchangeAnswerSetDTO> partialUpdate(ExchangeAnswerSetDTO exchangeAnswerSetDTO);

    /**
     * Get all the exchangeAnswerSets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangeAnswerSetDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exchangeAnswerSet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangeAnswerSetDTO> findOne(Long id);

    /**
     * Delete the "id" exchangeAnswerSet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
