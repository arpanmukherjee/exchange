package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import com.reliance.retail.jmd.exchange.repository.ExchangeOrderRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeOrderService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeOrderMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExchangeOrder}.
 */
@Service
@Transactional
public class ExchangeOrderServiceImpl implements ExchangeOrderService {

    private final Logger log = LoggerFactory.getLogger(ExchangeOrderServiceImpl.class);

    private final ExchangeOrderRepository exchangeOrderRepository;

    private final ExchangeOrderMapper exchangeOrderMapper;

    public ExchangeOrderServiceImpl(ExchangeOrderRepository exchangeOrderRepository, ExchangeOrderMapper exchangeOrderMapper) {
        this.exchangeOrderRepository = exchangeOrderRepository;
        this.exchangeOrderMapper = exchangeOrderMapper;
    }

    @Override
    public ExchangeOrderDTO save(ExchangeOrderDTO exchangeOrderDTO) {
        log.debug("Request to save ExchangeOrder : {}", exchangeOrderDTO);
        ExchangeOrder exchangeOrder = exchangeOrderMapper.toEntity(exchangeOrderDTO);
        exchangeOrder = exchangeOrderRepository.save(exchangeOrder);
        return exchangeOrderMapper.toDto(exchangeOrder);
    }

    @Override
    public ExchangeOrderDTO update(ExchangeOrderDTO exchangeOrderDTO) {
        log.debug("Request to update ExchangeOrder : {}", exchangeOrderDTO);
        ExchangeOrder exchangeOrder = exchangeOrderMapper.toEntity(exchangeOrderDTO);
        exchangeOrder = exchangeOrderRepository.save(exchangeOrder);
        return exchangeOrderMapper.toDto(exchangeOrder);
    }

    @Override
    public Optional<ExchangeOrderDTO> partialUpdate(ExchangeOrderDTO exchangeOrderDTO) {
        log.debug("Request to partially update ExchangeOrder : {}", exchangeOrderDTO);

        return exchangeOrderRepository
            .findById(exchangeOrderDTO.getId())
            .map(existingExchangeOrder -> {
                exchangeOrderMapper.partialUpdate(existingExchangeOrder, exchangeOrderDTO);

                return existingExchangeOrder;
            })
            .map(exchangeOrderRepository::save)
            .map(exchangeOrderMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExchangeOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangeOrders");
        return exchangeOrderRepository.findAll(pageable).map(exchangeOrderMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangeOrderDTO> findOne(Long id) {
        log.debug("Request to get ExchangeOrder : {}", id);
        return exchangeOrderRepository.findById(id).map(exchangeOrderMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangeOrder : {}", id);
        exchangeOrderRepository.deleteById(id);
    }
}
