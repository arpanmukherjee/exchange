package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Address;
import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.Vendor;
import com.reliance.retail.jmd.exchange.domain.VendorHub;
import com.reliance.retail.jmd.exchange.service.dto.AddressDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link VendorHub} and its DTO {@link VendorHubDTO}.
 */
@Mapper(componentModel = "spring")
public interface VendorHubMapper extends EntityMapper<VendorHubDTO, VendorHub> {
    @Mapping(target = "address", source = "address", qualifiedByName = "addressId")
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    @Mapping(target = "vendor", source = "vendor", qualifiedByName = "vendorId")
    VendorHubDTO toDto(VendorHub s);

    @Named("addressId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AddressDTO toDtoAddressId(Address address);

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);

    @Named("vendorId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    VendorDTO toDtoVendorId(Vendor vendor);
}
