package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerSessionRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeAnswerSessionService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSessionDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession}.
 */
@RestController
@RequestMapping("/api")
public class ExchangeAnswerSessionResource {

    private final Logger log = LoggerFactory.getLogger(ExchangeAnswerSessionResource.class);

    private static final String ENTITY_NAME = "exchangeAnswerSession";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangeAnswerSessionService exchangeAnswerSessionService;

    private final ExchangeAnswerSessionRepository exchangeAnswerSessionRepository;

    public ExchangeAnswerSessionResource(
        ExchangeAnswerSessionService exchangeAnswerSessionService,
        ExchangeAnswerSessionRepository exchangeAnswerSessionRepository
    ) {
        this.exchangeAnswerSessionService = exchangeAnswerSessionService;
        this.exchangeAnswerSessionRepository = exchangeAnswerSessionRepository;
    }

    /**
     * {@code POST  /exchange-answer-sessions} : Create a new exchangeAnswerSession.
     *
     * @param exchangeAnswerSessionDTO the exchangeAnswerSessionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangeAnswerSessionDTO, or with status {@code 400 (Bad Request)} if the exchangeAnswerSession has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-answer-sessions")
    public ResponseEntity<ExchangeAnswerSessionDTO> createExchangeAnswerSession(
        @RequestBody ExchangeAnswerSessionDTO exchangeAnswerSessionDTO
    ) throws URISyntaxException {
        log.debug("REST request to save ExchangeAnswerSession : {}", exchangeAnswerSessionDTO);
        if (exchangeAnswerSessionDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangeAnswerSession cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangeAnswerSessionDTO result = exchangeAnswerSessionService.save(exchangeAnswerSessionDTO);
        return ResponseEntity
            .created(new URI("/api/exchange-answer-sessions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-answer-sessions/:id} : Updates an existing exchangeAnswerSession.
     *
     * @param id the id of the exchangeAnswerSessionDTO to save.
     * @param exchangeAnswerSessionDTO the exchangeAnswerSessionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeAnswerSessionDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeAnswerSessionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangeAnswerSessionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-answer-sessions/{id}")
    public ResponseEntity<ExchangeAnswerSessionDTO> updateExchangeAnswerSession(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeAnswerSessionDTO exchangeAnswerSessionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExchangeAnswerSession : {}, {}", id, exchangeAnswerSessionDTO);
        if (exchangeAnswerSessionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeAnswerSessionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeAnswerSessionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExchangeAnswerSessionDTO result = exchangeAnswerSessionService.update(exchangeAnswerSessionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeAnswerSessionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /exchange-answer-sessions/:id} : Partial updates given fields of an existing exchangeAnswerSession, field will ignore if it is null
     *
     * @param id the id of the exchangeAnswerSessionDTO to save.
     * @param exchangeAnswerSessionDTO the exchangeAnswerSessionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeAnswerSessionDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeAnswerSessionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the exchangeAnswerSessionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the exchangeAnswerSessionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/exchange-answer-sessions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExchangeAnswerSessionDTO> partialUpdateExchangeAnswerSession(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeAnswerSessionDTO exchangeAnswerSessionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExchangeAnswerSession partially : {}, {}", id, exchangeAnswerSessionDTO);
        if (exchangeAnswerSessionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeAnswerSessionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeAnswerSessionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExchangeAnswerSessionDTO> result = exchangeAnswerSessionService.partialUpdate(exchangeAnswerSessionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeAnswerSessionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /exchange-answer-sessions} : get all the exchangeAnswerSessions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangeAnswerSessions in body.
     */
    @GetMapping("/exchange-answer-sessions")
    public ResponseEntity<List<ExchangeAnswerSessionDTO>> getAllExchangeAnswerSessions(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of ExchangeAnswerSessions");
        Page<ExchangeAnswerSessionDTO> page = exchangeAnswerSessionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exchange-answer-sessions/:id} : get the "id" exchangeAnswerSession.
     *
     * @param id the id of the exchangeAnswerSessionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangeAnswerSessionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-answer-sessions/{id}")
    public ResponseEntity<ExchangeAnswerSessionDTO> getExchangeAnswerSession(@PathVariable Long id) {
        log.debug("REST request to get ExchangeAnswerSession : {}", id);
        Optional<ExchangeAnswerSessionDTO> exchangeAnswerSessionDTO = exchangeAnswerSessionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangeAnswerSessionDTO);
    }

    /**
     * {@code DELETE  /exchange-answer-sessions/:id} : delete the "id" exchangeAnswerSession.
     *
     * @param id the id of the exchangeAnswerSessionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-answer-sessions/{id}")
    public ResponseEntity<Void> deleteExchangeAnswerSession(@PathVariable Long id) {
        log.debug("REST request to delete ExchangeAnswerSession : {}", id);
        exchangeAnswerSessionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
