package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ProductContext;
import com.reliance.retail.jmd.exchange.repository.ProductContextRepository;
import com.reliance.retail.jmd.exchange.service.ProductContextService;
import com.reliance.retail.jmd.exchange.service.dto.ProductContextDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ProductContextMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ProductContext}.
 */
@Service
@Transactional
public class ProductContextServiceImpl implements ProductContextService {

    private final Logger log = LoggerFactory.getLogger(ProductContextServiceImpl.class);

    private final ProductContextRepository productContextRepository;

    private final ProductContextMapper productContextMapper;

    public ProductContextServiceImpl(ProductContextRepository productContextRepository, ProductContextMapper productContextMapper) {
        this.productContextRepository = productContextRepository;
        this.productContextMapper = productContextMapper;
    }

    @Override
    public ProductContextDTO save(ProductContextDTO productContextDTO) {
        log.debug("Request to save ProductContext : {}", productContextDTO);
        ProductContext productContext = productContextMapper.toEntity(productContextDTO);
        productContext = productContextRepository.save(productContext);
        return productContextMapper.toDto(productContext);
    }

    @Override
    public ProductContextDTO update(ProductContextDTO productContextDTO) {
        log.debug("Request to update ProductContext : {}", productContextDTO);
        ProductContext productContext = productContextMapper.toEntity(productContextDTO);
        productContext = productContextRepository.save(productContext);
        return productContextMapper.toDto(productContext);
    }

    @Override
    public Optional<ProductContextDTO> partialUpdate(ProductContextDTO productContextDTO) {
        log.debug("Request to partially update ProductContext : {}", productContextDTO);

        return productContextRepository
            .findById(productContextDTO.getId())
            .map(existingProductContext -> {
                productContextMapper.partialUpdate(existingProductContext, productContextDTO);

                return existingProductContext;
            })
            .map(productContextRepository::save)
            .map(productContextMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProductContextDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductContexts");
        return productContextRepository.findAll(pageable).map(productContextMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ProductContextDTO> findOne(Long id) {
        log.debug("Request to get ProductContext : {}", id);
        return productContextRepository.findById(id).map(productContextMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductContext : {}", id);
        productContextRepository.deleteById(id);
    }
}
