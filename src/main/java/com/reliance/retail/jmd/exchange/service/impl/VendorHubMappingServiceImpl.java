package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.VendorHubMapping;
import com.reliance.retail.jmd.exchange.repository.VendorHubMappingRepository;
import com.reliance.retail.jmd.exchange.service.VendorHubMappingService;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubMappingDTO;
import com.reliance.retail.jmd.exchange.service.mapper.VendorHubMappingMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link VendorHubMapping}.
 */
@Service
@Transactional
public class VendorHubMappingServiceImpl implements VendorHubMappingService {

    private final Logger log = LoggerFactory.getLogger(VendorHubMappingServiceImpl.class);

    private final VendorHubMappingRepository vendorHubMappingRepository;

    private final VendorHubMappingMapper vendorHubMappingMapper;

    public VendorHubMappingServiceImpl(
        VendorHubMappingRepository vendorHubMappingRepository,
        VendorHubMappingMapper vendorHubMappingMapper
    ) {
        this.vendorHubMappingRepository = vendorHubMappingRepository;
        this.vendorHubMappingMapper = vendorHubMappingMapper;
    }

    @Override
    public VendorHubMappingDTO save(VendorHubMappingDTO vendorHubMappingDTO) {
        log.debug("Request to save VendorHubMapping : {}", vendorHubMappingDTO);
        VendorHubMapping vendorHubMapping = vendorHubMappingMapper.toEntity(vendorHubMappingDTO);
        vendorHubMapping = vendorHubMappingRepository.save(vendorHubMapping);
        return vendorHubMappingMapper.toDto(vendorHubMapping);
    }

    @Override
    public VendorHubMappingDTO update(VendorHubMappingDTO vendorHubMappingDTO) {
        log.debug("Request to update VendorHubMapping : {}", vendorHubMappingDTO);
        VendorHubMapping vendorHubMapping = vendorHubMappingMapper.toEntity(vendorHubMappingDTO);
        vendorHubMapping = vendorHubMappingRepository.save(vendorHubMapping);
        return vendorHubMappingMapper.toDto(vendorHubMapping);
    }

    @Override
    public Optional<VendorHubMappingDTO> partialUpdate(VendorHubMappingDTO vendorHubMappingDTO) {
        log.debug("Request to partially update VendorHubMapping : {}", vendorHubMappingDTO);

        return vendorHubMappingRepository
            .findById(vendorHubMappingDTO.getId())
            .map(existingVendorHubMapping -> {
                vendorHubMappingMapper.partialUpdate(existingVendorHubMapping, vendorHubMappingDTO);

                return existingVendorHubMapping;
            })
            .map(vendorHubMappingRepository::save)
            .map(vendorHubMappingMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VendorHubMappingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VendorHubMappings");
        return vendorHubMappingRepository.findAll(pageable).map(vendorHubMappingMapper::toDto);
    }

    public Page<VendorHubMappingDTO> findAllWithEagerRelationships(Pageable pageable) {
        return vendorHubMappingRepository.findAllWithEagerRelationships(pageable).map(vendorHubMappingMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<VendorHubMappingDTO> findOne(Long id) {
        log.debug("Request to get VendorHubMapping : {}", id);
        return vendorHubMappingRepository.findOneWithEagerRelationships(id).map(vendorHubMappingMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete VendorHubMapping : {}", id);
        vendorHubMappingRepository.deleteById(id);
    }
}
