package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser}.
 */
public interface ExchangeOpsUserService {
    /**
     * Save a exchangeOpsUser.
     *
     * @param exchangeOpsUserDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangeOpsUserDTO save(ExchangeOpsUserDTO exchangeOpsUserDTO);

    /**
     * Updates a exchangeOpsUser.
     *
     * @param exchangeOpsUserDTO the entity to update.
     * @return the persisted entity.
     */
    ExchangeOpsUserDTO update(ExchangeOpsUserDTO exchangeOpsUserDTO);

    /**
     * Partially updates a exchangeOpsUser.
     *
     * @param exchangeOpsUserDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExchangeOpsUserDTO> partialUpdate(ExchangeOpsUserDTO exchangeOpsUserDTO);

    /**
     * Get all the exchangeOpsUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangeOpsUserDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exchangeOpsUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangeOpsUserDTO> findOne(Long id);

    /**
     * Delete the "id" exchangeOpsUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
