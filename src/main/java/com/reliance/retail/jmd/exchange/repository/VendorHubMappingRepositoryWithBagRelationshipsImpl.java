package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.VendorHubMapping;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class VendorHubMappingRepositoryWithBagRelationshipsImpl implements VendorHubMappingRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<VendorHubMapping> fetchBagRelationships(Optional<VendorHubMapping> vendorHubMapping) {
        return vendorHubMapping.map(this::fetchServingPincodes);
    }

    @Override
    public Page<VendorHubMapping> fetchBagRelationships(Page<VendorHubMapping> vendorHubMappings) {
        return new PageImpl<>(
            fetchBagRelationships(vendorHubMappings.getContent()),
            vendorHubMappings.getPageable(),
            vendorHubMappings.getTotalElements()
        );
    }

    @Override
    public List<VendorHubMapping> fetchBagRelationships(List<VendorHubMapping> vendorHubMappings) {
        return Optional.of(vendorHubMappings).map(this::fetchServingPincodes).orElse(Collections.emptyList());
    }

    VendorHubMapping fetchServingPincodes(VendorHubMapping result) {
        return entityManager
            .createQuery(
                "select vendorHubMapping from VendorHubMapping vendorHubMapping left join fetch vendorHubMapping.servingPincodes where vendorHubMapping is :vendorHubMapping",
                VendorHubMapping.class
            )
            .setParameter("vendorHubMapping", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<VendorHubMapping> fetchServingPincodes(List<VendorHubMapping> vendorHubMappings) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, vendorHubMappings.size()).forEach(index -> order.put(vendorHubMappings.get(index).getId(), index));
        List<VendorHubMapping> result = entityManager
            .createQuery(
                "select distinct vendorHubMapping from VendorHubMapping vendorHubMapping left join fetch vendorHubMapping.servingPincodes where vendorHubMapping in :vendorHubMappings",
                VendorHubMapping.class
            )
            .setParameter("vendorHubMappings", vendorHubMappings)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
