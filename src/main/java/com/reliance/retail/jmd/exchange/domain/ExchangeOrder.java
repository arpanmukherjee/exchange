package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.CancellationReason;
import com.reliance.retail.jmd.exchange.domain.enumeration.ExchangeOrderStatus;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExchangeOrder.
 */
@Entity
@Table(name = "exchange_order")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "client_order_ref_id")
    private String clientOrderRefId;

    @Column(name = "customer_phone_no")
    private String customerPhoneNo;

    @Column(name = "client_order_delivery_ref_id")
    private String clientOrderDeliveryRefId;

    @Column(name = "delivery_partner_phone_no")
    private String deliveryPartnerPhoneNo;

    @Column(name = "total_order_amount")
    private Double totalOrderAmount;

    @Column(name = "paid_amount")
    private Double paidAmount;

    @Column(name = "cod_initial_pending_amount")
    private Double codInitialPendingAmount;

    @Column(name = "exchange_estimate_amount")
    private Double exchangeEstimateAmount;

    @Column(name = "exchange_final_amount_to_user")
    private Double exchangeFinalAmountToUser;

    @Column(name = "cod_final_amount")
    private Double codFinalAmount;

    @Column(name = "cod_amount_paid")
    private Double codAmountPaid;

    @Column(name = "cod_payment_txn_ref_id")
    private String codPaymentTxnRefId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ExchangeOrderStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "cancellation_reason")
    private CancellationReason cancellationReason;

    @Column(name = "expected_pickup_date")
    private LocalDate expectedPickupDate;

    @Column(name = "customer_pickup_date")
    private LocalDate customerPickupDate;

    @Column(name = "dc_delivered_date")
    private LocalDate dcDeliveredDate;

    @Column(name = "dc_pickup_date")
    private LocalDate dcPickupDate;

    @Column(name = "exchange_final_amount_to_dc")
    private Double exchangeFinalAmountToDc;

    @Column(name = "vendor_delivered_date")
    private LocalDate vendorDeliveredDate;

    @Column(name = "exchange_final_amount_to_vendor")
    private Double exchangeFinalAmountToVendor;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @JsonIgnoreProperties(value = { "vendorHub", "pincode" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Address customerAddress;

    @JsonIgnoreProperties(value = { "vendorHub", "pincode" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Address vendorAddress;

    @JsonIgnoreProperties(value = { "vendorHub", "pincode" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Address dcAddress;

    @OneToMany(mappedBy = "order")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "vendor", "product", "order" }, allowSetters = true)
    private Set<Document> images = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "price", "exchangeAnswers", "client", "order" }, allowSetters = true)
    private Set<ExchangeAnswerSession> exchangeAnswerSessions = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "order", "approvedBy" }, allowSetters = true)
    private Set<DifferentialAmount> differentialAmounts = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "productContexts", "exchangePrices", "exchangeOrders", "exchangeAnswerSessions", "questions", "options", "approvedBy" },
        allowSetters = true
    )
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = { "documents", "exchangeOrders", "client" }, allowSetters = true)
    private ProductContext product;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExchangeOrder id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientOrderRefId() {
        return this.clientOrderRefId;
    }

    public ExchangeOrder clientOrderRefId(String clientOrderRefId) {
        this.setClientOrderRefId(clientOrderRefId);
        return this;
    }

    public void setClientOrderRefId(String clientOrderRefId) {
        this.clientOrderRefId = clientOrderRefId;
    }

    public String getCustomerPhoneNo() {
        return this.customerPhoneNo;
    }

    public ExchangeOrder customerPhoneNo(String customerPhoneNo) {
        this.setCustomerPhoneNo(customerPhoneNo);
        return this;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getClientOrderDeliveryRefId() {
        return this.clientOrderDeliveryRefId;
    }

    public ExchangeOrder clientOrderDeliveryRefId(String clientOrderDeliveryRefId) {
        this.setClientOrderDeliveryRefId(clientOrderDeliveryRefId);
        return this;
    }

    public void setClientOrderDeliveryRefId(String clientOrderDeliveryRefId) {
        this.clientOrderDeliveryRefId = clientOrderDeliveryRefId;
    }

    public String getDeliveryPartnerPhoneNo() {
        return this.deliveryPartnerPhoneNo;
    }

    public ExchangeOrder deliveryPartnerPhoneNo(String deliveryPartnerPhoneNo) {
        this.setDeliveryPartnerPhoneNo(deliveryPartnerPhoneNo);
        return this;
    }

    public void setDeliveryPartnerPhoneNo(String deliveryPartnerPhoneNo) {
        this.deliveryPartnerPhoneNo = deliveryPartnerPhoneNo;
    }

    public Double getTotalOrderAmount() {
        return this.totalOrderAmount;
    }

    public ExchangeOrder totalOrderAmount(Double totalOrderAmount) {
        this.setTotalOrderAmount(totalOrderAmount);
        return this;
    }

    public void setTotalOrderAmount(Double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public Double getPaidAmount() {
        return this.paidAmount;
    }

    public ExchangeOrder paidAmount(Double paidAmount) {
        this.setPaidAmount(paidAmount);
        return this;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Double getCodInitialPendingAmount() {
        return this.codInitialPendingAmount;
    }

    public ExchangeOrder codInitialPendingAmount(Double codInitialPendingAmount) {
        this.setCodInitialPendingAmount(codInitialPendingAmount);
        return this;
    }

    public void setCodInitialPendingAmount(Double codInitialPendingAmount) {
        this.codInitialPendingAmount = codInitialPendingAmount;
    }

    public Double getExchangeEstimateAmount() {
        return this.exchangeEstimateAmount;
    }

    public ExchangeOrder exchangeEstimateAmount(Double exchangeEstimateAmount) {
        this.setExchangeEstimateAmount(exchangeEstimateAmount);
        return this;
    }

    public void setExchangeEstimateAmount(Double exchangeEstimateAmount) {
        this.exchangeEstimateAmount = exchangeEstimateAmount;
    }

    public Double getExchangeFinalAmountToUser() {
        return this.exchangeFinalAmountToUser;
    }

    public ExchangeOrder exchangeFinalAmountToUser(Double exchangeFinalAmountToUser) {
        this.setExchangeFinalAmountToUser(exchangeFinalAmountToUser);
        return this;
    }

    public void setExchangeFinalAmountToUser(Double exchangeFinalAmountToUser) {
        this.exchangeFinalAmountToUser = exchangeFinalAmountToUser;
    }

    public Double getCodFinalAmount() {
        return this.codFinalAmount;
    }

    public ExchangeOrder codFinalAmount(Double codFinalAmount) {
        this.setCodFinalAmount(codFinalAmount);
        return this;
    }

    public void setCodFinalAmount(Double codFinalAmount) {
        this.codFinalAmount = codFinalAmount;
    }

    public Double getCodAmountPaid() {
        return this.codAmountPaid;
    }

    public ExchangeOrder codAmountPaid(Double codAmountPaid) {
        this.setCodAmountPaid(codAmountPaid);
        return this;
    }

    public void setCodAmountPaid(Double codAmountPaid) {
        this.codAmountPaid = codAmountPaid;
    }

    public String getCodPaymentTxnRefId() {
        return this.codPaymentTxnRefId;
    }

    public ExchangeOrder codPaymentTxnRefId(String codPaymentTxnRefId) {
        this.setCodPaymentTxnRefId(codPaymentTxnRefId);
        return this;
    }

    public void setCodPaymentTxnRefId(String codPaymentTxnRefId) {
        this.codPaymentTxnRefId = codPaymentTxnRefId;
    }

    public ExchangeOrderStatus getStatus() {
        return this.status;
    }

    public ExchangeOrder status(ExchangeOrderStatus status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(ExchangeOrderStatus status) {
        this.status = status;
    }

    public CancellationReason getCancellationReason() {
        return this.cancellationReason;
    }

    public ExchangeOrder cancellationReason(CancellationReason cancellationReason) {
        this.setCancellationReason(cancellationReason);
        return this;
    }

    public void setCancellationReason(CancellationReason cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public LocalDate getExpectedPickupDate() {
        return this.expectedPickupDate;
    }

    public ExchangeOrder expectedPickupDate(LocalDate expectedPickupDate) {
        this.setExpectedPickupDate(expectedPickupDate);
        return this;
    }

    public void setExpectedPickupDate(LocalDate expectedPickupDate) {
        this.expectedPickupDate = expectedPickupDate;
    }

    public LocalDate getCustomerPickupDate() {
        return this.customerPickupDate;
    }

    public ExchangeOrder customerPickupDate(LocalDate customerPickupDate) {
        this.setCustomerPickupDate(customerPickupDate);
        return this;
    }

    public void setCustomerPickupDate(LocalDate customerPickupDate) {
        this.customerPickupDate = customerPickupDate;
    }

    public LocalDate getDcDeliveredDate() {
        return this.dcDeliveredDate;
    }

    public ExchangeOrder dcDeliveredDate(LocalDate dcDeliveredDate) {
        this.setDcDeliveredDate(dcDeliveredDate);
        return this;
    }

    public void setDcDeliveredDate(LocalDate dcDeliveredDate) {
        this.dcDeliveredDate = dcDeliveredDate;
    }

    public LocalDate getDcPickupDate() {
        return this.dcPickupDate;
    }

    public ExchangeOrder dcPickupDate(LocalDate dcPickupDate) {
        this.setDcPickupDate(dcPickupDate);
        return this;
    }

    public void setDcPickupDate(LocalDate dcPickupDate) {
        this.dcPickupDate = dcPickupDate;
    }

    public Double getExchangeFinalAmountToDc() {
        return this.exchangeFinalAmountToDc;
    }

    public ExchangeOrder exchangeFinalAmountToDc(Double exchangeFinalAmountToDc) {
        this.setExchangeFinalAmountToDc(exchangeFinalAmountToDc);
        return this;
    }

    public void setExchangeFinalAmountToDc(Double exchangeFinalAmountToDc) {
        this.exchangeFinalAmountToDc = exchangeFinalAmountToDc;
    }

    public LocalDate getVendorDeliveredDate() {
        return this.vendorDeliveredDate;
    }

    public ExchangeOrder vendorDeliveredDate(LocalDate vendorDeliveredDate) {
        this.setVendorDeliveredDate(vendorDeliveredDate);
        return this;
    }

    public void setVendorDeliveredDate(LocalDate vendorDeliveredDate) {
        this.vendorDeliveredDate = vendorDeliveredDate;
    }

    public Double getExchangeFinalAmountToVendor() {
        return this.exchangeFinalAmountToVendor;
    }

    public ExchangeOrder exchangeFinalAmountToVendor(Double exchangeFinalAmountToVendor) {
        this.setExchangeFinalAmountToVendor(exchangeFinalAmountToVendor);
        return this;
    }

    public void setExchangeFinalAmountToVendor(Double exchangeFinalAmountToVendor) {
        this.exchangeFinalAmountToVendor = exchangeFinalAmountToVendor;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public ExchangeOrder createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public ExchangeOrder updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Address getCustomerAddress() {
        return this.customerAddress;
    }

    public void setCustomerAddress(Address address) {
        this.customerAddress = address;
    }

    public ExchangeOrder customerAddress(Address address) {
        this.setCustomerAddress(address);
        return this;
    }

    public Address getVendorAddress() {
        return this.vendorAddress;
    }

    public void setVendorAddress(Address address) {
        this.vendorAddress = address;
    }

    public ExchangeOrder vendorAddress(Address address) {
        this.setVendorAddress(address);
        return this;
    }

    public Address getDcAddress() {
        return this.dcAddress;
    }

    public void setDcAddress(Address address) {
        this.dcAddress = address;
    }

    public ExchangeOrder dcAddress(Address address) {
        this.setDcAddress(address);
        return this;
    }

    public Set<Document> getImages() {
        return this.images;
    }

    public void setImages(Set<Document> documents) {
        if (this.images != null) {
            this.images.forEach(i -> i.setOrder(null));
        }
        if (documents != null) {
            documents.forEach(i -> i.setOrder(this));
        }
        this.images = documents;
    }

    public ExchangeOrder images(Set<Document> documents) {
        this.setImages(documents);
        return this;
    }

    public ExchangeOrder addImages(Document document) {
        this.images.add(document);
        document.setOrder(this);
        return this;
    }

    public ExchangeOrder removeImages(Document document) {
        this.images.remove(document);
        document.setOrder(null);
        return this;
    }

    public Set<ExchangeAnswerSession> getExchangeAnswerSessions() {
        return this.exchangeAnswerSessions;
    }

    public void setExchangeAnswerSessions(Set<ExchangeAnswerSession> exchangeAnswerSessions) {
        if (this.exchangeAnswerSessions != null) {
            this.exchangeAnswerSessions.forEach(i -> i.setOrder(null));
        }
        if (exchangeAnswerSessions != null) {
            exchangeAnswerSessions.forEach(i -> i.setOrder(this));
        }
        this.exchangeAnswerSessions = exchangeAnswerSessions;
    }

    public ExchangeOrder exchangeAnswerSessions(Set<ExchangeAnswerSession> exchangeAnswerSessions) {
        this.setExchangeAnswerSessions(exchangeAnswerSessions);
        return this;
    }

    public ExchangeOrder addExchangeAnswerSession(ExchangeAnswerSession exchangeAnswerSession) {
        this.exchangeAnswerSessions.add(exchangeAnswerSession);
        exchangeAnswerSession.setOrder(this);
        return this;
    }

    public ExchangeOrder removeExchangeAnswerSession(ExchangeAnswerSession exchangeAnswerSession) {
        this.exchangeAnswerSessions.remove(exchangeAnswerSession);
        exchangeAnswerSession.setOrder(null);
        return this;
    }

    public Set<DifferentialAmount> getDifferentialAmounts() {
        return this.differentialAmounts;
    }

    public void setDifferentialAmounts(Set<DifferentialAmount> differentialAmounts) {
        if (this.differentialAmounts != null) {
            this.differentialAmounts.forEach(i -> i.setOrder(null));
        }
        if (differentialAmounts != null) {
            differentialAmounts.forEach(i -> i.setOrder(this));
        }
        this.differentialAmounts = differentialAmounts;
    }

    public ExchangeOrder differentialAmounts(Set<DifferentialAmount> differentialAmounts) {
        this.setDifferentialAmounts(differentialAmounts);
        return this;
    }

    public ExchangeOrder addDifferentialAmount(DifferentialAmount differentialAmount) {
        this.differentialAmounts.add(differentialAmount);
        differentialAmount.setOrder(this);
        return this;
    }

    public ExchangeOrder removeDifferentialAmount(DifferentialAmount differentialAmount) {
        this.differentialAmounts.remove(differentialAmount);
        differentialAmount.setOrder(null);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ExchangeOrder client(Client client) {
        this.setClient(client);
        return this;
    }

    public ProductContext getProduct() {
        return this.product;
    }

    public void setProduct(ProductContext productContext) {
        this.product = productContext;
    }

    public ExchangeOrder product(ProductContext productContext) {
        this.setProduct(productContext);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeOrder)) {
            return false;
        }
        return id != null && id.equals(((ExchangeOrder) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeOrder{" +
            "id=" + getId() +
            ", clientOrderRefId='" + getClientOrderRefId() + "'" +
            ", customerPhoneNo='" + getCustomerPhoneNo() + "'" +
            ", clientOrderDeliveryRefId='" + getClientOrderDeliveryRefId() + "'" +
            ", deliveryPartnerPhoneNo='" + getDeliveryPartnerPhoneNo() + "'" +
            ", totalOrderAmount=" + getTotalOrderAmount() +
            ", paidAmount=" + getPaidAmount() +
            ", codInitialPendingAmount=" + getCodInitialPendingAmount() +
            ", exchangeEstimateAmount=" + getExchangeEstimateAmount() +
            ", exchangeFinalAmountToUser=" + getExchangeFinalAmountToUser() +
            ", codFinalAmount=" + getCodFinalAmount() +
            ", codAmountPaid=" + getCodAmountPaid() +
            ", codPaymentTxnRefId='" + getCodPaymentTxnRefId() + "'" +
            ", status='" + getStatus() + "'" +
            ", cancellationReason='" + getCancellationReason() + "'" +
            ", expectedPickupDate='" + getExpectedPickupDate() + "'" +
            ", customerPickupDate='" + getCustomerPickupDate() + "'" +
            ", dcDeliveredDate='" + getDcDeliveredDate() + "'" +
            ", dcPickupDate='" + getDcPickupDate() + "'" +
            ", exchangeFinalAmountToDc=" + getExchangeFinalAmountToDc() +
            ", vendorDeliveredDate='" + getVendorDeliveredDate() + "'" +
            ", exchangeFinalAmountToVendor=" + getExchangeFinalAmountToVendor() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
