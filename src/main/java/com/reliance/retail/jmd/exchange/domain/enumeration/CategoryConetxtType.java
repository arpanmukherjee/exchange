package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The CategoryConetxtType enumeration.
 */
public enum CategoryConetxtType {
    REFRIGERATOR,
    WASHING_MACHINE,
    TV,
    MOBILE_PHONE,
}
