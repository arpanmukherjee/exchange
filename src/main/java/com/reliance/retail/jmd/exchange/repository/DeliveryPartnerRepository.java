package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.DeliveryPartner;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DeliveryPartner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeliveryPartnerRepository extends JpaRepository<DeliveryPartner, Long> {}
