package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ProductContextDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ProductContext}.
 */
public interface ProductContextService {
    /**
     * Save a productContext.
     *
     * @param productContextDTO the entity to save.
     * @return the persisted entity.
     */
    ProductContextDTO save(ProductContextDTO productContextDTO);

    /**
     * Updates a productContext.
     *
     * @param productContextDTO the entity to update.
     * @return the persisted entity.
     */
    ProductContextDTO update(ProductContextDTO productContextDTO);

    /**
     * Partially updates a productContext.
     *
     * @param productContextDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ProductContextDTO> partialUpdate(ProductContextDTO productContextDTO);

    /**
     * Get all the productContexts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProductContextDTO> findAll(Pageable pageable);

    /**
     * Get the "id" productContext.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductContextDTO> findOne(Long id);

    /**
     * Delete the "id" productContext.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
