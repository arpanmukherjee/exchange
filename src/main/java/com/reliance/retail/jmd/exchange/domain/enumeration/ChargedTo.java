package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The ChargedTo enumeration.
 */
public enum ChargedTo {
    Customer,
    DeliveryPartner,
    DC,
    Vendor,
    Client,
}
