package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.ChargedTo;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.DifferentialAmount} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DifferentialAmountDTO implements Serializable {

    private Long id;

    private Double differentialAmount;

    private ChargedTo chargedTo;

    private LocalDate approvedAt;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private String createdBy;

    private String updatedBy;

    private ExchangeOrderDTO order;

    private ExchangeOpsUserDTO approvedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDifferentialAmount() {
        return differentialAmount;
    }

    public void setDifferentialAmount(Double differentialAmount) {
        this.differentialAmount = differentialAmount;
    }

    public ChargedTo getChargedTo() {
        return chargedTo;
    }

    public void setChargedTo(ChargedTo chargedTo) {
        this.chargedTo = chargedTo;
    }

    public LocalDate getApprovedAt() {
        return approvedAt;
    }

    public void setApprovedAt(LocalDate approvedAt) {
        this.approvedAt = approvedAt;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ExchangeOrderDTO getOrder() {
        return order;
    }

    public void setOrder(ExchangeOrderDTO order) {
        this.order = order;
    }

    public ExchangeOpsUserDTO getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUserDTO approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DifferentialAmountDTO)) {
            return false;
        }

        DifferentialAmountDTO differentialAmountDTO = (DifferentialAmountDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, differentialAmountDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DifferentialAmountDTO{" +
            "id=" + getId() +
            ", differentialAmount=" + getDifferentialAmount() +
            ", chargedTo='" + getChargedTo() + "'" +
            ", approvedAt='" + getApprovedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", order=" + getOrder() +
            ", approvedBy=" + getApprovedBy() +
            "}";
    }
}
