package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeOrder}.
 */
public interface ExchangeOrderService {
    /**
     * Save a exchangeOrder.
     *
     * @param exchangeOrderDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangeOrderDTO save(ExchangeOrderDTO exchangeOrderDTO);

    /**
     * Updates a exchangeOrder.
     *
     * @param exchangeOrderDTO the entity to update.
     * @return the persisted entity.
     */
    ExchangeOrderDTO update(ExchangeOrderDTO exchangeOrderDTO);

    /**
     * Partially updates a exchangeOrder.
     *
     * @param exchangeOrderDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExchangeOrderDTO> partialUpdate(ExchangeOrderDTO exchangeOrderDTO);

    /**
     * Get all the exchangeOrders.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangeOrderDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exchangeOrder.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangeOrderDTO> findOne(Long id);

    /**
     * Delete the "id" exchangeOrder.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
