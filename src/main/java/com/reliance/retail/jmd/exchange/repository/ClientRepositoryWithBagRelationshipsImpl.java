package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.Client;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class ClientRepositoryWithBagRelationshipsImpl implements ClientRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Client> fetchBagRelationships(Optional<Client> client) {
        return client.map(this::fetchQuestions).map(this::fetchOptions);
    }

    @Override
    public Page<Client> fetchBagRelationships(Page<Client> clients) {
        return new PageImpl<>(fetchBagRelationships(clients.getContent()), clients.getPageable(), clients.getTotalElements());
    }

    @Override
    public List<Client> fetchBagRelationships(List<Client> clients) {
        return Optional.of(clients).map(this::fetchQuestions).map(this::fetchOptions).orElse(Collections.emptyList());
    }

    Client fetchQuestions(Client result) {
        return entityManager
            .createQuery("select client from Client client left join fetch client.questions where client is :client", Client.class)
            .setParameter("client", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Client> fetchQuestions(List<Client> clients) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, clients.size()).forEach(index -> order.put(clients.get(index).getId(), index));
        List<Client> result = entityManager
            .createQuery(
                "select distinct client from Client client left join fetch client.questions where client in :clients",
                Client.class
            )
            .setParameter("clients", clients)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }

    Client fetchOptions(Client result) {
        return entityManager
            .createQuery("select client from Client client left join fetch client.options where client is :client", Client.class)
            .setParameter("client", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Client> fetchOptions(List<Client> clients) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, clients.size()).forEach(index -> order.put(clients.get(index).getId(), index));
        List<Client> result = entityManager
            .createQuery("select distinct client from Client client left join fetch client.options where client in :clients", Client.class)
            .setParameter("clients", clients)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
