package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.DifferentialAmount;
import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import com.reliance.retail.jmd.exchange.service.dto.DifferentialAmountDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DifferentialAmount} and its DTO {@link DifferentialAmountDTO}.
 */
@Mapper(componentModel = "spring")
public interface DifferentialAmountMapper extends EntityMapper<DifferentialAmountDTO, DifferentialAmount> {
    @Mapping(target = "order", source = "order", qualifiedByName = "exchangeOrderId")
    @Mapping(target = "approvedBy", source = "approvedBy", qualifiedByName = "exchangeOpsUserId")
    DifferentialAmountDTO toDto(DifferentialAmount s);

    @Named("exchangeOrderId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOrderDTO toDtoExchangeOrderId(ExchangeOrder exchangeOrder);

    @Named("exchangeOpsUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOpsUserDTO toDtoExchangeOpsUserId(ExchangeOpsUser exchangeOpsUser);
}
