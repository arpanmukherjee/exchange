package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.VendorHub}.
 */
public interface VendorHubService {
    /**
     * Save a vendorHub.
     *
     * @param vendorHubDTO the entity to save.
     * @return the persisted entity.
     */
    VendorHubDTO save(VendorHubDTO vendorHubDTO);

    /**
     * Updates a vendorHub.
     *
     * @param vendorHubDTO the entity to update.
     * @return the persisted entity.
     */
    VendorHubDTO update(VendorHubDTO vendorHubDTO);

    /**
     * Partially updates a vendorHub.
     *
     * @param vendorHubDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<VendorHubDTO> partialUpdate(VendorHubDTO vendorHubDTO);

    /**
     * Get all the vendorHubs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VendorHubDTO> findAll(Pageable pageable);

    /**
     * Get the "id" vendorHub.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VendorHubDTO> findOne(Long id);

    /**
     * Delete the "id" vendorHub.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
