package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The DocumentType enumeration.
 */
public enum DocumentType {
    PAN,
    AADHAAR,
    GST,
    ProductImage,
    PickupQcImage,
    DcQcImage,
    VendorQcImage,
}
