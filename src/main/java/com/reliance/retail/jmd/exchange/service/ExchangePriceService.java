package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangePrice}.
 */
public interface ExchangePriceService {
    /**
     * Save a exchangePrice.
     *
     * @param exchangePriceDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangePriceDTO save(ExchangePriceDTO exchangePriceDTO);

    /**
     * Updates a exchangePrice.
     *
     * @param exchangePriceDTO the entity to update.
     * @return the persisted entity.
     */
    ExchangePriceDTO update(ExchangePriceDTO exchangePriceDTO);

    /**
     * Partially updates a exchangePrice.
     *
     * @param exchangePriceDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExchangePriceDTO> partialUpdate(ExchangePriceDTO exchangePriceDTO);

    /**
     * Get all the exchangePrices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangePriceDTO> findAll(Pageable pageable);
    /**
     * Get all the ExchangePriceDTO where Set is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<ExchangePriceDTO> findAllWhereSetIsNull();
    /**
     * Get all the ExchangePriceDTO where Session is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<ExchangePriceDTO> findAllWhereSessionIsNull();

    /**
     * Get the "id" exchangePrice.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangePriceDTO> findOne(Long id);

    /**
     * Delete the "id" exchangePrice.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
