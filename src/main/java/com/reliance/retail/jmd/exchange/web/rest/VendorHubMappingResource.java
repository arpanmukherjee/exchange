package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.VendorHubMappingRepository;
import com.reliance.retail.jmd.exchange.service.VendorHubMappingService;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubMappingDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.VendorHubMapping}.
 */
@RestController
@RequestMapping("/api")
public class VendorHubMappingResource {

    private final Logger log = LoggerFactory.getLogger(VendorHubMappingResource.class);

    private static final String ENTITY_NAME = "vendorHubMapping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendorHubMappingService vendorHubMappingService;

    private final VendorHubMappingRepository vendorHubMappingRepository;

    public VendorHubMappingResource(
        VendorHubMappingService vendorHubMappingService,
        VendorHubMappingRepository vendorHubMappingRepository
    ) {
        this.vendorHubMappingService = vendorHubMappingService;
        this.vendorHubMappingRepository = vendorHubMappingRepository;
    }

    /**
     * {@code POST  /vendor-hub-mappings} : Create a new vendorHubMapping.
     *
     * @param vendorHubMappingDTO the vendorHubMappingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vendorHubMappingDTO, or with status {@code 400 (Bad Request)} if the vendorHubMapping has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vendor-hub-mappings")
    public ResponseEntity<VendorHubMappingDTO> createVendorHubMapping(@RequestBody VendorHubMappingDTO vendorHubMappingDTO)
        throws URISyntaxException {
        log.debug("REST request to save VendorHubMapping : {}", vendorHubMappingDTO);
        if (vendorHubMappingDTO.getId() != null) {
            throw new BadRequestAlertException("A new vendorHubMapping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorHubMappingDTO result = vendorHubMappingService.save(vendorHubMappingDTO);
        return ResponseEntity
            .created(new URI("/api/vendor-hub-mappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vendor-hub-mappings/:id} : Updates an existing vendorHubMapping.
     *
     * @param id the id of the vendorHubMappingDTO to save.
     * @param vendorHubMappingDTO the vendorHubMappingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vendorHubMappingDTO,
     * or with status {@code 400 (Bad Request)} if the vendorHubMappingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vendorHubMappingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vendor-hub-mappings/{id}")
    public ResponseEntity<VendorHubMappingDTO> updateVendorHubMapping(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody VendorHubMappingDTO vendorHubMappingDTO
    ) throws URISyntaxException {
        log.debug("REST request to update VendorHubMapping : {}, {}", id, vendorHubMappingDTO);
        if (vendorHubMappingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vendorHubMappingDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vendorHubMappingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        VendorHubMappingDTO result = vendorHubMappingService.update(vendorHubMappingDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vendorHubMappingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /vendor-hub-mappings/:id} : Partial updates given fields of an existing vendorHubMapping, field will ignore if it is null
     *
     * @param id the id of the vendorHubMappingDTO to save.
     * @param vendorHubMappingDTO the vendorHubMappingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vendorHubMappingDTO,
     * or with status {@code 400 (Bad Request)} if the vendorHubMappingDTO is not valid,
     * or with status {@code 404 (Not Found)} if the vendorHubMappingDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the vendorHubMappingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/vendor-hub-mappings/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<VendorHubMappingDTO> partialUpdateVendorHubMapping(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody VendorHubMappingDTO vendorHubMappingDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update VendorHubMapping partially : {}, {}", id, vendorHubMappingDTO);
        if (vendorHubMappingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vendorHubMappingDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vendorHubMappingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<VendorHubMappingDTO> result = vendorHubMappingService.partialUpdate(vendorHubMappingDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vendorHubMappingDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /vendor-hub-mappings} : get all the vendorHubMappings.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vendorHubMappings in body.
     */
    @GetMapping("/vendor-hub-mappings")
    public ResponseEntity<List<VendorHubMappingDTO>> getAllVendorHubMappings(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of VendorHubMappings");
        Page<VendorHubMappingDTO> page;
        if (eagerload) {
            page = vendorHubMappingService.findAllWithEagerRelationships(pageable);
        } else {
            page = vendorHubMappingService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vendor-hub-mappings/:id} : get the "id" vendorHubMapping.
     *
     * @param id the id of the vendorHubMappingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vendorHubMappingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vendor-hub-mappings/{id}")
    public ResponseEntity<VendorHubMappingDTO> getVendorHubMapping(@PathVariable Long id) {
        log.debug("REST request to get VendorHubMapping : {}", id);
        Optional<VendorHubMappingDTO> vendorHubMappingDTO = vendorHubMappingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vendorHubMappingDTO);
    }

    /**
     * {@code DELETE  /vendor-hub-mappings/:id} : delete the "id" vendorHubMapping.
     *
     * @param id the id of the vendorHubMappingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vendor-hub-mappings/{id}")
    public ResponseEntity<Void> deleteVendorHubMapping(@PathVariable Long id) {
        log.debug("REST request to delete VendorHubMapping : {}", id);
        vendorHubMappingService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
