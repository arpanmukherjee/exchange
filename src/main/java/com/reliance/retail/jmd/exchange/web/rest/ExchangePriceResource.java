package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.ExchangePriceRepository;
import com.reliance.retail.jmd.exchange.service.ExchangePriceService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangePrice}.
 */
@RestController
@RequestMapping("/api")
public class ExchangePriceResource {

    private final Logger log = LoggerFactory.getLogger(ExchangePriceResource.class);

    private static final String ENTITY_NAME = "exchangePrice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangePriceService exchangePriceService;

    private final ExchangePriceRepository exchangePriceRepository;

    public ExchangePriceResource(ExchangePriceService exchangePriceService, ExchangePriceRepository exchangePriceRepository) {
        this.exchangePriceService = exchangePriceService;
        this.exchangePriceRepository = exchangePriceRepository;
    }

    /**
     * {@code POST  /exchange-prices} : Create a new exchangePrice.
     *
     * @param exchangePriceDTO the exchangePriceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangePriceDTO, or with status {@code 400 (Bad Request)} if the exchangePrice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-prices")
    public ResponseEntity<ExchangePriceDTO> createExchangePrice(@RequestBody ExchangePriceDTO exchangePriceDTO) throws URISyntaxException {
        log.debug("REST request to save ExchangePrice : {}", exchangePriceDTO);
        if (exchangePriceDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangePrice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangePriceDTO result = exchangePriceService.save(exchangePriceDTO);
        return ResponseEntity
            .created(new URI("/api/exchange-prices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-prices/:id} : Updates an existing exchangePrice.
     *
     * @param id the id of the exchangePriceDTO to save.
     * @param exchangePriceDTO the exchangePriceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangePriceDTO,
     * or with status {@code 400 (Bad Request)} if the exchangePriceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangePriceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-prices/{id}")
    public ResponseEntity<ExchangePriceDTO> updateExchangePrice(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangePriceDTO exchangePriceDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExchangePrice : {}, {}", id, exchangePriceDTO);
        if (exchangePriceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangePriceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangePriceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExchangePriceDTO result = exchangePriceService.update(exchangePriceDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangePriceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /exchange-prices/:id} : Partial updates given fields of an existing exchangePrice, field will ignore if it is null
     *
     * @param id the id of the exchangePriceDTO to save.
     * @param exchangePriceDTO the exchangePriceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangePriceDTO,
     * or with status {@code 400 (Bad Request)} if the exchangePriceDTO is not valid,
     * or with status {@code 404 (Not Found)} if the exchangePriceDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the exchangePriceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/exchange-prices/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExchangePriceDTO> partialUpdateExchangePrice(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangePriceDTO exchangePriceDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExchangePrice partially : {}, {}", id, exchangePriceDTO);
        if (exchangePriceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangePriceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangePriceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExchangePriceDTO> result = exchangePriceService.partialUpdate(exchangePriceDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangePriceDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /exchange-prices} : get all the exchangePrices.
     *
     * @param pageable the pagination information.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangePrices in body.
     */
    @GetMapping("/exchange-prices")
    public ResponseEntity<List<ExchangePriceDTO>> getAllExchangePrices(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false) String filter
    ) {
        if ("set-is-null".equals(filter)) {
            log.debug("REST request to get all ExchangePrices where set is null");
            return new ResponseEntity<>(exchangePriceService.findAllWhereSetIsNull(), HttpStatus.OK);
        }

        if ("session-is-null".equals(filter)) {
            log.debug("REST request to get all ExchangePrices where session is null");
            return new ResponseEntity<>(exchangePriceService.findAllWhereSessionIsNull(), HttpStatus.OK);
        }
        log.debug("REST request to get a page of ExchangePrices");
        Page<ExchangePriceDTO> page = exchangePriceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exchange-prices/:id} : get the "id" exchangePrice.
     *
     * @param id the id of the exchangePriceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangePriceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-prices/{id}")
    public ResponseEntity<ExchangePriceDTO> getExchangePrice(@PathVariable Long id) {
        log.debug("REST request to get ExchangePrice : {}", id);
        Optional<ExchangePriceDTO> exchangePriceDTO = exchangePriceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangePriceDTO);
    }

    /**
     * {@code DELETE  /exchange-prices/:id} : delete the "id" exchangePrice.
     *
     * @param id the id of the exchangePriceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-prices/{id}")
    public ResponseEntity<Void> deleteExchangePrice(@PathVariable Long id) {
        log.debug("REST request to delete ExchangePrice : {}", id);
        exchangePriceService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
