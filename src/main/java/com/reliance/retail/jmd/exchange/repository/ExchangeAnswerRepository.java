package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswer;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ExchangeAnswer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExchangeAnswerRepository extends JpaRepository<ExchangeAnswer, Long> {}
