package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Address;
import com.reliance.retail.jmd.exchange.domain.Client;
import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import com.reliance.retail.jmd.exchange.domain.ProductContext;
import com.reliance.retail.jmd.exchange.service.dto.AddressDTO;
import com.reliance.retail.jmd.exchange.service.dto.ClientDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import com.reliance.retail.jmd.exchange.service.dto.ProductContextDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangeOrder} and its DTO {@link ExchangeOrderDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangeOrderMapper extends EntityMapper<ExchangeOrderDTO, ExchangeOrder> {
    @Mapping(target = "customerAddress", source = "customerAddress", qualifiedByName = "addressId")
    @Mapping(target = "vendorAddress", source = "vendorAddress", qualifiedByName = "addressId")
    @Mapping(target = "dcAddress", source = "dcAddress", qualifiedByName = "addressId")
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    @Mapping(target = "product", source = "product", qualifiedByName = "productContextId")
    ExchangeOrderDTO toDto(ExchangeOrder s);

    @Named("addressId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AddressDTO toDtoAddressId(Address address);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("productContextId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductContextDTO toDtoProductContextId(ProductContext productContext);
}
