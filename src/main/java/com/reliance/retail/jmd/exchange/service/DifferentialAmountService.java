package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.DifferentialAmountDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.DifferentialAmount}.
 */
public interface DifferentialAmountService {
    /**
     * Save a differentialAmount.
     *
     * @param differentialAmountDTO the entity to save.
     * @return the persisted entity.
     */
    DifferentialAmountDTO save(DifferentialAmountDTO differentialAmountDTO);

    /**
     * Updates a differentialAmount.
     *
     * @param differentialAmountDTO the entity to update.
     * @return the persisted entity.
     */
    DifferentialAmountDTO update(DifferentialAmountDTO differentialAmountDTO);

    /**
     * Partially updates a differentialAmount.
     *
     * @param differentialAmountDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DifferentialAmountDTO> partialUpdate(DifferentialAmountDTO differentialAmountDTO);

    /**
     * Get all the differentialAmounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DifferentialAmountDTO> findAll(Pageable pageable);

    /**
     * Get the "id" differentialAmount.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DifferentialAmountDTO> findOne(Long id);

    /**
     * Delete the "id" differentialAmount.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
