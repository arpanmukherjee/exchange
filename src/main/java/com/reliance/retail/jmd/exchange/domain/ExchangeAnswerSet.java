package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExchangeAnswerSet.
 */
@Entity
@Table(name = "exchange_answer_set")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeAnswerSet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "tag")
    private String tag;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnoreProperties(value = { "set", "session", "approvedBy", "client", "vendorHub" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private ExchangePrice price;

    @OneToMany(mappedBy = "set")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "question", "option", "set", "session" }, allowSetters = true)
    private Set<ExchangeAnswer> exchangeAnswers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExchangeAnswerSet id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return this.tag;
    }

    public ExchangeAnswerSet tag(String tag) {
        this.setTag(tag);
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public ExchangeAnswerSet createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public ExchangeAnswerSet updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public ExchangeAnswerSet createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public ExchangeAnswerSet updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ExchangePrice getPrice() {
        return this.price;
    }

    public void setPrice(ExchangePrice exchangePrice) {
        this.price = exchangePrice;
    }

    public ExchangeAnswerSet price(ExchangePrice exchangePrice) {
        this.setPrice(exchangePrice);
        return this;
    }

    public Set<ExchangeAnswer> getExchangeAnswers() {
        return this.exchangeAnswers;
    }

    public void setExchangeAnswers(Set<ExchangeAnswer> exchangeAnswers) {
        if (this.exchangeAnswers != null) {
            this.exchangeAnswers.forEach(i -> i.setSet(null));
        }
        if (exchangeAnswers != null) {
            exchangeAnswers.forEach(i -> i.setSet(this));
        }
        this.exchangeAnswers = exchangeAnswers;
    }

    public ExchangeAnswerSet exchangeAnswers(Set<ExchangeAnswer> exchangeAnswers) {
        this.setExchangeAnswers(exchangeAnswers);
        return this;
    }

    public ExchangeAnswerSet addExchangeAnswer(ExchangeAnswer exchangeAnswer) {
        this.exchangeAnswers.add(exchangeAnswer);
        exchangeAnswer.setSet(this);
        return this;
    }

    public ExchangeAnswerSet removeExchangeAnswer(ExchangeAnswer exchangeAnswer) {
        this.exchangeAnswers.remove(exchangeAnswer);
        exchangeAnswer.setSet(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeAnswerSet)) {
            return false;
        }
        return id != null && id.equals(((ExchangeAnswerSet) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeAnswerSet{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
