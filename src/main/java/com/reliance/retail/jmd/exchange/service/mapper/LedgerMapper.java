package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Ledger;
import com.reliance.retail.jmd.exchange.domain.Vendor;
import com.reliance.retail.jmd.exchange.service.dto.LedgerDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Ledger} and its DTO {@link LedgerDTO}.
 */
@Mapper(componentModel = "spring")
public interface LedgerMapper extends EntityMapper<LedgerDTO, Ledger> {
    @Mapping(target = "vendor", source = "vendor", qualifiedByName = "vendorId")
    LedgerDTO toDto(Ledger s);

    @Named("vendorId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    VendorDTO toDtoVendorId(Vendor vendor);
}
