package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.DifferentialAmountRepository;
import com.reliance.retail.jmd.exchange.service.DifferentialAmountService;
import com.reliance.retail.jmd.exchange.service.dto.DifferentialAmountDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.DifferentialAmount}.
 */
@RestController
@RequestMapping("/api")
public class DifferentialAmountResource {

    private final Logger log = LoggerFactory.getLogger(DifferentialAmountResource.class);

    private static final String ENTITY_NAME = "differentialAmount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DifferentialAmountService differentialAmountService;

    private final DifferentialAmountRepository differentialAmountRepository;

    public DifferentialAmountResource(
        DifferentialAmountService differentialAmountService,
        DifferentialAmountRepository differentialAmountRepository
    ) {
        this.differentialAmountService = differentialAmountService;
        this.differentialAmountRepository = differentialAmountRepository;
    }

    /**
     * {@code POST  /differential-amounts} : Create a new differentialAmount.
     *
     * @param differentialAmountDTO the differentialAmountDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new differentialAmountDTO, or with status {@code 400 (Bad Request)} if the differentialAmount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/differential-amounts")
    public ResponseEntity<DifferentialAmountDTO> createDifferentialAmount(@RequestBody DifferentialAmountDTO differentialAmountDTO)
        throws URISyntaxException {
        log.debug("REST request to save DifferentialAmount : {}", differentialAmountDTO);
        if (differentialAmountDTO.getId() != null) {
            throw new BadRequestAlertException("A new differentialAmount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DifferentialAmountDTO result = differentialAmountService.save(differentialAmountDTO);
        return ResponseEntity
            .created(new URI("/api/differential-amounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /differential-amounts/:id} : Updates an existing differentialAmount.
     *
     * @param id the id of the differentialAmountDTO to save.
     * @param differentialAmountDTO the differentialAmountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated differentialAmountDTO,
     * or with status {@code 400 (Bad Request)} if the differentialAmountDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the differentialAmountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/differential-amounts/{id}")
    public ResponseEntity<DifferentialAmountDTO> updateDifferentialAmount(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DifferentialAmountDTO differentialAmountDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DifferentialAmount : {}, {}", id, differentialAmountDTO);
        if (differentialAmountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, differentialAmountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!differentialAmountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DifferentialAmountDTO result = differentialAmountService.update(differentialAmountDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, differentialAmountDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /differential-amounts/:id} : Partial updates given fields of an existing differentialAmount, field will ignore if it is null
     *
     * @param id the id of the differentialAmountDTO to save.
     * @param differentialAmountDTO the differentialAmountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated differentialAmountDTO,
     * or with status {@code 400 (Bad Request)} if the differentialAmountDTO is not valid,
     * or with status {@code 404 (Not Found)} if the differentialAmountDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the differentialAmountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/differential-amounts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DifferentialAmountDTO> partialUpdateDifferentialAmount(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DifferentialAmountDTO differentialAmountDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DifferentialAmount partially : {}, {}", id, differentialAmountDTO);
        if (differentialAmountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, differentialAmountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!differentialAmountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DifferentialAmountDTO> result = differentialAmountService.partialUpdate(differentialAmountDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, differentialAmountDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /differential-amounts} : get all the differentialAmounts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of differentialAmounts in body.
     */
    @GetMapping("/differential-amounts")
    public ResponseEntity<List<DifferentialAmountDTO>> getAllDifferentialAmounts(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of DifferentialAmounts");
        Page<DifferentialAmountDTO> page = differentialAmountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /differential-amounts/:id} : get the "id" differentialAmount.
     *
     * @param id the id of the differentialAmountDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the differentialAmountDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/differential-amounts/{id}")
    public ResponseEntity<DifferentialAmountDTO> getDifferentialAmount(@PathVariable Long id) {
        log.debug("REST request to get DifferentialAmount : {}", id);
        Optional<DifferentialAmountDTO> differentialAmountDTO = differentialAmountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(differentialAmountDTO);
    }

    /**
     * {@code DELETE  /differential-amounts/:id} : delete the "id" differentialAmount.
     *
     * @param id the id of the differentialAmountDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/differential-amounts/{id}")
    public ResponseEntity<Void> deleteDifferentialAmount(@PathVariable Long id) {
        log.debug("REST request to delete DifferentialAmount : {}", id);
        differentialAmountService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
