package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The CancellationReason enumeration.
 */
public enum CancellationReason {
    CancelledByCustomer,
    CancelledByDeliveryPartner,
    CancelledByVendor,
    CancelledByClient,
}
