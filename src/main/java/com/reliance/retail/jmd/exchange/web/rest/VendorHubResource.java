package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.VendorHubRepository;
import com.reliance.retail.jmd.exchange.service.VendorHubService;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.VendorHub}.
 */
@RestController
@RequestMapping("/api")
public class VendorHubResource {

    private final Logger log = LoggerFactory.getLogger(VendorHubResource.class);

    private static final String ENTITY_NAME = "vendorHub";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendorHubService vendorHubService;

    private final VendorHubRepository vendorHubRepository;

    public VendorHubResource(VendorHubService vendorHubService, VendorHubRepository vendorHubRepository) {
        this.vendorHubService = vendorHubService;
        this.vendorHubRepository = vendorHubRepository;
    }

    /**
     * {@code POST  /vendor-hubs} : Create a new vendorHub.
     *
     * @param vendorHubDTO the vendorHubDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vendorHubDTO, or with status {@code 400 (Bad Request)} if the vendorHub has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vendor-hubs")
    public ResponseEntity<VendorHubDTO> createVendorHub(@RequestBody VendorHubDTO vendorHubDTO) throws URISyntaxException {
        log.debug("REST request to save VendorHub : {}", vendorHubDTO);
        if (vendorHubDTO.getId() != null) {
            throw new BadRequestAlertException("A new vendorHub cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorHubDTO result = vendorHubService.save(vendorHubDTO);
        return ResponseEntity
            .created(new URI("/api/vendor-hubs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vendor-hubs/:id} : Updates an existing vendorHub.
     *
     * @param id the id of the vendorHubDTO to save.
     * @param vendorHubDTO the vendorHubDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vendorHubDTO,
     * or with status {@code 400 (Bad Request)} if the vendorHubDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vendorHubDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vendor-hubs/{id}")
    public ResponseEntity<VendorHubDTO> updateVendorHub(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody VendorHubDTO vendorHubDTO
    ) throws URISyntaxException {
        log.debug("REST request to update VendorHub : {}, {}", id, vendorHubDTO);
        if (vendorHubDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vendorHubDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vendorHubRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        VendorHubDTO result = vendorHubService.update(vendorHubDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vendorHubDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /vendor-hubs/:id} : Partial updates given fields of an existing vendorHub, field will ignore if it is null
     *
     * @param id the id of the vendorHubDTO to save.
     * @param vendorHubDTO the vendorHubDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vendorHubDTO,
     * or with status {@code 400 (Bad Request)} if the vendorHubDTO is not valid,
     * or with status {@code 404 (Not Found)} if the vendorHubDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the vendorHubDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/vendor-hubs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<VendorHubDTO> partialUpdateVendorHub(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody VendorHubDTO vendorHubDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update VendorHub partially : {}, {}", id, vendorHubDTO);
        if (vendorHubDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, vendorHubDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!vendorHubRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<VendorHubDTO> result = vendorHubService.partialUpdate(vendorHubDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vendorHubDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /vendor-hubs} : get all the vendorHubs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vendorHubs in body.
     */
    @GetMapping("/vendor-hubs")
    public ResponseEntity<List<VendorHubDTO>> getAllVendorHubs(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of VendorHubs");
        Page<VendorHubDTO> page = vendorHubService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vendor-hubs/:id} : get the "id" vendorHub.
     *
     * @param id the id of the vendorHubDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vendorHubDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vendor-hubs/{id}")
    public ResponseEntity<VendorHubDTO> getVendorHub(@PathVariable Long id) {
        log.debug("REST request to get VendorHub : {}", id);
        Optional<VendorHubDTO> vendorHubDTO = vendorHubService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vendorHubDTO);
    }

    /**
     * {@code DELETE  /vendor-hubs/:id} : delete the "id" vendorHub.
     *
     * @param id the id of the vendorHubDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vendor-hubs/{id}")
    public ResponseEntity<Void> deleteVendorHub(@PathVariable Long id) {
        log.debug("REST request to delete VendorHub : {}", id);
        vendorHubService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
