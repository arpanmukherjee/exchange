package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.LedgerTransaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the LedgerTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LedgerTransactionRepository extends JpaRepository<LedgerTransaction, Long> {}
