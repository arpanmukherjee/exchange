package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.CategoryConetxtType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A VendorHubMapping.
 */
@Entity
@Table(name = "vendor_hub_mapping")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class VendorHubMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "category_context_type")
    private CategoryConetxtType categoryContextType;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @ManyToMany
    @JoinTable(
        name = "rel_vendor_hub_mapping__serving_pincodes",
        joinColumns = @JoinColumn(name = "vendor_hub_mapping_id"),
        inverseJoinColumns = @JoinColumn(name = "serving_pincodes_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "addresses", "vendorHubMappings" }, allowSetters = true)
    private Set<Pincode> servingPincodes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "clients", "deliveryPartners", "vendors", "vendorHubs", "vendorHubMappings", "exchangePrices", "differentialAmounts" },
        allowSetters = true
    )
    private ExchangeOpsUser approvedBy;

    @ManyToOne
    @JsonIgnoreProperties(value = { "address", "vendorHubMappings", "exchangePrices", "approvedBy", "vendor" }, allowSetters = true)
    private VendorHub vendorHub;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public VendorHubMapping id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CategoryConetxtType getCategoryContextType() {
        return this.categoryContextType;
    }

    public VendorHubMapping categoryContextType(CategoryConetxtType categoryContextType) {
        this.setCategoryContextType(categoryContextType);
        return this;
    }

    public void setCategoryContextType(CategoryConetxtType categoryContextType) {
        this.categoryContextType = categoryContextType;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public VendorHubMapping isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public VendorHubMapping createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public VendorHubMapping updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public VendorHubMapping createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public VendorHubMapping updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<Pincode> getServingPincodes() {
        return this.servingPincodes;
    }

    public void setServingPincodes(Set<Pincode> pincodes) {
        this.servingPincodes = pincodes;
    }

    public VendorHubMapping servingPincodes(Set<Pincode> pincodes) {
        this.setServingPincodes(pincodes);
        return this;
    }

    public VendorHubMapping addServingPincodes(Pincode pincode) {
        this.servingPincodes.add(pincode);
        pincode.getVendorHubMappings().add(this);
        return this;
    }

    public VendorHubMapping removeServingPincodes(Pincode pincode) {
        this.servingPincodes.remove(pincode);
        pincode.getVendorHubMappings().remove(this);
        return this;
    }

    public ExchangeOpsUser getApprovedBy() {
        return this.approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUser exchangeOpsUser) {
        this.approvedBy = exchangeOpsUser;
    }

    public VendorHubMapping approvedBy(ExchangeOpsUser exchangeOpsUser) {
        this.setApprovedBy(exchangeOpsUser);
        return this;
    }

    public VendorHub getVendorHub() {
        return this.vendorHub;
    }

    public void setVendorHub(VendorHub vendorHub) {
        this.vendorHub = vendorHub;
    }

    public VendorHubMapping vendorHub(VendorHub vendorHub) {
        this.setVendorHub(vendorHub);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VendorHubMapping)) {
            return false;
        }
        return id != null && id.equals(((VendorHubMapping) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VendorHubMapping{" +
            "id=" + getId() +
            ", categoryContextType='" + getCategoryContextType() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
