package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    SuperAdmin,
    ClientAdmin,
    ClientDcUser,
    ClientFinanceApprover,
    ClientVendorOnboardingApprover,
    VendorAdmin,
    VendorPickupUser,
    VendorQCUser,
    DeliveryPartnerAdmin,
    DeliveryPartnerPickupUser,
}
