package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.LedgerTransactionDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.LedgerTransaction}.
 */
public interface LedgerTransactionService {
    /**
     * Save a ledgerTransaction.
     *
     * @param ledgerTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    LedgerTransactionDTO save(LedgerTransactionDTO ledgerTransactionDTO);

    /**
     * Updates a ledgerTransaction.
     *
     * @param ledgerTransactionDTO the entity to update.
     * @return the persisted entity.
     */
    LedgerTransactionDTO update(LedgerTransactionDTO ledgerTransactionDTO);

    /**
     * Partially updates a ledgerTransaction.
     *
     * @param ledgerTransactionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<LedgerTransactionDTO> partialUpdate(LedgerTransactionDTO ledgerTransactionDTO);

    /**
     * Get all the ledgerTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LedgerTransactionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" ledgerTransaction.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LedgerTransactionDTO> findOne(Long id);

    /**
     * Delete the "id" ledgerTransaction.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
