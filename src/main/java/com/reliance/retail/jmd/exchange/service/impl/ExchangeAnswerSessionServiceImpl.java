package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession;
import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerSessionRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeAnswerSessionService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSessionDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeAnswerSessionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExchangeAnswerSession}.
 */
@Service
@Transactional
public class ExchangeAnswerSessionServiceImpl implements ExchangeAnswerSessionService {

    private final Logger log = LoggerFactory.getLogger(ExchangeAnswerSessionServiceImpl.class);

    private final ExchangeAnswerSessionRepository exchangeAnswerSessionRepository;

    private final ExchangeAnswerSessionMapper exchangeAnswerSessionMapper;

    public ExchangeAnswerSessionServiceImpl(
        ExchangeAnswerSessionRepository exchangeAnswerSessionRepository,
        ExchangeAnswerSessionMapper exchangeAnswerSessionMapper
    ) {
        this.exchangeAnswerSessionRepository = exchangeAnswerSessionRepository;
        this.exchangeAnswerSessionMapper = exchangeAnswerSessionMapper;
    }

    @Override
    public ExchangeAnswerSessionDTO save(ExchangeAnswerSessionDTO exchangeAnswerSessionDTO) {
        log.debug("Request to save ExchangeAnswerSession : {}", exchangeAnswerSessionDTO);
        ExchangeAnswerSession exchangeAnswerSession = exchangeAnswerSessionMapper.toEntity(exchangeAnswerSessionDTO);
        exchangeAnswerSession = exchangeAnswerSessionRepository.save(exchangeAnswerSession);
        return exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);
    }

    @Override
    public ExchangeAnswerSessionDTO update(ExchangeAnswerSessionDTO exchangeAnswerSessionDTO) {
        log.debug("Request to update ExchangeAnswerSession : {}", exchangeAnswerSessionDTO);
        ExchangeAnswerSession exchangeAnswerSession = exchangeAnswerSessionMapper.toEntity(exchangeAnswerSessionDTO);
        exchangeAnswerSession = exchangeAnswerSessionRepository.save(exchangeAnswerSession);
        return exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);
    }

    @Override
    public Optional<ExchangeAnswerSessionDTO> partialUpdate(ExchangeAnswerSessionDTO exchangeAnswerSessionDTO) {
        log.debug("Request to partially update ExchangeAnswerSession : {}", exchangeAnswerSessionDTO);

        return exchangeAnswerSessionRepository
            .findById(exchangeAnswerSessionDTO.getId())
            .map(existingExchangeAnswerSession -> {
                exchangeAnswerSessionMapper.partialUpdate(existingExchangeAnswerSession, exchangeAnswerSessionDTO);

                return existingExchangeAnswerSession;
            })
            .map(exchangeAnswerSessionRepository::save)
            .map(exchangeAnswerSessionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExchangeAnswerSessionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangeAnswerSessions");
        return exchangeAnswerSessionRepository.findAll(pageable).map(exchangeAnswerSessionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangeAnswerSessionDTO> findOne(Long id) {
        log.debug("Request to get ExchangeAnswerSession : {}", id);
        return exchangeAnswerSessionRepository.findById(id).map(exchangeAnswerSessionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangeAnswerSession : {}", id);
        exchangeAnswerSessionRepository.deleteById(id);
    }
}
