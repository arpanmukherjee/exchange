package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.LedgerTransaction;
import com.reliance.retail.jmd.exchange.repository.LedgerTransactionRepository;
import com.reliance.retail.jmd.exchange.service.LedgerTransactionService;
import com.reliance.retail.jmd.exchange.service.dto.LedgerTransactionDTO;
import com.reliance.retail.jmd.exchange.service.mapper.LedgerTransactionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link LedgerTransaction}.
 */
@Service
@Transactional
public class LedgerTransactionServiceImpl implements LedgerTransactionService {

    private final Logger log = LoggerFactory.getLogger(LedgerTransactionServiceImpl.class);

    private final LedgerTransactionRepository ledgerTransactionRepository;

    private final LedgerTransactionMapper ledgerTransactionMapper;

    public LedgerTransactionServiceImpl(
        LedgerTransactionRepository ledgerTransactionRepository,
        LedgerTransactionMapper ledgerTransactionMapper
    ) {
        this.ledgerTransactionRepository = ledgerTransactionRepository;
        this.ledgerTransactionMapper = ledgerTransactionMapper;
    }

    @Override
    public LedgerTransactionDTO save(LedgerTransactionDTO ledgerTransactionDTO) {
        log.debug("Request to save LedgerTransaction : {}", ledgerTransactionDTO);
        LedgerTransaction ledgerTransaction = ledgerTransactionMapper.toEntity(ledgerTransactionDTO);
        ledgerTransaction = ledgerTransactionRepository.save(ledgerTransaction);
        return ledgerTransactionMapper.toDto(ledgerTransaction);
    }

    @Override
    public LedgerTransactionDTO update(LedgerTransactionDTO ledgerTransactionDTO) {
        log.debug("Request to update LedgerTransaction : {}", ledgerTransactionDTO);
        LedgerTransaction ledgerTransaction = ledgerTransactionMapper.toEntity(ledgerTransactionDTO);
        ledgerTransaction = ledgerTransactionRepository.save(ledgerTransaction);
        return ledgerTransactionMapper.toDto(ledgerTransaction);
    }

    @Override
    public Optional<LedgerTransactionDTO> partialUpdate(LedgerTransactionDTO ledgerTransactionDTO) {
        log.debug("Request to partially update LedgerTransaction : {}", ledgerTransactionDTO);

        return ledgerTransactionRepository
            .findById(ledgerTransactionDTO.getId())
            .map(existingLedgerTransaction -> {
                ledgerTransactionMapper.partialUpdate(existingLedgerTransaction, ledgerTransactionDTO);

                return existingLedgerTransaction;
            })
            .map(ledgerTransactionRepository::save)
            .map(ledgerTransactionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LedgerTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LedgerTransactions");
        return ledgerTransactionRepository.findAll(pageable).map(ledgerTransactionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<LedgerTransactionDTO> findOne(Long id) {
        log.debug("Request to get LedgerTransaction : {}", id);
        return ledgerTransactionRepository.findById(id).map(ledgerTransactionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete LedgerTransaction : {}", id);
        ledgerTransactionRepository.deleteById(id);
    }
}
