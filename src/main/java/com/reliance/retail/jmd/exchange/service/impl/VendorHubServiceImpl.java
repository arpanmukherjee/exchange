package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.VendorHub;
import com.reliance.retail.jmd.exchange.repository.VendorHubRepository;
import com.reliance.retail.jmd.exchange.service.VendorHubService;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import com.reliance.retail.jmd.exchange.service.mapper.VendorHubMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link VendorHub}.
 */
@Service
@Transactional
public class VendorHubServiceImpl implements VendorHubService {

    private final Logger log = LoggerFactory.getLogger(VendorHubServiceImpl.class);

    private final VendorHubRepository vendorHubRepository;

    private final VendorHubMapper vendorHubMapper;

    public VendorHubServiceImpl(VendorHubRepository vendorHubRepository, VendorHubMapper vendorHubMapper) {
        this.vendorHubRepository = vendorHubRepository;
        this.vendorHubMapper = vendorHubMapper;
    }

    @Override
    public VendorHubDTO save(VendorHubDTO vendorHubDTO) {
        log.debug("Request to save VendorHub : {}", vendorHubDTO);
        VendorHub vendorHub = vendorHubMapper.toEntity(vendorHubDTO);
        vendorHub = vendorHubRepository.save(vendorHub);
        return vendorHubMapper.toDto(vendorHub);
    }

    @Override
    public VendorHubDTO update(VendorHubDTO vendorHubDTO) {
        log.debug("Request to update VendorHub : {}", vendorHubDTO);
        VendorHub vendorHub = vendorHubMapper.toEntity(vendorHubDTO);
        vendorHub = vendorHubRepository.save(vendorHub);
        return vendorHubMapper.toDto(vendorHub);
    }

    @Override
    public Optional<VendorHubDTO> partialUpdate(VendorHubDTO vendorHubDTO) {
        log.debug("Request to partially update VendorHub : {}", vendorHubDTO);

        return vendorHubRepository
            .findById(vendorHubDTO.getId())
            .map(existingVendorHub -> {
                vendorHubMapper.partialUpdate(existingVendorHub, vendorHubDTO);

                return existingVendorHub;
            })
            .map(vendorHubRepository::save)
            .map(vendorHubMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VendorHubDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VendorHubs");
        return vendorHubRepository.findAll(pageable).map(vendorHubMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<VendorHubDTO> findOne(Long id) {
        log.debug("Request to get VendorHub : {}", id);
        return vendorHubRepository.findById(id).map(vendorHubMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete VendorHub : {}", id);
        vendorHubRepository.deleteById(id);
    }
}
