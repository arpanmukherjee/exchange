package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Ledger;
import com.reliance.retail.jmd.exchange.domain.LedgerTransaction;
import com.reliance.retail.jmd.exchange.service.dto.LedgerDTO;
import com.reliance.retail.jmd.exchange.service.dto.LedgerTransactionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link LedgerTransaction} and its DTO {@link LedgerTransactionDTO}.
 */
@Mapper(componentModel = "spring")
public interface LedgerTransactionMapper extends EntityMapper<LedgerTransactionDTO, LedgerTransaction> {
    @Mapping(target = "ledger", source = "ledger", qualifiedByName = "ledgerId")
    LedgerTransactionDTO toDto(LedgerTransaction s);

    @Named("ledgerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    LedgerDTO toDtoLedgerId(Ledger ledger);
}
