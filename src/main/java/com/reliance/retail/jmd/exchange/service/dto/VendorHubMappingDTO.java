package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.CategoryConetxtType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.VendorHubMapping} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class VendorHubMappingDTO implements Serializable {

    private Long id;

    private CategoryConetxtType categoryContextType;

    private Boolean isActive;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private String createdBy;

    private String updatedBy;

    private Set<PincodeDTO> servingPincodes = new HashSet<>();

    private ExchangeOpsUserDTO approvedBy;

    private VendorHubDTO vendorHub;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CategoryConetxtType getCategoryContextType() {
        return categoryContextType;
    }

    public void setCategoryContextType(CategoryConetxtType categoryContextType) {
        this.categoryContextType = categoryContextType;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<PincodeDTO> getServingPincodes() {
        return servingPincodes;
    }

    public void setServingPincodes(Set<PincodeDTO> servingPincodes) {
        this.servingPincodes = servingPincodes;
    }

    public ExchangeOpsUserDTO getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUserDTO approvedBy) {
        this.approvedBy = approvedBy;
    }

    public VendorHubDTO getVendorHub() {
        return vendorHub;
    }

    public void setVendorHub(VendorHubDTO vendorHub) {
        this.vendorHub = vendorHub;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VendorHubMappingDTO)) {
            return false;
        }

        VendorHubMappingDTO vendorHubMappingDTO = (VendorHubMappingDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, vendorHubMappingDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VendorHubMappingDTO{" +
            "id=" + getId() +
            ", categoryContextType='" + getCategoryContextType() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", servingPincodes=" + getServingPincodes() +
            ", approvedBy=" + getApprovedBy() +
            ", vendorHub=" + getVendorHub() +
            "}";
    }
}
