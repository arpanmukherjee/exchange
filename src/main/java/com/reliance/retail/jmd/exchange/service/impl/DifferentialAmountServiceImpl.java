package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.DifferentialAmount;
import com.reliance.retail.jmd.exchange.repository.DifferentialAmountRepository;
import com.reliance.retail.jmd.exchange.service.DifferentialAmountService;
import com.reliance.retail.jmd.exchange.service.dto.DifferentialAmountDTO;
import com.reliance.retail.jmd.exchange.service.mapper.DifferentialAmountMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DifferentialAmount}.
 */
@Service
@Transactional
public class DifferentialAmountServiceImpl implements DifferentialAmountService {

    private final Logger log = LoggerFactory.getLogger(DifferentialAmountServiceImpl.class);

    private final DifferentialAmountRepository differentialAmountRepository;

    private final DifferentialAmountMapper differentialAmountMapper;

    public DifferentialAmountServiceImpl(
        DifferentialAmountRepository differentialAmountRepository,
        DifferentialAmountMapper differentialAmountMapper
    ) {
        this.differentialAmountRepository = differentialAmountRepository;
        this.differentialAmountMapper = differentialAmountMapper;
    }

    @Override
    public DifferentialAmountDTO save(DifferentialAmountDTO differentialAmountDTO) {
        log.debug("Request to save DifferentialAmount : {}", differentialAmountDTO);
        DifferentialAmount differentialAmount = differentialAmountMapper.toEntity(differentialAmountDTO);
        differentialAmount = differentialAmountRepository.save(differentialAmount);
        return differentialAmountMapper.toDto(differentialAmount);
    }

    @Override
    public DifferentialAmountDTO update(DifferentialAmountDTO differentialAmountDTO) {
        log.debug("Request to update DifferentialAmount : {}", differentialAmountDTO);
        DifferentialAmount differentialAmount = differentialAmountMapper.toEntity(differentialAmountDTO);
        differentialAmount = differentialAmountRepository.save(differentialAmount);
        return differentialAmountMapper.toDto(differentialAmount);
    }

    @Override
    public Optional<DifferentialAmountDTO> partialUpdate(DifferentialAmountDTO differentialAmountDTO) {
        log.debug("Request to partially update DifferentialAmount : {}", differentialAmountDTO);

        return differentialAmountRepository
            .findById(differentialAmountDTO.getId())
            .map(existingDifferentialAmount -> {
                differentialAmountMapper.partialUpdate(existingDifferentialAmount, differentialAmountDTO);

                return existingDifferentialAmount;
            })
            .map(differentialAmountRepository::save)
            .map(differentialAmountMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DifferentialAmountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DifferentialAmounts");
        return differentialAmountRepository.findAll(pageable).map(differentialAmountMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DifferentialAmountDTO> findOne(Long id) {
        log.debug("Request to get DifferentialAmount : {}", id);
        return differentialAmountRepository.findById(id).map(differentialAmountMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DifferentialAmount : {}", id);
        differentialAmountRepository.deleteById(id);
    }
}
