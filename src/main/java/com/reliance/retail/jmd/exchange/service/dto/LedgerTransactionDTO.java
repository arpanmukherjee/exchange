package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.TransactionStatus;
import com.reliance.retail.jmd.exchange.domain.enumeration.TransactionType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.LedgerTransaction} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class LedgerTransactionDTO implements Serializable {

    private Long id;

    private Double amount;

    private TransactionType type;

    private String reference;

    private TransactionStatus status;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private LedgerDTO ledger;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LedgerDTO getLedger() {
        return ledger;
    }

    public void setLedger(LedgerDTO ledger) {
        this.ledger = ledger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LedgerTransactionDTO)) {
            return false;
        }

        LedgerTransactionDTO ledgerTransactionDTO = (LedgerTransactionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, ledgerTransactionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LedgerTransactionDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", type='" + getType() + "'" +
            ", reference='" + getReference() + "'" +
            ", status='" + getStatus() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", ledger=" + getLedger() +
            "}";
    }
}
