package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.PriceMetricStatus;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.ExchangePrice} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangePriceDTO implements Serializable {

    private Long id;

    private Double exchangeAmount;

    private PriceMetricStatus status;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private String createdBy;

    private String updatedBy;

    private ExchangeOpsUserDTO approvedBy;

    private ClientDTO client;

    private VendorHubDTO vendorHub;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getExchangeAmount() {
        return exchangeAmount;
    }

    public void setExchangeAmount(Double exchangeAmount) {
        this.exchangeAmount = exchangeAmount;
    }

    public PriceMetricStatus getStatus() {
        return status;
    }

    public void setStatus(PriceMetricStatus status) {
        this.status = status;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ExchangeOpsUserDTO getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUserDTO approvedBy) {
        this.approvedBy = approvedBy;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public VendorHubDTO getVendorHub() {
        return vendorHub;
    }

    public void setVendorHub(VendorHubDTO vendorHub) {
        this.vendorHub = vendorHub;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangePriceDTO)) {
            return false;
        }

        ExchangePriceDTO exchangePriceDTO = (ExchangePriceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exchangePriceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangePriceDTO{" +
            "id=" + getId() +
            ", exchangeAmount=" + getExchangeAmount() +
            ", status='" + getStatus() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", approvedBy=" + getApprovedBy() +
            ", client=" + getClient() +
            ", vendorHub=" + getVendorHub() +
            "}";
    }
}
