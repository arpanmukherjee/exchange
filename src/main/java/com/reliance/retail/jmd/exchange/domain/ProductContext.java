package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.CategoryConetxtType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ProductContext.
 */
@Entity
@Table(name = "product_context")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProductContext implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "category_context_type")
    private CategoryConetxtType categoryContextType;

    @Column(name = "client_product_id")
    private String clientProductId;

    @Column(name = "client_product_name")
    private String clientProductName;

    @Column(name = "client_product_url")
    private String clientProductUrl;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "vendor", "product", "order" }, allowSetters = true)
    private Set<Document> documents = new HashSet<>();

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = {
            "customerAddress", "vendorAddress", "dcAddress", "images", "exchangeAnswerSessions", "differentialAmounts", "client", "product",
        },
        allowSetters = true
    )
    private Set<ExchangeOrder> exchangeOrders = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "productContexts", "exchangePrices", "exchangeOrders", "exchangeAnswerSessions", "questions", "options", "approvedBy" },
        allowSetters = true
    )
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ProductContext id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CategoryConetxtType getCategoryContextType() {
        return this.categoryContextType;
    }

    public ProductContext categoryContextType(CategoryConetxtType categoryContextType) {
        this.setCategoryContextType(categoryContextType);
        return this;
    }

    public void setCategoryContextType(CategoryConetxtType categoryContextType) {
        this.categoryContextType = categoryContextType;
    }

    public String getClientProductId() {
        return this.clientProductId;
    }

    public ProductContext clientProductId(String clientProductId) {
        this.setClientProductId(clientProductId);
        return this;
    }

    public void setClientProductId(String clientProductId) {
        this.clientProductId = clientProductId;
    }

    public String getClientProductName() {
        return this.clientProductName;
    }

    public ProductContext clientProductName(String clientProductName) {
        this.setClientProductName(clientProductName);
        return this;
    }

    public void setClientProductName(String clientProductName) {
        this.clientProductName = clientProductName;
    }

    public String getClientProductUrl() {
        return this.clientProductUrl;
    }

    public ProductContext clientProductUrl(String clientProductUrl) {
        this.setClientProductUrl(clientProductUrl);
        return this;
    }

    public void setClientProductUrl(String clientProductUrl) {
        this.clientProductUrl = clientProductUrl;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public ProductContext isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public ProductContext createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public ProductContext updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public ProductContext createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public ProductContext updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<Document> getDocuments() {
        return this.documents;
    }

    public void setDocuments(Set<Document> documents) {
        if (this.documents != null) {
            this.documents.forEach(i -> i.setProduct(null));
        }
        if (documents != null) {
            documents.forEach(i -> i.setProduct(this));
        }
        this.documents = documents;
    }

    public ProductContext documents(Set<Document> documents) {
        this.setDocuments(documents);
        return this;
    }

    public ProductContext addDocument(Document document) {
        this.documents.add(document);
        document.setProduct(this);
        return this;
    }

    public ProductContext removeDocument(Document document) {
        this.documents.remove(document);
        document.setProduct(null);
        return this;
    }

    public Set<ExchangeOrder> getExchangeOrders() {
        return this.exchangeOrders;
    }

    public void setExchangeOrders(Set<ExchangeOrder> exchangeOrders) {
        if (this.exchangeOrders != null) {
            this.exchangeOrders.forEach(i -> i.setProduct(null));
        }
        if (exchangeOrders != null) {
            exchangeOrders.forEach(i -> i.setProduct(this));
        }
        this.exchangeOrders = exchangeOrders;
    }

    public ProductContext exchangeOrders(Set<ExchangeOrder> exchangeOrders) {
        this.setExchangeOrders(exchangeOrders);
        return this;
    }

    public ProductContext addExchangeOrder(ExchangeOrder exchangeOrder) {
        this.exchangeOrders.add(exchangeOrder);
        exchangeOrder.setProduct(this);
        return this;
    }

    public ProductContext removeExchangeOrder(ExchangeOrder exchangeOrder) {
        this.exchangeOrders.remove(exchangeOrder);
        exchangeOrder.setProduct(null);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ProductContext client(Client client) {
        this.setClient(client);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductContext)) {
            return false;
        }
        return id != null && id.equals(((ProductContext) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductContext{" +
            "id=" + getId() +
            ", categoryContextType='" + getCategoryContextType() + "'" +
            ", clientProductId='" + getClientProductId() + "'" +
            ", clientProductName='" + getClientProductName() + "'" +
            ", clientProductUrl='" + getClientProductUrl() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
