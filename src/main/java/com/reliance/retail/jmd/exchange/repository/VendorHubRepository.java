package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.VendorHub;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the VendorHub entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendorHubRepository extends JpaRepository<VendorHub, Long> {}
