package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerSetRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeAnswerSetService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSetDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet}.
 */
@RestController
@RequestMapping("/api")
public class ExchangeAnswerSetResource {

    private final Logger log = LoggerFactory.getLogger(ExchangeAnswerSetResource.class);

    private static final String ENTITY_NAME = "exchangeAnswerSet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangeAnswerSetService exchangeAnswerSetService;

    private final ExchangeAnswerSetRepository exchangeAnswerSetRepository;

    public ExchangeAnswerSetResource(
        ExchangeAnswerSetService exchangeAnswerSetService,
        ExchangeAnswerSetRepository exchangeAnswerSetRepository
    ) {
        this.exchangeAnswerSetService = exchangeAnswerSetService;
        this.exchangeAnswerSetRepository = exchangeAnswerSetRepository;
    }

    /**
     * {@code POST  /exchange-answer-sets} : Create a new exchangeAnswerSet.
     *
     * @param exchangeAnswerSetDTO the exchangeAnswerSetDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangeAnswerSetDTO, or with status {@code 400 (Bad Request)} if the exchangeAnswerSet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-answer-sets")
    public ResponseEntity<ExchangeAnswerSetDTO> createExchangeAnswerSet(@RequestBody ExchangeAnswerSetDTO exchangeAnswerSetDTO)
        throws URISyntaxException {
        log.debug("REST request to save ExchangeAnswerSet : {}", exchangeAnswerSetDTO);
        if (exchangeAnswerSetDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangeAnswerSet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangeAnswerSetDTO result = exchangeAnswerSetService.save(exchangeAnswerSetDTO);
        return ResponseEntity
            .created(new URI("/api/exchange-answer-sets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-answer-sets/:id} : Updates an existing exchangeAnswerSet.
     *
     * @param id the id of the exchangeAnswerSetDTO to save.
     * @param exchangeAnswerSetDTO the exchangeAnswerSetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeAnswerSetDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeAnswerSetDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangeAnswerSetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-answer-sets/{id}")
    public ResponseEntity<ExchangeAnswerSetDTO> updateExchangeAnswerSet(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeAnswerSetDTO exchangeAnswerSetDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExchangeAnswerSet : {}, {}", id, exchangeAnswerSetDTO);
        if (exchangeAnswerSetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeAnswerSetDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeAnswerSetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExchangeAnswerSetDTO result = exchangeAnswerSetService.update(exchangeAnswerSetDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeAnswerSetDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /exchange-answer-sets/:id} : Partial updates given fields of an existing exchangeAnswerSet, field will ignore if it is null
     *
     * @param id the id of the exchangeAnswerSetDTO to save.
     * @param exchangeAnswerSetDTO the exchangeAnswerSetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeAnswerSetDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeAnswerSetDTO is not valid,
     * or with status {@code 404 (Not Found)} if the exchangeAnswerSetDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the exchangeAnswerSetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/exchange-answer-sets/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExchangeAnswerSetDTO> partialUpdateExchangeAnswerSet(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeAnswerSetDTO exchangeAnswerSetDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExchangeAnswerSet partially : {}, {}", id, exchangeAnswerSetDTO);
        if (exchangeAnswerSetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeAnswerSetDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeAnswerSetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExchangeAnswerSetDTO> result = exchangeAnswerSetService.partialUpdate(exchangeAnswerSetDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeAnswerSetDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /exchange-answer-sets} : get all the exchangeAnswerSets.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangeAnswerSets in body.
     */
    @GetMapping("/exchange-answer-sets")
    public ResponseEntity<List<ExchangeAnswerSetDTO>> getAllExchangeAnswerSets(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of ExchangeAnswerSets");
        Page<ExchangeAnswerSetDTO> page = exchangeAnswerSetService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exchange-answer-sets/:id} : get the "id" exchangeAnswerSet.
     *
     * @param id the id of the exchangeAnswerSetDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangeAnswerSetDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-answer-sets/{id}")
    public ResponseEntity<ExchangeAnswerSetDTO> getExchangeAnswerSet(@PathVariable Long id) {
        log.debug("REST request to get ExchangeAnswerSet : {}", id);
        Optional<ExchangeAnswerSetDTO> exchangeAnswerSetDTO = exchangeAnswerSetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangeAnswerSetDTO);
    }

    /**
     * {@code DELETE  /exchange-answer-sets/:id} : delete the "id" exchangeAnswerSet.
     *
     * @param id the id of the exchangeAnswerSetDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-answer-sets/{id}")
    public ResponseEntity<Void> deleteExchangeAnswerSet(@PathVariable Long id) {
        log.debug("REST request to delete ExchangeAnswerSet : {}", id);
        exchangeAnswerSetService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
