package com.reliance.retail.jmd.exchange.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswer} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeAnswerDTO implements Serializable {

    private Long id;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private QuestionDTO question;

    private OptionDTO option;

    private ExchangeAnswerSetDTO set;

    private ExchangeAnswerSessionDTO session;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public QuestionDTO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionDTO question) {
        this.question = question;
    }

    public OptionDTO getOption() {
        return option;
    }

    public void setOption(OptionDTO option) {
        this.option = option;
    }

    public ExchangeAnswerSetDTO getSet() {
        return set;
    }

    public void setSet(ExchangeAnswerSetDTO set) {
        this.set = set;
    }

    public ExchangeAnswerSessionDTO getSession() {
        return session;
    }

    public void setSession(ExchangeAnswerSessionDTO session) {
        this.session = session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeAnswerDTO)) {
            return false;
        }

        ExchangeAnswerDTO exchangeAnswerDTO = (ExchangeAnswerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exchangeAnswerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeAnswerDTO{" +
            "id=" + getId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", question=" + getQuestion() +
            ", option=" + getOption() +
            ", set=" + getSet() +
            ", session=" + getSession() +
            "}";
    }
}
