package com.reliance.retail.jmd.exchange.service.dto;

import com.reliance.retail.jmd.exchange.domain.enumeration.CancellationReason;
import com.reliance.retail.jmd.exchange.domain.enumeration.ExchangeOrderStatus;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.reliance.retail.jmd.exchange.domain.ExchangeOrder} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeOrderDTO implements Serializable {

    private Long id;

    private String clientOrderRefId;

    private String customerPhoneNo;

    private String clientOrderDeliveryRefId;

    private String deliveryPartnerPhoneNo;

    private Double totalOrderAmount;

    private Double paidAmount;

    private Double codInitialPendingAmount;

    private Double exchangeEstimateAmount;

    private Double exchangeFinalAmountToUser;

    private Double codFinalAmount;

    private Double codAmountPaid;

    private String codPaymentTxnRefId;

    private ExchangeOrderStatus status;

    private CancellationReason cancellationReason;

    private LocalDate expectedPickupDate;

    private LocalDate customerPickupDate;

    private LocalDate dcDeliveredDate;

    private LocalDate dcPickupDate;

    private Double exchangeFinalAmountToDc;

    private LocalDate vendorDeliveredDate;

    private Double exchangeFinalAmountToVendor;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private AddressDTO customerAddress;

    private AddressDTO vendorAddress;

    private AddressDTO dcAddress;

    private ClientDTO client;

    private ProductContextDTO product;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientOrderRefId() {
        return clientOrderRefId;
    }

    public void setClientOrderRefId(String clientOrderRefId) {
        this.clientOrderRefId = clientOrderRefId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getClientOrderDeliveryRefId() {
        return clientOrderDeliveryRefId;
    }

    public void setClientOrderDeliveryRefId(String clientOrderDeliveryRefId) {
        this.clientOrderDeliveryRefId = clientOrderDeliveryRefId;
    }

    public String getDeliveryPartnerPhoneNo() {
        return deliveryPartnerPhoneNo;
    }

    public void setDeliveryPartnerPhoneNo(String deliveryPartnerPhoneNo) {
        this.deliveryPartnerPhoneNo = deliveryPartnerPhoneNo;
    }

    public Double getTotalOrderAmount() {
        return totalOrderAmount;
    }

    public void setTotalOrderAmount(Double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Double getCodInitialPendingAmount() {
        return codInitialPendingAmount;
    }

    public void setCodInitialPendingAmount(Double codInitialPendingAmount) {
        this.codInitialPendingAmount = codInitialPendingAmount;
    }

    public Double getExchangeEstimateAmount() {
        return exchangeEstimateAmount;
    }

    public void setExchangeEstimateAmount(Double exchangeEstimateAmount) {
        this.exchangeEstimateAmount = exchangeEstimateAmount;
    }

    public Double getExchangeFinalAmountToUser() {
        return exchangeFinalAmountToUser;
    }

    public void setExchangeFinalAmountToUser(Double exchangeFinalAmountToUser) {
        this.exchangeFinalAmountToUser = exchangeFinalAmountToUser;
    }

    public Double getCodFinalAmount() {
        return codFinalAmount;
    }

    public void setCodFinalAmount(Double codFinalAmount) {
        this.codFinalAmount = codFinalAmount;
    }

    public Double getCodAmountPaid() {
        return codAmountPaid;
    }

    public void setCodAmountPaid(Double codAmountPaid) {
        this.codAmountPaid = codAmountPaid;
    }

    public String getCodPaymentTxnRefId() {
        return codPaymentTxnRefId;
    }

    public void setCodPaymentTxnRefId(String codPaymentTxnRefId) {
        this.codPaymentTxnRefId = codPaymentTxnRefId;
    }

    public ExchangeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(ExchangeOrderStatus status) {
        this.status = status;
    }

    public CancellationReason getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(CancellationReason cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public LocalDate getExpectedPickupDate() {
        return expectedPickupDate;
    }

    public void setExpectedPickupDate(LocalDate expectedPickupDate) {
        this.expectedPickupDate = expectedPickupDate;
    }

    public LocalDate getCustomerPickupDate() {
        return customerPickupDate;
    }

    public void setCustomerPickupDate(LocalDate customerPickupDate) {
        this.customerPickupDate = customerPickupDate;
    }

    public LocalDate getDcDeliveredDate() {
        return dcDeliveredDate;
    }

    public void setDcDeliveredDate(LocalDate dcDeliveredDate) {
        this.dcDeliveredDate = dcDeliveredDate;
    }

    public LocalDate getDcPickupDate() {
        return dcPickupDate;
    }

    public void setDcPickupDate(LocalDate dcPickupDate) {
        this.dcPickupDate = dcPickupDate;
    }

    public Double getExchangeFinalAmountToDc() {
        return exchangeFinalAmountToDc;
    }

    public void setExchangeFinalAmountToDc(Double exchangeFinalAmountToDc) {
        this.exchangeFinalAmountToDc = exchangeFinalAmountToDc;
    }

    public LocalDate getVendorDeliveredDate() {
        return vendorDeliveredDate;
    }

    public void setVendorDeliveredDate(LocalDate vendorDeliveredDate) {
        this.vendorDeliveredDate = vendorDeliveredDate;
    }

    public Double getExchangeFinalAmountToVendor() {
        return exchangeFinalAmountToVendor;
    }

    public void setExchangeFinalAmountToVendor(Double exchangeFinalAmountToVendor) {
        this.exchangeFinalAmountToVendor = exchangeFinalAmountToVendor;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public AddressDTO getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(AddressDTO customerAddress) {
        this.customerAddress = customerAddress;
    }

    public AddressDTO getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(AddressDTO vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public AddressDTO getDcAddress() {
        return dcAddress;
    }

    public void setDcAddress(AddressDTO dcAddress) {
        this.dcAddress = dcAddress;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public ProductContextDTO getProduct() {
        return product;
    }

    public void setProduct(ProductContextDTO product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeOrderDTO)) {
            return false;
        }

        ExchangeOrderDTO exchangeOrderDTO = (ExchangeOrderDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exchangeOrderDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeOrderDTO{" +
            "id=" + getId() +
            ", clientOrderRefId='" + getClientOrderRefId() + "'" +
            ", customerPhoneNo='" + getCustomerPhoneNo() + "'" +
            ", clientOrderDeliveryRefId='" + getClientOrderDeliveryRefId() + "'" +
            ", deliveryPartnerPhoneNo='" + getDeliveryPartnerPhoneNo() + "'" +
            ", totalOrderAmount=" + getTotalOrderAmount() +
            ", paidAmount=" + getPaidAmount() +
            ", codInitialPendingAmount=" + getCodInitialPendingAmount() +
            ", exchangeEstimateAmount=" + getExchangeEstimateAmount() +
            ", exchangeFinalAmountToUser=" + getExchangeFinalAmountToUser() +
            ", codFinalAmount=" + getCodFinalAmount() +
            ", codAmountPaid=" + getCodAmountPaid() +
            ", codPaymentTxnRefId='" + getCodPaymentTxnRefId() + "'" +
            ", status='" + getStatus() + "'" +
            ", cancellationReason='" + getCancellationReason() + "'" +
            ", expectedPickupDate='" + getExpectedPickupDate() + "'" +
            ", customerPickupDate='" + getCustomerPickupDate() + "'" +
            ", dcDeliveredDate='" + getDcDeliveredDate() + "'" +
            ", dcPickupDate='" + getDcPickupDate() + "'" +
            ", exchangeFinalAmountToDc=" + getExchangeFinalAmountToDc() +
            ", vendorDeliveredDate='" + getVendorDeliveredDate() + "'" +
            ", exchangeFinalAmountToVendor=" + getExchangeFinalAmountToVendor() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", customerAddress=" + getCustomerAddress() +
            ", vendorAddress=" + getVendorAddress() +
            ", dcAddress=" + getDcAddress() +
            ", client=" + getClient() +
            ", product=" + getProduct() +
            "}";
    }
}
