package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.ChargedTo;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DifferentialAmount.
 */
@Entity
@Table(name = "differential_amount")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DifferentialAmount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "differential_amount")
    private Double differentialAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "charged_to")
    private ChargedTo chargedTo;

    @Column(name = "approved_at")
    private LocalDate approvedAt;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "customerAddress", "vendorAddress", "dcAddress", "images", "exchangeAnswerSessions", "differentialAmounts", "client", "product",
        },
        allowSetters = true
    )
    private ExchangeOrder order;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "clients", "deliveryPartners", "vendors", "vendorHubs", "vendorHubMappings", "exchangePrices", "differentialAmounts" },
        allowSetters = true
    )
    private ExchangeOpsUser approvedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public DifferentialAmount id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDifferentialAmount() {
        return this.differentialAmount;
    }

    public DifferentialAmount differentialAmount(Double differentialAmount) {
        this.setDifferentialAmount(differentialAmount);
        return this;
    }

    public void setDifferentialAmount(Double differentialAmount) {
        this.differentialAmount = differentialAmount;
    }

    public ChargedTo getChargedTo() {
        return this.chargedTo;
    }

    public DifferentialAmount chargedTo(ChargedTo chargedTo) {
        this.setChargedTo(chargedTo);
        return this;
    }

    public void setChargedTo(ChargedTo chargedTo) {
        this.chargedTo = chargedTo;
    }

    public LocalDate getApprovedAt() {
        return this.approvedAt;
    }

    public DifferentialAmount approvedAt(LocalDate approvedAt) {
        this.setApprovedAt(approvedAt);
        return this;
    }

    public void setApprovedAt(LocalDate approvedAt) {
        this.approvedAt = approvedAt;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public DifferentialAmount createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public DifferentialAmount updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public DifferentialAmount createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public DifferentialAmount updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public ExchangeOrder getOrder() {
        return this.order;
    }

    public void setOrder(ExchangeOrder exchangeOrder) {
        this.order = exchangeOrder;
    }

    public DifferentialAmount order(ExchangeOrder exchangeOrder) {
        this.setOrder(exchangeOrder);
        return this;
    }

    public ExchangeOpsUser getApprovedBy() {
        return this.approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUser exchangeOpsUser) {
        this.approvedBy = exchangeOpsUser;
    }

    public DifferentialAmount approvedBy(ExchangeOpsUser exchangeOpsUser) {
        this.setApprovedBy(exchangeOpsUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DifferentialAmount)) {
            return false;
        }
        return id != null && id.equals(((DifferentialAmount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DifferentialAmount{" +
            "id=" + getId() +
            ", differentialAmount=" + getDifferentialAmount() +
            ", chargedTo='" + getChargedTo() + "'" +
            ", approvedAt='" + getApprovedAt() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
