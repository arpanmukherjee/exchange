package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExchangeAnswer.
 */
@Entity
@Table(name = "exchange_answer")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeAnswer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @JsonIgnoreProperties(value = { "options", "clients" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Question question;

    @JsonIgnoreProperties(value = { "question", "clients" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Option option;

    @ManyToOne
    @JsonIgnoreProperties(value = { "price", "exchangeAnswers" }, allowSetters = true)
    private ExchangeAnswerSet set;

    @ManyToOne
    @JsonIgnoreProperties(value = { "price", "exchangeAnswers", "client", "order" }, allowSetters = true)
    private ExchangeAnswerSession session;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExchangeAnswer id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public ExchangeAnswer createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public ExchangeAnswer updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Question getQuestion() {
        return this.question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public ExchangeAnswer question(Question question) {
        this.setQuestion(question);
        return this;
    }

    public Option getOption() {
        return this.option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public ExchangeAnswer option(Option option) {
        this.setOption(option);
        return this;
    }

    public ExchangeAnswerSet getSet() {
        return this.set;
    }

    public void setSet(ExchangeAnswerSet exchangeAnswerSet) {
        this.set = exchangeAnswerSet;
    }

    public ExchangeAnswer set(ExchangeAnswerSet exchangeAnswerSet) {
        this.setSet(exchangeAnswerSet);
        return this;
    }

    public ExchangeAnswerSession getSession() {
        return this.session;
    }

    public void setSession(ExchangeAnswerSession exchangeAnswerSession) {
        this.session = exchangeAnswerSession;
    }

    public ExchangeAnswer session(ExchangeAnswerSession exchangeAnswerSession) {
        this.setSession(exchangeAnswerSession);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeAnswer)) {
            return false;
        }
        return id != null && id.equals(((ExchangeAnswer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeAnswer{" +
            "id=" + getId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
