package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A VendorHub.
 */
@Entity
@Table(name = "vendor_hub")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class VendorHub implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnoreProperties(value = { "vendorHub", "pincode" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Address address;

    @OneToMany(mappedBy = "vendorHub")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "servingPincodes", "approvedBy", "vendorHub" }, allowSetters = true)
    private Set<VendorHubMapping> vendorHubMappings = new HashSet<>();

    @OneToMany(mappedBy = "vendorHub")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "set", "session", "approvedBy", "client", "vendorHub" }, allowSetters = true)
    private Set<ExchangePrice> exchangePrices = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "clients", "deliveryPartners", "vendors", "vendorHubs", "vendorHubMappings", "exchangePrices", "differentialAmounts" },
        allowSetters = true
    )
    private ExchangeOpsUser approvedBy;

    @ManyToOne
    @JsonIgnoreProperties(value = { "documents", "vendorHubs", "approvedBy" }, allowSetters = true)
    private Vendor vendor;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public VendorHub id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public VendorHub name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public VendorHub isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public VendorHub createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public VendorHub updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public VendorHub createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public VendorHub updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public VendorHub address(Address address) {
        this.setAddress(address);
        return this;
    }

    public Set<VendorHubMapping> getVendorHubMappings() {
        return this.vendorHubMappings;
    }

    public void setVendorHubMappings(Set<VendorHubMapping> vendorHubMappings) {
        if (this.vendorHubMappings != null) {
            this.vendorHubMappings.forEach(i -> i.setVendorHub(null));
        }
        if (vendorHubMappings != null) {
            vendorHubMappings.forEach(i -> i.setVendorHub(this));
        }
        this.vendorHubMappings = vendorHubMappings;
    }

    public VendorHub vendorHubMappings(Set<VendorHubMapping> vendorHubMappings) {
        this.setVendorHubMappings(vendorHubMappings);
        return this;
    }

    public VendorHub addVendorHubMapping(VendorHubMapping vendorHubMapping) {
        this.vendorHubMappings.add(vendorHubMapping);
        vendorHubMapping.setVendorHub(this);
        return this;
    }

    public VendorHub removeVendorHubMapping(VendorHubMapping vendorHubMapping) {
        this.vendorHubMappings.remove(vendorHubMapping);
        vendorHubMapping.setVendorHub(null);
        return this;
    }

    public Set<ExchangePrice> getExchangePrices() {
        return this.exchangePrices;
    }

    public void setExchangePrices(Set<ExchangePrice> exchangePrices) {
        if (this.exchangePrices != null) {
            this.exchangePrices.forEach(i -> i.setVendorHub(null));
        }
        if (exchangePrices != null) {
            exchangePrices.forEach(i -> i.setVendorHub(this));
        }
        this.exchangePrices = exchangePrices;
    }

    public VendorHub exchangePrices(Set<ExchangePrice> exchangePrices) {
        this.setExchangePrices(exchangePrices);
        return this;
    }

    public VendorHub addExchangePrice(ExchangePrice exchangePrice) {
        this.exchangePrices.add(exchangePrice);
        exchangePrice.setVendorHub(this);
        return this;
    }

    public VendorHub removeExchangePrice(ExchangePrice exchangePrice) {
        this.exchangePrices.remove(exchangePrice);
        exchangePrice.setVendorHub(null);
        return this;
    }

    public ExchangeOpsUser getApprovedBy() {
        return this.approvedBy;
    }

    public void setApprovedBy(ExchangeOpsUser exchangeOpsUser) {
        this.approvedBy = exchangeOpsUser;
    }

    public VendorHub approvedBy(ExchangeOpsUser exchangeOpsUser) {
        this.setApprovedBy(exchangeOpsUser);
        return this;
    }

    public Vendor getVendor() {
        return this.vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public VendorHub vendor(Vendor vendor) {
        this.setVendor(vendor);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VendorHub)) {
            return false;
        }
        return id != null && id.equals(((VendorHub) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VendorHub{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
