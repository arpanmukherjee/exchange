package com.reliance.retail.jmd.exchange.service.impl;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswer;
import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeAnswerService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeAnswerMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExchangeAnswer}.
 */
@Service
@Transactional
public class ExchangeAnswerServiceImpl implements ExchangeAnswerService {

    private final Logger log = LoggerFactory.getLogger(ExchangeAnswerServiceImpl.class);

    private final ExchangeAnswerRepository exchangeAnswerRepository;

    private final ExchangeAnswerMapper exchangeAnswerMapper;

    public ExchangeAnswerServiceImpl(ExchangeAnswerRepository exchangeAnswerRepository, ExchangeAnswerMapper exchangeAnswerMapper) {
        this.exchangeAnswerRepository = exchangeAnswerRepository;
        this.exchangeAnswerMapper = exchangeAnswerMapper;
    }

    @Override
    public ExchangeAnswerDTO save(ExchangeAnswerDTO exchangeAnswerDTO) {
        log.debug("Request to save ExchangeAnswer : {}", exchangeAnswerDTO);
        ExchangeAnswer exchangeAnswer = exchangeAnswerMapper.toEntity(exchangeAnswerDTO);
        exchangeAnswer = exchangeAnswerRepository.save(exchangeAnswer);
        return exchangeAnswerMapper.toDto(exchangeAnswer);
    }

    @Override
    public ExchangeAnswerDTO update(ExchangeAnswerDTO exchangeAnswerDTO) {
        log.debug("Request to update ExchangeAnswer : {}", exchangeAnswerDTO);
        ExchangeAnswer exchangeAnswer = exchangeAnswerMapper.toEntity(exchangeAnswerDTO);
        exchangeAnswer = exchangeAnswerRepository.save(exchangeAnswer);
        return exchangeAnswerMapper.toDto(exchangeAnswer);
    }

    @Override
    public Optional<ExchangeAnswerDTO> partialUpdate(ExchangeAnswerDTO exchangeAnswerDTO) {
        log.debug("Request to partially update ExchangeAnswer : {}", exchangeAnswerDTO);

        return exchangeAnswerRepository
            .findById(exchangeAnswerDTO.getId())
            .map(existingExchangeAnswer -> {
                exchangeAnswerMapper.partialUpdate(existingExchangeAnswer, exchangeAnswerDTO);

                return existingExchangeAnswer;
            })
            .map(exchangeAnswerRepository::save)
            .map(exchangeAnswerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExchangeAnswerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangeAnswers");
        return exchangeAnswerRepository.findAll(pageable).map(exchangeAnswerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangeAnswerDTO> findOne(Long id) {
        log.debug("Request to get ExchangeAnswer : {}", id);
        return exchangeAnswerRepository.findById(id).map(exchangeAnswerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangeAnswer : {}", id);
        exchangeAnswerRepository.deleteById(id);
    }
}
