package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.VendorHubMapping;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface VendorHubMappingRepositoryWithBagRelationships {
    Optional<VendorHubMapping> fetchBagRelationships(Optional<VendorHubMapping> vendorHubMapping);

    List<VendorHubMapping> fetchBagRelationships(List<VendorHubMapping> vendorHubMappings);

    Page<VendorHubMapping> fetchBagRelationships(Page<VendorHubMapping> vendorHubMappings);
}
