package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.VendorHubMapping;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the VendorHubMapping entity.
 *
 * When extending this class, extend VendorHubMappingRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface VendorHubMappingRepository extends VendorHubMappingRepositoryWithBagRelationships, JpaRepository<VendorHubMapping, Long> {
    default Optional<VendorHubMapping> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<VendorHubMapping> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<VendorHubMapping> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }
}
