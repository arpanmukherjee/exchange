package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.ExchangeOrderRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeOrderService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeOrder}.
 */
@RestController
@RequestMapping("/api")
public class ExchangeOrderResource {

    private final Logger log = LoggerFactory.getLogger(ExchangeOrderResource.class);

    private static final String ENTITY_NAME = "exchangeOrder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangeOrderService exchangeOrderService;

    private final ExchangeOrderRepository exchangeOrderRepository;

    public ExchangeOrderResource(ExchangeOrderService exchangeOrderService, ExchangeOrderRepository exchangeOrderRepository) {
        this.exchangeOrderService = exchangeOrderService;
        this.exchangeOrderRepository = exchangeOrderRepository;
    }

    /**
     * {@code POST  /exchange-orders} : Create a new exchangeOrder.
     *
     * @param exchangeOrderDTO the exchangeOrderDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangeOrderDTO, or with status {@code 400 (Bad Request)} if the exchangeOrder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-orders")
    public ResponseEntity<ExchangeOrderDTO> createExchangeOrder(@RequestBody ExchangeOrderDTO exchangeOrderDTO) throws URISyntaxException {
        log.debug("REST request to save ExchangeOrder : {}", exchangeOrderDTO);
        if (exchangeOrderDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangeOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangeOrderDTO result = exchangeOrderService.save(exchangeOrderDTO);
        return ResponseEntity
            .created(new URI("/api/exchange-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-orders/:id} : Updates an existing exchangeOrder.
     *
     * @param id the id of the exchangeOrderDTO to save.
     * @param exchangeOrderDTO the exchangeOrderDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeOrderDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeOrderDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangeOrderDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-orders/{id}")
    public ResponseEntity<ExchangeOrderDTO> updateExchangeOrder(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeOrderDTO exchangeOrderDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExchangeOrder : {}, {}", id, exchangeOrderDTO);
        if (exchangeOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeOrderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeOrderRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExchangeOrderDTO result = exchangeOrderService.update(exchangeOrderDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeOrderDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /exchange-orders/:id} : Partial updates given fields of an existing exchangeOrder, field will ignore if it is null
     *
     * @param id the id of the exchangeOrderDTO to save.
     * @param exchangeOrderDTO the exchangeOrderDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeOrderDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeOrderDTO is not valid,
     * or with status {@code 404 (Not Found)} if the exchangeOrderDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the exchangeOrderDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/exchange-orders/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExchangeOrderDTO> partialUpdateExchangeOrder(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeOrderDTO exchangeOrderDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExchangeOrder partially : {}, {}", id, exchangeOrderDTO);
        if (exchangeOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeOrderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeOrderRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExchangeOrderDTO> result = exchangeOrderService.partialUpdate(exchangeOrderDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeOrderDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /exchange-orders} : get all the exchangeOrders.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangeOrders in body.
     */
    @GetMapping("/exchange-orders")
    public ResponseEntity<List<ExchangeOrderDTO>> getAllExchangeOrders(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ExchangeOrders");
        Page<ExchangeOrderDTO> page = exchangeOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exchange-orders/:id} : get the "id" exchangeOrder.
     *
     * @param id the id of the exchangeOrderDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangeOrderDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-orders/{id}")
    public ResponseEntity<ExchangeOrderDTO> getExchangeOrder(@PathVariable Long id) {
        log.debug("REST request to get ExchangeOrder : {}", id);
        Optional<ExchangeOrderDTO> exchangeOrderDTO = exchangeOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangeOrderDTO);
    }

    /**
     * {@code DELETE  /exchange-orders/:id} : delete the "id" exchangeOrder.
     *
     * @param id the id of the exchangeOrderDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-orders/{id}")
    public ResponseEntity<Void> deleteExchangeOrder(@PathVariable Long id) {
        log.debug("REST request to delete ExchangeOrder : {}", id);
        exchangeOrderService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
