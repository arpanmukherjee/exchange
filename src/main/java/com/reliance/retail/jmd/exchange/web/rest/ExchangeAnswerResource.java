package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerRepository;
import com.reliance.retail.jmd.exchange.service.ExchangeAnswerService;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswer}.
 */
@RestController
@RequestMapping("/api")
public class ExchangeAnswerResource {

    private final Logger log = LoggerFactory.getLogger(ExchangeAnswerResource.class);

    private static final String ENTITY_NAME = "exchangeAnswer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangeAnswerService exchangeAnswerService;

    private final ExchangeAnswerRepository exchangeAnswerRepository;

    public ExchangeAnswerResource(ExchangeAnswerService exchangeAnswerService, ExchangeAnswerRepository exchangeAnswerRepository) {
        this.exchangeAnswerService = exchangeAnswerService;
        this.exchangeAnswerRepository = exchangeAnswerRepository;
    }

    /**
     * {@code POST  /exchange-answers} : Create a new exchangeAnswer.
     *
     * @param exchangeAnswerDTO the exchangeAnswerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangeAnswerDTO, or with status {@code 400 (Bad Request)} if the exchangeAnswer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-answers")
    public ResponseEntity<ExchangeAnswerDTO> createExchangeAnswer(@RequestBody ExchangeAnswerDTO exchangeAnswerDTO)
        throws URISyntaxException {
        log.debug("REST request to save ExchangeAnswer : {}", exchangeAnswerDTO);
        if (exchangeAnswerDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangeAnswer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangeAnswerDTO result = exchangeAnswerService.save(exchangeAnswerDTO);
        return ResponseEntity
            .created(new URI("/api/exchange-answers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-answers/:id} : Updates an existing exchangeAnswer.
     *
     * @param id the id of the exchangeAnswerDTO to save.
     * @param exchangeAnswerDTO the exchangeAnswerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeAnswerDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeAnswerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangeAnswerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-answers/{id}")
    public ResponseEntity<ExchangeAnswerDTO> updateExchangeAnswer(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeAnswerDTO exchangeAnswerDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExchangeAnswer : {}, {}", id, exchangeAnswerDTO);
        if (exchangeAnswerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeAnswerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeAnswerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExchangeAnswerDTO result = exchangeAnswerService.update(exchangeAnswerDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeAnswerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /exchange-answers/:id} : Partial updates given fields of an existing exchangeAnswer, field will ignore if it is null
     *
     * @param id the id of the exchangeAnswerDTO to save.
     * @param exchangeAnswerDTO the exchangeAnswerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeAnswerDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeAnswerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the exchangeAnswerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the exchangeAnswerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/exchange-answers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExchangeAnswerDTO> partialUpdateExchangeAnswer(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ExchangeAnswerDTO exchangeAnswerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExchangeAnswer partially : {}, {}", id, exchangeAnswerDTO);
        if (exchangeAnswerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, exchangeAnswerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!exchangeAnswerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExchangeAnswerDTO> result = exchangeAnswerService.partialUpdate(exchangeAnswerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, exchangeAnswerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /exchange-answers} : get all the exchangeAnswers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangeAnswers in body.
     */
    @GetMapping("/exchange-answers")
    public ResponseEntity<List<ExchangeAnswerDTO>> getAllExchangeAnswers(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ExchangeAnswers");
        Page<ExchangeAnswerDTO> page = exchangeAnswerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /exchange-answers/:id} : get the "id" exchangeAnswer.
     *
     * @param id the id of the exchangeAnswerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangeAnswerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-answers/{id}")
    public ResponseEntity<ExchangeAnswerDTO> getExchangeAnswer(@PathVariable Long id) {
        log.debug("REST request to get ExchangeAnswer : {}", id);
        Optional<ExchangeAnswerDTO> exchangeAnswerDTO = exchangeAnswerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangeAnswerDTO);
    }

    /**
     * {@code DELETE  /exchange-answers/:id} : delete the "id" exchangeAnswer.
     *
     * @param id the id of the exchangeAnswerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-answers/{id}")
    public ResponseEntity<Void> deleteExchangeAnswer(@PathVariable Long id) {
        log.debug("REST request to delete ExchangeAnswer : {}", id);
        exchangeAnswerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
