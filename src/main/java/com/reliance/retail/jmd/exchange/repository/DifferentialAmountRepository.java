package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.DifferentialAmount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DifferentialAmount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DifferentialAmountRepository extends JpaRepository<DifferentialAmount, Long> {}
