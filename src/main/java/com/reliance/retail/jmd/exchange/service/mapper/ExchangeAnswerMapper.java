package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.ExchangeAnswer;
import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession;
import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet;
import com.reliance.retail.jmd.exchange.domain.Option;
import com.reliance.retail.jmd.exchange.domain.Question;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSessionDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSetDTO;
import com.reliance.retail.jmd.exchange.service.dto.OptionDTO;
import com.reliance.retail.jmd.exchange.service.dto.QuestionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangeAnswer} and its DTO {@link ExchangeAnswerDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangeAnswerMapper extends EntityMapper<ExchangeAnswerDTO, ExchangeAnswer> {
    @Mapping(target = "question", source = "question", qualifiedByName = "questionId")
    @Mapping(target = "option", source = "option", qualifiedByName = "optionId")
    @Mapping(target = "set", source = "set", qualifiedByName = "exchangeAnswerSetId")
    @Mapping(target = "session", source = "session", qualifiedByName = "exchangeAnswerSessionId")
    ExchangeAnswerDTO toDto(ExchangeAnswer s);

    @Named("questionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    QuestionDTO toDtoQuestionId(Question question);

    @Named("optionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OptionDTO toDtoOptionId(Option option);

    @Named("exchangeAnswerSetId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeAnswerSetDTO toDtoExchangeAnswerSetId(ExchangeAnswerSet exchangeAnswerSet);

    @Named("exchangeAnswerSessionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeAnswerSessionDTO toDtoExchangeAnswerSessionId(ExchangeAnswerSession exchangeAnswerSession);
}
