package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Client;
import com.reliance.retail.jmd.exchange.domain.ProductContext;
import com.reliance.retail.jmd.exchange.service.dto.ClientDTO;
import com.reliance.retail.jmd.exchange.service.dto.ProductContextDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProductContext} and its DTO {@link ProductContextDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductContextMapper extends EntityMapper<ProductContextDTO, ProductContext> {
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    ProductContextDTO toDto(ProductContext s);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);
}
