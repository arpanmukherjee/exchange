package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.Client;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface ClientRepositoryWithBagRelationships {
    Optional<Client> fetchBagRelationships(Optional<Client> client);

    List<Client> fetchBagRelationships(List<Client> clients);

    Page<Client> fetchBagRelationships(Page<Client> clients);
}
