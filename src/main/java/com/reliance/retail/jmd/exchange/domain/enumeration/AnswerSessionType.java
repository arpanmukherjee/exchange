package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The AnswerSessionType enumeration.
 */
public enum AnswerSessionType {
    User,
    DeliveryPartner,
    DC,
    Vendor,
}
