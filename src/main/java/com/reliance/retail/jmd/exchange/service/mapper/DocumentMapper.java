package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Document;
import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import com.reliance.retail.jmd.exchange.domain.ProductContext;
import com.reliance.retail.jmd.exchange.domain.Vendor;
import com.reliance.retail.jmd.exchange.service.dto.DocumentDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import com.reliance.retail.jmd.exchange.service.dto.ProductContextDTO;
import com.reliance.retail.jmd.exchange.service.dto.VendorDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Document} and its DTO {@link DocumentDTO}.
 */
@Mapper(componentModel = "spring")
public interface DocumentMapper extends EntityMapper<DocumentDTO, Document> {
    @Mapping(target = "vendor", source = "vendor", qualifiedByName = "vendorId")
    @Mapping(target = "product", source = "product", qualifiedByName = "productContextId")
    @Mapping(target = "order", source = "order", qualifiedByName = "exchangeOrderId")
    DocumentDTO toDto(Document s);

    @Named("vendorId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    VendorDTO toDtoVendorId(Vendor vendor);

    @Named("productContextId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductContextDTO toDtoProductContextId(ProductContext productContext);

    @Named("exchangeOrderId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOrderDTO toDtoExchangeOrderId(ExchangeOrder exchangeOrder);
}
