package com.reliance.retail.jmd.exchange.service.mapper;

import com.reliance.retail.jmd.exchange.domain.Client;
import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession;
import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import com.reliance.retail.jmd.exchange.domain.ExchangePrice;
import com.reliance.retail.jmd.exchange.service.dto.ClientDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSessionDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangeAnswerSession} and its DTO {@link ExchangeAnswerSessionDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangeAnswerSessionMapper extends EntityMapper<ExchangeAnswerSessionDTO, ExchangeAnswerSession> {
    @Mapping(target = "price", source = "price", qualifiedByName = "exchangePriceId")
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    @Mapping(target = "order", source = "order", qualifiedByName = "exchangeOrderId")
    ExchangeAnswerSessionDTO toDto(ExchangeAnswerSession s);

    @Named("exchangePriceId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangePriceDTO toDtoExchangePriceId(ExchangePrice exchangePrice);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("exchangeOrderId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExchangeOrderDTO toDtoExchangeOrderId(ExchangeOrder exchangeOrder);
}
