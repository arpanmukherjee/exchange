package com.reliance.retail.jmd.exchange.domain.enumeration;

/**
 * The State enumeration.
 */
public enum State {
    Maharastra,
    Gujrat,
    Karnataka,
    Delhi,
}
