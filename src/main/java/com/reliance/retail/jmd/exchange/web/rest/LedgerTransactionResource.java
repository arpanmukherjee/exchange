package com.reliance.retail.jmd.exchange.web.rest;

import com.reliance.retail.jmd.exchange.repository.LedgerTransactionRepository;
import com.reliance.retail.jmd.exchange.service.LedgerTransactionService;
import com.reliance.retail.jmd.exchange.service.dto.LedgerTransactionDTO;
import com.reliance.retail.jmd.exchange.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.reliance.retail.jmd.exchange.domain.LedgerTransaction}.
 */
@RestController
@RequestMapping("/api")
public class LedgerTransactionResource {

    private final Logger log = LoggerFactory.getLogger(LedgerTransactionResource.class);

    private static final String ENTITY_NAME = "ledgerTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LedgerTransactionService ledgerTransactionService;

    private final LedgerTransactionRepository ledgerTransactionRepository;

    public LedgerTransactionResource(
        LedgerTransactionService ledgerTransactionService,
        LedgerTransactionRepository ledgerTransactionRepository
    ) {
        this.ledgerTransactionService = ledgerTransactionService;
        this.ledgerTransactionRepository = ledgerTransactionRepository;
    }

    /**
     * {@code POST  /ledger-transactions} : Create a new ledgerTransaction.
     *
     * @param ledgerTransactionDTO the ledgerTransactionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ledgerTransactionDTO, or with status {@code 400 (Bad Request)} if the ledgerTransaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ledger-transactions")
    public ResponseEntity<LedgerTransactionDTO> createLedgerTransaction(@RequestBody LedgerTransactionDTO ledgerTransactionDTO)
        throws URISyntaxException {
        log.debug("REST request to save LedgerTransaction : {}", ledgerTransactionDTO);
        if (ledgerTransactionDTO.getId() != null) {
            throw new BadRequestAlertException("A new ledgerTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LedgerTransactionDTO result = ledgerTransactionService.save(ledgerTransactionDTO);
        return ResponseEntity
            .created(new URI("/api/ledger-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ledger-transactions/:id} : Updates an existing ledgerTransaction.
     *
     * @param id the id of the ledgerTransactionDTO to save.
     * @param ledgerTransactionDTO the ledgerTransactionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ledgerTransactionDTO,
     * or with status {@code 400 (Bad Request)} if the ledgerTransactionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ledgerTransactionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ledger-transactions/{id}")
    public ResponseEntity<LedgerTransactionDTO> updateLedgerTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LedgerTransactionDTO ledgerTransactionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update LedgerTransaction : {}, {}", id, ledgerTransactionDTO);
        if (ledgerTransactionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ledgerTransactionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ledgerTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LedgerTransactionDTO result = ledgerTransactionService.update(ledgerTransactionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ledgerTransactionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ledger-transactions/:id} : Partial updates given fields of an existing ledgerTransaction, field will ignore if it is null
     *
     * @param id the id of the ledgerTransactionDTO to save.
     * @param ledgerTransactionDTO the ledgerTransactionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ledgerTransactionDTO,
     * or with status {@code 400 (Bad Request)} if the ledgerTransactionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the ledgerTransactionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the ledgerTransactionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ledger-transactions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<LedgerTransactionDTO> partialUpdateLedgerTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LedgerTransactionDTO ledgerTransactionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update LedgerTransaction partially : {}, {}", id, ledgerTransactionDTO);
        if (ledgerTransactionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ledgerTransactionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ledgerTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LedgerTransactionDTO> result = ledgerTransactionService.partialUpdate(ledgerTransactionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ledgerTransactionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /ledger-transactions} : get all the ledgerTransactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ledgerTransactions in body.
     */
    @GetMapping("/ledger-transactions")
    public ResponseEntity<List<LedgerTransactionDTO>> getAllLedgerTransactions(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of LedgerTransactions");
        Page<LedgerTransactionDTO> page = ledgerTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ledger-transactions/:id} : get the "id" ledgerTransaction.
     *
     * @param id the id of the ledgerTransactionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ledgerTransactionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ledger-transactions/{id}")
    public ResponseEntity<LedgerTransactionDTO> getLedgerTransaction(@PathVariable Long id) {
        log.debug("REST request to get LedgerTransaction : {}", id);
        Optional<LedgerTransactionDTO> ledgerTransactionDTO = ledgerTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ledgerTransactionDTO);
    }

    /**
     * {@code DELETE  /ledger-transactions/:id} : delete the "id" ledgerTransaction.
     *
     * @param id the id of the ledgerTransactionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ledger-transactions/{id}")
    public ResponseEntity<Void> deleteLedgerTransaction(@PathVariable Long id) {
        log.debug("REST request to delete LedgerTransaction : {}", id);
        ledgerTransactionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
