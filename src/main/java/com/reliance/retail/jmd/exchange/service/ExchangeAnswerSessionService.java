package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSessionDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession}.
 */
public interface ExchangeAnswerSessionService {
    /**
     * Save a exchangeAnswerSession.
     *
     * @param exchangeAnswerSessionDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangeAnswerSessionDTO save(ExchangeAnswerSessionDTO exchangeAnswerSessionDTO);

    /**
     * Updates a exchangeAnswerSession.
     *
     * @param exchangeAnswerSessionDTO the entity to update.
     * @return the persisted entity.
     */
    ExchangeAnswerSessionDTO update(ExchangeAnswerSessionDTO exchangeAnswerSessionDTO);

    /**
     * Partially updates a exchangeAnswerSession.
     *
     * @param exchangeAnswerSessionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExchangeAnswerSessionDTO> partialUpdate(ExchangeAnswerSessionDTO exchangeAnswerSessionDTO);

    /**
     * Get all the exchangeAnswerSessions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangeAnswerSessionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" exchangeAnswerSession.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangeAnswerSessionDTO> findOne(Long id);

    /**
     * Delete the "id" exchangeAnswerSession.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
