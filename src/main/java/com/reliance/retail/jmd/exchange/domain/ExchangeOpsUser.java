package com.reliance.retail.jmd.exchange.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.reliance.retail.jmd.exchange.domain.enumeration.Role;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExchangeOpsUser.
 */
@Entity
@Table(name = "exchange_ops_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeOpsUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    @Column(name = "email")
    private String email;

    @NotNull
    @Min(value = 1000000000L)
    @Max(value = 9999999999L)
    @Column(name = "phone", nullable = false)
    private Long phone;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "productContexts", "exchangePrices", "exchangeOrders", "exchangeAnswerSessions", "questions", "options", "approvedBy" },
        allowSetters = true
    )
    private Set<Client> clients = new HashSet<>();

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "approvedBy" }, allowSetters = true)
    private Set<DeliveryPartner> deliveryPartners = new HashSet<>();

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "documents", "vendorHubs", "approvedBy" }, allowSetters = true)
    private Set<Vendor> vendors = new HashSet<>();

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "address", "vendorHubMappings", "exchangePrices", "approvedBy", "vendor" }, allowSetters = true)
    private Set<VendorHub> vendorHubs = new HashSet<>();

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "servingPincodes", "approvedBy", "vendorHub" }, allowSetters = true)
    private Set<VendorHubMapping> vendorHubMappings = new HashSet<>();

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "set", "session", "approvedBy", "client", "vendorHub" }, allowSetters = true)
    private Set<ExchangePrice> exchangePrices = new HashSet<>();

    @OneToMany(mappedBy = "approvedBy")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "order", "approvedBy" }, allowSetters = true)
    private Set<DifferentialAmount> differentialAmounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExchangeOpsUser id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public ExchangeOpsUser name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public ExchangeOpsUser email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return this.phone;
    }

    public ExchangeOpsUser phone(Long phone) {
        this.setPhone(phone);
        return this;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Role getRole() {
        return this.role;
    }

    public ExchangeOpsUser role(Role role) {
        this.setRole(role);
        return this;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public ExchangeOpsUser isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getCreatedAt() {
        return this.createdAt;
    }

    public ExchangeOpsUser createdAt(LocalDate createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return this.updatedAt;
    }

    public ExchangeOpsUser updatedAt(LocalDate updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public ExchangeOpsUser createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public ExchangeOpsUser updatedBy(String updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<Client> getClients() {
        return this.clients;
    }

    public void setClients(Set<Client> clients) {
        if (this.clients != null) {
            this.clients.forEach(i -> i.setApprovedBy(null));
        }
        if (clients != null) {
            clients.forEach(i -> i.setApprovedBy(this));
        }
        this.clients = clients;
    }

    public ExchangeOpsUser clients(Set<Client> clients) {
        this.setClients(clients);
        return this;
    }

    public ExchangeOpsUser addClient(Client client) {
        this.clients.add(client);
        client.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeClient(Client client) {
        this.clients.remove(client);
        client.setApprovedBy(null);
        return this;
    }

    public Set<DeliveryPartner> getDeliveryPartners() {
        return this.deliveryPartners;
    }

    public void setDeliveryPartners(Set<DeliveryPartner> deliveryPartners) {
        if (this.deliveryPartners != null) {
            this.deliveryPartners.forEach(i -> i.setApprovedBy(null));
        }
        if (deliveryPartners != null) {
            deliveryPartners.forEach(i -> i.setApprovedBy(this));
        }
        this.deliveryPartners = deliveryPartners;
    }

    public ExchangeOpsUser deliveryPartners(Set<DeliveryPartner> deliveryPartners) {
        this.setDeliveryPartners(deliveryPartners);
        return this;
    }

    public ExchangeOpsUser addDeliveryPartner(DeliveryPartner deliveryPartner) {
        this.deliveryPartners.add(deliveryPartner);
        deliveryPartner.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeDeliveryPartner(DeliveryPartner deliveryPartner) {
        this.deliveryPartners.remove(deliveryPartner);
        deliveryPartner.setApprovedBy(null);
        return this;
    }

    public Set<Vendor> getVendors() {
        return this.vendors;
    }

    public void setVendors(Set<Vendor> vendors) {
        if (this.vendors != null) {
            this.vendors.forEach(i -> i.setApprovedBy(null));
        }
        if (vendors != null) {
            vendors.forEach(i -> i.setApprovedBy(this));
        }
        this.vendors = vendors;
    }

    public ExchangeOpsUser vendors(Set<Vendor> vendors) {
        this.setVendors(vendors);
        return this;
    }

    public ExchangeOpsUser addVendor(Vendor vendor) {
        this.vendors.add(vendor);
        vendor.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeVendor(Vendor vendor) {
        this.vendors.remove(vendor);
        vendor.setApprovedBy(null);
        return this;
    }

    public Set<VendorHub> getVendorHubs() {
        return this.vendorHubs;
    }

    public void setVendorHubs(Set<VendorHub> vendorHubs) {
        if (this.vendorHubs != null) {
            this.vendorHubs.forEach(i -> i.setApprovedBy(null));
        }
        if (vendorHubs != null) {
            vendorHubs.forEach(i -> i.setApprovedBy(this));
        }
        this.vendorHubs = vendorHubs;
    }

    public ExchangeOpsUser vendorHubs(Set<VendorHub> vendorHubs) {
        this.setVendorHubs(vendorHubs);
        return this;
    }

    public ExchangeOpsUser addVendorHub(VendorHub vendorHub) {
        this.vendorHubs.add(vendorHub);
        vendorHub.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeVendorHub(VendorHub vendorHub) {
        this.vendorHubs.remove(vendorHub);
        vendorHub.setApprovedBy(null);
        return this;
    }

    public Set<VendorHubMapping> getVendorHubMappings() {
        return this.vendorHubMappings;
    }

    public void setVendorHubMappings(Set<VendorHubMapping> vendorHubMappings) {
        if (this.vendorHubMappings != null) {
            this.vendorHubMappings.forEach(i -> i.setApprovedBy(null));
        }
        if (vendorHubMappings != null) {
            vendorHubMappings.forEach(i -> i.setApprovedBy(this));
        }
        this.vendorHubMappings = vendorHubMappings;
    }

    public ExchangeOpsUser vendorHubMappings(Set<VendorHubMapping> vendorHubMappings) {
        this.setVendorHubMappings(vendorHubMappings);
        return this;
    }

    public ExchangeOpsUser addVendorHubMapping(VendorHubMapping vendorHubMapping) {
        this.vendorHubMappings.add(vendorHubMapping);
        vendorHubMapping.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeVendorHubMapping(VendorHubMapping vendorHubMapping) {
        this.vendorHubMappings.remove(vendorHubMapping);
        vendorHubMapping.setApprovedBy(null);
        return this;
    }

    public Set<ExchangePrice> getExchangePrices() {
        return this.exchangePrices;
    }

    public void setExchangePrices(Set<ExchangePrice> exchangePrices) {
        if (this.exchangePrices != null) {
            this.exchangePrices.forEach(i -> i.setApprovedBy(null));
        }
        if (exchangePrices != null) {
            exchangePrices.forEach(i -> i.setApprovedBy(this));
        }
        this.exchangePrices = exchangePrices;
    }

    public ExchangeOpsUser exchangePrices(Set<ExchangePrice> exchangePrices) {
        this.setExchangePrices(exchangePrices);
        return this;
    }

    public ExchangeOpsUser addExchangePrice(ExchangePrice exchangePrice) {
        this.exchangePrices.add(exchangePrice);
        exchangePrice.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeExchangePrice(ExchangePrice exchangePrice) {
        this.exchangePrices.remove(exchangePrice);
        exchangePrice.setApprovedBy(null);
        return this;
    }

    public Set<DifferentialAmount> getDifferentialAmounts() {
        return this.differentialAmounts;
    }

    public void setDifferentialAmounts(Set<DifferentialAmount> differentialAmounts) {
        if (this.differentialAmounts != null) {
            this.differentialAmounts.forEach(i -> i.setApprovedBy(null));
        }
        if (differentialAmounts != null) {
            differentialAmounts.forEach(i -> i.setApprovedBy(this));
        }
        this.differentialAmounts = differentialAmounts;
    }

    public ExchangeOpsUser differentialAmounts(Set<DifferentialAmount> differentialAmounts) {
        this.setDifferentialAmounts(differentialAmounts);
        return this;
    }

    public ExchangeOpsUser addDifferentialAmount(DifferentialAmount differentialAmount) {
        this.differentialAmounts.add(differentialAmount);
        differentialAmount.setApprovedBy(this);
        return this;
    }

    public ExchangeOpsUser removeDifferentialAmount(DifferentialAmount differentialAmount) {
        this.differentialAmounts.remove(differentialAmount);
        differentialAmount.setApprovedBy(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeOpsUser)) {
            return false;
        }
        return id != null && id.equals(((ExchangeOpsUser) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeOpsUser{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone=" + getPhone() +
            ", role='" + getRole() + "'" +
            ", isActive='" + getIsActive() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
