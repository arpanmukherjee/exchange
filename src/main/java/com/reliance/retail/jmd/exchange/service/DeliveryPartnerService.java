package com.reliance.retail.jmd.exchange.service;

import com.reliance.retail.jmd.exchange.service.dto.DeliveryPartnerDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.reliance.retail.jmd.exchange.domain.DeliveryPartner}.
 */
public interface DeliveryPartnerService {
    /**
     * Save a deliveryPartner.
     *
     * @param deliveryPartnerDTO the entity to save.
     * @return the persisted entity.
     */
    DeliveryPartnerDTO save(DeliveryPartnerDTO deliveryPartnerDTO);

    /**
     * Updates a deliveryPartner.
     *
     * @param deliveryPartnerDTO the entity to update.
     * @return the persisted entity.
     */
    DeliveryPartnerDTO update(DeliveryPartnerDTO deliveryPartnerDTO);

    /**
     * Partially updates a deliveryPartner.
     *
     * @param deliveryPartnerDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DeliveryPartnerDTO> partialUpdate(DeliveryPartnerDTO deliveryPartnerDTO);

    /**
     * Get all the deliveryPartners.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DeliveryPartnerDTO> findAll(Pageable pageable);

    /**
     * Get the "id" deliveryPartner.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DeliveryPartnerDTO> findOne(Long id);

    /**
     * Delete the "id" deliveryPartner.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
