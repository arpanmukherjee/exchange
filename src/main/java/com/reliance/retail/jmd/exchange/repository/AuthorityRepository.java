package com.reliance.retail.jmd.exchange.repository;

import com.reliance.retail.jmd.exchange.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
