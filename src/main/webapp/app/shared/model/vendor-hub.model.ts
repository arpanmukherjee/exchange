import { IAddress } from '@/shared/model/address.model';
import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';
import { IExchangePrice } from '@/shared/model/exchange-price.model';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';
import { IVendor } from '@/shared/model/vendor.model';

export interface IVendorHub {
  id?: number;
  name?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  address?: IAddress | null;
  vendorHubMappings?: IVendorHubMapping[] | null;
  exchangePrices?: IExchangePrice[] | null;
  approvedBy?: IExchangeOpsUser | null;
  vendor?: IVendor | null;
}

export class VendorHub implements IVendorHub {
  constructor(
    public id?: number,
    public name?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public address?: IAddress | null,
    public vendorHubMappings?: IVendorHubMapping[] | null,
    public exchangePrices?: IExchangePrice[] | null,
    public approvedBy?: IExchangeOpsUser | null,
    public vendor?: IVendor | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
