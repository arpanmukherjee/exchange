import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

export interface IDeliveryPartner {
  id?: number;
  name?: string;
  deliveryPartnerApiKey?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  approvedBy?: IExchangeOpsUser | null;
}

export class DeliveryPartner implements IDeliveryPartner {
  constructor(
    public id?: number,
    public name?: string,
    public deliveryPartnerApiKey?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public approvedBy?: IExchangeOpsUser | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
