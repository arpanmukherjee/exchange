import { IProductContext } from '@/shared/model/product-context.model';
import { IExchangePrice } from '@/shared/model/exchange-price.model';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';
import { IQuestion } from '@/shared/model/question.model';
import { IOption } from '@/shared/model/option.model';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

export interface IClient {
  id?: number;
  name?: string;
  clientApiKey?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  productContexts?: IProductContext[] | null;
  exchangePrices?: IExchangePrice[] | null;
  exchangeOrders?: IExchangeOrder[] | null;
  exchangeAnswerSessions?: IExchangeAnswerSession[] | null;
  questions?: IQuestion[] | null;
  options?: IOption[] | null;
  approvedBy?: IExchangeOpsUser | null;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public name?: string,
    public clientApiKey?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public productContexts?: IProductContext[] | null,
    public exchangePrices?: IExchangePrice[] | null,
    public exchangeOrders?: IExchangeOrder[] | null,
    public exchangeAnswerSessions?: IExchangeAnswerSession[] | null,
    public questions?: IQuestion[] | null,
    public options?: IOption[] | null,
    public approvedBy?: IExchangeOpsUser | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
