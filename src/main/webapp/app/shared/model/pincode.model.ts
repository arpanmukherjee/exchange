import { IAddress } from '@/shared/model/address.model';
import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';

import { State } from '@/shared/model/enumerations/state.model';
export interface IPincode {
  id?: number;
  pin?: number;
  city?: string | null;
  state?: State | null;
  country?: string | null;
  addresses?: IAddress[] | null;
  vendorHubMappings?: IVendorHubMapping[] | null;
}

export class Pincode implements IPincode {
  constructor(
    public id?: number,
    public pin?: number,
    public city?: string | null,
    public state?: State | null,
    public country?: string | null,
    public addresses?: IAddress[] | null,
    public vendorHubMappings?: IVendorHubMapping[] | null
  ) {}
}
