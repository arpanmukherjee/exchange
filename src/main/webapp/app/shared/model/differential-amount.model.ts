import { IExchangeOrder } from '@/shared/model/exchange-order.model';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import { ChargedTo } from '@/shared/model/enumerations/charged-to.model';
export interface IDifferentialAmount {
  id?: number;
  differentialAmount?: number | null;
  chargedTo?: ChargedTo | null;
  approvedAt?: Date | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  order?: IExchangeOrder | null;
  approvedBy?: IExchangeOpsUser | null;
}

export class DifferentialAmount implements IDifferentialAmount {
  constructor(
    public id?: number,
    public differentialAmount?: number | null,
    public chargedTo?: ChargedTo | null,
    public approvedAt?: Date | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public order?: IExchangeOrder | null,
    public approvedBy?: IExchangeOpsUser | null
  ) {}
}
