import { IOption } from '@/shared/model/option.model';
import { IClient } from '@/shared/model/client.model';

import { CategoryConetxtType } from '@/shared/model/enumerations/category-conetxt-type.model';
export interface IQuestion {
  id?: number;
  categoryContextType?: CategoryConetxtType | null;
  question?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  options?: IOption[] | null;
  clients?: IClient[] | null;
}

export class Question implements IQuestion {
  constructor(
    public id?: number,
    public categoryContextType?: CategoryConetxtType | null,
    public question?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public options?: IOption[] | null,
    public clients?: IClient[] | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
