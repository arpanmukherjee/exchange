export enum ExchangeOrderStatus {
  Created = 'Created',

  Picked = 'Picked',

  DeliveredToDc = 'DeliveredToDc',

  PickedFromDc = 'PickedFromDc',

  DeliveredToVendor = 'DeliveredToVendor',

  Cancelled = 'Cancelled',
}
