export enum ChargedTo {
  Customer = 'Customer',

  DeliveryPartner = 'DeliveryPartner',

  DC = 'DC',

  Vendor = 'Vendor',

  Client = 'Client',
}
