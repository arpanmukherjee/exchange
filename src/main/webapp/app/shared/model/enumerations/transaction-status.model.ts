export enum TransactionStatus {
  Blocked = 'Blocked',

  Debited = 'Debited',

  Credited = 'Credited',
}
