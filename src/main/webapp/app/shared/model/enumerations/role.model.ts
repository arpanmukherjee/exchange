export enum Role {
  SuperAdmin = 'SuperAdmin',

  ClientAdmin = 'ClientAdmin',

  ClientDcUser = 'ClientDcUser',

  ClientFinanceApprover = 'ClientFinanceApprover',

  ClientVendorOnboardingApprover = 'ClientVendorOnboardingApprover',

  VendorAdmin = 'VendorAdmin',

  VendorPickupUser = 'VendorPickupUser',

  VendorQCUser = 'VendorQCUser',

  DeliveryPartnerAdmin = 'DeliveryPartnerAdmin',

  DeliveryPartnerPickupUser = 'DeliveryPartnerPickupUser',
}
