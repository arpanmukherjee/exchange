export enum DocumentType {
  PAN = 'PAN',

  AADHAAR = 'AADHAAR',

  GST = 'GST',

  ProductImage = 'ProductImage',

  PickupQcImage = 'PickupQcImage',

  DcQcImage = 'DcQcImage',

  VendorQcImage = 'VendorQcImage',
}
