export enum AnswerSessionType {
  User = 'User',

  DeliveryPartner = 'DeliveryPartner',

  DC = 'DC',

  Vendor = 'Vendor',
}
