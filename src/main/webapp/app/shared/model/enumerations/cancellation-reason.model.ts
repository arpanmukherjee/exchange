export enum CancellationReason {
  CancelledByCustomer = 'CancelledByCustomer',

  CancelledByDeliveryPartner = 'CancelledByDeliveryPartner',

  CancelledByVendor = 'CancelledByVendor',

  CancelledByClient = 'CancelledByClient',
}
