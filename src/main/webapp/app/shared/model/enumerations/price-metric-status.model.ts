export enum PriceMetricStatus {
  Approved = 'Approved',

  Rejected = 'Rejected',

  Pending = 'Pending',
}
