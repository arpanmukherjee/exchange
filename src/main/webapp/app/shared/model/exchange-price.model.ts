import { IExchangeAnswerSet } from '@/shared/model/exchange-answer-set.model';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';
import { IClient } from '@/shared/model/client.model';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import { PriceMetricStatus } from '@/shared/model/enumerations/price-metric-status.model';
export interface IExchangePrice {
  id?: number;
  exchangeAmount?: number | null;
  status?: PriceMetricStatus | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  set?: IExchangeAnswerSet | null;
  session?: IExchangeAnswerSession | null;
  approvedBy?: IExchangeOpsUser | null;
  client?: IClient | null;
  vendorHub?: IVendorHub | null;
}

export class ExchangePrice implements IExchangePrice {
  constructor(
    public id?: number,
    public exchangeAmount?: number | null,
    public status?: PriceMetricStatus | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public set?: IExchangeAnswerSet | null,
    public session?: IExchangeAnswerSession | null,
    public approvedBy?: IExchangeOpsUser | null,
    public client?: IClient | null,
    public vendorHub?: IVendorHub | null
  ) {}
}
