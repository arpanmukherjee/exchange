import { IVendor } from '@/shared/model/vendor.model';
import { ILedgerTransaction } from '@/shared/model/ledger-transaction.model';

export interface ILedger {
  id?: number;
  balance?: number | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  vendor?: IVendor | null;
  ledgerTransactions?: ILedgerTransaction[] | null;
}

export class Ledger implements ILedger {
  constructor(
    public id?: number,
    public balance?: number | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public vendor?: IVendor | null,
    public ledgerTransactions?: ILedgerTransaction[] | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
