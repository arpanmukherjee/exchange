import { IVendorHub } from '@/shared/model/vendor-hub.model';
import { IPincode } from '@/shared/model/pincode.model';

export interface IAddress {
  id?: number;
  tag?: string | null;
  firstLine?: string | null;
  secondLine?: string | null;
  latitude?: number | null;
  longitude?: number | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  vendorHub?: IVendorHub | null;
  pincode?: IPincode | null;
}

export class Address implements IAddress {
  constructor(
    public id?: number,
    public tag?: string | null,
    public firstLine?: string | null,
    public secondLine?: string | null,
    public latitude?: number | null,
    public longitude?: number | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public vendorHub?: IVendorHub | null,
    public pincode?: IPincode | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
