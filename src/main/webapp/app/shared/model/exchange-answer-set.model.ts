import { IExchangePrice } from '@/shared/model/exchange-price.model';
import { IExchangeAnswer } from '@/shared/model/exchange-answer.model';

export interface IExchangeAnswerSet {
  id?: number;
  tag?: string | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  price?: IExchangePrice | null;
  exchangeAnswers?: IExchangeAnswer[] | null;
}

export class ExchangeAnswerSet implements IExchangeAnswerSet {
  constructor(
    public id?: number,
    public tag?: string | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public price?: IExchangePrice | null,
    public exchangeAnswers?: IExchangeAnswer[] | null
  ) {}
}
