import { IClient } from '@/shared/model/client.model';
import { IDeliveryPartner } from '@/shared/model/delivery-partner.model';
import { IVendor } from '@/shared/model/vendor.model';
import { IVendorHub } from '@/shared/model/vendor-hub.model';
import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';
import { IExchangePrice } from '@/shared/model/exchange-price.model';
import { IDifferentialAmount } from '@/shared/model/differential-amount.model';

import { Role } from '@/shared/model/enumerations/role.model';
export interface IExchangeOpsUser {
  id?: number;
  name?: string;
  email?: string | null;
  phone?: number;
  role?: Role | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  clients?: IClient[] | null;
  deliveryPartners?: IDeliveryPartner[] | null;
  vendors?: IVendor[] | null;
  vendorHubs?: IVendorHub[] | null;
  vendorHubMappings?: IVendorHubMapping[] | null;
  exchangePrices?: IExchangePrice[] | null;
  differentialAmounts?: IDifferentialAmount[] | null;
}

export class ExchangeOpsUser implements IExchangeOpsUser {
  constructor(
    public id?: number,
    public name?: string,
    public email?: string | null,
    public phone?: number,
    public role?: Role | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public clients?: IClient[] | null,
    public deliveryPartners?: IDeliveryPartner[] | null,
    public vendors?: IVendor[] | null,
    public vendorHubs?: IVendorHub[] | null,
    public vendorHubMappings?: IVendorHubMapping[] | null,
    public exchangePrices?: IExchangePrice[] | null,
    public differentialAmounts?: IDifferentialAmount[] | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
