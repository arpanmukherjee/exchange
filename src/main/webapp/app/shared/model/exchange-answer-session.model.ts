import { IExchangePrice } from '@/shared/model/exchange-price.model';
import { IExchangeAnswer } from '@/shared/model/exchange-answer.model';
import { IClient } from '@/shared/model/client.model';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import { AnswerSessionType } from '@/shared/model/enumerations/answer-session-type.model';
export interface IExchangeAnswerSession {
  id?: number;
  phoneNo?: number | null;
  userId?: string | null;
  browserSessionId?: string | null;
  sessionType?: AnswerSessionType | null;
  price?: IExchangePrice | null;
  exchangeAnswers?: IExchangeAnswer[] | null;
  client?: IClient | null;
  order?: IExchangeOrder | null;
}

export class ExchangeAnswerSession implements IExchangeAnswerSession {
  constructor(
    public id?: number,
    public phoneNo?: number | null,
    public userId?: string | null,
    public browserSessionId?: string | null,
    public sessionType?: AnswerSessionType | null,
    public price?: IExchangePrice | null,
    public exchangeAnswers?: IExchangeAnswer[] | null,
    public client?: IClient | null,
    public order?: IExchangeOrder | null
  ) {}
}
