import { IVendor } from '@/shared/model/vendor.model';
import { IProductContext } from '@/shared/model/product-context.model';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import { DocumentType } from '@/shared/model/enumerations/document-type.model';
export interface IDocument {
  id?: number;
  tag?: string | null;
  url?: string | null;
  type?: DocumentType | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  vendor?: IVendor | null;
  product?: IProductContext | null;
  order?: IExchangeOrder | null;
}

export class Document implements IDocument {
  constructor(
    public id?: number,
    public tag?: string | null,
    public url?: string | null,
    public type?: DocumentType | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public vendor?: IVendor | null,
    public product?: IProductContext | null,
    public order?: IExchangeOrder | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
