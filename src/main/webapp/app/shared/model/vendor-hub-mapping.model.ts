import { IPincode } from '@/shared/model/pincode.model';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import { CategoryConetxtType } from '@/shared/model/enumerations/category-conetxt-type.model';
export interface IVendorHubMapping {
  id?: number;
  categoryContextType?: CategoryConetxtType | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  servingPincodes?: IPincode[] | null;
  approvedBy?: IExchangeOpsUser | null;
  vendorHub?: IVendorHub | null;
}

export class VendorHubMapping implements IVendorHubMapping {
  constructor(
    public id?: number,
    public categoryContextType?: CategoryConetxtType | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public servingPincodes?: IPincode[] | null,
    public approvedBy?: IExchangeOpsUser | null,
    public vendorHub?: IVendorHub | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
