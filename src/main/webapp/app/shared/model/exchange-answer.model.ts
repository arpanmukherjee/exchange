import { IQuestion } from '@/shared/model/question.model';
import { IOption } from '@/shared/model/option.model';
import { IExchangeAnswerSet } from '@/shared/model/exchange-answer-set.model';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';

export interface IExchangeAnswer {
  id?: number;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  question?: IQuestion | null;
  option?: IOption | null;
  set?: IExchangeAnswerSet | null;
  session?: IExchangeAnswerSession | null;
}

export class ExchangeAnswer implements IExchangeAnswer {
  constructor(
    public id?: number,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public question?: IQuestion | null,
    public option?: IOption | null,
    public set?: IExchangeAnswerSet | null,
    public session?: IExchangeAnswerSession | null
  ) {}
}
