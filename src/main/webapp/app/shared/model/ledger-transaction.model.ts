import { ILedger } from '@/shared/model/ledger.model';

import { TransactionType } from '@/shared/model/enumerations/transaction-type.model';
import { TransactionStatus } from '@/shared/model/enumerations/transaction-status.model';
export interface ILedgerTransaction {
  id?: number;
  amount?: number | null;
  type?: TransactionType | null;
  reference?: string | null;
  status?: TransactionStatus | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  ledger?: ILedger | null;
}

export class LedgerTransaction implements ILedgerTransaction {
  constructor(
    public id?: number,
    public amount?: number | null,
    public type?: TransactionType | null,
    public reference?: string | null,
    public status?: TransactionStatus | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public ledger?: ILedger | null
  ) {}
}
