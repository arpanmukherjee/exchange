import { IAddress } from '@/shared/model/address.model';
import { IDocument } from '@/shared/model/document.model';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';
import { IDifferentialAmount } from '@/shared/model/differential-amount.model';
import { IClient } from '@/shared/model/client.model';
import { IProductContext } from '@/shared/model/product-context.model';

import { ExchangeOrderStatus } from '@/shared/model/enumerations/exchange-order-status.model';
import { CancellationReason } from '@/shared/model/enumerations/cancellation-reason.model';
export interface IExchangeOrder {
  id?: number;
  clientOrderRefId?: string | null;
  customerPhoneNo?: string | null;
  clientOrderDeliveryRefId?: string | null;
  deliveryPartnerPhoneNo?: string | null;
  totalOrderAmount?: number | null;
  paidAmount?: number | null;
  codInitialPendingAmount?: number | null;
  exchangeEstimateAmount?: number | null;
  exchangeFinalAmountToUser?: number | null;
  codFinalAmount?: number | null;
  codAmountPaid?: number | null;
  codPaymentTxnRefId?: string | null;
  status?: ExchangeOrderStatus | null;
  cancellationReason?: CancellationReason | null;
  expectedPickupDate?: Date | null;
  customerPickupDate?: Date | null;
  dcDeliveredDate?: Date | null;
  dcPickupDate?: Date | null;
  exchangeFinalAmountToDc?: number | null;
  vendorDeliveredDate?: Date | null;
  exchangeFinalAmountToVendor?: number | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  customerAddress?: IAddress | null;
  vendorAddress?: IAddress | null;
  dcAddress?: IAddress | null;
  images?: IDocument[] | null;
  exchangeAnswerSessions?: IExchangeAnswerSession[] | null;
  differentialAmounts?: IDifferentialAmount[] | null;
  client?: IClient | null;
  product?: IProductContext | null;
}

export class ExchangeOrder implements IExchangeOrder {
  constructor(
    public id?: number,
    public clientOrderRefId?: string | null,
    public customerPhoneNo?: string | null,
    public clientOrderDeliveryRefId?: string | null,
    public deliveryPartnerPhoneNo?: string | null,
    public totalOrderAmount?: number | null,
    public paidAmount?: number | null,
    public codInitialPendingAmount?: number | null,
    public exchangeEstimateAmount?: number | null,
    public exchangeFinalAmountToUser?: number | null,
    public codFinalAmount?: number | null,
    public codAmountPaid?: number | null,
    public codPaymentTxnRefId?: string | null,
    public status?: ExchangeOrderStatus | null,
    public cancellationReason?: CancellationReason | null,
    public expectedPickupDate?: Date | null,
    public customerPickupDate?: Date | null,
    public dcDeliveredDate?: Date | null,
    public dcPickupDate?: Date | null,
    public exchangeFinalAmountToDc?: number | null,
    public vendorDeliveredDate?: Date | null,
    public exchangeFinalAmountToVendor?: number | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public customerAddress?: IAddress | null,
    public vendorAddress?: IAddress | null,
    public dcAddress?: IAddress | null,
    public images?: IDocument[] | null,
    public exchangeAnswerSessions?: IExchangeAnswerSession[] | null,
    public differentialAmounts?: IDifferentialAmount[] | null,
    public client?: IClient | null,
    public product?: IProductContext | null
  ) {}
}
