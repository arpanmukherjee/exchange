import { IDocument } from '@/shared/model/document.model';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';
import { IClient } from '@/shared/model/client.model';

import { CategoryConetxtType } from '@/shared/model/enumerations/category-conetxt-type.model';
export interface IProductContext {
  id?: number;
  categoryContextType?: CategoryConetxtType | null;
  clientProductId?: string | null;
  clientProductName?: string | null;
  clientProductUrl?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  documents?: IDocument[] | null;
  exchangeOrders?: IExchangeOrder[] | null;
  client?: IClient | null;
}

export class ProductContext implements IProductContext {
  constructor(
    public id?: number,
    public categoryContextType?: CategoryConetxtType | null,
    public clientProductId?: string | null,
    public clientProductName?: string | null,
    public clientProductUrl?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public documents?: IDocument[] | null,
    public exchangeOrders?: IExchangeOrder[] | null,
    public client?: IClient | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
