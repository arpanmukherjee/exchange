import { IQuestion } from '@/shared/model/question.model';
import { IClient } from '@/shared/model/client.model';

export interface IOption {
  id?: number;
  option?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  question?: IQuestion | null;
  clients?: IClient[] | null;
}

export class Option implements IOption {
  constructor(
    public id?: number,
    public option?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public question?: IQuestion | null,
    public clients?: IClient[] | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
