import { IDocument } from '@/shared/model/document.model';
import { IVendorHub } from '@/shared/model/vendor-hub.model';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

export interface IVendor {
  id?: number;
  name?: string;
  panNo?: string;
  aadharNo?: string | null;
  gstNo?: string | null;
  isActive?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  createdBy?: string | null;
  updatedBy?: string | null;
  documents?: IDocument[] | null;
  vendorHubs?: IVendorHub[] | null;
  approvedBy?: IExchangeOpsUser | null;
}

export class Vendor implements IVendor {
  constructor(
    public id?: number,
    public name?: string,
    public panNo?: string,
    public aadharNo?: string | null,
    public gstNo?: string | null,
    public isActive?: boolean | null,
    public createdAt?: Date | null,
    public updatedAt?: Date | null,
    public createdBy?: string | null,
    public updatedBy?: string | null,
    public documents?: IDocument[] | null,
    public vendorHubs?: IVendorHub[] | null,
    public approvedBy?: IExchangeOpsUser | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}
