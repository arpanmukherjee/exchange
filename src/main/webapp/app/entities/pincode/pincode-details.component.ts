import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPincode } from '@/shared/model/pincode.model';
import PincodeService from './pincode.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PincodeDetails extends Vue {
  @Inject('pincodeService') private pincodeService: () => PincodeService;
  @Inject('alertService') private alertService: () => AlertService;

  public pincode: IPincode = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pincodeId) {
        vm.retrievePincode(to.params.pincodeId);
      }
    });
  }

  public retrievePincode(pincodeId) {
    this.pincodeService()
      .find(pincodeId)
      .then(res => {
        this.pincode = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
