import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import AddressService from '@/entities/address/address.service';
import { IAddress } from '@/shared/model/address.model';

import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';
import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';

import { IPincode, Pincode } from '@/shared/model/pincode.model';
import PincodeService from './pincode.service';
import { State } from '@/shared/model/enumerations/state.model';

const validations: any = {
  pincode: {
    pin: {
      required,
      numeric,
    },
    city: {},
    state: {},
    country: {},
  },
};

@Component({
  validations,
})
export default class PincodeUpdate extends Vue {
  @Inject('pincodeService') private pincodeService: () => PincodeService;
  @Inject('alertService') private alertService: () => AlertService;

  public pincode: IPincode = new Pincode();

  @Inject('addressService') private addressService: () => AddressService;

  public addresses: IAddress[] = [];

  @Inject('vendorHubMappingService') private vendorHubMappingService: () => VendorHubMappingService;

  public vendorHubMappings: IVendorHubMapping[] = [];
  public stateValues: string[] = Object.keys(State);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.pincodeId) {
        vm.retrievePincode(to.params.pincodeId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.pincode.id) {
      this.pincodeService()
        .update(this.pincode)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.pincode.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.pincodeService()
        .create(this.pincode)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.pincode.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrievePincode(pincodeId): void {
    this.pincodeService()
      .find(pincodeId)
      .then(res => {
        this.pincode = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.addressService()
      .retrieve()
      .then(res => {
        this.addresses = res.data;
      });
    this.vendorHubMappingService()
      .retrieve()
      .then(res => {
        this.vendorHubMappings = res.data;
      });
  }
}
