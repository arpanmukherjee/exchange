import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDocument } from '@/shared/model/document.model';
import DocumentService from './document.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DocumentDetails extends Vue {
  @Inject('documentService') private documentService: () => DocumentService;
  @Inject('alertService') private alertService: () => AlertService;

  public document: IDocument = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.documentId) {
        vm.retrieveDocument(to.params.documentId);
      }
    });
  }

  public retrieveDocument(documentId) {
    this.documentService()
      .find(documentId)
      .then(res => {
        this.document = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
