import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import VendorService from '@/entities/vendor/vendor.service';
import { IVendor } from '@/shared/model/vendor.model';

import ProductContextService from '@/entities/product-context/product-context.service';
import { IProductContext } from '@/shared/model/product-context.model';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import { IDocument, Document } from '@/shared/model/document.model';
import DocumentService from './document.service';
import { DocumentType } from '@/shared/model/enumerations/document-type.model';

const validations: any = {
  document: {
    tag: {},
    url: {},
    type: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class DocumentUpdate extends Vue {
  @Inject('documentService') private documentService: () => DocumentService;
  @Inject('alertService') private alertService: () => AlertService;

  public document: IDocument = new Document();

  @Inject('vendorService') private vendorService: () => VendorService;

  public vendors: IVendor[] = [];

  @Inject('productContextService') private productContextService: () => ProductContextService;

  public productContexts: IProductContext[] = [];

  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;

  public exchangeOrders: IExchangeOrder[] = [];
  public documentTypeValues: string[] = Object.keys(DocumentType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.documentId) {
        vm.retrieveDocument(to.params.documentId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.document.id) {
      this.documentService()
        .update(this.document)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.document.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.documentService()
        .create(this.document)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.document.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveDocument(documentId): void {
    this.documentService()
      .find(documentId)
      .then(res => {
        this.document = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.vendorService()
      .retrieve()
      .then(res => {
        this.vendors = res.data;
      });
    this.productContextService()
      .retrieve()
      .then(res => {
        this.productContexts = res.data;
      });
    this.exchangeOrderService()
      .retrieve()
      .then(res => {
        this.exchangeOrders = res.data;
      });
  }
}
