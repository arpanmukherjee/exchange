import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

const baseApiUrl = 'api/exchange-ops-users';

export default class ExchangeOpsUserService {
  public find(id: number): Promise<IExchangeOpsUser> {
    return new Promise<IExchangeOpsUser>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IExchangeOpsUser): Promise<IExchangeOpsUser> {
    return new Promise<IExchangeOpsUser>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IExchangeOpsUser): Promise<IExchangeOpsUser> {
    return new Promise<IExchangeOpsUser>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public partialUpdate(entity: IExchangeOpsUser): Promise<IExchangeOpsUser> {
    return new Promise<IExchangeOpsUser>((resolve, reject) => {
      axios
        .patch(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
