import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, numeric, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import DeliveryPartnerService from '@/entities/delivery-partner/delivery-partner.service';
import { IDeliveryPartner } from '@/shared/model/delivery-partner.model';

import VendorService from '@/entities/vendor/vendor.service';
import { IVendor } from '@/shared/model/vendor.model';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';
import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';
import { IExchangePrice } from '@/shared/model/exchange-price.model';

import DifferentialAmountService from '@/entities/differential-amount/differential-amount.service';
import { IDifferentialAmount } from '@/shared/model/differential-amount.model';

import { IExchangeOpsUser, ExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';
import ExchangeOpsUserService from './exchange-ops-user.service';
import { Role } from '@/shared/model/enumerations/role.model';

const validations: any = {
  exchangeOpsUser: {
    name: {
      required,
    },
    email: {},
    phone: {
      required,
      numeric,
      min: minValue(1000000000),
      max: maxValue(9999999999),
    },
    role: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class ExchangeOpsUserUpdate extends Vue {
  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeOpsUser: IExchangeOpsUser = new ExchangeOpsUser();

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];

  @Inject('deliveryPartnerService') private deliveryPartnerService: () => DeliveryPartnerService;

  public deliveryPartners: IDeliveryPartner[] = [];

  @Inject('vendorService') private vendorService: () => VendorService;

  public vendors: IVendor[] = [];

  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;

  public vendorHubs: IVendorHub[] = [];

  @Inject('vendorHubMappingService') private vendorHubMappingService: () => VendorHubMappingService;

  public vendorHubMappings: IVendorHubMapping[] = [];

  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;

  public exchangePrices: IExchangePrice[] = [];

  @Inject('differentialAmountService') private differentialAmountService: () => DifferentialAmountService;

  public differentialAmounts: IDifferentialAmount[] = [];
  public roleValues: string[] = Object.keys(Role);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeOpsUserId) {
        vm.retrieveExchangeOpsUser(to.params.exchangeOpsUserId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.exchangeOpsUser.id) {
      this.exchangeOpsUserService()
        .update(this.exchangeOpsUser)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeOpsUser.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.exchangeOpsUserService()
        .create(this.exchangeOpsUser)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeOpsUser.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveExchangeOpsUser(exchangeOpsUserId): void {
    this.exchangeOpsUserService()
      .find(exchangeOpsUserId)
      .then(res => {
        this.exchangeOpsUser = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
    this.deliveryPartnerService()
      .retrieve()
      .then(res => {
        this.deliveryPartners = res.data;
      });
    this.vendorService()
      .retrieve()
      .then(res => {
        this.vendors = res.data;
      });
    this.vendorHubService()
      .retrieve()
      .then(res => {
        this.vendorHubs = res.data;
      });
    this.vendorHubMappingService()
      .retrieve()
      .then(res => {
        this.vendorHubMappings = res.data;
      });
    this.exchangePriceService()
      .retrieve()
      .then(res => {
        this.exchangePrices = res.data;
      });
    this.differentialAmountService()
      .retrieve()
      .then(res => {
        this.differentialAmounts = res.data;
      });
  }
}
