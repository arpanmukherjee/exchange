import { Component, Vue, Inject } from 'vue-property-decorator';

import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';
import ExchangeOpsUserService from './exchange-ops-user.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ExchangeOpsUserDetails extends Vue {
  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeOpsUser: IExchangeOpsUser = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeOpsUserId) {
        vm.retrieveExchangeOpsUser(to.params.exchangeOpsUserId);
      }
    });
  }

  public retrieveExchangeOpsUser(exchangeOpsUserId) {
    this.exchangeOpsUserService()
      .find(exchangeOpsUserId)
      .then(res => {
        this.exchangeOpsUser = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
