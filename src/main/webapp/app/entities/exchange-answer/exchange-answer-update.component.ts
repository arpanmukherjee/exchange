import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import QuestionService from '@/entities/question/question.service';
import { IQuestion } from '@/shared/model/question.model';

import OptionService from '@/entities/option/option.service';
import { IOption } from '@/shared/model/option.model';

import ExchangeAnswerSetService from '@/entities/exchange-answer-set/exchange-answer-set.service';
import { IExchangeAnswerSet } from '@/shared/model/exchange-answer-set.model';

import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';

import { IExchangeAnswer, ExchangeAnswer } from '@/shared/model/exchange-answer.model';
import ExchangeAnswerService from './exchange-answer.service';

const validations: any = {
  exchangeAnswer: {
    createdAt: {},
    updatedAt: {},
  },
};

@Component({
  validations,
})
export default class ExchangeAnswerUpdate extends Vue {
  @Inject('exchangeAnswerService') private exchangeAnswerService: () => ExchangeAnswerService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeAnswer: IExchangeAnswer = new ExchangeAnswer();

  @Inject('questionService') private questionService: () => QuestionService;

  public questions: IQuestion[] = [];

  @Inject('optionService') private optionService: () => OptionService;

  public options: IOption[] = [];

  @Inject('exchangeAnswerSetService') private exchangeAnswerSetService: () => ExchangeAnswerSetService;

  public exchangeAnswerSets: IExchangeAnswerSet[] = [];

  @Inject('exchangeAnswerSessionService') private exchangeAnswerSessionService: () => ExchangeAnswerSessionService;

  public exchangeAnswerSessions: IExchangeAnswerSession[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeAnswerId) {
        vm.retrieveExchangeAnswer(to.params.exchangeAnswerId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.exchangeAnswer.id) {
      this.exchangeAnswerService()
        .update(this.exchangeAnswer)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeAnswer.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.exchangeAnswerService()
        .create(this.exchangeAnswer)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeAnswer.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveExchangeAnswer(exchangeAnswerId): void {
    this.exchangeAnswerService()
      .find(exchangeAnswerId)
      .then(res => {
        this.exchangeAnswer = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.questionService()
      .retrieve()
      .then(res => {
        this.questions = res.data;
      });
    this.optionService()
      .retrieve()
      .then(res => {
        this.options = res.data;
      });
    this.exchangeAnswerSetService()
      .retrieve()
      .then(res => {
        this.exchangeAnswerSets = res.data;
      });
    this.exchangeAnswerSessionService()
      .retrieve()
      .then(res => {
        this.exchangeAnswerSessions = res.data;
      });
  }
}
