import { Component, Vue, Inject } from 'vue-property-decorator';

import { IExchangeAnswer } from '@/shared/model/exchange-answer.model';
import ExchangeAnswerService from './exchange-answer.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ExchangeAnswerDetails extends Vue {
  @Inject('exchangeAnswerService') private exchangeAnswerService: () => ExchangeAnswerService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeAnswer: IExchangeAnswer = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeAnswerId) {
        vm.retrieveExchangeAnswer(to.params.exchangeAnswerId);
      }
    });
  }

  public retrieveExchangeAnswer(exchangeAnswerId) {
    this.exchangeAnswerService()
      .find(exchangeAnswerId)
      .then(res => {
        this.exchangeAnswer = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
