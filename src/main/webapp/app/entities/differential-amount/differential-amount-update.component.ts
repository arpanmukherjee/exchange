import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import { IDifferentialAmount, DifferentialAmount } from '@/shared/model/differential-amount.model';
import DifferentialAmountService from './differential-amount.service';
import { ChargedTo } from '@/shared/model/enumerations/charged-to.model';

const validations: any = {
  differentialAmount: {
    differentialAmount: {},
    chargedTo: {},
    approvedAt: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class DifferentialAmountUpdate extends Vue {
  @Inject('differentialAmountService') private differentialAmountService: () => DifferentialAmountService;
  @Inject('alertService') private alertService: () => AlertService;

  public differentialAmount: IDifferentialAmount = new DifferentialAmount();

  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;

  public exchangeOrders: IExchangeOrder[] = [];

  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;

  public exchangeOpsUsers: IExchangeOpsUser[] = [];
  public chargedToValues: string[] = Object.keys(ChargedTo);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.differentialAmountId) {
        vm.retrieveDifferentialAmount(to.params.differentialAmountId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.differentialAmount.id) {
      this.differentialAmountService()
        .update(this.differentialAmount)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.differentialAmount.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.differentialAmountService()
        .create(this.differentialAmount)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.differentialAmount.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveDifferentialAmount(differentialAmountId): void {
    this.differentialAmountService()
      .find(differentialAmountId)
      .then(res => {
        this.differentialAmount = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.exchangeOrderService()
      .retrieve()
      .then(res => {
        this.exchangeOrders = res.data;
      });
    this.exchangeOpsUserService()
      .retrieve()
      .then(res => {
        this.exchangeOpsUsers = res.data;
      });
  }
}
