import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDifferentialAmount } from '@/shared/model/differential-amount.model';
import DifferentialAmountService from './differential-amount.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DifferentialAmountDetails extends Vue {
  @Inject('differentialAmountService') private differentialAmountService: () => DifferentialAmountService;
  @Inject('alertService') private alertService: () => AlertService;

  public differentialAmount: IDifferentialAmount = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.differentialAmountId) {
        vm.retrieveDifferentialAmount(to.params.differentialAmountId);
      }
    });
  }

  public retrieveDifferentialAmount(differentialAmountId) {
    this.differentialAmountService()
      .find(differentialAmountId)
      .then(res => {
        this.differentialAmount = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
