import { Component, Vue, Inject } from 'vue-property-decorator';

import { IExchangeOrder } from '@/shared/model/exchange-order.model';
import ExchangeOrderService from './exchange-order.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ExchangeOrderDetails extends Vue {
  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeOrder: IExchangeOrder = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeOrderId) {
        vm.retrieveExchangeOrder(to.params.exchangeOrderId);
      }
    });
  }

  public retrieveExchangeOrder(exchangeOrderId) {
    this.exchangeOrderService()
      .find(exchangeOrderId)
      .then(res => {
        this.exchangeOrder = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
