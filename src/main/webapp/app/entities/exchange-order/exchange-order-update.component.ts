import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import AddressService from '@/entities/address/address.service';
import { IAddress } from '@/shared/model/address.model';

import DocumentService from '@/entities/document/document.service';
import { IDocument } from '@/shared/model/document.model';

import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';

import DifferentialAmountService from '@/entities/differential-amount/differential-amount.service';
import { IDifferentialAmount } from '@/shared/model/differential-amount.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import ProductContextService from '@/entities/product-context/product-context.service';
import { IProductContext } from '@/shared/model/product-context.model';

import { IExchangeOrder, ExchangeOrder } from '@/shared/model/exchange-order.model';
import ExchangeOrderService from './exchange-order.service';
import { ExchangeOrderStatus } from '@/shared/model/enumerations/exchange-order-status.model';
import { CancellationReason } from '@/shared/model/enumerations/cancellation-reason.model';

const validations: any = {
  exchangeOrder: {
    clientOrderRefId: {},
    customerPhoneNo: {},
    clientOrderDeliveryRefId: {},
    deliveryPartnerPhoneNo: {},
    totalOrderAmount: {},
    paidAmount: {},
    codInitialPendingAmount: {},
    exchangeEstimateAmount: {},
    exchangeFinalAmountToUser: {},
    codFinalAmount: {},
    codAmountPaid: {},
    codPaymentTxnRefId: {},
    status: {},
    cancellationReason: {},
    expectedPickupDate: {},
    customerPickupDate: {},
    dcDeliveredDate: {},
    dcPickupDate: {},
    exchangeFinalAmountToDc: {},
    vendorDeliveredDate: {},
    exchangeFinalAmountToVendor: {},
    createdAt: {},
    updatedAt: {},
  },
};

@Component({
  validations,
})
export default class ExchangeOrderUpdate extends Vue {
  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeOrder: IExchangeOrder = new ExchangeOrder();

  @Inject('addressService') private addressService: () => AddressService;

  public addresses: IAddress[] = [];

  @Inject('documentService') private documentService: () => DocumentService;

  public documents: IDocument[] = [];

  @Inject('exchangeAnswerSessionService') private exchangeAnswerSessionService: () => ExchangeAnswerSessionService;

  public exchangeAnswerSessions: IExchangeAnswerSession[] = [];

  @Inject('differentialAmountService') private differentialAmountService: () => DifferentialAmountService;

  public differentialAmounts: IDifferentialAmount[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];

  @Inject('productContextService') private productContextService: () => ProductContextService;

  public productContexts: IProductContext[] = [];
  public exchangeOrderStatusValues: string[] = Object.keys(ExchangeOrderStatus);
  public cancellationReasonValues: string[] = Object.keys(CancellationReason);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeOrderId) {
        vm.retrieveExchangeOrder(to.params.exchangeOrderId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.exchangeOrder.id) {
      this.exchangeOrderService()
        .update(this.exchangeOrder)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeOrder.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.exchangeOrderService()
        .create(this.exchangeOrder)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeOrder.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveExchangeOrder(exchangeOrderId): void {
    this.exchangeOrderService()
      .find(exchangeOrderId)
      .then(res => {
        this.exchangeOrder = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.addressService()
      .retrieve()
      .then(res => {
        this.addresses = res.data;
      });
    this.documentService()
      .retrieve()
      .then(res => {
        this.documents = res.data;
      });
    this.exchangeAnswerSessionService()
      .retrieve()
      .then(res => {
        this.exchangeAnswerSessions = res.data;
      });
    this.differentialAmountService()
      .retrieve()
      .then(res => {
        this.differentialAmounts = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
    this.productContextService()
      .retrieve()
      .then(res => {
        this.productContexts = res.data;
      });
  }
}
