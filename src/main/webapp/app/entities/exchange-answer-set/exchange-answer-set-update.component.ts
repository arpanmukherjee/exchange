import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';
import { IExchangePrice } from '@/shared/model/exchange-price.model';

import ExchangeAnswerService from '@/entities/exchange-answer/exchange-answer.service';
import { IExchangeAnswer } from '@/shared/model/exchange-answer.model';

import { IExchangeAnswerSet, ExchangeAnswerSet } from '@/shared/model/exchange-answer-set.model';
import ExchangeAnswerSetService from './exchange-answer-set.service';

const validations: any = {
  exchangeAnswerSet: {
    tag: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class ExchangeAnswerSetUpdate extends Vue {
  @Inject('exchangeAnswerSetService') private exchangeAnswerSetService: () => ExchangeAnswerSetService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeAnswerSet: IExchangeAnswerSet = new ExchangeAnswerSet();

  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;

  public exchangePrices: IExchangePrice[] = [];

  @Inject('exchangeAnswerService') private exchangeAnswerService: () => ExchangeAnswerService;

  public exchangeAnswers: IExchangeAnswer[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeAnswerSetId) {
        vm.retrieveExchangeAnswerSet(to.params.exchangeAnswerSetId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.exchangeAnswerSet.id) {
      this.exchangeAnswerSetService()
        .update(this.exchangeAnswerSet)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeAnswerSet.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.exchangeAnswerSetService()
        .create(this.exchangeAnswerSet)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeAnswerSet.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveExchangeAnswerSet(exchangeAnswerSetId): void {
    this.exchangeAnswerSetService()
      .find(exchangeAnswerSetId)
      .then(res => {
        this.exchangeAnswerSet = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.exchangePriceService()
      .retrieve()
      .then(res => {
        this.exchangePrices = res.data;
      });
    this.exchangeAnswerService()
      .retrieve()
      .then(res => {
        this.exchangeAnswers = res.data;
      });
  }
}
