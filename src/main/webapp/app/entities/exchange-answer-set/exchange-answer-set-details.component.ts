import { Component, Vue, Inject } from 'vue-property-decorator';

import { IExchangeAnswerSet } from '@/shared/model/exchange-answer-set.model';
import ExchangeAnswerSetService from './exchange-answer-set.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ExchangeAnswerSetDetails extends Vue {
  @Inject('exchangeAnswerSetService') private exchangeAnswerSetService: () => ExchangeAnswerSetService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeAnswerSet: IExchangeAnswerSet = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeAnswerSetId) {
        vm.retrieveExchangeAnswerSet(to.params.exchangeAnswerSetId);
      }
    });
  }

  public retrieveExchangeAnswerSet(exchangeAnswerSetId) {
    this.exchangeAnswerSetService()
      .find(exchangeAnswerSetId)
      .then(res => {
        this.exchangeAnswerSet = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
