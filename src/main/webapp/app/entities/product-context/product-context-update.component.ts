import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import DocumentService from '@/entities/document/document.service';
import { IDocument } from '@/shared/model/document.model';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import { IProductContext, ProductContext } from '@/shared/model/product-context.model';
import ProductContextService from './product-context.service';
import { CategoryConetxtType } from '@/shared/model/enumerations/category-conetxt-type.model';

const validations: any = {
  productContext: {
    categoryContextType: {},
    clientProductId: {},
    clientProductName: {},
    clientProductUrl: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class ProductContextUpdate extends Vue {
  @Inject('productContextService') private productContextService: () => ProductContextService;
  @Inject('alertService') private alertService: () => AlertService;

  public productContext: IProductContext = new ProductContext();

  @Inject('documentService') private documentService: () => DocumentService;

  public documents: IDocument[] = [];

  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;

  public exchangeOrders: IExchangeOrder[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];
  public categoryConetxtTypeValues: string[] = Object.keys(CategoryConetxtType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productContextId) {
        vm.retrieveProductContext(to.params.productContextId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.productContext.id) {
      this.productContextService()
        .update(this.productContext)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.productContext.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.productContextService()
        .create(this.productContext)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.productContext.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveProductContext(productContextId): void {
    this.productContextService()
      .find(productContextId)
      .then(res => {
        this.productContext = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.documentService()
      .retrieve()
      .then(res => {
        this.documents = res.data;
      });
    this.exchangeOrderService()
      .retrieve()
      .then(res => {
        this.exchangeOrders = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
  }
}
