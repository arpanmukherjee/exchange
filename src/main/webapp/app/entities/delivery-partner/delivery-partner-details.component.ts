import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDeliveryPartner } from '@/shared/model/delivery-partner.model';
import DeliveryPartnerService from './delivery-partner.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DeliveryPartnerDetails extends Vue {
  @Inject('deliveryPartnerService') private deliveryPartnerService: () => DeliveryPartnerService;
  @Inject('alertService') private alertService: () => AlertService;

  public deliveryPartner: IDeliveryPartner = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deliveryPartnerId) {
        vm.retrieveDeliveryPartner(to.params.deliveryPartnerId);
      }
    });
  }

  public retrieveDeliveryPartner(deliveryPartnerId) {
    this.deliveryPartnerService()
      .find(deliveryPartnerId)
      .then(res => {
        this.deliveryPartner = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
