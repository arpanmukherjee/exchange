import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import PincodeService from '@/entities/pincode/pincode.service';
import { IPincode } from '@/shared/model/pincode.model';

import { IAddress, Address } from '@/shared/model/address.model';
import AddressService from './address.service';

const validations: any = {
  address: {
    tag: {},
    firstLine: {},
    secondLine: {},
    latitude: {},
    longitude: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class AddressUpdate extends Vue {
  @Inject('addressService') private addressService: () => AddressService;
  @Inject('alertService') private alertService: () => AlertService;

  public address: IAddress = new Address();

  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;

  public vendorHubs: IVendorHub[] = [];

  @Inject('pincodeService') private pincodeService: () => PincodeService;

  public pincodes: IPincode[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.addressId) {
        vm.retrieveAddress(to.params.addressId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.address.id) {
      this.addressService()
        .update(this.address)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.address.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.addressService()
        .create(this.address)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.address.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveAddress(addressId): void {
    this.addressService()
      .find(addressId)
      .then(res => {
        this.address = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.vendorHubService()
      .retrieve()
      .then(res => {
        this.vendorHubs = res.data;
      });
    this.pincodeService()
      .retrieve()
      .then(res => {
        this.pincodes = res.data;
      });
  }
}
