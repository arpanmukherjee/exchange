import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import AddressService from '@/entities/address/address.service';
import { IAddress } from '@/shared/model/address.model';

import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';
import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';
import { IExchangePrice } from '@/shared/model/exchange-price.model';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import VendorService from '@/entities/vendor/vendor.service';
import { IVendor } from '@/shared/model/vendor.model';

import { IVendorHub, VendorHub } from '@/shared/model/vendor-hub.model';
import VendorHubService from './vendor-hub.service';

const validations: any = {
  vendorHub: {
    name: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class VendorHubUpdate extends Vue {
  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;
  @Inject('alertService') private alertService: () => AlertService;

  public vendorHub: IVendorHub = new VendorHub();

  @Inject('addressService') private addressService: () => AddressService;

  public addresses: IAddress[] = [];

  @Inject('vendorHubMappingService') private vendorHubMappingService: () => VendorHubMappingService;

  public vendorHubMappings: IVendorHubMapping[] = [];

  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;

  public exchangePrices: IExchangePrice[] = [];

  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;

  public exchangeOpsUsers: IExchangeOpsUser[] = [];

  @Inject('vendorService') private vendorService: () => VendorService;

  public vendors: IVendor[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vendorHubId) {
        vm.retrieveVendorHub(to.params.vendorHubId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.vendorHub.id) {
      this.vendorHubService()
        .update(this.vendorHub)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.vendorHub.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.vendorHubService()
        .create(this.vendorHub)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.vendorHub.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveVendorHub(vendorHubId): void {
    this.vendorHubService()
      .find(vendorHubId)
      .then(res => {
        this.vendorHub = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.addressService()
      .retrieve()
      .then(res => {
        this.addresses = res.data;
      });
    this.vendorHubMappingService()
      .retrieve()
      .then(res => {
        this.vendorHubMappings = res.data;
      });
    this.exchangePriceService()
      .retrieve()
      .then(res => {
        this.exchangePrices = res.data;
      });
    this.exchangeOpsUserService()
      .retrieve()
      .then(res => {
        this.exchangeOpsUsers = res.data;
      });
    this.vendorService()
      .retrieve()
      .then(res => {
        this.vendors = res.data;
      });
  }
}
