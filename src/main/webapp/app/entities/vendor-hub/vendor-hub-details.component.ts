import { Component, Vue, Inject } from 'vue-property-decorator';

import { IVendorHub } from '@/shared/model/vendor-hub.model';
import VendorHubService from './vendor-hub.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class VendorHubDetails extends Vue {
  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;
  @Inject('alertService') private alertService: () => AlertService;

  public vendorHub: IVendorHub = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vendorHubId) {
        vm.retrieveVendorHub(to.params.vendorHubId);
      }
    });
  }

  public retrieveVendorHub(vendorHubId) {
    this.vendorHubService()
      .find(vendorHubId)
      .then(res => {
        this.vendorHub = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
