import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import PincodeService from '@/entities/pincode/pincode.service';
import { IPincode } from '@/shared/model/pincode.model';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import { IVendorHubMapping, VendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';
import VendorHubMappingService from './vendor-hub-mapping.service';
import { CategoryConetxtType } from '@/shared/model/enumerations/category-conetxt-type.model';

const validations: any = {
  vendorHubMapping: {
    categoryContextType: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class VendorHubMappingUpdate extends Vue {
  @Inject('vendorHubMappingService') private vendorHubMappingService: () => VendorHubMappingService;
  @Inject('alertService') private alertService: () => AlertService;

  public vendorHubMapping: IVendorHubMapping = new VendorHubMapping();

  @Inject('pincodeService') private pincodeService: () => PincodeService;

  public pincodes: IPincode[] = [];

  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;

  public exchangeOpsUsers: IExchangeOpsUser[] = [];

  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;

  public vendorHubs: IVendorHub[] = [];
  public categoryConetxtTypeValues: string[] = Object.keys(CategoryConetxtType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vendorHubMappingId) {
        vm.retrieveVendorHubMapping(to.params.vendorHubMappingId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.vendorHubMapping.servingPincodes = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.vendorHubMapping.id) {
      this.vendorHubMappingService()
        .update(this.vendorHubMapping)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.vendorHubMapping.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.vendorHubMappingService()
        .create(this.vendorHubMapping)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.vendorHubMapping.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveVendorHubMapping(vendorHubMappingId): void {
    this.vendorHubMappingService()
      .find(vendorHubMappingId)
      .then(res => {
        this.vendorHubMapping = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.pincodeService()
      .retrieve()
      .then(res => {
        this.pincodes = res.data;
      });
    this.exchangeOpsUserService()
      .retrieve()
      .then(res => {
        this.exchangeOpsUsers = res.data;
      });
    this.vendorHubService()
      .retrieve()
      .then(res => {
        this.vendorHubs = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      return selectedVals.find(value => option.id === value.id) ?? option;
    }
    return option;
  }
}
