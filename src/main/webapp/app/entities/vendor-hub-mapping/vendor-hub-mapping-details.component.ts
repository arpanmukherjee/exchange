import { Component, Vue, Inject } from 'vue-property-decorator';

import { IVendorHubMapping } from '@/shared/model/vendor-hub-mapping.model';
import VendorHubMappingService from './vendor-hub-mapping.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class VendorHubMappingDetails extends Vue {
  @Inject('vendorHubMappingService') private vendorHubMappingService: () => VendorHubMappingService;
  @Inject('alertService') private alertService: () => AlertService;

  public vendorHubMapping: IVendorHubMapping = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vendorHubMappingId) {
        vm.retrieveVendorHubMapping(to.params.vendorHubMappingId);
      }
    });
  }

  public retrieveVendorHubMapping(vendorHubMappingId) {
    this.vendorHubMappingService()
      .find(vendorHubMappingId)
      .then(res => {
        this.vendorHubMapping = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
