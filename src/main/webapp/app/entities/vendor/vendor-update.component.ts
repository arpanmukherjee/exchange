import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import DocumentService from '@/entities/document/document.service';
import { IDocument } from '@/shared/model/document.model';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import { IVendor, Vendor } from '@/shared/model/vendor.model';
import VendorService from './vendor.service';

const validations: any = {
  vendor: {
    name: {
      required,
    },
    panNo: {
      required,
    },
    aadharNo: {},
    gstNo: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class VendorUpdate extends Vue {
  @Inject('vendorService') private vendorService: () => VendorService;
  @Inject('alertService') private alertService: () => AlertService;

  public vendor: IVendor = new Vendor();

  @Inject('documentService') private documentService: () => DocumentService;

  public documents: IDocument[] = [];

  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;

  public vendorHubs: IVendorHub[] = [];

  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;

  public exchangeOpsUsers: IExchangeOpsUser[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.vendorId) {
        vm.retrieveVendor(to.params.vendorId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.vendor.id) {
      this.vendorService()
        .update(this.vendor)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.vendor.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.vendorService()
        .create(this.vendor)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.vendor.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveVendor(vendorId): void {
    this.vendorService()
      .find(vendorId)
      .then(res => {
        this.vendor = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.documentService()
      .retrieve()
      .then(res => {
        this.documents = res.data;
      });
    this.vendorHubService()
      .retrieve()
      .then(res => {
        this.vendorHubs = res.data;
      });
    this.exchangeOpsUserService()
      .retrieve()
      .then(res => {
        this.exchangeOpsUsers = res.data;
      });
  }
}
