import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';
import { IExchangePrice } from '@/shared/model/exchange-price.model';

import ExchangeAnswerService from '@/entities/exchange-answer/exchange-answer.service';
import { IExchangeAnswer } from '@/shared/model/exchange-answer.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import { IExchangeAnswerSession, ExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';
import ExchangeAnswerSessionService from './exchange-answer-session.service';
import { AnswerSessionType } from '@/shared/model/enumerations/answer-session-type.model';

const validations: any = {
  exchangeAnswerSession: {
    phoneNo: {},
    userId: {},
    browserSessionId: {},
    sessionType: {},
  },
};

@Component({
  validations,
})
export default class ExchangeAnswerSessionUpdate extends Vue {
  @Inject('exchangeAnswerSessionService') private exchangeAnswerSessionService: () => ExchangeAnswerSessionService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeAnswerSession: IExchangeAnswerSession = new ExchangeAnswerSession();

  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;

  public exchangePrices: IExchangePrice[] = [];

  @Inject('exchangeAnswerService') private exchangeAnswerService: () => ExchangeAnswerService;

  public exchangeAnswers: IExchangeAnswer[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];

  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;

  public exchangeOrders: IExchangeOrder[] = [];
  public answerSessionTypeValues: string[] = Object.keys(AnswerSessionType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeAnswerSessionId) {
        vm.retrieveExchangeAnswerSession(to.params.exchangeAnswerSessionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.exchangeAnswerSession.id) {
      this.exchangeAnswerSessionService()
        .update(this.exchangeAnswerSession)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeAnswerSession.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.exchangeAnswerSessionService()
        .create(this.exchangeAnswerSession)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangeAnswerSession.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveExchangeAnswerSession(exchangeAnswerSessionId): void {
    this.exchangeAnswerSessionService()
      .find(exchangeAnswerSessionId)
      .then(res => {
        this.exchangeAnswerSession = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.exchangePriceService()
      .retrieve()
      .then(res => {
        this.exchangePrices = res.data;
      });
    this.exchangeAnswerService()
      .retrieve()
      .then(res => {
        this.exchangeAnswers = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
    this.exchangeOrderService()
      .retrieve()
      .then(res => {
        this.exchangeOrders = res.data;
      });
  }
}
