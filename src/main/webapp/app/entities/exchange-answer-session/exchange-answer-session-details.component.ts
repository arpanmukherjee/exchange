import { Component, Vue, Inject } from 'vue-property-decorator';

import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';
import ExchangeAnswerSessionService from './exchange-answer-session.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ExchangeAnswerSessionDetails extends Vue {
  @Inject('exchangeAnswerSessionService') private exchangeAnswerSessionService: () => ExchangeAnswerSessionService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangeAnswerSession: IExchangeAnswerSession = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangeAnswerSessionId) {
        vm.retrieveExchangeAnswerSession(to.params.exchangeAnswerSessionId);
      }
    });
  }

  public retrieveExchangeAnswerSession(exchangeAnswerSessionId) {
    this.exchangeAnswerSessionService()
      .find(exchangeAnswerSessionId)
      .then(res => {
        this.exchangeAnswerSession = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
