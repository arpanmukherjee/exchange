import { Component, Vue, Inject } from 'vue-property-decorator';

import { IExchangePrice } from '@/shared/model/exchange-price.model';
import ExchangePriceService from './exchange-price.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ExchangePriceDetails extends Vue {
  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangePrice: IExchangePrice = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangePriceId) {
        vm.retrieveExchangePrice(to.params.exchangePriceId);
      }
    });
  }

  public retrieveExchangePrice(exchangePriceId) {
    this.exchangePriceService()
      .find(exchangePriceId)
      .then(res => {
        this.exchangePrice = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
