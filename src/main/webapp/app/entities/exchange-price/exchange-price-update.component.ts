import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import ExchangeAnswerSetService from '@/entities/exchange-answer-set/exchange-answer-set.service';
import { IExchangeAnswerSet } from '@/shared/model/exchange-answer-set.model';

import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import { IVendorHub } from '@/shared/model/vendor-hub.model';

import { IExchangePrice, ExchangePrice } from '@/shared/model/exchange-price.model';
import ExchangePriceService from './exchange-price.service';
import { PriceMetricStatus } from '@/shared/model/enumerations/price-metric-status.model';

const validations: any = {
  exchangePrice: {
    exchangeAmount: {},
    status: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class ExchangePriceUpdate extends Vue {
  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;
  @Inject('alertService') private alertService: () => AlertService;

  public exchangePrice: IExchangePrice = new ExchangePrice();

  @Inject('exchangeAnswerSetService') private exchangeAnswerSetService: () => ExchangeAnswerSetService;

  public exchangeAnswerSets: IExchangeAnswerSet[] = [];

  @Inject('exchangeAnswerSessionService') private exchangeAnswerSessionService: () => ExchangeAnswerSessionService;

  public exchangeAnswerSessions: IExchangeAnswerSession[] = [];

  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;

  public exchangeOpsUsers: IExchangeOpsUser[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];

  @Inject('vendorHubService') private vendorHubService: () => VendorHubService;

  public vendorHubs: IVendorHub[] = [];
  public priceMetricStatusValues: string[] = Object.keys(PriceMetricStatus);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.exchangePriceId) {
        vm.retrieveExchangePrice(to.params.exchangePriceId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.exchangePrice.id) {
      this.exchangePriceService()
        .update(this.exchangePrice)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangePrice.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.exchangePriceService()
        .create(this.exchangePrice)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.exchangePrice.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveExchangePrice(exchangePriceId): void {
    this.exchangePriceService()
      .find(exchangePriceId)
      .then(res => {
        this.exchangePrice = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.exchangeAnswerSetService()
      .retrieve()
      .then(res => {
        this.exchangeAnswerSets = res.data;
      });
    this.exchangeAnswerSessionService()
      .retrieve()
      .then(res => {
        this.exchangeAnswerSessions = res.data;
      });
    this.exchangeOpsUserService()
      .retrieve()
      .then(res => {
        this.exchangeOpsUsers = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
    this.vendorHubService()
      .retrieve()
      .then(res => {
        this.vendorHubs = res.data;
      });
  }
}
