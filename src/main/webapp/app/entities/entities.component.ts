import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import ExchangeOpsUserService from './exchange-ops-user/exchange-ops-user.service';
import ClientService from './client/client.service';
import DeliveryPartnerService from './delivery-partner/delivery-partner.service';
import ProductContextService from './product-context/product-context.service';
import VendorService from './vendor/vendor.service';
import DocumentService from './document/document.service';
import PincodeService from './pincode/pincode.service';
import AddressService from './address/address.service';
import VendorHubService from './vendor-hub/vendor-hub.service';
import VendorHubMappingService from './vendor-hub-mapping/vendor-hub-mapping.service';
import QuestionService from './question/question.service';
import OptionService from './option/option.service';
import ExchangeAnswerService from './exchange-answer/exchange-answer.service';
import ExchangeAnswerSetService from './exchange-answer-set/exchange-answer-set.service';
import ExchangePriceService from './exchange-price/exchange-price.service';
import LedgerService from './ledger/ledger.service';
import LedgerTransactionService from './ledger-transaction/ledger-transaction.service';
import ExchangeOrderService from './exchange-order/exchange-order.service';
import ExchangeAnswerSessionService from './exchange-answer-session/exchange-answer-session.service';
import DifferentialAmountService from './differential-amount/differential-amount.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('exchangeOpsUserService') private exchangeOpsUserService = () => new ExchangeOpsUserService();
  @Provide('clientService') private clientService = () => new ClientService();
  @Provide('deliveryPartnerService') private deliveryPartnerService = () => new DeliveryPartnerService();
  @Provide('productContextService') private productContextService = () => new ProductContextService();
  @Provide('vendorService') private vendorService = () => new VendorService();
  @Provide('documentService') private documentService = () => new DocumentService();
  @Provide('pincodeService') private pincodeService = () => new PincodeService();
  @Provide('addressService') private addressService = () => new AddressService();
  @Provide('vendorHubService') private vendorHubService = () => new VendorHubService();
  @Provide('vendorHubMappingService') private vendorHubMappingService = () => new VendorHubMappingService();
  @Provide('questionService') private questionService = () => new QuestionService();
  @Provide('optionService') private optionService = () => new OptionService();
  @Provide('exchangeAnswerService') private exchangeAnswerService = () => new ExchangeAnswerService();
  @Provide('exchangeAnswerSetService') private exchangeAnswerSetService = () => new ExchangeAnswerSetService();
  @Provide('exchangePriceService') private exchangePriceService = () => new ExchangePriceService();
  @Provide('ledgerService') private ledgerService = () => new LedgerService();
  @Provide('ledgerTransactionService') private ledgerTransactionService = () => new LedgerTransactionService();
  @Provide('exchangeOrderService') private exchangeOrderService = () => new ExchangeOrderService();
  @Provide('exchangeAnswerSessionService') private exchangeAnswerSessionService = () => new ExchangeAnswerSessionService();
  @Provide('differentialAmountService') private differentialAmountService = () => new DifferentialAmountService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
