import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import VendorService from '@/entities/vendor/vendor.service';
import { IVendor } from '@/shared/model/vendor.model';

import LedgerTransactionService from '@/entities/ledger-transaction/ledger-transaction.service';
import { ILedgerTransaction } from '@/shared/model/ledger-transaction.model';

import { ILedger, Ledger } from '@/shared/model/ledger.model';
import LedgerService from './ledger.service';

const validations: any = {
  ledger: {
    balance: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class LedgerUpdate extends Vue {
  @Inject('ledgerService') private ledgerService: () => LedgerService;
  @Inject('alertService') private alertService: () => AlertService;

  public ledger: ILedger = new Ledger();

  @Inject('vendorService') private vendorService: () => VendorService;

  public vendors: IVendor[] = [];

  @Inject('ledgerTransactionService') private ledgerTransactionService: () => LedgerTransactionService;

  public ledgerTransactions: ILedgerTransaction[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.ledgerId) {
        vm.retrieveLedger(to.params.ledgerId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.ledger.id) {
      this.ledgerService()
        .update(this.ledger)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.ledger.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.ledgerService()
        .create(this.ledger)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.ledger.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveLedger(ledgerId): void {
    this.ledgerService()
      .find(ledgerId)
      .then(res => {
        this.ledger = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.vendorService()
      .retrieve()
      .then(res => {
        this.vendors = res.data;
      });
    this.ledgerTransactionService()
      .retrieve()
      .then(res => {
        this.ledgerTransactions = res.data;
      });
  }
}
