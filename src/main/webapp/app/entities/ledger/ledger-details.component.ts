import { Component, Vue, Inject } from 'vue-property-decorator';

import { ILedger } from '@/shared/model/ledger.model';
import LedgerService from './ledger.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class LedgerDetails extends Vue {
  @Inject('ledgerService') private ledgerService: () => LedgerService;
  @Inject('alertService') private alertService: () => AlertService;

  public ledger: ILedger = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.ledgerId) {
        vm.retrieveLedger(to.params.ledgerId);
      }
    });
  }

  public retrieveLedger(ledgerId) {
    this.ledgerService()
      .find(ledgerId)
      .then(res => {
        this.ledger = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
