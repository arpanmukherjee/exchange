import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import ProductContextService from '@/entities/product-context/product-context.service';
import { IProductContext } from '@/shared/model/product-context.model';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';
import { IExchangePrice } from '@/shared/model/exchange-price.model';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import { IExchangeOrder } from '@/shared/model/exchange-order.model';

import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';
import { IExchangeAnswerSession } from '@/shared/model/exchange-answer-session.model';

import QuestionService from '@/entities/question/question.service';
import { IQuestion } from '@/shared/model/question.model';

import OptionService from '@/entities/option/option.service';
import { IOption } from '@/shared/model/option.model';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import { IExchangeOpsUser } from '@/shared/model/exchange-ops-user.model';

import { IClient, Client } from '@/shared/model/client.model';
import ClientService from './client.service';

const validations: any = {
  client: {
    name: {
      required,
    },
    clientApiKey: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class ClientUpdate extends Vue {
  @Inject('clientService') private clientService: () => ClientService;
  @Inject('alertService') private alertService: () => AlertService;

  public client: IClient = new Client();

  @Inject('productContextService') private productContextService: () => ProductContextService;

  public productContexts: IProductContext[] = [];

  @Inject('exchangePriceService') private exchangePriceService: () => ExchangePriceService;

  public exchangePrices: IExchangePrice[] = [];

  @Inject('exchangeOrderService') private exchangeOrderService: () => ExchangeOrderService;

  public exchangeOrders: IExchangeOrder[] = [];

  @Inject('exchangeAnswerSessionService') private exchangeAnswerSessionService: () => ExchangeAnswerSessionService;

  public exchangeAnswerSessions: IExchangeAnswerSession[] = [];

  @Inject('questionService') private questionService: () => QuestionService;

  public questions: IQuestion[] = [];

  @Inject('optionService') private optionService: () => OptionService;

  public options: IOption[] = [];

  @Inject('exchangeOpsUserService') private exchangeOpsUserService: () => ExchangeOpsUserService;

  public exchangeOpsUsers: IExchangeOpsUser[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.clientId) {
        vm.retrieveClient(to.params.clientId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.client.questions = [];
    this.client.options = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.client.id) {
      this.clientService()
        .update(this.client)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.client.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.clientService()
        .create(this.client)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.client.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveClient(clientId): void {
    this.clientService()
      .find(clientId)
      .then(res => {
        this.client = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productContextService()
      .retrieve()
      .then(res => {
        this.productContexts = res.data;
      });
    this.exchangePriceService()
      .retrieve()
      .then(res => {
        this.exchangePrices = res.data;
      });
    this.exchangeOrderService()
      .retrieve()
      .then(res => {
        this.exchangeOrders = res.data;
      });
    this.exchangeAnswerSessionService()
      .retrieve()
      .then(res => {
        this.exchangeAnswerSessions = res.data;
      });
    this.questionService()
      .retrieve()
      .then(res => {
        this.questions = res.data;
      });
    this.optionService()
      .retrieve()
      .then(res => {
        this.options = res.data;
      });
    this.exchangeOpsUserService()
      .retrieve()
      .then(res => {
        this.exchangeOpsUsers = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      return selectedVals.find(value => option.id === value.id) ?? option;
    }
    return option;
  }
}
