import { Component, Vue, Inject } from 'vue-property-decorator';

import { ILedgerTransaction } from '@/shared/model/ledger-transaction.model';
import LedgerTransactionService from './ledger-transaction.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class LedgerTransactionDetails extends Vue {
  @Inject('ledgerTransactionService') private ledgerTransactionService: () => LedgerTransactionService;
  @Inject('alertService') private alertService: () => AlertService;

  public ledgerTransaction: ILedgerTransaction = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.ledgerTransactionId) {
        vm.retrieveLedgerTransaction(to.params.ledgerTransactionId);
      }
    });
  }

  public retrieveLedgerTransaction(ledgerTransactionId) {
    this.ledgerTransactionService()
      .find(ledgerTransactionId)
      .then(res => {
        this.ledgerTransaction = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
