import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import LedgerService from '@/entities/ledger/ledger.service';
import { ILedger } from '@/shared/model/ledger.model';

import { ILedgerTransaction, LedgerTransaction } from '@/shared/model/ledger-transaction.model';
import LedgerTransactionService from './ledger-transaction.service';
import { TransactionType } from '@/shared/model/enumerations/transaction-type.model';
import { TransactionStatus } from '@/shared/model/enumerations/transaction-status.model';

const validations: any = {
  ledgerTransaction: {
    amount: {},
    type: {},
    reference: {},
    status: {},
    createdAt: {},
    updatedAt: {},
  },
};

@Component({
  validations,
})
export default class LedgerTransactionUpdate extends Vue {
  @Inject('ledgerTransactionService') private ledgerTransactionService: () => LedgerTransactionService;
  @Inject('alertService') private alertService: () => AlertService;

  public ledgerTransaction: ILedgerTransaction = new LedgerTransaction();

  @Inject('ledgerService') private ledgerService: () => LedgerService;

  public ledgers: ILedger[] = [];
  public transactionTypeValues: string[] = Object.keys(TransactionType);
  public transactionStatusValues: string[] = Object.keys(TransactionStatus);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.ledgerTransactionId) {
        vm.retrieveLedgerTransaction(to.params.ledgerTransactionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.ledgerTransaction.id) {
      this.ledgerTransactionService()
        .update(this.ledgerTransaction)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.ledgerTransaction.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.ledgerTransactionService()
        .create(this.ledgerTransaction)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.ledgerTransaction.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveLedgerTransaction(ledgerTransactionId): void {
    this.ledgerTransactionService()
      .find(ledgerTransactionId)
      .then(res => {
        this.ledgerTransaction = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.ledgerService()
      .retrieve()
      .then(res => {
        this.ledgers = res.data;
      });
  }
}
