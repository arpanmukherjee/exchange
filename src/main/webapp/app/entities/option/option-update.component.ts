import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import QuestionService from '@/entities/question/question.service';
import { IQuestion } from '@/shared/model/question.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import { IOption, Option } from '@/shared/model/option.model';
import OptionService from './option.service';

const validations: any = {
  option: {
    option: {},
    isActive: {},
    createdAt: {},
    updatedAt: {},
    createdBy: {},
    updatedBy: {},
  },
};

@Component({
  validations,
})
export default class OptionUpdate extends Vue {
  @Inject('optionService') private optionService: () => OptionService;
  @Inject('alertService') private alertService: () => AlertService;

  public option: IOption = new Option();

  @Inject('questionService') private questionService: () => QuestionService;

  public questions: IQuestion[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.optionId) {
        vm.retrieveOption(to.params.optionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.option.id) {
      this.optionService()
        .update(this.option)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.option.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.optionService()
        .create(this.option)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('exchangeApp.option.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveOption(optionId): void {
    this.optionService()
      .find(optionId)
      .then(res => {
        this.option = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.questionService()
      .retrieve()
      .then(res => {
        this.questions = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
  }
}
