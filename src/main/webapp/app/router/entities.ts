import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

const ExchangeOpsUser = () => import('@/entities/exchange-ops-user/exchange-ops-user.vue');
const ExchangeOpsUserUpdate = () => import('@/entities/exchange-ops-user/exchange-ops-user-update.vue');
const ExchangeOpsUserDetails = () => import('@/entities/exchange-ops-user/exchange-ops-user-details.vue');

const Client = () => import('@/entities/client/client.vue');
const ClientUpdate = () => import('@/entities/client/client-update.vue');
const ClientDetails = () => import('@/entities/client/client-details.vue');

const DeliveryPartner = () => import('@/entities/delivery-partner/delivery-partner.vue');
const DeliveryPartnerUpdate = () => import('@/entities/delivery-partner/delivery-partner-update.vue');
const DeliveryPartnerDetails = () => import('@/entities/delivery-partner/delivery-partner-details.vue');

const ProductContext = () => import('@/entities/product-context/product-context.vue');
const ProductContextUpdate = () => import('@/entities/product-context/product-context-update.vue');
const ProductContextDetails = () => import('@/entities/product-context/product-context-details.vue');

const Vendor = () => import('@/entities/vendor/vendor.vue');
const VendorUpdate = () => import('@/entities/vendor/vendor-update.vue');
const VendorDetails = () => import('@/entities/vendor/vendor-details.vue');

const Document = () => import('@/entities/document/document.vue');
const DocumentUpdate = () => import('@/entities/document/document-update.vue');
const DocumentDetails = () => import('@/entities/document/document-details.vue');

const Pincode = () => import('@/entities/pincode/pincode.vue');
const PincodeUpdate = () => import('@/entities/pincode/pincode-update.vue');
const PincodeDetails = () => import('@/entities/pincode/pincode-details.vue');

const Address = () => import('@/entities/address/address.vue');
const AddressUpdate = () => import('@/entities/address/address-update.vue');
const AddressDetails = () => import('@/entities/address/address-details.vue');

const VendorHub = () => import('@/entities/vendor-hub/vendor-hub.vue');
const VendorHubUpdate = () => import('@/entities/vendor-hub/vendor-hub-update.vue');
const VendorHubDetails = () => import('@/entities/vendor-hub/vendor-hub-details.vue');

const VendorHubMapping = () => import('@/entities/vendor-hub-mapping/vendor-hub-mapping.vue');
const VendorHubMappingUpdate = () => import('@/entities/vendor-hub-mapping/vendor-hub-mapping-update.vue');
const VendorHubMappingDetails = () => import('@/entities/vendor-hub-mapping/vendor-hub-mapping-details.vue');

const Question = () => import('@/entities/question/question.vue');
const QuestionUpdate = () => import('@/entities/question/question-update.vue');
const QuestionDetails = () => import('@/entities/question/question-details.vue');

const Option = () => import('@/entities/option/option.vue');
const OptionUpdate = () => import('@/entities/option/option-update.vue');
const OptionDetails = () => import('@/entities/option/option-details.vue');

const ExchangeAnswer = () => import('@/entities/exchange-answer/exchange-answer.vue');
const ExchangeAnswerUpdate = () => import('@/entities/exchange-answer/exchange-answer-update.vue');
const ExchangeAnswerDetails = () => import('@/entities/exchange-answer/exchange-answer-details.vue');

const ExchangeAnswerSet = () => import('@/entities/exchange-answer-set/exchange-answer-set.vue');
const ExchangeAnswerSetUpdate = () => import('@/entities/exchange-answer-set/exchange-answer-set-update.vue');
const ExchangeAnswerSetDetails = () => import('@/entities/exchange-answer-set/exchange-answer-set-details.vue');

const ExchangePrice = () => import('@/entities/exchange-price/exchange-price.vue');
const ExchangePriceUpdate = () => import('@/entities/exchange-price/exchange-price-update.vue');
const ExchangePriceDetails = () => import('@/entities/exchange-price/exchange-price-details.vue');

const Ledger = () => import('@/entities/ledger/ledger.vue');
const LedgerUpdate = () => import('@/entities/ledger/ledger-update.vue');
const LedgerDetails = () => import('@/entities/ledger/ledger-details.vue');

const LedgerTransaction = () => import('@/entities/ledger-transaction/ledger-transaction.vue');
const LedgerTransactionUpdate = () => import('@/entities/ledger-transaction/ledger-transaction-update.vue');
const LedgerTransactionDetails = () => import('@/entities/ledger-transaction/ledger-transaction-details.vue');

const ExchangeOrder = () => import('@/entities/exchange-order/exchange-order.vue');
const ExchangeOrderUpdate = () => import('@/entities/exchange-order/exchange-order-update.vue');
const ExchangeOrderDetails = () => import('@/entities/exchange-order/exchange-order-details.vue');

const ExchangeAnswerSession = () => import('@/entities/exchange-answer-session/exchange-answer-session.vue');
const ExchangeAnswerSessionUpdate = () => import('@/entities/exchange-answer-session/exchange-answer-session-update.vue');
const ExchangeAnswerSessionDetails = () => import('@/entities/exchange-answer-session/exchange-answer-session-details.vue');

const DifferentialAmount = () => import('@/entities/differential-amount/differential-amount.vue');
const DifferentialAmountUpdate = () => import('@/entities/differential-amount/differential-amount-update.vue');
const DifferentialAmountDetails = () => import('@/entities/differential-amount/differential-amount-details.vue');

// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'exchange-ops-user',
      name: 'ExchangeOpsUser',
      component: ExchangeOpsUser,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-ops-user/new',
      name: 'ExchangeOpsUserCreate',
      component: ExchangeOpsUserUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-ops-user/:exchangeOpsUserId/edit',
      name: 'ExchangeOpsUserEdit',
      component: ExchangeOpsUserUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-ops-user/:exchangeOpsUserId/view',
      name: 'ExchangeOpsUserView',
      component: ExchangeOpsUserDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client',
      name: 'Client',
      component: Client,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/new',
      name: 'ClientCreate',
      component: ClientUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/:clientId/edit',
      name: 'ClientEdit',
      component: ClientUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/:clientId/view',
      name: 'ClientView',
      component: ClientDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-partner',
      name: 'DeliveryPartner',
      component: DeliveryPartner,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-partner/new',
      name: 'DeliveryPartnerCreate',
      component: DeliveryPartnerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-partner/:deliveryPartnerId/edit',
      name: 'DeliveryPartnerEdit',
      component: DeliveryPartnerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-partner/:deliveryPartnerId/view',
      name: 'DeliveryPartnerView',
      component: DeliveryPartnerDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-context',
      name: 'ProductContext',
      component: ProductContext,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-context/new',
      name: 'ProductContextCreate',
      component: ProductContextUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-context/:productContextId/edit',
      name: 'ProductContextEdit',
      component: ProductContextUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-context/:productContextId/view',
      name: 'ProductContextView',
      component: ProductContextDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor',
      name: 'Vendor',
      component: Vendor,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor/new',
      name: 'VendorCreate',
      component: VendorUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor/:vendorId/edit',
      name: 'VendorEdit',
      component: VendorUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor/:vendorId/view',
      name: 'VendorView',
      component: VendorDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'document',
      name: 'Document',
      component: Document,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'document/new',
      name: 'DocumentCreate',
      component: DocumentUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'document/:documentId/edit',
      name: 'DocumentEdit',
      component: DocumentUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'document/:documentId/view',
      name: 'DocumentView',
      component: DocumentDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'pincode',
      name: 'Pincode',
      component: Pincode,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'pincode/new',
      name: 'PincodeCreate',
      component: PincodeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'pincode/:pincodeId/edit',
      name: 'PincodeEdit',
      component: PincodeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'pincode/:pincodeId/view',
      name: 'PincodeView',
      component: PincodeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'address',
      name: 'Address',
      component: Address,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'address/new',
      name: 'AddressCreate',
      component: AddressUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'address/:addressId/edit',
      name: 'AddressEdit',
      component: AddressUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'address/:addressId/view',
      name: 'AddressView',
      component: AddressDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub',
      name: 'VendorHub',
      component: VendorHub,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub/new',
      name: 'VendorHubCreate',
      component: VendorHubUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub/:vendorHubId/edit',
      name: 'VendorHubEdit',
      component: VendorHubUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub/:vendorHubId/view',
      name: 'VendorHubView',
      component: VendorHubDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub-mapping',
      name: 'VendorHubMapping',
      component: VendorHubMapping,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub-mapping/new',
      name: 'VendorHubMappingCreate',
      component: VendorHubMappingUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub-mapping/:vendorHubMappingId/edit',
      name: 'VendorHubMappingEdit',
      component: VendorHubMappingUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'vendor-hub-mapping/:vendorHubMappingId/view',
      name: 'VendorHubMappingView',
      component: VendorHubMappingDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'question',
      name: 'Question',
      component: Question,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'question/new',
      name: 'QuestionCreate',
      component: QuestionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'question/:questionId/edit',
      name: 'QuestionEdit',
      component: QuestionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'question/:questionId/view',
      name: 'QuestionView',
      component: QuestionDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'option',
      name: 'Option',
      component: Option,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'option/new',
      name: 'OptionCreate',
      component: OptionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'option/:optionId/edit',
      name: 'OptionEdit',
      component: OptionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'option/:optionId/view',
      name: 'OptionView',
      component: OptionDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer',
      name: 'ExchangeAnswer',
      component: ExchangeAnswer,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer/new',
      name: 'ExchangeAnswerCreate',
      component: ExchangeAnswerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer/:exchangeAnswerId/edit',
      name: 'ExchangeAnswerEdit',
      component: ExchangeAnswerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer/:exchangeAnswerId/view',
      name: 'ExchangeAnswerView',
      component: ExchangeAnswerDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-set',
      name: 'ExchangeAnswerSet',
      component: ExchangeAnswerSet,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-set/new',
      name: 'ExchangeAnswerSetCreate',
      component: ExchangeAnswerSetUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-set/:exchangeAnswerSetId/edit',
      name: 'ExchangeAnswerSetEdit',
      component: ExchangeAnswerSetUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-set/:exchangeAnswerSetId/view',
      name: 'ExchangeAnswerSetView',
      component: ExchangeAnswerSetDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-price',
      name: 'ExchangePrice',
      component: ExchangePrice,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-price/new',
      name: 'ExchangePriceCreate',
      component: ExchangePriceUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-price/:exchangePriceId/edit',
      name: 'ExchangePriceEdit',
      component: ExchangePriceUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-price/:exchangePriceId/view',
      name: 'ExchangePriceView',
      component: ExchangePriceDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger',
      name: 'Ledger',
      component: Ledger,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger/new',
      name: 'LedgerCreate',
      component: LedgerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger/:ledgerId/edit',
      name: 'LedgerEdit',
      component: LedgerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger/:ledgerId/view',
      name: 'LedgerView',
      component: LedgerDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger-transaction',
      name: 'LedgerTransaction',
      component: LedgerTransaction,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger-transaction/new',
      name: 'LedgerTransactionCreate',
      component: LedgerTransactionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger-transaction/:ledgerTransactionId/edit',
      name: 'LedgerTransactionEdit',
      component: LedgerTransactionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'ledger-transaction/:ledgerTransactionId/view',
      name: 'LedgerTransactionView',
      component: LedgerTransactionDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-order',
      name: 'ExchangeOrder',
      component: ExchangeOrder,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-order/new',
      name: 'ExchangeOrderCreate',
      component: ExchangeOrderUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-order/:exchangeOrderId/edit',
      name: 'ExchangeOrderEdit',
      component: ExchangeOrderUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-order/:exchangeOrderId/view',
      name: 'ExchangeOrderView',
      component: ExchangeOrderDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-session',
      name: 'ExchangeAnswerSession',
      component: ExchangeAnswerSession,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-session/new',
      name: 'ExchangeAnswerSessionCreate',
      component: ExchangeAnswerSessionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-session/:exchangeAnswerSessionId/edit',
      name: 'ExchangeAnswerSessionEdit',
      component: ExchangeAnswerSessionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'exchange-answer-session/:exchangeAnswerSessionId/view',
      name: 'ExchangeAnswerSessionView',
      component: ExchangeAnswerSessionDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'differential-amount',
      name: 'DifferentialAmount',
      component: DifferentialAmount,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'differential-amount/new',
      name: 'DifferentialAmountCreate',
      component: DifferentialAmountUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'differential-amount/:differentialAmountId/edit',
      name: 'DifferentialAmountEdit',
      component: DifferentialAmountUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'differential-amount/:differentialAmountId/view',
      name: 'DifferentialAmountView',
      component: DifferentialAmountDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
