package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeOpsUserTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeOpsUser.class);
        ExchangeOpsUser exchangeOpsUser1 = new ExchangeOpsUser();
        exchangeOpsUser1.setId(1L);
        ExchangeOpsUser exchangeOpsUser2 = new ExchangeOpsUser();
        exchangeOpsUser2.setId(exchangeOpsUser1.getId());
        assertThat(exchangeOpsUser1).isEqualTo(exchangeOpsUser2);
        exchangeOpsUser2.setId(2L);
        assertThat(exchangeOpsUser1).isNotEqualTo(exchangeOpsUser2);
        exchangeOpsUser1.setId(null);
        assertThat(exchangeOpsUser1).isNotEqualTo(exchangeOpsUser2);
    }
}
