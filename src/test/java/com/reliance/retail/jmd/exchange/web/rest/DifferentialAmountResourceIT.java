package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.DifferentialAmount;
import com.reliance.retail.jmd.exchange.domain.enumeration.ChargedTo;
import com.reliance.retail.jmd.exchange.repository.DifferentialAmountRepository;
import com.reliance.retail.jmd.exchange.service.dto.DifferentialAmountDTO;
import com.reliance.retail.jmd.exchange.service.mapper.DifferentialAmountMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DifferentialAmountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DifferentialAmountResourceIT {

    private static final Double DEFAULT_DIFFERENTIAL_AMOUNT = 1D;
    private static final Double UPDATED_DIFFERENTIAL_AMOUNT = 2D;

    private static final ChargedTo DEFAULT_CHARGED_TO = ChargedTo.Customer;
    private static final ChargedTo UPDATED_CHARGED_TO = ChargedTo.DeliveryPartner;

    private static final LocalDate DEFAULT_APPROVED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_APPROVED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/differential-amounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DifferentialAmountRepository differentialAmountRepository;

    @Autowired
    private DifferentialAmountMapper differentialAmountMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDifferentialAmountMockMvc;

    private DifferentialAmount differentialAmount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DifferentialAmount createEntity(EntityManager em) {
        DifferentialAmount differentialAmount = new DifferentialAmount()
            .differentialAmount(DEFAULT_DIFFERENTIAL_AMOUNT)
            .chargedTo(DEFAULT_CHARGED_TO)
            .approvedAt(DEFAULT_APPROVED_AT)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return differentialAmount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DifferentialAmount createUpdatedEntity(EntityManager em) {
        DifferentialAmount differentialAmount = new DifferentialAmount()
            .differentialAmount(UPDATED_DIFFERENTIAL_AMOUNT)
            .chargedTo(UPDATED_CHARGED_TO)
            .approvedAt(UPDATED_APPROVED_AT)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return differentialAmount;
    }

    @BeforeEach
    public void initTest() {
        differentialAmount = createEntity(em);
    }

    @Test
    @Transactional
    void createDifferentialAmount() throws Exception {
        int databaseSizeBeforeCreate = differentialAmountRepository.findAll().size();
        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);
        restDifferentialAmountMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeCreate + 1);
        DifferentialAmount testDifferentialAmount = differentialAmountList.get(differentialAmountList.size() - 1);
        assertThat(testDifferentialAmount.getDifferentialAmount()).isEqualTo(DEFAULT_DIFFERENTIAL_AMOUNT);
        assertThat(testDifferentialAmount.getChargedTo()).isEqualTo(DEFAULT_CHARGED_TO);
        assertThat(testDifferentialAmount.getApprovedAt()).isEqualTo(DEFAULT_APPROVED_AT);
        assertThat(testDifferentialAmount.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDifferentialAmount.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDifferentialAmount.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDifferentialAmount.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createDifferentialAmountWithExistingId() throws Exception {
        // Create the DifferentialAmount with an existing ID
        differentialAmount.setId(1L);
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        int databaseSizeBeforeCreate = differentialAmountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDifferentialAmountMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDifferentialAmounts() throws Exception {
        // Initialize the database
        differentialAmountRepository.saveAndFlush(differentialAmount);

        // Get all the differentialAmountList
        restDifferentialAmountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(differentialAmount.getId().intValue())))
            .andExpect(jsonPath("$.[*].differentialAmount").value(hasItem(DEFAULT_DIFFERENTIAL_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].chargedTo").value(hasItem(DEFAULT_CHARGED_TO.toString())))
            .andExpect(jsonPath("$.[*].approvedAt").value(hasItem(DEFAULT_APPROVED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getDifferentialAmount() throws Exception {
        // Initialize the database
        differentialAmountRepository.saveAndFlush(differentialAmount);

        // Get the differentialAmount
        restDifferentialAmountMockMvc
            .perform(get(ENTITY_API_URL_ID, differentialAmount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(differentialAmount.getId().intValue()))
            .andExpect(jsonPath("$.differentialAmount").value(DEFAULT_DIFFERENTIAL_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.chargedTo").value(DEFAULT_CHARGED_TO.toString()))
            .andExpect(jsonPath("$.approvedAt").value(DEFAULT_APPROVED_AT.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingDifferentialAmount() throws Exception {
        // Get the differentialAmount
        restDifferentialAmountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDifferentialAmount() throws Exception {
        // Initialize the database
        differentialAmountRepository.saveAndFlush(differentialAmount);

        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();

        // Update the differentialAmount
        DifferentialAmount updatedDifferentialAmount = differentialAmountRepository.findById(differentialAmount.getId()).get();
        // Disconnect from session so that the updates on updatedDifferentialAmount are not directly saved in db
        em.detach(updatedDifferentialAmount);
        updatedDifferentialAmount
            .differentialAmount(UPDATED_DIFFERENTIAL_AMOUNT)
            .chargedTo(UPDATED_CHARGED_TO)
            .approvedAt(UPDATED_APPROVED_AT)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(updatedDifferentialAmount);

        restDifferentialAmountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, differentialAmountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isOk());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
        DifferentialAmount testDifferentialAmount = differentialAmountList.get(differentialAmountList.size() - 1);
        assertThat(testDifferentialAmount.getDifferentialAmount()).isEqualTo(UPDATED_DIFFERENTIAL_AMOUNT);
        assertThat(testDifferentialAmount.getChargedTo()).isEqualTo(UPDATED_CHARGED_TO);
        assertThat(testDifferentialAmount.getApprovedAt()).isEqualTo(UPDATED_APPROVED_AT);
        assertThat(testDifferentialAmount.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDifferentialAmount.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDifferentialAmount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDifferentialAmount.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingDifferentialAmount() throws Exception {
        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();
        differentialAmount.setId(count.incrementAndGet());

        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDifferentialAmountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, differentialAmountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDifferentialAmount() throws Exception {
        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();
        differentialAmount.setId(count.incrementAndGet());

        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDifferentialAmountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDifferentialAmount() throws Exception {
        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();
        differentialAmount.setId(count.incrementAndGet());

        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDifferentialAmountMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDifferentialAmountWithPatch() throws Exception {
        // Initialize the database
        differentialAmountRepository.saveAndFlush(differentialAmount);

        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();

        // Update the differentialAmount using partial update
        DifferentialAmount partialUpdatedDifferentialAmount = new DifferentialAmount();
        partialUpdatedDifferentialAmount.setId(differentialAmount.getId());

        partialUpdatedDifferentialAmount.approvedAt(UPDATED_APPROVED_AT).createdAt(UPDATED_CREATED_AT).createdBy(UPDATED_CREATED_BY);

        restDifferentialAmountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDifferentialAmount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDifferentialAmount))
            )
            .andExpect(status().isOk());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
        DifferentialAmount testDifferentialAmount = differentialAmountList.get(differentialAmountList.size() - 1);
        assertThat(testDifferentialAmount.getDifferentialAmount()).isEqualTo(DEFAULT_DIFFERENTIAL_AMOUNT);
        assertThat(testDifferentialAmount.getChargedTo()).isEqualTo(DEFAULT_CHARGED_TO);
        assertThat(testDifferentialAmount.getApprovedAt()).isEqualTo(UPDATED_APPROVED_AT);
        assertThat(testDifferentialAmount.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDifferentialAmount.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDifferentialAmount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDifferentialAmount.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateDifferentialAmountWithPatch() throws Exception {
        // Initialize the database
        differentialAmountRepository.saveAndFlush(differentialAmount);

        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();

        // Update the differentialAmount using partial update
        DifferentialAmount partialUpdatedDifferentialAmount = new DifferentialAmount();
        partialUpdatedDifferentialAmount.setId(differentialAmount.getId());

        partialUpdatedDifferentialAmount
            .differentialAmount(UPDATED_DIFFERENTIAL_AMOUNT)
            .chargedTo(UPDATED_CHARGED_TO)
            .approvedAt(UPDATED_APPROVED_AT)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restDifferentialAmountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDifferentialAmount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDifferentialAmount))
            )
            .andExpect(status().isOk());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
        DifferentialAmount testDifferentialAmount = differentialAmountList.get(differentialAmountList.size() - 1);
        assertThat(testDifferentialAmount.getDifferentialAmount()).isEqualTo(UPDATED_DIFFERENTIAL_AMOUNT);
        assertThat(testDifferentialAmount.getChargedTo()).isEqualTo(UPDATED_CHARGED_TO);
        assertThat(testDifferentialAmount.getApprovedAt()).isEqualTo(UPDATED_APPROVED_AT);
        assertThat(testDifferentialAmount.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDifferentialAmount.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDifferentialAmount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDifferentialAmount.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingDifferentialAmount() throws Exception {
        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();
        differentialAmount.setId(count.incrementAndGet());

        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDifferentialAmountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, differentialAmountDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDifferentialAmount() throws Exception {
        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();
        differentialAmount.setId(count.incrementAndGet());

        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDifferentialAmountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDifferentialAmount() throws Exception {
        int databaseSizeBeforeUpdate = differentialAmountRepository.findAll().size();
        differentialAmount.setId(count.incrementAndGet());

        // Create the DifferentialAmount
        DifferentialAmountDTO differentialAmountDTO = differentialAmountMapper.toDto(differentialAmount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDifferentialAmountMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(differentialAmountDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DifferentialAmount in the database
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDifferentialAmount() throws Exception {
        // Initialize the database
        differentialAmountRepository.saveAndFlush(differentialAmount);

        int databaseSizeBeforeDelete = differentialAmountRepository.findAll().size();

        // Delete the differentialAmount
        restDifferentialAmountMockMvc
            .perform(delete(ENTITY_API_URL_ID, differentialAmount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DifferentialAmount> differentialAmountList = differentialAmountRepository.findAll();
        assertThat(differentialAmountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
