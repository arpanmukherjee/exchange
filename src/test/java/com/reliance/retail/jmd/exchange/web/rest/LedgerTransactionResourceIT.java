package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.LedgerTransaction;
import com.reliance.retail.jmd.exchange.domain.enumeration.TransactionStatus;
import com.reliance.retail.jmd.exchange.domain.enumeration.TransactionType;
import com.reliance.retail.jmd.exchange.repository.LedgerTransactionRepository;
import com.reliance.retail.jmd.exchange.service.dto.LedgerTransactionDTO;
import com.reliance.retail.jmd.exchange.service.mapper.LedgerTransactionMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LedgerTransactionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LedgerTransactionResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final TransactionType DEFAULT_TYPE = TransactionType.Credit;
    private static final TransactionType UPDATED_TYPE = TransactionType.Debit;

    private static final String DEFAULT_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_REFERENCE = "BBBBBBBBBB";

    private static final TransactionStatus DEFAULT_STATUS = TransactionStatus.Blocked;
    private static final TransactionStatus UPDATED_STATUS = TransactionStatus.Debited;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/ledger-transactions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LedgerTransactionRepository ledgerTransactionRepository;

    @Autowired
    private LedgerTransactionMapper ledgerTransactionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLedgerTransactionMockMvc;

    private LedgerTransaction ledgerTransaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LedgerTransaction createEntity(EntityManager em) {
        LedgerTransaction ledgerTransaction = new LedgerTransaction()
            .amount(DEFAULT_AMOUNT)
            .type(DEFAULT_TYPE)
            .reference(DEFAULT_REFERENCE)
            .status(DEFAULT_STATUS)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return ledgerTransaction;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LedgerTransaction createUpdatedEntity(EntityManager em) {
        LedgerTransaction ledgerTransaction = new LedgerTransaction()
            .amount(UPDATED_AMOUNT)
            .type(UPDATED_TYPE)
            .reference(UPDATED_REFERENCE)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return ledgerTransaction;
    }

    @BeforeEach
    public void initTest() {
        ledgerTransaction = createEntity(em);
    }

    @Test
    @Transactional
    void createLedgerTransaction() throws Exception {
        int databaseSizeBeforeCreate = ledgerTransactionRepository.findAll().size();
        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);
        restLedgerTransactionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        LedgerTransaction testLedgerTransaction = ledgerTransactionList.get(ledgerTransactionList.size() - 1);
        assertThat(testLedgerTransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testLedgerTransaction.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testLedgerTransaction.getReference()).isEqualTo(DEFAULT_REFERENCE);
        assertThat(testLedgerTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testLedgerTransaction.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testLedgerTransaction.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    void createLedgerTransactionWithExistingId() throws Exception {
        // Create the LedgerTransaction with an existing ID
        ledgerTransaction.setId(1L);
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        int databaseSizeBeforeCreate = ledgerTransactionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLedgerTransactionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLedgerTransactions() throws Exception {
        // Initialize the database
        ledgerTransactionRepository.saveAndFlush(ledgerTransaction);

        // Get all the ledgerTransactionList
        restLedgerTransactionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ledgerTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].reference").value(hasItem(DEFAULT_REFERENCE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }

    @Test
    @Transactional
    void getLedgerTransaction() throws Exception {
        // Initialize the database
        ledgerTransactionRepository.saveAndFlush(ledgerTransaction);

        // Get the ledgerTransaction
        restLedgerTransactionMockMvc
            .perform(get(ENTITY_API_URL_ID, ledgerTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ledgerTransaction.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.reference").value(DEFAULT_REFERENCE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingLedgerTransaction() throws Exception {
        // Get the ledgerTransaction
        restLedgerTransactionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingLedgerTransaction() throws Exception {
        // Initialize the database
        ledgerTransactionRepository.saveAndFlush(ledgerTransaction);

        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();

        // Update the ledgerTransaction
        LedgerTransaction updatedLedgerTransaction = ledgerTransactionRepository.findById(ledgerTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedLedgerTransaction are not directly saved in db
        em.detach(updatedLedgerTransaction);
        updatedLedgerTransaction
            .amount(UPDATED_AMOUNT)
            .type(UPDATED_TYPE)
            .reference(UPDATED_REFERENCE)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(updatedLedgerTransaction);

        restLedgerTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ledgerTransactionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isOk());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
        LedgerTransaction testLedgerTransaction = ledgerTransactionList.get(ledgerTransactionList.size() - 1);
        assertThat(testLedgerTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testLedgerTransaction.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testLedgerTransaction.getReference()).isEqualTo(UPDATED_REFERENCE);
        assertThat(testLedgerTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testLedgerTransaction.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testLedgerTransaction.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void putNonExistingLedgerTransaction() throws Exception {
        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();
        ledgerTransaction.setId(count.incrementAndGet());

        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLedgerTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ledgerTransactionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLedgerTransaction() throws Exception {
        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();
        ledgerTransaction.setId(count.incrementAndGet());

        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLedgerTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLedgerTransaction() throws Exception {
        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();
        ledgerTransaction.setId(count.incrementAndGet());

        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLedgerTransactionMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLedgerTransactionWithPatch() throws Exception {
        // Initialize the database
        ledgerTransactionRepository.saveAndFlush(ledgerTransaction);

        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();

        // Update the ledgerTransaction using partial update
        LedgerTransaction partialUpdatedLedgerTransaction = new LedgerTransaction();
        partialUpdatedLedgerTransaction.setId(ledgerTransaction.getId());

        partialUpdatedLedgerTransaction.amount(UPDATED_AMOUNT).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        restLedgerTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLedgerTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLedgerTransaction))
            )
            .andExpect(status().isOk());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
        LedgerTransaction testLedgerTransaction = ledgerTransactionList.get(ledgerTransactionList.size() - 1);
        assertThat(testLedgerTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testLedgerTransaction.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testLedgerTransaction.getReference()).isEqualTo(DEFAULT_REFERENCE);
        assertThat(testLedgerTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testLedgerTransaction.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testLedgerTransaction.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void fullUpdateLedgerTransactionWithPatch() throws Exception {
        // Initialize the database
        ledgerTransactionRepository.saveAndFlush(ledgerTransaction);

        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();

        // Update the ledgerTransaction using partial update
        LedgerTransaction partialUpdatedLedgerTransaction = new LedgerTransaction();
        partialUpdatedLedgerTransaction.setId(ledgerTransaction.getId());

        partialUpdatedLedgerTransaction
            .amount(UPDATED_AMOUNT)
            .type(UPDATED_TYPE)
            .reference(UPDATED_REFERENCE)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restLedgerTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLedgerTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLedgerTransaction))
            )
            .andExpect(status().isOk());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
        LedgerTransaction testLedgerTransaction = ledgerTransactionList.get(ledgerTransactionList.size() - 1);
        assertThat(testLedgerTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testLedgerTransaction.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testLedgerTransaction.getReference()).isEqualTo(UPDATED_REFERENCE);
        assertThat(testLedgerTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testLedgerTransaction.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testLedgerTransaction.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void patchNonExistingLedgerTransaction() throws Exception {
        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();
        ledgerTransaction.setId(count.incrementAndGet());

        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLedgerTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ledgerTransactionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLedgerTransaction() throws Exception {
        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();
        ledgerTransaction.setId(count.incrementAndGet());

        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLedgerTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLedgerTransaction() throws Exception {
        int databaseSizeBeforeUpdate = ledgerTransactionRepository.findAll().size();
        ledgerTransaction.setId(count.incrementAndGet());

        // Create the LedgerTransaction
        LedgerTransactionDTO ledgerTransactionDTO = ledgerTransactionMapper.toDto(ledgerTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLedgerTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ledgerTransactionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LedgerTransaction in the database
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLedgerTransaction() throws Exception {
        // Initialize the database
        ledgerTransactionRepository.saveAndFlush(ledgerTransaction);

        int databaseSizeBeforeDelete = ledgerTransactionRepository.findAll().size();

        // Delete the ledgerTransaction
        restLedgerTransactionMockMvc
            .perform(delete(ENTITY_API_URL_ID, ledgerTransaction.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LedgerTransaction> ledgerTransactionList = ledgerTransactionRepository.findAll();
        assertThat(ledgerTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
