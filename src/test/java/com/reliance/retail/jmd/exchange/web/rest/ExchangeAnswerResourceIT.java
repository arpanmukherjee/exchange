package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ExchangeAnswer;
import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerRepository;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeAnswerMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangeAnswerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangeAnswerResourceIT {

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/exchange-answers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExchangeAnswerRepository exchangeAnswerRepository;

    @Autowired
    private ExchangeAnswerMapper exchangeAnswerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangeAnswerMockMvc;

    private ExchangeAnswer exchangeAnswer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeAnswer createEntity(EntityManager em) {
        ExchangeAnswer exchangeAnswer = new ExchangeAnswer().createdAt(DEFAULT_CREATED_AT).updatedAt(DEFAULT_UPDATED_AT);
        return exchangeAnswer;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeAnswer createUpdatedEntity(EntityManager em) {
        ExchangeAnswer exchangeAnswer = new ExchangeAnswer().createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);
        return exchangeAnswer;
    }

    @BeforeEach
    public void initTest() {
        exchangeAnswer = createEntity(em);
    }

    @Test
    @Transactional
    void createExchangeAnswer() throws Exception {
        int databaseSizeBeforeCreate = exchangeAnswerRepository.findAll().size();
        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);
        restExchangeAnswerMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangeAnswer testExchangeAnswer = exchangeAnswerList.get(exchangeAnswerList.size() - 1);
        assertThat(testExchangeAnswer.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangeAnswer.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    void createExchangeAnswerWithExistingId() throws Exception {
        // Create the ExchangeAnswer with an existing ID
        exchangeAnswer.setId(1L);
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        int databaseSizeBeforeCreate = exchangeAnswerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeAnswerMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllExchangeAnswers() throws Exception {
        // Initialize the database
        exchangeAnswerRepository.saveAndFlush(exchangeAnswer);

        // Get all the exchangeAnswerList
        restExchangeAnswerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeAnswer.getId().intValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }

    @Test
    @Transactional
    void getExchangeAnswer() throws Exception {
        // Initialize the database
        exchangeAnswerRepository.saveAndFlush(exchangeAnswer);

        // Get the exchangeAnswer
        restExchangeAnswerMockMvc
            .perform(get(ENTITY_API_URL_ID, exchangeAnswer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchangeAnswer.getId().intValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingExchangeAnswer() throws Exception {
        // Get the exchangeAnswer
        restExchangeAnswerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchangeAnswer() throws Exception {
        // Initialize the database
        exchangeAnswerRepository.saveAndFlush(exchangeAnswer);

        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();

        // Update the exchangeAnswer
        ExchangeAnswer updatedExchangeAnswer = exchangeAnswerRepository.findById(exchangeAnswer.getId()).get();
        // Disconnect from session so that the updates on updatedExchangeAnswer are not directly saved in db
        em.detach(updatedExchangeAnswer);
        updatedExchangeAnswer.createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(updatedExchangeAnswer);

        restExchangeAnswerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeAnswerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswer testExchangeAnswer = exchangeAnswerList.get(exchangeAnswerList.size() - 1);
        assertThat(testExchangeAnswer.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeAnswer.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void putNonExistingExchangeAnswer() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();
        exchangeAnswer.setId(count.incrementAndGet());

        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeAnswerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeAnswerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchangeAnswer() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();
        exchangeAnswer.setId(count.incrementAndGet());

        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchangeAnswer() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();
        exchangeAnswer.setId(count.incrementAndGet());

        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangeAnswerWithPatch() throws Exception {
        // Initialize the database
        exchangeAnswerRepository.saveAndFlush(exchangeAnswer);

        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();

        // Update the exchangeAnswer using partial update
        ExchangeAnswer partialUpdatedExchangeAnswer = new ExchangeAnswer();
        partialUpdatedExchangeAnswer.setId(exchangeAnswer.getId());

        partialUpdatedExchangeAnswer.createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        restExchangeAnswerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeAnswer.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeAnswer))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswer testExchangeAnswer = exchangeAnswerList.get(exchangeAnswerList.size() - 1);
        assertThat(testExchangeAnswer.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeAnswer.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void fullUpdateExchangeAnswerWithPatch() throws Exception {
        // Initialize the database
        exchangeAnswerRepository.saveAndFlush(exchangeAnswer);

        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();

        // Update the exchangeAnswer using partial update
        ExchangeAnswer partialUpdatedExchangeAnswer = new ExchangeAnswer();
        partialUpdatedExchangeAnswer.setId(exchangeAnswer.getId());

        partialUpdatedExchangeAnswer.createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        restExchangeAnswerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeAnswer.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeAnswer))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswer testExchangeAnswer = exchangeAnswerList.get(exchangeAnswerList.size() - 1);
        assertThat(testExchangeAnswer.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeAnswer.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void patchNonExistingExchangeAnswer() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();
        exchangeAnswer.setId(count.incrementAndGet());

        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeAnswerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangeAnswerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchangeAnswer() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();
        exchangeAnswer.setId(count.incrementAndGet());

        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchangeAnswer() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerRepository.findAll().size();
        exchangeAnswer.setId(count.incrementAndGet());

        // Create the ExchangeAnswer
        ExchangeAnswerDTO exchangeAnswerDTO = exchangeAnswerMapper.toDto(exchangeAnswer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeAnswer in the database
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchangeAnswer() throws Exception {
        // Initialize the database
        exchangeAnswerRepository.saveAndFlush(exchangeAnswer);

        int databaseSizeBeforeDelete = exchangeAnswerRepository.findAll().size();

        // Delete the exchangeAnswer
        restExchangeAnswerMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchangeAnswer.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangeAnswer> exchangeAnswerList = exchangeAnswerRepository.findAll();
        assertThat(exchangeAnswerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
