package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VendorHubMappingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VendorHubMapping.class);
        VendorHubMapping vendorHubMapping1 = new VendorHubMapping();
        vendorHubMapping1.setId(1L);
        VendorHubMapping vendorHubMapping2 = new VendorHubMapping();
        vendorHubMapping2.setId(vendorHubMapping1.getId());
        assertThat(vendorHubMapping1).isEqualTo(vendorHubMapping2);
        vendorHubMapping2.setId(2L);
        assertThat(vendorHubMapping1).isNotEqualTo(vendorHubMapping2);
        vendorHubMapping1.setId(null);
        assertThat(vendorHubMapping1).isNotEqualTo(vendorHubMapping2);
    }
}
