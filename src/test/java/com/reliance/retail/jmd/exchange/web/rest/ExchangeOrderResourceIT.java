package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ExchangeOrder;
import com.reliance.retail.jmd.exchange.domain.enumeration.CancellationReason;
import com.reliance.retail.jmd.exchange.domain.enumeration.ExchangeOrderStatus;
import com.reliance.retail.jmd.exchange.repository.ExchangeOrderRepository;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOrderDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeOrderMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangeOrderResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangeOrderResourceIT {

    private static final String DEFAULT_CLIENT_ORDER_REF_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ORDER_REF_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_PHONE_NO = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_PHONE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_ORDER_DELIVERY_REF_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ORDER_DELIVERY_REF_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_PARTNER_PHONE_NO = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_PARTNER_PHONE_NO = "BBBBBBBBBB";

    private static final Double DEFAULT_TOTAL_ORDER_AMOUNT = 1D;
    private static final Double UPDATED_TOTAL_ORDER_AMOUNT = 2D;

    private static final Double DEFAULT_PAID_AMOUNT = 1D;
    private static final Double UPDATED_PAID_AMOUNT = 2D;

    private static final Double DEFAULT_COD_INITIAL_PENDING_AMOUNT = 1D;
    private static final Double UPDATED_COD_INITIAL_PENDING_AMOUNT = 2D;

    private static final Double DEFAULT_EXCHANGE_ESTIMATE_AMOUNT = 1D;
    private static final Double UPDATED_EXCHANGE_ESTIMATE_AMOUNT = 2D;

    private static final Double DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_USER = 1D;
    private static final Double UPDATED_EXCHANGE_FINAL_AMOUNT_TO_USER = 2D;

    private static final Double DEFAULT_COD_FINAL_AMOUNT = 1D;
    private static final Double UPDATED_COD_FINAL_AMOUNT = 2D;

    private static final Double DEFAULT_COD_AMOUNT_PAID = 1D;
    private static final Double UPDATED_COD_AMOUNT_PAID = 2D;

    private static final String DEFAULT_COD_PAYMENT_TXN_REF_ID = "AAAAAAAAAA";
    private static final String UPDATED_COD_PAYMENT_TXN_REF_ID = "BBBBBBBBBB";

    private static final ExchangeOrderStatus DEFAULT_STATUS = ExchangeOrderStatus.Created;
    private static final ExchangeOrderStatus UPDATED_STATUS = ExchangeOrderStatus.Picked;

    private static final CancellationReason DEFAULT_CANCELLATION_REASON = CancellationReason.CancelledByCustomer;
    private static final CancellationReason UPDATED_CANCELLATION_REASON = CancellationReason.CancelledByDeliveryPartner;

    private static final LocalDate DEFAULT_EXPECTED_PICKUP_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPECTED_PICKUP_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CUSTOMER_PICKUP_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CUSTOMER_PICKUP_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DC_DELIVERED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DC_DELIVERED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DC_PICKUP_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DC_PICKUP_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_DC = 1D;
    private static final Double UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC = 2D;

    private static final LocalDate DEFAULT_VENDOR_DELIVERED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VENDOR_DELIVERED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_VENDOR = 1D;
    private static final Double UPDATED_EXCHANGE_FINAL_AMOUNT_TO_VENDOR = 2D;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/exchange-orders";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExchangeOrderRepository exchangeOrderRepository;

    @Autowired
    private ExchangeOrderMapper exchangeOrderMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangeOrderMockMvc;

    private ExchangeOrder exchangeOrder;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeOrder createEntity(EntityManager em) {
        ExchangeOrder exchangeOrder = new ExchangeOrder()
            .clientOrderRefId(DEFAULT_CLIENT_ORDER_REF_ID)
            .customerPhoneNo(DEFAULT_CUSTOMER_PHONE_NO)
            .clientOrderDeliveryRefId(DEFAULT_CLIENT_ORDER_DELIVERY_REF_ID)
            .deliveryPartnerPhoneNo(DEFAULT_DELIVERY_PARTNER_PHONE_NO)
            .totalOrderAmount(DEFAULT_TOTAL_ORDER_AMOUNT)
            .paidAmount(DEFAULT_PAID_AMOUNT)
            .codInitialPendingAmount(DEFAULT_COD_INITIAL_PENDING_AMOUNT)
            .exchangeEstimateAmount(DEFAULT_EXCHANGE_ESTIMATE_AMOUNT)
            .exchangeFinalAmountToUser(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_USER)
            .codFinalAmount(DEFAULT_COD_FINAL_AMOUNT)
            .codAmountPaid(DEFAULT_COD_AMOUNT_PAID)
            .codPaymentTxnRefId(DEFAULT_COD_PAYMENT_TXN_REF_ID)
            .status(DEFAULT_STATUS)
            .cancellationReason(DEFAULT_CANCELLATION_REASON)
            .expectedPickupDate(DEFAULT_EXPECTED_PICKUP_DATE)
            .customerPickupDate(DEFAULT_CUSTOMER_PICKUP_DATE)
            .dcDeliveredDate(DEFAULT_DC_DELIVERED_DATE)
            .dcPickupDate(DEFAULT_DC_PICKUP_DATE)
            .exchangeFinalAmountToDc(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_DC)
            .vendorDeliveredDate(DEFAULT_VENDOR_DELIVERED_DATE)
            .exchangeFinalAmountToVendor(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_VENDOR)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return exchangeOrder;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeOrder createUpdatedEntity(EntityManager em) {
        ExchangeOrder exchangeOrder = new ExchangeOrder()
            .clientOrderRefId(UPDATED_CLIENT_ORDER_REF_ID)
            .customerPhoneNo(UPDATED_CUSTOMER_PHONE_NO)
            .clientOrderDeliveryRefId(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID)
            .deliveryPartnerPhoneNo(UPDATED_DELIVERY_PARTNER_PHONE_NO)
            .totalOrderAmount(UPDATED_TOTAL_ORDER_AMOUNT)
            .paidAmount(UPDATED_PAID_AMOUNT)
            .codInitialPendingAmount(UPDATED_COD_INITIAL_PENDING_AMOUNT)
            .exchangeEstimateAmount(UPDATED_EXCHANGE_ESTIMATE_AMOUNT)
            .exchangeFinalAmountToUser(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_USER)
            .codFinalAmount(UPDATED_COD_FINAL_AMOUNT)
            .codAmountPaid(UPDATED_COD_AMOUNT_PAID)
            .codPaymentTxnRefId(UPDATED_COD_PAYMENT_TXN_REF_ID)
            .status(UPDATED_STATUS)
            .cancellationReason(UPDATED_CANCELLATION_REASON)
            .expectedPickupDate(UPDATED_EXPECTED_PICKUP_DATE)
            .customerPickupDate(UPDATED_CUSTOMER_PICKUP_DATE)
            .dcDeliveredDate(UPDATED_DC_DELIVERED_DATE)
            .dcPickupDate(UPDATED_DC_PICKUP_DATE)
            .exchangeFinalAmountToDc(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC)
            .vendorDeliveredDate(UPDATED_VENDOR_DELIVERED_DATE)
            .exchangeFinalAmountToVendor(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_VENDOR)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return exchangeOrder;
    }

    @BeforeEach
    public void initTest() {
        exchangeOrder = createEntity(em);
    }

    @Test
    @Transactional
    void createExchangeOrder() throws Exception {
        int databaseSizeBeforeCreate = exchangeOrderRepository.findAll().size();
        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);
        restExchangeOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangeOrder testExchangeOrder = exchangeOrderList.get(exchangeOrderList.size() - 1);
        assertThat(testExchangeOrder.getClientOrderRefId()).isEqualTo(DEFAULT_CLIENT_ORDER_REF_ID);
        assertThat(testExchangeOrder.getCustomerPhoneNo()).isEqualTo(DEFAULT_CUSTOMER_PHONE_NO);
        assertThat(testExchangeOrder.getClientOrderDeliveryRefId()).isEqualTo(DEFAULT_CLIENT_ORDER_DELIVERY_REF_ID);
        assertThat(testExchangeOrder.getDeliveryPartnerPhoneNo()).isEqualTo(DEFAULT_DELIVERY_PARTNER_PHONE_NO);
        assertThat(testExchangeOrder.getTotalOrderAmount()).isEqualTo(DEFAULT_TOTAL_ORDER_AMOUNT);
        assertThat(testExchangeOrder.getPaidAmount()).isEqualTo(DEFAULT_PAID_AMOUNT);
        assertThat(testExchangeOrder.getCodInitialPendingAmount()).isEqualTo(DEFAULT_COD_INITIAL_PENDING_AMOUNT);
        assertThat(testExchangeOrder.getExchangeEstimateAmount()).isEqualTo(DEFAULT_EXCHANGE_ESTIMATE_AMOUNT);
        assertThat(testExchangeOrder.getExchangeFinalAmountToUser()).isEqualTo(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_USER);
        assertThat(testExchangeOrder.getCodFinalAmount()).isEqualTo(DEFAULT_COD_FINAL_AMOUNT);
        assertThat(testExchangeOrder.getCodAmountPaid()).isEqualTo(DEFAULT_COD_AMOUNT_PAID);
        assertThat(testExchangeOrder.getCodPaymentTxnRefId()).isEqualTo(DEFAULT_COD_PAYMENT_TXN_REF_ID);
        assertThat(testExchangeOrder.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testExchangeOrder.getCancellationReason()).isEqualTo(DEFAULT_CANCELLATION_REASON);
        assertThat(testExchangeOrder.getExpectedPickupDate()).isEqualTo(DEFAULT_EXPECTED_PICKUP_DATE);
        assertThat(testExchangeOrder.getCustomerPickupDate()).isEqualTo(DEFAULT_CUSTOMER_PICKUP_DATE);
        assertThat(testExchangeOrder.getDcDeliveredDate()).isEqualTo(DEFAULT_DC_DELIVERED_DATE);
        assertThat(testExchangeOrder.getDcPickupDate()).isEqualTo(DEFAULT_DC_PICKUP_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToDc()).isEqualTo(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_DC);
        assertThat(testExchangeOrder.getVendorDeliveredDate()).isEqualTo(DEFAULT_VENDOR_DELIVERED_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToVendor()).isEqualTo(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_VENDOR);
        assertThat(testExchangeOrder.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangeOrder.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    void createExchangeOrderWithExistingId() throws Exception {
        // Create the ExchangeOrder with an existing ID
        exchangeOrder.setId(1L);
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        int databaseSizeBeforeCreate = exchangeOrderRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllExchangeOrders() throws Exception {
        // Initialize the database
        exchangeOrderRepository.saveAndFlush(exchangeOrder);

        // Get all the exchangeOrderList
        restExchangeOrderMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientOrderRefId").value(hasItem(DEFAULT_CLIENT_ORDER_REF_ID)))
            .andExpect(jsonPath("$.[*].customerPhoneNo").value(hasItem(DEFAULT_CUSTOMER_PHONE_NO)))
            .andExpect(jsonPath("$.[*].clientOrderDeliveryRefId").value(hasItem(DEFAULT_CLIENT_ORDER_DELIVERY_REF_ID)))
            .andExpect(jsonPath("$.[*].deliveryPartnerPhoneNo").value(hasItem(DEFAULT_DELIVERY_PARTNER_PHONE_NO)))
            .andExpect(jsonPath("$.[*].totalOrderAmount").value(hasItem(DEFAULT_TOTAL_ORDER_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].paidAmount").value(hasItem(DEFAULT_PAID_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].codInitialPendingAmount").value(hasItem(DEFAULT_COD_INITIAL_PENDING_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].exchangeEstimateAmount").value(hasItem(DEFAULT_EXCHANGE_ESTIMATE_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].exchangeFinalAmountToUser").value(hasItem(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_USER.doubleValue())))
            .andExpect(jsonPath("$.[*].codFinalAmount").value(hasItem(DEFAULT_COD_FINAL_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].codAmountPaid").value(hasItem(DEFAULT_COD_AMOUNT_PAID.doubleValue())))
            .andExpect(jsonPath("$.[*].codPaymentTxnRefId").value(hasItem(DEFAULT_COD_PAYMENT_TXN_REF_ID)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].cancellationReason").value(hasItem(DEFAULT_CANCELLATION_REASON.toString())))
            .andExpect(jsonPath("$.[*].expectedPickupDate").value(hasItem(DEFAULT_EXPECTED_PICKUP_DATE.toString())))
            .andExpect(jsonPath("$.[*].customerPickupDate").value(hasItem(DEFAULT_CUSTOMER_PICKUP_DATE.toString())))
            .andExpect(jsonPath("$.[*].dcDeliveredDate").value(hasItem(DEFAULT_DC_DELIVERED_DATE.toString())))
            .andExpect(jsonPath("$.[*].dcPickupDate").value(hasItem(DEFAULT_DC_PICKUP_DATE.toString())))
            .andExpect(jsonPath("$.[*].exchangeFinalAmountToDc").value(hasItem(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_DC.doubleValue())))
            .andExpect(jsonPath("$.[*].vendorDeliveredDate").value(hasItem(DEFAULT_VENDOR_DELIVERED_DATE.toString())))
            .andExpect(jsonPath("$.[*].exchangeFinalAmountToVendor").value(hasItem(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_VENDOR.doubleValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }

    @Test
    @Transactional
    void getExchangeOrder() throws Exception {
        // Initialize the database
        exchangeOrderRepository.saveAndFlush(exchangeOrder);

        // Get the exchangeOrder
        restExchangeOrderMockMvc
            .perform(get(ENTITY_API_URL_ID, exchangeOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchangeOrder.getId().intValue()))
            .andExpect(jsonPath("$.clientOrderRefId").value(DEFAULT_CLIENT_ORDER_REF_ID))
            .andExpect(jsonPath("$.customerPhoneNo").value(DEFAULT_CUSTOMER_PHONE_NO))
            .andExpect(jsonPath("$.clientOrderDeliveryRefId").value(DEFAULT_CLIENT_ORDER_DELIVERY_REF_ID))
            .andExpect(jsonPath("$.deliveryPartnerPhoneNo").value(DEFAULT_DELIVERY_PARTNER_PHONE_NO))
            .andExpect(jsonPath("$.totalOrderAmount").value(DEFAULT_TOTAL_ORDER_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.paidAmount").value(DEFAULT_PAID_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.codInitialPendingAmount").value(DEFAULT_COD_INITIAL_PENDING_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.exchangeEstimateAmount").value(DEFAULT_EXCHANGE_ESTIMATE_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.exchangeFinalAmountToUser").value(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_USER.doubleValue()))
            .andExpect(jsonPath("$.codFinalAmount").value(DEFAULT_COD_FINAL_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.codAmountPaid").value(DEFAULT_COD_AMOUNT_PAID.doubleValue()))
            .andExpect(jsonPath("$.codPaymentTxnRefId").value(DEFAULT_COD_PAYMENT_TXN_REF_ID))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.cancellationReason").value(DEFAULT_CANCELLATION_REASON.toString()))
            .andExpect(jsonPath("$.expectedPickupDate").value(DEFAULT_EXPECTED_PICKUP_DATE.toString()))
            .andExpect(jsonPath("$.customerPickupDate").value(DEFAULT_CUSTOMER_PICKUP_DATE.toString()))
            .andExpect(jsonPath("$.dcDeliveredDate").value(DEFAULT_DC_DELIVERED_DATE.toString()))
            .andExpect(jsonPath("$.dcPickupDate").value(DEFAULT_DC_PICKUP_DATE.toString()))
            .andExpect(jsonPath("$.exchangeFinalAmountToDc").value(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_DC.doubleValue()))
            .andExpect(jsonPath("$.vendorDeliveredDate").value(DEFAULT_VENDOR_DELIVERED_DATE.toString()))
            .andExpect(jsonPath("$.exchangeFinalAmountToVendor").value(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_VENDOR.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingExchangeOrder() throws Exception {
        // Get the exchangeOrder
        restExchangeOrderMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchangeOrder() throws Exception {
        // Initialize the database
        exchangeOrderRepository.saveAndFlush(exchangeOrder);

        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();

        // Update the exchangeOrder
        ExchangeOrder updatedExchangeOrder = exchangeOrderRepository.findById(exchangeOrder.getId()).get();
        // Disconnect from session so that the updates on updatedExchangeOrder are not directly saved in db
        em.detach(updatedExchangeOrder);
        updatedExchangeOrder
            .clientOrderRefId(UPDATED_CLIENT_ORDER_REF_ID)
            .customerPhoneNo(UPDATED_CUSTOMER_PHONE_NO)
            .clientOrderDeliveryRefId(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID)
            .deliveryPartnerPhoneNo(UPDATED_DELIVERY_PARTNER_PHONE_NO)
            .totalOrderAmount(UPDATED_TOTAL_ORDER_AMOUNT)
            .paidAmount(UPDATED_PAID_AMOUNT)
            .codInitialPendingAmount(UPDATED_COD_INITIAL_PENDING_AMOUNT)
            .exchangeEstimateAmount(UPDATED_EXCHANGE_ESTIMATE_AMOUNT)
            .exchangeFinalAmountToUser(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_USER)
            .codFinalAmount(UPDATED_COD_FINAL_AMOUNT)
            .codAmountPaid(UPDATED_COD_AMOUNT_PAID)
            .codPaymentTxnRefId(UPDATED_COD_PAYMENT_TXN_REF_ID)
            .status(UPDATED_STATUS)
            .cancellationReason(UPDATED_CANCELLATION_REASON)
            .expectedPickupDate(UPDATED_EXPECTED_PICKUP_DATE)
            .customerPickupDate(UPDATED_CUSTOMER_PICKUP_DATE)
            .dcDeliveredDate(UPDATED_DC_DELIVERED_DATE)
            .dcPickupDate(UPDATED_DC_PICKUP_DATE)
            .exchangeFinalAmountToDc(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC)
            .vendorDeliveredDate(UPDATED_VENDOR_DELIVERED_DATE)
            .exchangeFinalAmountToVendor(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_VENDOR)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(updatedExchangeOrder);

        restExchangeOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeOrderDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
        ExchangeOrder testExchangeOrder = exchangeOrderList.get(exchangeOrderList.size() - 1);
        assertThat(testExchangeOrder.getClientOrderRefId()).isEqualTo(UPDATED_CLIENT_ORDER_REF_ID);
        assertThat(testExchangeOrder.getCustomerPhoneNo()).isEqualTo(UPDATED_CUSTOMER_PHONE_NO);
        assertThat(testExchangeOrder.getClientOrderDeliveryRefId()).isEqualTo(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID);
        assertThat(testExchangeOrder.getDeliveryPartnerPhoneNo()).isEqualTo(UPDATED_DELIVERY_PARTNER_PHONE_NO);
        assertThat(testExchangeOrder.getTotalOrderAmount()).isEqualTo(UPDATED_TOTAL_ORDER_AMOUNT);
        assertThat(testExchangeOrder.getPaidAmount()).isEqualTo(UPDATED_PAID_AMOUNT);
        assertThat(testExchangeOrder.getCodInitialPendingAmount()).isEqualTo(UPDATED_COD_INITIAL_PENDING_AMOUNT);
        assertThat(testExchangeOrder.getExchangeEstimateAmount()).isEqualTo(UPDATED_EXCHANGE_ESTIMATE_AMOUNT);
        assertThat(testExchangeOrder.getExchangeFinalAmountToUser()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_USER);
        assertThat(testExchangeOrder.getCodFinalAmount()).isEqualTo(UPDATED_COD_FINAL_AMOUNT);
        assertThat(testExchangeOrder.getCodAmountPaid()).isEqualTo(UPDATED_COD_AMOUNT_PAID);
        assertThat(testExchangeOrder.getCodPaymentTxnRefId()).isEqualTo(UPDATED_COD_PAYMENT_TXN_REF_ID);
        assertThat(testExchangeOrder.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExchangeOrder.getCancellationReason()).isEqualTo(UPDATED_CANCELLATION_REASON);
        assertThat(testExchangeOrder.getExpectedPickupDate()).isEqualTo(UPDATED_EXPECTED_PICKUP_DATE);
        assertThat(testExchangeOrder.getCustomerPickupDate()).isEqualTo(UPDATED_CUSTOMER_PICKUP_DATE);
        assertThat(testExchangeOrder.getDcDeliveredDate()).isEqualTo(UPDATED_DC_DELIVERED_DATE);
        assertThat(testExchangeOrder.getDcPickupDate()).isEqualTo(UPDATED_DC_PICKUP_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToDc()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC);
        assertThat(testExchangeOrder.getVendorDeliveredDate()).isEqualTo(UPDATED_VENDOR_DELIVERED_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToVendor()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_VENDOR);
        assertThat(testExchangeOrder.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeOrder.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void putNonExistingExchangeOrder() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();
        exchangeOrder.setId(count.incrementAndGet());

        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeOrderDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchangeOrder() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();
        exchangeOrder.setId(count.incrementAndGet());

        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchangeOrder() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();
        exchangeOrder.setId(count.incrementAndGet());

        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOrderMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangeOrderWithPatch() throws Exception {
        // Initialize the database
        exchangeOrderRepository.saveAndFlush(exchangeOrder);

        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();

        // Update the exchangeOrder using partial update
        ExchangeOrder partialUpdatedExchangeOrder = new ExchangeOrder();
        partialUpdatedExchangeOrder.setId(exchangeOrder.getId());

        partialUpdatedExchangeOrder
            .clientOrderRefId(UPDATED_CLIENT_ORDER_REF_ID)
            .clientOrderDeliveryRefId(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID)
            .deliveryPartnerPhoneNo(UPDATED_DELIVERY_PARTNER_PHONE_NO)
            .codInitialPendingAmount(UPDATED_COD_INITIAL_PENDING_AMOUNT)
            .exchangeEstimateAmount(UPDATED_EXCHANGE_ESTIMATE_AMOUNT)
            .codAmountPaid(UPDATED_COD_AMOUNT_PAID)
            .codPaymentTxnRefId(UPDATED_COD_PAYMENT_TXN_REF_ID)
            .status(UPDATED_STATUS)
            .dcDeliveredDate(UPDATED_DC_DELIVERED_DATE)
            .dcPickupDate(UPDATED_DC_PICKUP_DATE)
            .exchangeFinalAmountToDc(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC)
            .vendorDeliveredDate(UPDATED_VENDOR_DELIVERED_DATE)
            .updatedAt(UPDATED_UPDATED_AT);

        restExchangeOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeOrder.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeOrder))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
        ExchangeOrder testExchangeOrder = exchangeOrderList.get(exchangeOrderList.size() - 1);
        assertThat(testExchangeOrder.getClientOrderRefId()).isEqualTo(UPDATED_CLIENT_ORDER_REF_ID);
        assertThat(testExchangeOrder.getCustomerPhoneNo()).isEqualTo(DEFAULT_CUSTOMER_PHONE_NO);
        assertThat(testExchangeOrder.getClientOrderDeliveryRefId()).isEqualTo(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID);
        assertThat(testExchangeOrder.getDeliveryPartnerPhoneNo()).isEqualTo(UPDATED_DELIVERY_PARTNER_PHONE_NO);
        assertThat(testExchangeOrder.getTotalOrderAmount()).isEqualTo(DEFAULT_TOTAL_ORDER_AMOUNT);
        assertThat(testExchangeOrder.getPaidAmount()).isEqualTo(DEFAULT_PAID_AMOUNT);
        assertThat(testExchangeOrder.getCodInitialPendingAmount()).isEqualTo(UPDATED_COD_INITIAL_PENDING_AMOUNT);
        assertThat(testExchangeOrder.getExchangeEstimateAmount()).isEqualTo(UPDATED_EXCHANGE_ESTIMATE_AMOUNT);
        assertThat(testExchangeOrder.getExchangeFinalAmountToUser()).isEqualTo(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_USER);
        assertThat(testExchangeOrder.getCodFinalAmount()).isEqualTo(DEFAULT_COD_FINAL_AMOUNT);
        assertThat(testExchangeOrder.getCodAmountPaid()).isEqualTo(UPDATED_COD_AMOUNT_PAID);
        assertThat(testExchangeOrder.getCodPaymentTxnRefId()).isEqualTo(UPDATED_COD_PAYMENT_TXN_REF_ID);
        assertThat(testExchangeOrder.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExchangeOrder.getCancellationReason()).isEqualTo(DEFAULT_CANCELLATION_REASON);
        assertThat(testExchangeOrder.getExpectedPickupDate()).isEqualTo(DEFAULT_EXPECTED_PICKUP_DATE);
        assertThat(testExchangeOrder.getCustomerPickupDate()).isEqualTo(DEFAULT_CUSTOMER_PICKUP_DATE);
        assertThat(testExchangeOrder.getDcDeliveredDate()).isEqualTo(UPDATED_DC_DELIVERED_DATE);
        assertThat(testExchangeOrder.getDcPickupDate()).isEqualTo(UPDATED_DC_PICKUP_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToDc()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC);
        assertThat(testExchangeOrder.getVendorDeliveredDate()).isEqualTo(UPDATED_VENDOR_DELIVERED_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToVendor()).isEqualTo(DEFAULT_EXCHANGE_FINAL_AMOUNT_TO_VENDOR);
        assertThat(testExchangeOrder.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangeOrder.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void fullUpdateExchangeOrderWithPatch() throws Exception {
        // Initialize the database
        exchangeOrderRepository.saveAndFlush(exchangeOrder);

        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();

        // Update the exchangeOrder using partial update
        ExchangeOrder partialUpdatedExchangeOrder = new ExchangeOrder();
        partialUpdatedExchangeOrder.setId(exchangeOrder.getId());

        partialUpdatedExchangeOrder
            .clientOrderRefId(UPDATED_CLIENT_ORDER_REF_ID)
            .customerPhoneNo(UPDATED_CUSTOMER_PHONE_NO)
            .clientOrderDeliveryRefId(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID)
            .deliveryPartnerPhoneNo(UPDATED_DELIVERY_PARTNER_PHONE_NO)
            .totalOrderAmount(UPDATED_TOTAL_ORDER_AMOUNT)
            .paidAmount(UPDATED_PAID_AMOUNT)
            .codInitialPendingAmount(UPDATED_COD_INITIAL_PENDING_AMOUNT)
            .exchangeEstimateAmount(UPDATED_EXCHANGE_ESTIMATE_AMOUNT)
            .exchangeFinalAmountToUser(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_USER)
            .codFinalAmount(UPDATED_COD_FINAL_AMOUNT)
            .codAmountPaid(UPDATED_COD_AMOUNT_PAID)
            .codPaymentTxnRefId(UPDATED_COD_PAYMENT_TXN_REF_ID)
            .status(UPDATED_STATUS)
            .cancellationReason(UPDATED_CANCELLATION_REASON)
            .expectedPickupDate(UPDATED_EXPECTED_PICKUP_DATE)
            .customerPickupDate(UPDATED_CUSTOMER_PICKUP_DATE)
            .dcDeliveredDate(UPDATED_DC_DELIVERED_DATE)
            .dcPickupDate(UPDATED_DC_PICKUP_DATE)
            .exchangeFinalAmountToDc(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC)
            .vendorDeliveredDate(UPDATED_VENDOR_DELIVERED_DATE)
            .exchangeFinalAmountToVendor(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_VENDOR)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restExchangeOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeOrder.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeOrder))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
        ExchangeOrder testExchangeOrder = exchangeOrderList.get(exchangeOrderList.size() - 1);
        assertThat(testExchangeOrder.getClientOrderRefId()).isEqualTo(UPDATED_CLIENT_ORDER_REF_ID);
        assertThat(testExchangeOrder.getCustomerPhoneNo()).isEqualTo(UPDATED_CUSTOMER_PHONE_NO);
        assertThat(testExchangeOrder.getClientOrderDeliveryRefId()).isEqualTo(UPDATED_CLIENT_ORDER_DELIVERY_REF_ID);
        assertThat(testExchangeOrder.getDeliveryPartnerPhoneNo()).isEqualTo(UPDATED_DELIVERY_PARTNER_PHONE_NO);
        assertThat(testExchangeOrder.getTotalOrderAmount()).isEqualTo(UPDATED_TOTAL_ORDER_AMOUNT);
        assertThat(testExchangeOrder.getPaidAmount()).isEqualTo(UPDATED_PAID_AMOUNT);
        assertThat(testExchangeOrder.getCodInitialPendingAmount()).isEqualTo(UPDATED_COD_INITIAL_PENDING_AMOUNT);
        assertThat(testExchangeOrder.getExchangeEstimateAmount()).isEqualTo(UPDATED_EXCHANGE_ESTIMATE_AMOUNT);
        assertThat(testExchangeOrder.getExchangeFinalAmountToUser()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_USER);
        assertThat(testExchangeOrder.getCodFinalAmount()).isEqualTo(UPDATED_COD_FINAL_AMOUNT);
        assertThat(testExchangeOrder.getCodAmountPaid()).isEqualTo(UPDATED_COD_AMOUNT_PAID);
        assertThat(testExchangeOrder.getCodPaymentTxnRefId()).isEqualTo(UPDATED_COD_PAYMENT_TXN_REF_ID);
        assertThat(testExchangeOrder.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExchangeOrder.getCancellationReason()).isEqualTo(UPDATED_CANCELLATION_REASON);
        assertThat(testExchangeOrder.getExpectedPickupDate()).isEqualTo(UPDATED_EXPECTED_PICKUP_DATE);
        assertThat(testExchangeOrder.getCustomerPickupDate()).isEqualTo(UPDATED_CUSTOMER_PICKUP_DATE);
        assertThat(testExchangeOrder.getDcDeliveredDate()).isEqualTo(UPDATED_DC_DELIVERED_DATE);
        assertThat(testExchangeOrder.getDcPickupDate()).isEqualTo(UPDATED_DC_PICKUP_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToDc()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_DC);
        assertThat(testExchangeOrder.getVendorDeliveredDate()).isEqualTo(UPDATED_VENDOR_DELIVERED_DATE);
        assertThat(testExchangeOrder.getExchangeFinalAmountToVendor()).isEqualTo(UPDATED_EXCHANGE_FINAL_AMOUNT_TO_VENDOR);
        assertThat(testExchangeOrder.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeOrder.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    void patchNonExistingExchangeOrder() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();
        exchangeOrder.setId(count.incrementAndGet());

        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangeOrderDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchangeOrder() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();
        exchangeOrder.setId(count.incrementAndGet());

        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchangeOrder() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOrderRepository.findAll().size();
        exchangeOrder.setId(count.incrementAndGet());

        // Create the ExchangeOrder
        ExchangeOrderDTO exchangeOrderDTO = exchangeOrderMapper.toDto(exchangeOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOrderMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOrderDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeOrder in the database
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchangeOrder() throws Exception {
        // Initialize the database
        exchangeOrderRepository.saveAndFlush(exchangeOrder);

        int databaseSizeBeforeDelete = exchangeOrderRepository.findAll().size();

        // Delete the exchangeOrder
        restExchangeOrderMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchangeOrder.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangeOrder> exchangeOrderList = exchangeOrderRepository.findAll();
        assertThat(exchangeOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
