package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSession;
import com.reliance.retail.jmd.exchange.domain.enumeration.AnswerSessionType;
import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerSessionRepository;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSessionDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeAnswerSessionMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangeAnswerSessionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangeAnswerSessionResourceIT {

    private static final Integer DEFAULT_PHONE_NO = 1;
    private static final Integer UPDATED_PHONE_NO = 2;

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_BROWSER_SESSION_ID = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_SESSION_ID = "BBBBBBBBBB";

    private static final AnswerSessionType DEFAULT_SESSION_TYPE = AnswerSessionType.User;
    private static final AnswerSessionType UPDATED_SESSION_TYPE = AnswerSessionType.DeliveryPartner;

    private static final String ENTITY_API_URL = "/api/exchange-answer-sessions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExchangeAnswerSessionRepository exchangeAnswerSessionRepository;

    @Autowired
    private ExchangeAnswerSessionMapper exchangeAnswerSessionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangeAnswerSessionMockMvc;

    private ExchangeAnswerSession exchangeAnswerSession;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeAnswerSession createEntity(EntityManager em) {
        ExchangeAnswerSession exchangeAnswerSession = new ExchangeAnswerSession()
            .phoneNo(DEFAULT_PHONE_NO)
            .userId(DEFAULT_USER_ID)
            .browserSessionId(DEFAULT_BROWSER_SESSION_ID)
            .sessionType(DEFAULT_SESSION_TYPE);
        return exchangeAnswerSession;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeAnswerSession createUpdatedEntity(EntityManager em) {
        ExchangeAnswerSession exchangeAnswerSession = new ExchangeAnswerSession()
            .phoneNo(UPDATED_PHONE_NO)
            .userId(UPDATED_USER_ID)
            .browserSessionId(UPDATED_BROWSER_SESSION_ID)
            .sessionType(UPDATED_SESSION_TYPE);
        return exchangeAnswerSession;
    }

    @BeforeEach
    public void initTest() {
        exchangeAnswerSession = createEntity(em);
    }

    @Test
    @Transactional
    void createExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeCreate = exchangeAnswerSessionRepository.findAll().size();
        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);
        restExchangeAnswerSessionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangeAnswerSession testExchangeAnswerSession = exchangeAnswerSessionList.get(exchangeAnswerSessionList.size() - 1);
        assertThat(testExchangeAnswerSession.getPhoneNo()).isEqualTo(DEFAULT_PHONE_NO);
        assertThat(testExchangeAnswerSession.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testExchangeAnswerSession.getBrowserSessionId()).isEqualTo(DEFAULT_BROWSER_SESSION_ID);
        assertThat(testExchangeAnswerSession.getSessionType()).isEqualTo(DEFAULT_SESSION_TYPE);
    }

    @Test
    @Transactional
    void createExchangeAnswerSessionWithExistingId() throws Exception {
        // Create the ExchangeAnswerSession with an existing ID
        exchangeAnswerSession.setId(1L);
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        int databaseSizeBeforeCreate = exchangeAnswerSessionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeAnswerSessionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllExchangeAnswerSessions() throws Exception {
        // Initialize the database
        exchangeAnswerSessionRepository.saveAndFlush(exchangeAnswerSession);

        // Get all the exchangeAnswerSessionList
        restExchangeAnswerSessionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeAnswerSession.getId().intValue())))
            .andExpect(jsonPath("$.[*].phoneNo").value(hasItem(DEFAULT_PHONE_NO)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].browserSessionId").value(hasItem(DEFAULT_BROWSER_SESSION_ID)))
            .andExpect(jsonPath("$.[*].sessionType").value(hasItem(DEFAULT_SESSION_TYPE.toString())));
    }

    @Test
    @Transactional
    void getExchangeAnswerSession() throws Exception {
        // Initialize the database
        exchangeAnswerSessionRepository.saveAndFlush(exchangeAnswerSession);

        // Get the exchangeAnswerSession
        restExchangeAnswerSessionMockMvc
            .perform(get(ENTITY_API_URL_ID, exchangeAnswerSession.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchangeAnswerSession.getId().intValue()))
            .andExpect(jsonPath("$.phoneNo").value(DEFAULT_PHONE_NO))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.browserSessionId").value(DEFAULT_BROWSER_SESSION_ID))
            .andExpect(jsonPath("$.sessionType").value(DEFAULT_SESSION_TYPE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingExchangeAnswerSession() throws Exception {
        // Get the exchangeAnswerSession
        restExchangeAnswerSessionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchangeAnswerSession() throws Exception {
        // Initialize the database
        exchangeAnswerSessionRepository.saveAndFlush(exchangeAnswerSession);

        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();

        // Update the exchangeAnswerSession
        ExchangeAnswerSession updatedExchangeAnswerSession = exchangeAnswerSessionRepository.findById(exchangeAnswerSession.getId()).get();
        // Disconnect from session so that the updates on updatedExchangeAnswerSession are not directly saved in db
        em.detach(updatedExchangeAnswerSession);
        updatedExchangeAnswerSession
            .phoneNo(UPDATED_PHONE_NO)
            .userId(UPDATED_USER_ID)
            .browserSessionId(UPDATED_BROWSER_SESSION_ID)
            .sessionType(UPDATED_SESSION_TYPE);
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(updatedExchangeAnswerSession);

        restExchangeAnswerSessionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeAnswerSessionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswerSession testExchangeAnswerSession = exchangeAnswerSessionList.get(exchangeAnswerSessionList.size() - 1);
        assertThat(testExchangeAnswerSession.getPhoneNo()).isEqualTo(UPDATED_PHONE_NO);
        assertThat(testExchangeAnswerSession.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testExchangeAnswerSession.getBrowserSessionId()).isEqualTo(UPDATED_BROWSER_SESSION_ID);
        assertThat(testExchangeAnswerSession.getSessionType()).isEqualTo(UPDATED_SESSION_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();
        exchangeAnswerSession.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeAnswerSessionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeAnswerSessionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();
        exchangeAnswerSession.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSessionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();
        exchangeAnswerSession.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSessionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangeAnswerSessionWithPatch() throws Exception {
        // Initialize the database
        exchangeAnswerSessionRepository.saveAndFlush(exchangeAnswerSession);

        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();

        // Update the exchangeAnswerSession using partial update
        ExchangeAnswerSession partialUpdatedExchangeAnswerSession = new ExchangeAnswerSession();
        partialUpdatedExchangeAnswerSession.setId(exchangeAnswerSession.getId());

        partialUpdatedExchangeAnswerSession.sessionType(UPDATED_SESSION_TYPE);

        restExchangeAnswerSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeAnswerSession.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeAnswerSession))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswerSession testExchangeAnswerSession = exchangeAnswerSessionList.get(exchangeAnswerSessionList.size() - 1);
        assertThat(testExchangeAnswerSession.getPhoneNo()).isEqualTo(DEFAULT_PHONE_NO);
        assertThat(testExchangeAnswerSession.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testExchangeAnswerSession.getBrowserSessionId()).isEqualTo(DEFAULT_BROWSER_SESSION_ID);
        assertThat(testExchangeAnswerSession.getSessionType()).isEqualTo(UPDATED_SESSION_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateExchangeAnswerSessionWithPatch() throws Exception {
        // Initialize the database
        exchangeAnswerSessionRepository.saveAndFlush(exchangeAnswerSession);

        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();

        // Update the exchangeAnswerSession using partial update
        ExchangeAnswerSession partialUpdatedExchangeAnswerSession = new ExchangeAnswerSession();
        partialUpdatedExchangeAnswerSession.setId(exchangeAnswerSession.getId());

        partialUpdatedExchangeAnswerSession
            .phoneNo(UPDATED_PHONE_NO)
            .userId(UPDATED_USER_ID)
            .browserSessionId(UPDATED_BROWSER_SESSION_ID)
            .sessionType(UPDATED_SESSION_TYPE);

        restExchangeAnswerSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeAnswerSession.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeAnswerSession))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswerSession testExchangeAnswerSession = exchangeAnswerSessionList.get(exchangeAnswerSessionList.size() - 1);
        assertThat(testExchangeAnswerSession.getPhoneNo()).isEqualTo(UPDATED_PHONE_NO);
        assertThat(testExchangeAnswerSession.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testExchangeAnswerSession.getBrowserSessionId()).isEqualTo(UPDATED_BROWSER_SESSION_ID);
        assertThat(testExchangeAnswerSession.getSessionType()).isEqualTo(UPDATED_SESSION_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();
        exchangeAnswerSession.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeAnswerSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangeAnswerSessionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();
        exchangeAnswerSession.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchangeAnswerSession() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSessionRepository.findAll().size();
        exchangeAnswerSession.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSession
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO = exchangeAnswerSessionMapper.toDto(exchangeAnswerSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSessionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSessionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeAnswerSession in the database
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchangeAnswerSession() throws Exception {
        // Initialize the database
        exchangeAnswerSessionRepository.saveAndFlush(exchangeAnswerSession);

        int databaseSizeBeforeDelete = exchangeAnswerSessionRepository.findAll().size();

        // Delete the exchangeAnswerSession
        restExchangeAnswerSessionMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchangeAnswerSession.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangeAnswerSession> exchangeAnswerSessionList = exchangeAnswerSessionRepository.findAll();
        assertThat(exchangeAnswerSessionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
