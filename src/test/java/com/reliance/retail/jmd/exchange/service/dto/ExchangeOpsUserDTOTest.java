package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeOpsUserDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeOpsUserDTO.class);
        ExchangeOpsUserDTO exchangeOpsUserDTO1 = new ExchangeOpsUserDTO();
        exchangeOpsUserDTO1.setId(1L);
        ExchangeOpsUserDTO exchangeOpsUserDTO2 = new ExchangeOpsUserDTO();
        assertThat(exchangeOpsUserDTO1).isNotEqualTo(exchangeOpsUserDTO2);
        exchangeOpsUserDTO2.setId(exchangeOpsUserDTO1.getId());
        assertThat(exchangeOpsUserDTO1).isEqualTo(exchangeOpsUserDTO2);
        exchangeOpsUserDTO2.setId(2L);
        assertThat(exchangeOpsUserDTO1).isNotEqualTo(exchangeOpsUserDTO2);
        exchangeOpsUserDTO1.setId(null);
        assertThat(exchangeOpsUserDTO1).isNotEqualTo(exchangeOpsUserDTO2);
    }
}
