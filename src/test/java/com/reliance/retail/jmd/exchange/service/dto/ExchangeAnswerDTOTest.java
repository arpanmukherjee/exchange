package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeAnswerDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeAnswerDTO.class);
        ExchangeAnswerDTO exchangeAnswerDTO1 = new ExchangeAnswerDTO();
        exchangeAnswerDTO1.setId(1L);
        ExchangeAnswerDTO exchangeAnswerDTO2 = new ExchangeAnswerDTO();
        assertThat(exchangeAnswerDTO1).isNotEqualTo(exchangeAnswerDTO2);
        exchangeAnswerDTO2.setId(exchangeAnswerDTO1.getId());
        assertThat(exchangeAnswerDTO1).isEqualTo(exchangeAnswerDTO2);
        exchangeAnswerDTO2.setId(2L);
        assertThat(exchangeAnswerDTO1).isNotEqualTo(exchangeAnswerDTO2);
        exchangeAnswerDTO1.setId(null);
        assertThat(exchangeAnswerDTO1).isNotEqualTo(exchangeAnswerDTO2);
    }
}
