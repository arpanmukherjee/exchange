package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ProductContextDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductContextDTO.class);
        ProductContextDTO productContextDTO1 = new ProductContextDTO();
        productContextDTO1.setId(1L);
        ProductContextDTO productContextDTO2 = new ProductContextDTO();
        assertThat(productContextDTO1).isNotEqualTo(productContextDTO2);
        productContextDTO2.setId(productContextDTO1.getId());
        assertThat(productContextDTO1).isEqualTo(productContextDTO2);
        productContextDTO2.setId(2L);
        assertThat(productContextDTO1).isNotEqualTo(productContextDTO2);
        productContextDTO1.setId(null);
        assertThat(productContextDTO1).isNotEqualTo(productContextDTO2);
    }
}
