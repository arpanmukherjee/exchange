package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.VendorHub;
import com.reliance.retail.jmd.exchange.repository.VendorHubRepository;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubDTO;
import com.reliance.retail.jmd.exchange.service.mapper.VendorHubMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VendorHubResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class VendorHubResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/vendor-hubs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VendorHubRepository vendorHubRepository;

    @Autowired
    private VendorHubMapper vendorHubMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVendorHubMockMvc;

    private VendorHub vendorHub;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VendorHub createEntity(EntityManager em) {
        VendorHub vendorHub = new VendorHub()
            .name(DEFAULT_NAME)
            .isActive(DEFAULT_IS_ACTIVE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return vendorHub;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VendorHub createUpdatedEntity(EntityManager em) {
        VendorHub vendorHub = new VendorHub()
            .name(UPDATED_NAME)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return vendorHub;
    }

    @BeforeEach
    public void initTest() {
        vendorHub = createEntity(em);
    }

    @Test
    @Transactional
    void createVendorHub() throws Exception {
        int databaseSizeBeforeCreate = vendorHubRepository.findAll().size();
        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);
        restVendorHubMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendorHubDTO)))
            .andExpect(status().isCreated());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeCreate + 1);
        VendorHub testVendorHub = vendorHubList.get(vendorHubList.size() - 1);
        assertThat(testVendorHub.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVendorHub.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testVendorHub.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testVendorHub.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testVendorHub.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testVendorHub.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createVendorHubWithExistingId() throws Exception {
        // Create the VendorHub with an existing ID
        vendorHub.setId(1L);
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        int databaseSizeBeforeCreate = vendorHubRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVendorHubMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendorHubDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllVendorHubs() throws Exception {
        // Initialize the database
        vendorHubRepository.saveAndFlush(vendorHub);

        // Get all the vendorHubList
        restVendorHubMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vendorHub.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getVendorHub() throws Exception {
        // Initialize the database
        vendorHubRepository.saveAndFlush(vendorHub);

        // Get the vendorHub
        restVendorHubMockMvc
            .perform(get(ENTITY_API_URL_ID, vendorHub.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vendorHub.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingVendorHub() throws Exception {
        // Get the vendorHub
        restVendorHubMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingVendorHub() throws Exception {
        // Initialize the database
        vendorHubRepository.saveAndFlush(vendorHub);

        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();

        // Update the vendorHub
        VendorHub updatedVendorHub = vendorHubRepository.findById(vendorHub.getId()).get();
        // Disconnect from session so that the updates on updatedVendorHub are not directly saved in db
        em.detach(updatedVendorHub);
        updatedVendorHub
            .name(UPDATED_NAME)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(updatedVendorHub);

        restVendorHubMockMvc
            .perform(
                put(ENTITY_API_URL_ID, vendorHubDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubDTO))
            )
            .andExpect(status().isOk());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
        VendorHub testVendorHub = vendorHubList.get(vendorHubList.size() - 1);
        assertThat(testVendorHub.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVendorHub.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testVendorHub.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testVendorHub.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testVendorHub.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVendorHub.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingVendorHub() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();
        vendorHub.setId(count.incrementAndGet());

        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVendorHubMockMvc
            .perform(
                put(ENTITY_API_URL_ID, vendorHubDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchVendorHub() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();
        vendorHub.setId(count.incrementAndGet());

        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamVendorHub() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();
        vendorHub.setId(count.incrementAndGet());

        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendorHubDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVendorHubWithPatch() throws Exception {
        // Initialize the database
        vendorHubRepository.saveAndFlush(vendorHub);

        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();

        // Update the vendorHub using partial update
        VendorHub partialUpdatedVendorHub = new VendorHub();
        partialUpdatedVendorHub.setId(vendorHub.getId());

        partialUpdatedVendorHub.name(UPDATED_NAME).createdBy(UPDATED_CREATED_BY);

        restVendorHubMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVendorHub.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVendorHub))
            )
            .andExpect(status().isOk());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
        VendorHub testVendorHub = vendorHubList.get(vendorHubList.size() - 1);
        assertThat(testVendorHub.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVendorHub.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testVendorHub.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testVendorHub.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testVendorHub.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVendorHub.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateVendorHubWithPatch() throws Exception {
        // Initialize the database
        vendorHubRepository.saveAndFlush(vendorHub);

        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();

        // Update the vendorHub using partial update
        VendorHub partialUpdatedVendorHub = new VendorHub();
        partialUpdatedVendorHub.setId(vendorHub.getId());

        partialUpdatedVendorHub
            .name(UPDATED_NAME)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restVendorHubMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVendorHub.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVendorHub))
            )
            .andExpect(status().isOk());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
        VendorHub testVendorHub = vendorHubList.get(vendorHubList.size() - 1);
        assertThat(testVendorHub.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVendorHub.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testVendorHub.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testVendorHub.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testVendorHub.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVendorHub.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingVendorHub() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();
        vendorHub.setId(count.incrementAndGet());

        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVendorHubMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, vendorHubDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchVendorHub() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();
        vendorHub.setId(count.incrementAndGet());

        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamVendorHub() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubRepository.findAll().size();
        vendorHub.setId(count.incrementAndGet());

        // Create the VendorHub
        VendorHubDTO vendorHubDTO = vendorHubMapper.toDto(vendorHub);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(vendorHubDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the VendorHub in the database
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteVendorHub() throws Exception {
        // Initialize the database
        vendorHubRepository.saveAndFlush(vendorHub);

        int databaseSizeBeforeDelete = vendorHubRepository.findAll().size();

        // Delete the vendorHub
        restVendorHubMockMvc
            .perform(delete(ENTITY_API_URL_ID, vendorHub.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VendorHub> vendorHubList = vendorHubRepository.findAll();
        assertThat(vendorHubList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
