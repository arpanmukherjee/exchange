package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ProductContext;
import com.reliance.retail.jmd.exchange.domain.enumeration.CategoryConetxtType;
import com.reliance.retail.jmd.exchange.repository.ProductContextRepository;
import com.reliance.retail.jmd.exchange.service.dto.ProductContextDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ProductContextMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ProductContextResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ProductContextResourceIT {

    private static final CategoryConetxtType DEFAULT_CATEGORY_CONTEXT_TYPE = CategoryConetxtType.REFRIGERATOR;
    private static final CategoryConetxtType UPDATED_CATEGORY_CONTEXT_TYPE = CategoryConetxtType.WASHING_MACHINE;

    private static final String DEFAULT_CLIENT_PRODUCT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_PRODUCT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_PRODUCT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_PRODUCT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_PRODUCT_URL = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_PRODUCT_URL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/product-contexts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ProductContextRepository productContextRepository;

    @Autowired
    private ProductContextMapper productContextMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductContextMockMvc;

    private ProductContext productContext;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductContext createEntity(EntityManager em) {
        ProductContext productContext = new ProductContext()
            .categoryContextType(DEFAULT_CATEGORY_CONTEXT_TYPE)
            .clientProductId(DEFAULT_CLIENT_PRODUCT_ID)
            .clientProductName(DEFAULT_CLIENT_PRODUCT_NAME)
            .clientProductUrl(DEFAULT_CLIENT_PRODUCT_URL)
            .isActive(DEFAULT_IS_ACTIVE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return productContext;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductContext createUpdatedEntity(EntityManager em) {
        ProductContext productContext = new ProductContext()
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .clientProductId(UPDATED_CLIENT_PRODUCT_ID)
            .clientProductName(UPDATED_CLIENT_PRODUCT_NAME)
            .clientProductUrl(UPDATED_CLIENT_PRODUCT_URL)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return productContext;
    }

    @BeforeEach
    public void initTest() {
        productContext = createEntity(em);
    }

    @Test
    @Transactional
    void createProductContext() throws Exception {
        int databaseSizeBeforeCreate = productContextRepository.findAll().size();
        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);
        restProductContextMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeCreate + 1);
        ProductContext testProductContext = productContextList.get(productContextList.size() - 1);
        assertThat(testProductContext.getCategoryContextType()).isEqualTo(DEFAULT_CATEGORY_CONTEXT_TYPE);
        assertThat(testProductContext.getClientProductId()).isEqualTo(DEFAULT_CLIENT_PRODUCT_ID);
        assertThat(testProductContext.getClientProductName()).isEqualTo(DEFAULT_CLIENT_PRODUCT_NAME);
        assertThat(testProductContext.getClientProductUrl()).isEqualTo(DEFAULT_CLIENT_PRODUCT_URL);
        assertThat(testProductContext.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testProductContext.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testProductContext.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testProductContext.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testProductContext.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createProductContextWithExistingId() throws Exception {
        // Create the ProductContext with an existing ID
        productContext.setId(1L);
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        int databaseSizeBeforeCreate = productContextRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductContextMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllProductContexts() throws Exception {
        // Initialize the database
        productContextRepository.saveAndFlush(productContext);

        // Get all the productContextList
        restProductContextMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productContext.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryContextType").value(hasItem(DEFAULT_CATEGORY_CONTEXT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].clientProductId").value(hasItem(DEFAULT_CLIENT_PRODUCT_ID)))
            .andExpect(jsonPath("$.[*].clientProductName").value(hasItem(DEFAULT_CLIENT_PRODUCT_NAME)))
            .andExpect(jsonPath("$.[*].clientProductUrl").value(hasItem(DEFAULT_CLIENT_PRODUCT_URL)))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getProductContext() throws Exception {
        // Initialize the database
        productContextRepository.saveAndFlush(productContext);

        // Get the productContext
        restProductContextMockMvc
            .perform(get(ENTITY_API_URL_ID, productContext.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productContext.getId().intValue()))
            .andExpect(jsonPath("$.categoryContextType").value(DEFAULT_CATEGORY_CONTEXT_TYPE.toString()))
            .andExpect(jsonPath("$.clientProductId").value(DEFAULT_CLIENT_PRODUCT_ID))
            .andExpect(jsonPath("$.clientProductName").value(DEFAULT_CLIENT_PRODUCT_NAME))
            .andExpect(jsonPath("$.clientProductUrl").value(DEFAULT_CLIENT_PRODUCT_URL))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingProductContext() throws Exception {
        // Get the productContext
        restProductContextMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingProductContext() throws Exception {
        // Initialize the database
        productContextRepository.saveAndFlush(productContext);

        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();

        // Update the productContext
        ProductContext updatedProductContext = productContextRepository.findById(productContext.getId()).get();
        // Disconnect from session so that the updates on updatedProductContext are not directly saved in db
        em.detach(updatedProductContext);
        updatedProductContext
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .clientProductId(UPDATED_CLIENT_PRODUCT_ID)
            .clientProductName(UPDATED_CLIENT_PRODUCT_NAME)
            .clientProductUrl(UPDATED_CLIENT_PRODUCT_URL)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        ProductContextDTO productContextDTO = productContextMapper.toDto(updatedProductContext);

        restProductContextMockMvc
            .perform(
                put(ENTITY_API_URL_ID, productContextDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isOk());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
        ProductContext testProductContext = productContextList.get(productContextList.size() - 1);
        assertThat(testProductContext.getCategoryContextType()).isEqualTo(UPDATED_CATEGORY_CONTEXT_TYPE);
        assertThat(testProductContext.getClientProductId()).isEqualTo(UPDATED_CLIENT_PRODUCT_ID);
        assertThat(testProductContext.getClientProductName()).isEqualTo(UPDATED_CLIENT_PRODUCT_NAME);
        assertThat(testProductContext.getClientProductUrl()).isEqualTo(UPDATED_CLIENT_PRODUCT_URL);
        assertThat(testProductContext.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testProductContext.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testProductContext.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testProductContext.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testProductContext.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingProductContext() throws Exception {
        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();
        productContext.setId(count.incrementAndGet());

        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductContextMockMvc
            .perform(
                put(ENTITY_API_URL_ID, productContextDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProductContext() throws Exception {
        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();
        productContext.setId(count.incrementAndGet());

        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductContextMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProductContext() throws Exception {
        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();
        productContext.setId(count.incrementAndGet());

        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductContextMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProductContextWithPatch() throws Exception {
        // Initialize the database
        productContextRepository.saveAndFlush(productContext);

        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();

        // Update the productContext using partial update
        ProductContext partialUpdatedProductContext = new ProductContext();
        partialUpdatedProductContext.setId(productContext.getId());

        partialUpdatedProductContext
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .clientProductId(UPDATED_CLIENT_PRODUCT_ID)
            .clientProductName(UPDATED_CLIENT_PRODUCT_NAME)
            .clientProductUrl(UPDATED_CLIENT_PRODUCT_URL)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedBy(UPDATED_UPDATED_BY);

        restProductContextMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductContext.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProductContext))
            )
            .andExpect(status().isOk());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
        ProductContext testProductContext = productContextList.get(productContextList.size() - 1);
        assertThat(testProductContext.getCategoryContextType()).isEqualTo(UPDATED_CATEGORY_CONTEXT_TYPE);
        assertThat(testProductContext.getClientProductId()).isEqualTo(UPDATED_CLIENT_PRODUCT_ID);
        assertThat(testProductContext.getClientProductName()).isEqualTo(UPDATED_CLIENT_PRODUCT_NAME);
        assertThat(testProductContext.getClientProductUrl()).isEqualTo(UPDATED_CLIENT_PRODUCT_URL);
        assertThat(testProductContext.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testProductContext.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testProductContext.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testProductContext.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testProductContext.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateProductContextWithPatch() throws Exception {
        // Initialize the database
        productContextRepository.saveAndFlush(productContext);

        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();

        // Update the productContext using partial update
        ProductContext partialUpdatedProductContext = new ProductContext();
        partialUpdatedProductContext.setId(productContext.getId());

        partialUpdatedProductContext
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .clientProductId(UPDATED_CLIENT_PRODUCT_ID)
            .clientProductName(UPDATED_CLIENT_PRODUCT_NAME)
            .clientProductUrl(UPDATED_CLIENT_PRODUCT_URL)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restProductContextMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductContext.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProductContext))
            )
            .andExpect(status().isOk());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
        ProductContext testProductContext = productContextList.get(productContextList.size() - 1);
        assertThat(testProductContext.getCategoryContextType()).isEqualTo(UPDATED_CATEGORY_CONTEXT_TYPE);
        assertThat(testProductContext.getClientProductId()).isEqualTo(UPDATED_CLIENT_PRODUCT_ID);
        assertThat(testProductContext.getClientProductName()).isEqualTo(UPDATED_CLIENT_PRODUCT_NAME);
        assertThat(testProductContext.getClientProductUrl()).isEqualTo(UPDATED_CLIENT_PRODUCT_URL);
        assertThat(testProductContext.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testProductContext.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testProductContext.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testProductContext.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testProductContext.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingProductContext() throws Exception {
        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();
        productContext.setId(count.incrementAndGet());

        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductContextMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, productContextDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProductContext() throws Exception {
        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();
        productContext.setId(count.incrementAndGet());

        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductContextMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProductContext() throws Exception {
        int databaseSizeBeforeUpdate = productContextRepository.findAll().size();
        productContext.setId(count.incrementAndGet());

        // Create the ProductContext
        ProductContextDTO productContextDTO = productContextMapper.toDto(productContext);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductContextMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productContextDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductContext in the database
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProductContext() throws Exception {
        // Initialize the database
        productContextRepository.saveAndFlush(productContext);

        int databaseSizeBeforeDelete = productContextRepository.findAll().size();

        // Delete the productContext
        restProductContextMockMvc
            .perform(delete(ENTITY_API_URL_ID, productContext.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductContext> productContextList = productContextRepository.findAll();
        assertThat(productContextList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
