package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangePriceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangePrice.class);
        ExchangePrice exchangePrice1 = new ExchangePrice();
        exchangePrice1.setId(1L);
        ExchangePrice exchangePrice2 = new ExchangePrice();
        exchangePrice2.setId(exchangePrice1.getId());
        assertThat(exchangePrice1).isEqualTo(exchangePrice2);
        exchangePrice2.setId(2L);
        assertThat(exchangePrice1).isNotEqualTo(exchangePrice2);
        exchangePrice1.setId(null);
        assertThat(exchangePrice1).isNotEqualTo(exchangePrice2);
    }
}
