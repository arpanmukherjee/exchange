package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VendorHubTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VendorHub.class);
        VendorHub vendorHub1 = new VendorHub();
        vendorHub1.setId(1L);
        VendorHub vendorHub2 = new VendorHub();
        vendorHub2.setId(vendorHub1.getId());
        assertThat(vendorHub1).isEqualTo(vendorHub2);
        vendorHub2.setId(2L);
        assertThat(vendorHub1).isNotEqualTo(vendorHub2);
        vendorHub1.setId(null);
        assertThat(vendorHub1).isNotEqualTo(vendorHub2);
    }
}
