package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ExchangePrice;
import com.reliance.retail.jmd.exchange.domain.enumeration.PriceMetricStatus;
import com.reliance.retail.jmd.exchange.repository.ExchangePriceRepository;
import com.reliance.retail.jmd.exchange.service.dto.ExchangePriceDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangePriceMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangePriceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangePriceResourceIT {

    private static final Double DEFAULT_EXCHANGE_AMOUNT = 1D;
    private static final Double UPDATED_EXCHANGE_AMOUNT = 2D;

    private static final PriceMetricStatus DEFAULT_STATUS = PriceMetricStatus.Approved;
    private static final PriceMetricStatus UPDATED_STATUS = PriceMetricStatus.Rejected;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/exchange-prices";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExchangePriceRepository exchangePriceRepository;

    @Autowired
    private ExchangePriceMapper exchangePriceMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangePriceMockMvc;

    private ExchangePrice exchangePrice;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangePrice createEntity(EntityManager em) {
        ExchangePrice exchangePrice = new ExchangePrice()
            .exchangeAmount(DEFAULT_EXCHANGE_AMOUNT)
            .status(DEFAULT_STATUS)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return exchangePrice;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangePrice createUpdatedEntity(EntityManager em) {
        ExchangePrice exchangePrice = new ExchangePrice()
            .exchangeAmount(UPDATED_EXCHANGE_AMOUNT)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return exchangePrice;
    }

    @BeforeEach
    public void initTest() {
        exchangePrice = createEntity(em);
    }

    @Test
    @Transactional
    void createExchangePrice() throws Exception {
        int databaseSizeBeforeCreate = exchangePriceRepository.findAll().size();
        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);
        restExchangePriceMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangePrice testExchangePrice = exchangePriceList.get(exchangePriceList.size() - 1);
        assertThat(testExchangePrice.getExchangeAmount()).isEqualTo(DEFAULT_EXCHANGE_AMOUNT);
        assertThat(testExchangePrice.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testExchangePrice.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangePrice.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testExchangePrice.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testExchangePrice.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createExchangePriceWithExistingId() throws Exception {
        // Create the ExchangePrice with an existing ID
        exchangePrice.setId(1L);
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        int databaseSizeBeforeCreate = exchangePriceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangePriceMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllExchangePrices() throws Exception {
        // Initialize the database
        exchangePriceRepository.saveAndFlush(exchangePrice);

        // Get all the exchangePriceList
        restExchangePriceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangePrice.getId().intValue())))
            .andExpect(jsonPath("$.[*].exchangeAmount").value(hasItem(DEFAULT_EXCHANGE_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getExchangePrice() throws Exception {
        // Initialize the database
        exchangePriceRepository.saveAndFlush(exchangePrice);

        // Get the exchangePrice
        restExchangePriceMockMvc
            .perform(get(ENTITY_API_URL_ID, exchangePrice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchangePrice.getId().intValue()))
            .andExpect(jsonPath("$.exchangeAmount").value(DEFAULT_EXCHANGE_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingExchangePrice() throws Exception {
        // Get the exchangePrice
        restExchangePriceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchangePrice() throws Exception {
        // Initialize the database
        exchangePriceRepository.saveAndFlush(exchangePrice);

        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();

        // Update the exchangePrice
        ExchangePrice updatedExchangePrice = exchangePriceRepository.findById(exchangePrice.getId()).get();
        // Disconnect from session so that the updates on updatedExchangePrice are not directly saved in db
        em.detach(updatedExchangePrice);
        updatedExchangePrice
            .exchangeAmount(UPDATED_EXCHANGE_AMOUNT)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(updatedExchangePrice);

        restExchangePriceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangePriceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
        ExchangePrice testExchangePrice = exchangePriceList.get(exchangePriceList.size() - 1);
        assertThat(testExchangePrice.getExchangeAmount()).isEqualTo(UPDATED_EXCHANGE_AMOUNT);
        assertThat(testExchangePrice.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExchangePrice.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangePrice.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangePrice.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangePrice.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingExchangePrice() throws Exception {
        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();
        exchangePrice.setId(count.incrementAndGet());

        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangePriceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangePriceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchangePrice() throws Exception {
        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();
        exchangePrice.setId(count.incrementAndGet());

        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangePriceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchangePrice() throws Exception {
        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();
        exchangePrice.setId(count.incrementAndGet());

        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangePriceMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangePriceWithPatch() throws Exception {
        // Initialize the database
        exchangePriceRepository.saveAndFlush(exchangePrice);

        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();

        // Update the exchangePrice using partial update
        ExchangePrice partialUpdatedExchangePrice = new ExchangePrice();
        partialUpdatedExchangePrice.setId(exchangePrice.getId());

        partialUpdatedExchangePrice.status(UPDATED_STATUS);

        restExchangePriceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangePrice.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangePrice))
            )
            .andExpect(status().isOk());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
        ExchangePrice testExchangePrice = exchangePriceList.get(exchangePriceList.size() - 1);
        assertThat(testExchangePrice.getExchangeAmount()).isEqualTo(DEFAULT_EXCHANGE_AMOUNT);
        assertThat(testExchangePrice.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExchangePrice.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangePrice.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testExchangePrice.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testExchangePrice.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateExchangePriceWithPatch() throws Exception {
        // Initialize the database
        exchangePriceRepository.saveAndFlush(exchangePrice);

        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();

        // Update the exchangePrice using partial update
        ExchangePrice partialUpdatedExchangePrice = new ExchangePrice();
        partialUpdatedExchangePrice.setId(exchangePrice.getId());

        partialUpdatedExchangePrice
            .exchangeAmount(UPDATED_EXCHANGE_AMOUNT)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restExchangePriceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangePrice.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangePrice))
            )
            .andExpect(status().isOk());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
        ExchangePrice testExchangePrice = exchangePriceList.get(exchangePriceList.size() - 1);
        assertThat(testExchangePrice.getExchangeAmount()).isEqualTo(UPDATED_EXCHANGE_AMOUNT);
        assertThat(testExchangePrice.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExchangePrice.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangePrice.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangePrice.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangePrice.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingExchangePrice() throws Exception {
        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();
        exchangePrice.setId(count.incrementAndGet());

        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangePriceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangePriceDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchangePrice() throws Exception {
        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();
        exchangePrice.setId(count.incrementAndGet());

        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangePriceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchangePrice() throws Exception {
        int databaseSizeBeforeUpdate = exchangePriceRepository.findAll().size();
        exchangePrice.setId(count.incrementAndGet());

        // Create the ExchangePrice
        ExchangePriceDTO exchangePriceDTO = exchangePriceMapper.toDto(exchangePrice);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangePriceMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangePriceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangePrice in the database
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchangePrice() throws Exception {
        // Initialize the database
        exchangePriceRepository.saveAndFlush(exchangePrice);

        int databaseSizeBeforeDelete = exchangePriceRepository.findAll().size();

        // Delete the exchangePrice
        restExchangePriceMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchangePrice.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangePrice> exchangePriceList = exchangePriceRepository.findAll();
        assertThat(exchangePriceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
