package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeAnswerSessionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeAnswerSessionDTO.class);
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO1 = new ExchangeAnswerSessionDTO();
        exchangeAnswerSessionDTO1.setId(1L);
        ExchangeAnswerSessionDTO exchangeAnswerSessionDTO2 = new ExchangeAnswerSessionDTO();
        assertThat(exchangeAnswerSessionDTO1).isNotEqualTo(exchangeAnswerSessionDTO2);
        exchangeAnswerSessionDTO2.setId(exchangeAnswerSessionDTO1.getId());
        assertThat(exchangeAnswerSessionDTO1).isEqualTo(exchangeAnswerSessionDTO2);
        exchangeAnswerSessionDTO2.setId(2L);
        assertThat(exchangeAnswerSessionDTO1).isNotEqualTo(exchangeAnswerSessionDTO2);
        exchangeAnswerSessionDTO1.setId(null);
        assertThat(exchangeAnswerSessionDTO1).isNotEqualTo(exchangeAnswerSessionDTO2);
    }
}
