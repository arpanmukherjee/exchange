package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ExchangeOpsUser;
import com.reliance.retail.jmd.exchange.domain.enumeration.Role;
import com.reliance.retail.jmd.exchange.repository.ExchangeOpsUserRepository;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeOpsUserDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeOpsUserMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangeOpsUserResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangeOpsUserResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = ">D+~a@<.<740";
    private static final String UPDATED_EMAIL = "XQ^G:t@L'.94ro31";

    private static final Long DEFAULT_PHONE = 1000000000L;
    private static final Long UPDATED_PHONE = 1000000001L;

    private static final Role DEFAULT_ROLE = Role.SuperAdmin;
    private static final Role UPDATED_ROLE = Role.ClientAdmin;

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/exchange-ops-users";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExchangeOpsUserRepository exchangeOpsUserRepository;

    @Autowired
    private ExchangeOpsUserMapper exchangeOpsUserMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangeOpsUserMockMvc;

    private ExchangeOpsUser exchangeOpsUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeOpsUser createEntity(EntityManager em) {
        ExchangeOpsUser exchangeOpsUser = new ExchangeOpsUser()
            .name(DEFAULT_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .role(DEFAULT_ROLE)
            .isActive(DEFAULT_IS_ACTIVE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return exchangeOpsUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeOpsUser createUpdatedEntity(EntityManager em) {
        ExchangeOpsUser exchangeOpsUser = new ExchangeOpsUser()
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .role(UPDATED_ROLE)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return exchangeOpsUser;
    }

    @BeforeEach
    public void initTest() {
        exchangeOpsUser = createEntity(em);
    }

    @Test
    @Transactional
    void createExchangeOpsUser() throws Exception {
        int databaseSizeBeforeCreate = exchangeOpsUserRepository.findAll().size();
        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);
        restExchangeOpsUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangeOpsUser testExchangeOpsUser = exchangeOpsUserList.get(exchangeOpsUserList.size() - 1);
        assertThat(testExchangeOpsUser.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExchangeOpsUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testExchangeOpsUser.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testExchangeOpsUser.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testExchangeOpsUser.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testExchangeOpsUser.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangeOpsUser.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testExchangeOpsUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testExchangeOpsUser.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createExchangeOpsUserWithExistingId() throws Exception {
        // Create the ExchangeOpsUser with an existing ID
        exchangeOpsUser.setId(1L);
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        int databaseSizeBeforeCreate = exchangeOpsUserRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeOpsUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = exchangeOpsUserRepository.findAll().size();
        // set the field null
        exchangeOpsUser.setName(null);

        // Create the ExchangeOpsUser, which fails.
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        restExchangeOpsUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = exchangeOpsUserRepository.findAll().size();
        // set the field null
        exchangeOpsUser.setPhone(null);

        // Create the ExchangeOpsUser, which fails.
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        restExchangeOpsUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllExchangeOpsUsers() throws Exception {
        // Initialize the database
        exchangeOpsUserRepository.saveAndFlush(exchangeOpsUser);

        // Get all the exchangeOpsUserList
        restExchangeOpsUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeOpsUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.intValue())))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getExchangeOpsUser() throws Exception {
        // Initialize the database
        exchangeOpsUserRepository.saveAndFlush(exchangeOpsUser);

        // Get the exchangeOpsUser
        restExchangeOpsUserMockMvc
            .perform(get(ENTITY_API_URL_ID, exchangeOpsUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchangeOpsUser.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.intValue()))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingExchangeOpsUser() throws Exception {
        // Get the exchangeOpsUser
        restExchangeOpsUserMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchangeOpsUser() throws Exception {
        // Initialize the database
        exchangeOpsUserRepository.saveAndFlush(exchangeOpsUser);

        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();

        // Update the exchangeOpsUser
        ExchangeOpsUser updatedExchangeOpsUser = exchangeOpsUserRepository.findById(exchangeOpsUser.getId()).get();
        // Disconnect from session so that the updates on updatedExchangeOpsUser are not directly saved in db
        em.detach(updatedExchangeOpsUser);
        updatedExchangeOpsUser
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .role(UPDATED_ROLE)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(updatedExchangeOpsUser);

        restExchangeOpsUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeOpsUserDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
        ExchangeOpsUser testExchangeOpsUser = exchangeOpsUserList.get(exchangeOpsUserList.size() - 1);
        assertThat(testExchangeOpsUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExchangeOpsUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testExchangeOpsUser.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testExchangeOpsUser.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testExchangeOpsUser.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testExchangeOpsUser.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeOpsUser.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangeOpsUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangeOpsUser.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingExchangeOpsUser() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();
        exchangeOpsUser.setId(count.incrementAndGet());

        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeOpsUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeOpsUserDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchangeOpsUser() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();
        exchangeOpsUser.setId(count.incrementAndGet());

        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOpsUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchangeOpsUser() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();
        exchangeOpsUser.setId(count.incrementAndGet());

        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOpsUserMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangeOpsUserWithPatch() throws Exception {
        // Initialize the database
        exchangeOpsUserRepository.saveAndFlush(exchangeOpsUser);

        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();

        // Update the exchangeOpsUser using partial update
        ExchangeOpsUser partialUpdatedExchangeOpsUser = new ExchangeOpsUser();
        partialUpdatedExchangeOpsUser.setId(exchangeOpsUser.getId());

        partialUpdatedExchangeOpsUser.isActive(UPDATED_IS_ACTIVE).createdAt(UPDATED_CREATED_AT).updatedBy(UPDATED_UPDATED_BY);

        restExchangeOpsUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeOpsUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeOpsUser))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
        ExchangeOpsUser testExchangeOpsUser = exchangeOpsUserList.get(exchangeOpsUserList.size() - 1);
        assertThat(testExchangeOpsUser.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExchangeOpsUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testExchangeOpsUser.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testExchangeOpsUser.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testExchangeOpsUser.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testExchangeOpsUser.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeOpsUser.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testExchangeOpsUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testExchangeOpsUser.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateExchangeOpsUserWithPatch() throws Exception {
        // Initialize the database
        exchangeOpsUserRepository.saveAndFlush(exchangeOpsUser);

        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();

        // Update the exchangeOpsUser using partial update
        ExchangeOpsUser partialUpdatedExchangeOpsUser = new ExchangeOpsUser();
        partialUpdatedExchangeOpsUser.setId(exchangeOpsUser.getId());

        partialUpdatedExchangeOpsUser
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .role(UPDATED_ROLE)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restExchangeOpsUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeOpsUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeOpsUser))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
        ExchangeOpsUser testExchangeOpsUser = exchangeOpsUserList.get(exchangeOpsUserList.size() - 1);
        assertThat(testExchangeOpsUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExchangeOpsUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testExchangeOpsUser.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testExchangeOpsUser.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testExchangeOpsUser.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testExchangeOpsUser.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeOpsUser.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangeOpsUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangeOpsUser.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingExchangeOpsUser() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();
        exchangeOpsUser.setId(count.incrementAndGet());

        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeOpsUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangeOpsUserDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchangeOpsUser() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();
        exchangeOpsUser.setId(count.incrementAndGet());

        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOpsUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchangeOpsUser() throws Exception {
        int databaseSizeBeforeUpdate = exchangeOpsUserRepository.findAll().size();
        exchangeOpsUser.setId(count.incrementAndGet());

        // Create the ExchangeOpsUser
        ExchangeOpsUserDTO exchangeOpsUserDTO = exchangeOpsUserMapper.toDto(exchangeOpsUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeOpsUserMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeOpsUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeOpsUser in the database
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchangeOpsUser() throws Exception {
        // Initialize the database
        exchangeOpsUserRepository.saveAndFlush(exchangeOpsUser);

        int databaseSizeBeforeDelete = exchangeOpsUserRepository.findAll().size();

        // Delete the exchangeOpsUser
        restExchangeOpsUserMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchangeOpsUser.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangeOpsUser> exchangeOpsUserList = exchangeOpsUserRepository.findAll();
        assertThat(exchangeOpsUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
