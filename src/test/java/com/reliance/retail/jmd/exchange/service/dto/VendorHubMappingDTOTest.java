package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VendorHubMappingDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VendorHubMappingDTO.class);
        VendorHubMappingDTO vendorHubMappingDTO1 = new VendorHubMappingDTO();
        vendorHubMappingDTO1.setId(1L);
        VendorHubMappingDTO vendorHubMappingDTO2 = new VendorHubMappingDTO();
        assertThat(vendorHubMappingDTO1).isNotEqualTo(vendorHubMappingDTO2);
        vendorHubMappingDTO2.setId(vendorHubMappingDTO1.getId());
        assertThat(vendorHubMappingDTO1).isEqualTo(vendorHubMappingDTO2);
        vendorHubMappingDTO2.setId(2L);
        assertThat(vendorHubMappingDTO1).isNotEqualTo(vendorHubMappingDTO2);
        vendorHubMappingDTO1.setId(null);
        assertThat(vendorHubMappingDTO1).isNotEqualTo(vendorHubMappingDTO2);
    }
}
