package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeAnswerSetTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeAnswerSet.class);
        ExchangeAnswerSet exchangeAnswerSet1 = new ExchangeAnswerSet();
        exchangeAnswerSet1.setId(1L);
        ExchangeAnswerSet exchangeAnswerSet2 = new ExchangeAnswerSet();
        exchangeAnswerSet2.setId(exchangeAnswerSet1.getId());
        assertThat(exchangeAnswerSet1).isEqualTo(exchangeAnswerSet2);
        exchangeAnswerSet2.setId(2L);
        assertThat(exchangeAnswerSet1).isNotEqualTo(exchangeAnswerSet2);
        exchangeAnswerSet1.setId(null);
        assertThat(exchangeAnswerSet1).isNotEqualTo(exchangeAnswerSet2);
    }
}
