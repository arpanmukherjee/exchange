package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.VendorHubMapping;
import com.reliance.retail.jmd.exchange.domain.enumeration.CategoryConetxtType;
import com.reliance.retail.jmd.exchange.repository.VendorHubMappingRepository;
import com.reliance.retail.jmd.exchange.service.VendorHubMappingService;
import com.reliance.retail.jmd.exchange.service.dto.VendorHubMappingDTO;
import com.reliance.retail.jmd.exchange.service.mapper.VendorHubMappingMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VendorHubMappingResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class VendorHubMappingResourceIT {

    private static final CategoryConetxtType DEFAULT_CATEGORY_CONTEXT_TYPE = CategoryConetxtType.REFRIGERATOR;
    private static final CategoryConetxtType UPDATED_CATEGORY_CONTEXT_TYPE = CategoryConetxtType.WASHING_MACHINE;

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/vendor-hub-mappings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VendorHubMappingRepository vendorHubMappingRepository;

    @Mock
    private VendorHubMappingRepository vendorHubMappingRepositoryMock;

    @Autowired
    private VendorHubMappingMapper vendorHubMappingMapper;

    @Mock
    private VendorHubMappingService vendorHubMappingServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVendorHubMappingMockMvc;

    private VendorHubMapping vendorHubMapping;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VendorHubMapping createEntity(EntityManager em) {
        VendorHubMapping vendorHubMapping = new VendorHubMapping()
            .categoryContextType(DEFAULT_CATEGORY_CONTEXT_TYPE)
            .isActive(DEFAULT_IS_ACTIVE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return vendorHubMapping;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VendorHubMapping createUpdatedEntity(EntityManager em) {
        VendorHubMapping vendorHubMapping = new VendorHubMapping()
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return vendorHubMapping;
    }

    @BeforeEach
    public void initTest() {
        vendorHubMapping = createEntity(em);
    }

    @Test
    @Transactional
    void createVendorHubMapping() throws Exception {
        int databaseSizeBeforeCreate = vendorHubMappingRepository.findAll().size();
        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);
        restVendorHubMappingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isCreated());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeCreate + 1);
        VendorHubMapping testVendorHubMapping = vendorHubMappingList.get(vendorHubMappingList.size() - 1);
        assertThat(testVendorHubMapping.getCategoryContextType()).isEqualTo(DEFAULT_CATEGORY_CONTEXT_TYPE);
        assertThat(testVendorHubMapping.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testVendorHubMapping.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testVendorHubMapping.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testVendorHubMapping.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testVendorHubMapping.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createVendorHubMappingWithExistingId() throws Exception {
        // Create the VendorHubMapping with an existing ID
        vendorHubMapping.setId(1L);
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        int databaseSizeBeforeCreate = vendorHubMappingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVendorHubMappingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllVendorHubMappings() throws Exception {
        // Initialize the database
        vendorHubMappingRepository.saveAndFlush(vendorHubMapping);

        // Get all the vendorHubMappingList
        restVendorHubMappingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vendorHubMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryContextType").value(hasItem(DEFAULT_CATEGORY_CONTEXT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllVendorHubMappingsWithEagerRelationshipsIsEnabled() throws Exception {
        when(vendorHubMappingServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVendorHubMappingMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(vendorHubMappingServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllVendorHubMappingsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(vendorHubMappingServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVendorHubMappingMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(vendorHubMappingRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getVendorHubMapping() throws Exception {
        // Initialize the database
        vendorHubMappingRepository.saveAndFlush(vendorHubMapping);

        // Get the vendorHubMapping
        restVendorHubMappingMockMvc
            .perform(get(ENTITY_API_URL_ID, vendorHubMapping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vendorHubMapping.getId().intValue()))
            .andExpect(jsonPath("$.categoryContextType").value(DEFAULT_CATEGORY_CONTEXT_TYPE.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingVendorHubMapping() throws Exception {
        // Get the vendorHubMapping
        restVendorHubMappingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingVendorHubMapping() throws Exception {
        // Initialize the database
        vendorHubMappingRepository.saveAndFlush(vendorHubMapping);

        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();

        // Update the vendorHubMapping
        VendorHubMapping updatedVendorHubMapping = vendorHubMappingRepository.findById(vendorHubMapping.getId()).get();
        // Disconnect from session so that the updates on updatedVendorHubMapping are not directly saved in db
        em.detach(updatedVendorHubMapping);
        updatedVendorHubMapping
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(updatedVendorHubMapping);

        restVendorHubMappingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, vendorHubMappingDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isOk());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
        VendorHubMapping testVendorHubMapping = vendorHubMappingList.get(vendorHubMappingList.size() - 1);
        assertThat(testVendorHubMapping.getCategoryContextType()).isEqualTo(UPDATED_CATEGORY_CONTEXT_TYPE);
        assertThat(testVendorHubMapping.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testVendorHubMapping.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testVendorHubMapping.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testVendorHubMapping.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVendorHubMapping.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingVendorHubMapping() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();
        vendorHubMapping.setId(count.incrementAndGet());

        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVendorHubMappingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, vendorHubMappingDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchVendorHubMapping() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();
        vendorHubMapping.setId(count.incrementAndGet());

        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMappingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamVendorHubMapping() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();
        vendorHubMapping.setId(count.incrementAndGet());

        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMappingMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVendorHubMappingWithPatch() throws Exception {
        // Initialize the database
        vendorHubMappingRepository.saveAndFlush(vendorHubMapping);

        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();

        // Update the vendorHubMapping using partial update
        VendorHubMapping partialUpdatedVendorHubMapping = new VendorHubMapping();
        partialUpdatedVendorHubMapping.setId(vendorHubMapping.getId());

        partialUpdatedVendorHubMapping.isActive(UPDATED_IS_ACTIVE).createdBy(UPDATED_CREATED_BY);

        restVendorHubMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVendorHubMapping.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVendorHubMapping))
            )
            .andExpect(status().isOk());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
        VendorHubMapping testVendorHubMapping = vendorHubMappingList.get(vendorHubMappingList.size() - 1);
        assertThat(testVendorHubMapping.getCategoryContextType()).isEqualTo(DEFAULT_CATEGORY_CONTEXT_TYPE);
        assertThat(testVendorHubMapping.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testVendorHubMapping.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testVendorHubMapping.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testVendorHubMapping.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVendorHubMapping.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateVendorHubMappingWithPatch() throws Exception {
        // Initialize the database
        vendorHubMappingRepository.saveAndFlush(vendorHubMapping);

        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();

        // Update the vendorHubMapping using partial update
        VendorHubMapping partialUpdatedVendorHubMapping = new VendorHubMapping();
        partialUpdatedVendorHubMapping.setId(vendorHubMapping.getId());

        partialUpdatedVendorHubMapping
            .categoryContextType(UPDATED_CATEGORY_CONTEXT_TYPE)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restVendorHubMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVendorHubMapping.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVendorHubMapping))
            )
            .andExpect(status().isOk());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
        VendorHubMapping testVendorHubMapping = vendorHubMappingList.get(vendorHubMappingList.size() - 1);
        assertThat(testVendorHubMapping.getCategoryContextType()).isEqualTo(UPDATED_CATEGORY_CONTEXT_TYPE);
        assertThat(testVendorHubMapping.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testVendorHubMapping.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testVendorHubMapping.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testVendorHubMapping.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVendorHubMapping.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingVendorHubMapping() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();
        vendorHubMapping.setId(count.incrementAndGet());

        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVendorHubMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, vendorHubMappingDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchVendorHubMapping() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();
        vendorHubMapping.setId(count.incrementAndGet());

        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamVendorHubMapping() throws Exception {
        int databaseSizeBeforeUpdate = vendorHubMappingRepository.findAll().size();
        vendorHubMapping.setId(count.incrementAndGet());

        // Create the VendorHubMapping
        VendorHubMappingDTO vendorHubMappingDTO = vendorHubMappingMapper.toDto(vendorHubMapping);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVendorHubMappingMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(vendorHubMappingDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the VendorHubMapping in the database
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteVendorHubMapping() throws Exception {
        // Initialize the database
        vendorHubMappingRepository.saveAndFlush(vendorHubMapping);

        int databaseSizeBeforeDelete = vendorHubMappingRepository.findAll().size();

        // Delete the vendorHubMapping
        restVendorHubMappingMockMvc
            .perform(delete(ENTITY_API_URL_ID, vendorHubMapping.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VendorHubMapping> vendorHubMappingList = vendorHubMappingRepository.findAll();
        assertThat(vendorHubMappingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
