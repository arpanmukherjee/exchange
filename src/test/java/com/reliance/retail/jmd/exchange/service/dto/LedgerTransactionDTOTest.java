package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LedgerTransactionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LedgerTransactionDTO.class);
        LedgerTransactionDTO ledgerTransactionDTO1 = new LedgerTransactionDTO();
        ledgerTransactionDTO1.setId(1L);
        LedgerTransactionDTO ledgerTransactionDTO2 = new LedgerTransactionDTO();
        assertThat(ledgerTransactionDTO1).isNotEqualTo(ledgerTransactionDTO2);
        ledgerTransactionDTO2.setId(ledgerTransactionDTO1.getId());
        assertThat(ledgerTransactionDTO1).isEqualTo(ledgerTransactionDTO2);
        ledgerTransactionDTO2.setId(2L);
        assertThat(ledgerTransactionDTO1).isNotEqualTo(ledgerTransactionDTO2);
        ledgerTransactionDTO1.setId(null);
        assertThat(ledgerTransactionDTO1).isNotEqualTo(ledgerTransactionDTO2);
    }
}
