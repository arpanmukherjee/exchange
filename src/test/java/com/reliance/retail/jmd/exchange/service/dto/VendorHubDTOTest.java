package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VendorHubDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VendorHubDTO.class);
        VendorHubDTO vendorHubDTO1 = new VendorHubDTO();
        vendorHubDTO1.setId(1L);
        VendorHubDTO vendorHubDTO2 = new VendorHubDTO();
        assertThat(vendorHubDTO1).isNotEqualTo(vendorHubDTO2);
        vendorHubDTO2.setId(vendorHubDTO1.getId());
        assertThat(vendorHubDTO1).isEqualTo(vendorHubDTO2);
        vendorHubDTO2.setId(2L);
        assertThat(vendorHubDTO1).isNotEqualTo(vendorHubDTO2);
        vendorHubDTO1.setId(null);
        assertThat(vendorHubDTO1).isNotEqualTo(vendorHubDTO2);
    }
}
