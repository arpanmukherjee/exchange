package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.DeliveryPartner;
import com.reliance.retail.jmd.exchange.repository.DeliveryPartnerRepository;
import com.reliance.retail.jmd.exchange.service.dto.DeliveryPartnerDTO;
import com.reliance.retail.jmd.exchange.service.mapper.DeliveryPartnerMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DeliveryPartnerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DeliveryPartnerResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_PARTNER_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_PARTNER_API_KEY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/delivery-partners";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DeliveryPartnerRepository deliveryPartnerRepository;

    @Autowired
    private DeliveryPartnerMapper deliveryPartnerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeliveryPartnerMockMvc;

    private DeliveryPartner deliveryPartner;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryPartner createEntity(EntityManager em) {
        DeliveryPartner deliveryPartner = new DeliveryPartner()
            .name(DEFAULT_NAME)
            .deliveryPartnerApiKey(DEFAULT_DELIVERY_PARTNER_API_KEY)
            .isActive(DEFAULT_IS_ACTIVE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return deliveryPartner;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryPartner createUpdatedEntity(EntityManager em) {
        DeliveryPartner deliveryPartner = new DeliveryPartner()
            .name(UPDATED_NAME)
            .deliveryPartnerApiKey(UPDATED_DELIVERY_PARTNER_API_KEY)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return deliveryPartner;
    }

    @BeforeEach
    public void initTest() {
        deliveryPartner = createEntity(em);
    }

    @Test
    @Transactional
    void createDeliveryPartner() throws Exception {
        int databaseSizeBeforeCreate = deliveryPartnerRepository.findAll().size();
        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);
        restDeliveryPartnerMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeCreate + 1);
        DeliveryPartner testDeliveryPartner = deliveryPartnerList.get(deliveryPartnerList.size() - 1);
        assertThat(testDeliveryPartner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDeliveryPartner.getDeliveryPartnerApiKey()).isEqualTo(DEFAULT_DELIVERY_PARTNER_API_KEY);
        assertThat(testDeliveryPartner.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testDeliveryPartner.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDeliveryPartner.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDeliveryPartner.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDeliveryPartner.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createDeliveryPartnerWithExistingId() throws Exception {
        // Create the DeliveryPartner with an existing ID
        deliveryPartner.setId(1L);
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        int databaseSizeBeforeCreate = deliveryPartnerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeliveryPartnerMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = deliveryPartnerRepository.findAll().size();
        // set the field null
        deliveryPartner.setName(null);

        // Create the DeliveryPartner, which fails.
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        restDeliveryPartnerMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isBadRequest());

        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDeliveryPartners() throws Exception {
        // Initialize the database
        deliveryPartnerRepository.saveAndFlush(deliveryPartner);

        // Get all the deliveryPartnerList
        restDeliveryPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deliveryPartner.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].deliveryPartnerApiKey").value(hasItem(DEFAULT_DELIVERY_PARTNER_API_KEY)))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getDeliveryPartner() throws Exception {
        // Initialize the database
        deliveryPartnerRepository.saveAndFlush(deliveryPartner);

        // Get the deliveryPartner
        restDeliveryPartnerMockMvc
            .perform(get(ENTITY_API_URL_ID, deliveryPartner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(deliveryPartner.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.deliveryPartnerApiKey").value(DEFAULT_DELIVERY_PARTNER_API_KEY))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingDeliveryPartner() throws Exception {
        // Get the deliveryPartner
        restDeliveryPartnerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDeliveryPartner() throws Exception {
        // Initialize the database
        deliveryPartnerRepository.saveAndFlush(deliveryPartner);

        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();

        // Update the deliveryPartner
        DeliveryPartner updatedDeliveryPartner = deliveryPartnerRepository.findById(deliveryPartner.getId()).get();
        // Disconnect from session so that the updates on updatedDeliveryPartner are not directly saved in db
        em.detach(updatedDeliveryPartner);
        updatedDeliveryPartner
            .name(UPDATED_NAME)
            .deliveryPartnerApiKey(UPDATED_DELIVERY_PARTNER_API_KEY)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(updatedDeliveryPartner);

        restDeliveryPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, deliveryPartnerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isOk());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
        DeliveryPartner testDeliveryPartner = deliveryPartnerList.get(deliveryPartnerList.size() - 1);
        assertThat(testDeliveryPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDeliveryPartner.getDeliveryPartnerApiKey()).isEqualTo(UPDATED_DELIVERY_PARTNER_API_KEY);
        assertThat(testDeliveryPartner.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testDeliveryPartner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDeliveryPartner.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDeliveryPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDeliveryPartner.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingDeliveryPartner() throws Exception {
        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();
        deliveryPartner.setId(count.incrementAndGet());

        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, deliveryPartnerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDeliveryPartner() throws Exception {
        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();
        deliveryPartner.setId(count.incrementAndGet());

        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDeliveryPartner() throws Exception {
        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();
        deliveryPartner.setId(count.incrementAndGet());

        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryPartnerMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDeliveryPartnerWithPatch() throws Exception {
        // Initialize the database
        deliveryPartnerRepository.saveAndFlush(deliveryPartner);

        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();

        // Update the deliveryPartner using partial update
        DeliveryPartner partialUpdatedDeliveryPartner = new DeliveryPartner();
        partialUpdatedDeliveryPartner.setId(deliveryPartner.getId());

        partialUpdatedDeliveryPartner
            .deliveryPartnerApiKey(UPDATED_DELIVERY_PARTNER_API_KEY)
            .createdAt(UPDATED_CREATED_AT)
            .createdBy(UPDATED_CREATED_BY);

        restDeliveryPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDeliveryPartner.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDeliveryPartner))
            )
            .andExpect(status().isOk());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
        DeliveryPartner testDeliveryPartner = deliveryPartnerList.get(deliveryPartnerList.size() - 1);
        assertThat(testDeliveryPartner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDeliveryPartner.getDeliveryPartnerApiKey()).isEqualTo(UPDATED_DELIVERY_PARTNER_API_KEY);
        assertThat(testDeliveryPartner.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testDeliveryPartner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDeliveryPartner.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDeliveryPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDeliveryPartner.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateDeliveryPartnerWithPatch() throws Exception {
        // Initialize the database
        deliveryPartnerRepository.saveAndFlush(deliveryPartner);

        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();

        // Update the deliveryPartner using partial update
        DeliveryPartner partialUpdatedDeliveryPartner = new DeliveryPartner();
        partialUpdatedDeliveryPartner.setId(deliveryPartner.getId());

        partialUpdatedDeliveryPartner
            .name(UPDATED_NAME)
            .deliveryPartnerApiKey(UPDATED_DELIVERY_PARTNER_API_KEY)
            .isActive(UPDATED_IS_ACTIVE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restDeliveryPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDeliveryPartner.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDeliveryPartner))
            )
            .andExpect(status().isOk());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
        DeliveryPartner testDeliveryPartner = deliveryPartnerList.get(deliveryPartnerList.size() - 1);
        assertThat(testDeliveryPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDeliveryPartner.getDeliveryPartnerApiKey()).isEqualTo(UPDATED_DELIVERY_PARTNER_API_KEY);
        assertThat(testDeliveryPartner.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testDeliveryPartner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDeliveryPartner.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDeliveryPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDeliveryPartner.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingDeliveryPartner() throws Exception {
        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();
        deliveryPartner.setId(count.incrementAndGet());

        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, deliveryPartnerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDeliveryPartner() throws Exception {
        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();
        deliveryPartner.setId(count.incrementAndGet());

        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDeliveryPartner() throws Exception {
        int databaseSizeBeforeUpdate = deliveryPartnerRepository.findAll().size();
        deliveryPartner.setId(count.incrementAndGet());

        // Create the DeliveryPartner
        DeliveryPartnerDTO deliveryPartnerDTO = deliveryPartnerMapper.toDto(deliveryPartner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(deliveryPartnerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DeliveryPartner in the database
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDeliveryPartner() throws Exception {
        // Initialize the database
        deliveryPartnerRepository.saveAndFlush(deliveryPartner);

        int databaseSizeBeforeDelete = deliveryPartnerRepository.findAll().size();

        // Delete the deliveryPartner
        restDeliveryPartnerMockMvc
            .perform(delete(ENTITY_API_URL_ID, deliveryPartner.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DeliveryPartner> deliveryPartnerList = deliveryPartnerRepository.findAll();
        assertThat(deliveryPartnerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
