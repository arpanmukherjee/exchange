package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeOrderDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeOrderDTO.class);
        ExchangeOrderDTO exchangeOrderDTO1 = new ExchangeOrderDTO();
        exchangeOrderDTO1.setId(1L);
        ExchangeOrderDTO exchangeOrderDTO2 = new ExchangeOrderDTO();
        assertThat(exchangeOrderDTO1).isNotEqualTo(exchangeOrderDTO2);
        exchangeOrderDTO2.setId(exchangeOrderDTO1.getId());
        assertThat(exchangeOrderDTO1).isEqualTo(exchangeOrderDTO2);
        exchangeOrderDTO2.setId(2L);
        assertThat(exchangeOrderDTO1).isNotEqualTo(exchangeOrderDTO2);
        exchangeOrderDTO1.setId(null);
        assertThat(exchangeOrderDTO1).isNotEqualTo(exchangeOrderDTO2);
    }
}
