package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.Pincode;
import com.reliance.retail.jmd.exchange.domain.enumeration.State;
import com.reliance.retail.jmd.exchange.repository.PincodeRepository;
import com.reliance.retail.jmd.exchange.service.dto.PincodeDTO;
import com.reliance.retail.jmd.exchange.service.mapper.PincodeMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PincodeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PincodeResourceIT {

    private static final Integer DEFAULT_PIN = 1;
    private static final Integer UPDATED_PIN = 2;

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final State DEFAULT_STATE = State.Maharastra;
    private static final State UPDATED_STATE = State.Gujrat;

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/pincodes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PincodeRepository pincodeRepository;

    @Autowired
    private PincodeMapper pincodeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPincodeMockMvc;

    private Pincode pincode;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pincode createEntity(EntityManager em) {
        Pincode pincode = new Pincode().pin(DEFAULT_PIN).city(DEFAULT_CITY).state(DEFAULT_STATE).country(DEFAULT_COUNTRY);
        return pincode;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pincode createUpdatedEntity(EntityManager em) {
        Pincode pincode = new Pincode().pin(UPDATED_PIN).city(UPDATED_CITY).state(UPDATED_STATE).country(UPDATED_COUNTRY);
        return pincode;
    }

    @BeforeEach
    public void initTest() {
        pincode = createEntity(em);
    }

    @Test
    @Transactional
    void createPincode() throws Exception {
        int databaseSizeBeforeCreate = pincodeRepository.findAll().size();
        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);
        restPincodeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pincodeDTO)))
            .andExpect(status().isCreated());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeCreate + 1);
        Pincode testPincode = pincodeList.get(pincodeList.size() - 1);
        assertThat(testPincode.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testPincode.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPincode.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testPincode.getCountry()).isEqualTo(DEFAULT_COUNTRY);
    }

    @Test
    @Transactional
    void createPincodeWithExistingId() throws Exception {
        // Create the Pincode with an existing ID
        pincode.setId(1L);
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        int databaseSizeBeforeCreate = pincodeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPincodeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pincodeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPinIsRequired() throws Exception {
        int databaseSizeBeforeTest = pincodeRepository.findAll().size();
        // set the field null
        pincode.setPin(null);

        // Create the Pincode, which fails.
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        restPincodeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pincodeDTO)))
            .andExpect(status().isBadRequest());

        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPincodes() throws Exception {
        // Initialize the database
        pincodeRepository.saveAndFlush(pincode);

        // Get all the pincodeList
        restPincodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pincode.getId().intValue())))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)));
    }

    @Test
    @Transactional
    void getPincode() throws Exception {
        // Initialize the database
        pincodeRepository.saveAndFlush(pincode);

        // Get the pincode
        restPincodeMockMvc
            .perform(get(ENTITY_API_URL_ID, pincode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pincode.getId().intValue()))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY));
    }

    @Test
    @Transactional
    void getNonExistingPincode() throws Exception {
        // Get the pincode
        restPincodeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPincode() throws Exception {
        // Initialize the database
        pincodeRepository.saveAndFlush(pincode);

        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();

        // Update the pincode
        Pincode updatedPincode = pincodeRepository.findById(pincode.getId()).get();
        // Disconnect from session so that the updates on updatedPincode are not directly saved in db
        em.detach(updatedPincode);
        updatedPincode.pin(UPDATED_PIN).city(UPDATED_CITY).state(UPDATED_STATE).country(UPDATED_COUNTRY);
        PincodeDTO pincodeDTO = pincodeMapper.toDto(updatedPincode);

        restPincodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, pincodeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pincodeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
        Pincode testPincode = pincodeList.get(pincodeList.size() - 1);
        assertThat(testPincode.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testPincode.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPincode.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPincode.getCountry()).isEqualTo(UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    void putNonExistingPincode() throws Exception {
        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();
        pincode.setId(count.incrementAndGet());

        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPincodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, pincodeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pincodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPincode() throws Exception {
        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();
        pincode.setId(count.incrementAndGet());

        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPincodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pincodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPincode() throws Exception {
        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();
        pincode.setId(count.incrementAndGet());

        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPincodeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pincodeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePincodeWithPatch() throws Exception {
        // Initialize the database
        pincodeRepository.saveAndFlush(pincode);

        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();

        // Update the pincode using partial update
        Pincode partialUpdatedPincode = new Pincode();
        partialUpdatedPincode.setId(pincode.getId());

        partialUpdatedPincode.city(UPDATED_CITY).state(UPDATED_STATE);

        restPincodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPincode.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPincode))
            )
            .andExpect(status().isOk());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
        Pincode testPincode = pincodeList.get(pincodeList.size() - 1);
        assertThat(testPincode.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testPincode.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPincode.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPincode.getCountry()).isEqualTo(DEFAULT_COUNTRY);
    }

    @Test
    @Transactional
    void fullUpdatePincodeWithPatch() throws Exception {
        // Initialize the database
        pincodeRepository.saveAndFlush(pincode);

        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();

        // Update the pincode using partial update
        Pincode partialUpdatedPincode = new Pincode();
        partialUpdatedPincode.setId(pincode.getId());

        partialUpdatedPincode.pin(UPDATED_PIN).city(UPDATED_CITY).state(UPDATED_STATE).country(UPDATED_COUNTRY);

        restPincodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPincode.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPincode))
            )
            .andExpect(status().isOk());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
        Pincode testPincode = pincodeList.get(pincodeList.size() - 1);
        assertThat(testPincode.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testPincode.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPincode.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPincode.getCountry()).isEqualTo(UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    void patchNonExistingPincode() throws Exception {
        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();
        pincode.setId(count.incrementAndGet());

        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPincodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, pincodeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pincodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPincode() throws Exception {
        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();
        pincode.setId(count.incrementAndGet());

        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPincodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pincodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPincode() throws Exception {
        int databaseSizeBeforeUpdate = pincodeRepository.findAll().size();
        pincode.setId(count.incrementAndGet());

        // Create the Pincode
        PincodeDTO pincodeDTO = pincodeMapper.toDto(pincode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPincodeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(pincodeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Pincode in the database
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePincode() throws Exception {
        // Initialize the database
        pincodeRepository.saveAndFlush(pincode);

        int databaseSizeBeforeDelete = pincodeRepository.findAll().size();

        // Delete the pincode
        restPincodeMockMvc
            .perform(delete(ENTITY_API_URL_ID, pincode.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pincode> pincodeList = pincodeRepository.findAll();
        assertThat(pincodeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
