package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DeliveryPartnerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryPartner.class);
        DeliveryPartner deliveryPartner1 = new DeliveryPartner();
        deliveryPartner1.setId(1L);
        DeliveryPartner deliveryPartner2 = new DeliveryPartner();
        deliveryPartner2.setId(deliveryPartner1.getId());
        assertThat(deliveryPartner1).isEqualTo(deliveryPartner2);
        deliveryPartner2.setId(2L);
        assertThat(deliveryPartner1).isNotEqualTo(deliveryPartner2);
        deliveryPartner1.setId(null);
        assertThat(deliveryPartner1).isNotEqualTo(deliveryPartner2);
    }
}
