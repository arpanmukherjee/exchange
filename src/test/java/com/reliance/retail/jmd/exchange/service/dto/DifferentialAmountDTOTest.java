package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DifferentialAmountDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DifferentialAmountDTO.class);
        DifferentialAmountDTO differentialAmountDTO1 = new DifferentialAmountDTO();
        differentialAmountDTO1.setId(1L);
        DifferentialAmountDTO differentialAmountDTO2 = new DifferentialAmountDTO();
        assertThat(differentialAmountDTO1).isNotEqualTo(differentialAmountDTO2);
        differentialAmountDTO2.setId(differentialAmountDTO1.getId());
        assertThat(differentialAmountDTO1).isEqualTo(differentialAmountDTO2);
        differentialAmountDTO2.setId(2L);
        assertThat(differentialAmountDTO1).isNotEqualTo(differentialAmountDTO2);
        differentialAmountDTO1.setId(null);
        assertThat(differentialAmountDTO1).isNotEqualTo(differentialAmountDTO2);
    }
}
