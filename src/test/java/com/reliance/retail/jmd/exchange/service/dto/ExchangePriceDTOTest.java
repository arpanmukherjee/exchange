package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangePriceDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangePriceDTO.class);
        ExchangePriceDTO exchangePriceDTO1 = new ExchangePriceDTO();
        exchangePriceDTO1.setId(1L);
        ExchangePriceDTO exchangePriceDTO2 = new ExchangePriceDTO();
        assertThat(exchangePriceDTO1).isNotEqualTo(exchangePriceDTO2);
        exchangePriceDTO2.setId(exchangePriceDTO1.getId());
        assertThat(exchangePriceDTO1).isEqualTo(exchangePriceDTO2);
        exchangePriceDTO2.setId(2L);
        assertThat(exchangePriceDTO1).isNotEqualTo(exchangePriceDTO2);
        exchangePriceDTO1.setId(null);
        assertThat(exchangePriceDTO1).isNotEqualTo(exchangePriceDTO2);
    }
}
