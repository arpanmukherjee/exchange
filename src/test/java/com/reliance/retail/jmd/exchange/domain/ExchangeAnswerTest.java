package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeAnswerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeAnswer.class);
        ExchangeAnswer exchangeAnswer1 = new ExchangeAnswer();
        exchangeAnswer1.setId(1L);
        ExchangeAnswer exchangeAnswer2 = new ExchangeAnswer();
        exchangeAnswer2.setId(exchangeAnswer1.getId());
        assertThat(exchangeAnswer1).isEqualTo(exchangeAnswer2);
        exchangeAnswer2.setId(2L);
        assertThat(exchangeAnswer1).isNotEqualTo(exchangeAnswer2);
        exchangeAnswer1.setId(null);
        assertThat(exchangeAnswer1).isNotEqualTo(exchangeAnswer2);
    }
}
