package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeOrderTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeOrder.class);
        ExchangeOrder exchangeOrder1 = new ExchangeOrder();
        exchangeOrder1.setId(1L);
        ExchangeOrder exchangeOrder2 = new ExchangeOrder();
        exchangeOrder2.setId(exchangeOrder1.getId());
        assertThat(exchangeOrder1).isEqualTo(exchangeOrder2);
        exchangeOrder2.setId(2L);
        assertThat(exchangeOrder1).isNotEqualTo(exchangeOrder2);
        exchangeOrder1.setId(null);
        assertThat(exchangeOrder1).isNotEqualTo(exchangeOrder2);
    }
}
