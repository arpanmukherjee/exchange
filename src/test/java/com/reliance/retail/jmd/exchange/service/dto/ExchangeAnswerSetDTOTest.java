package com.reliance.retail.jmd.exchange.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeAnswerSetDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeAnswerSetDTO.class);
        ExchangeAnswerSetDTO exchangeAnswerSetDTO1 = new ExchangeAnswerSetDTO();
        exchangeAnswerSetDTO1.setId(1L);
        ExchangeAnswerSetDTO exchangeAnswerSetDTO2 = new ExchangeAnswerSetDTO();
        assertThat(exchangeAnswerSetDTO1).isNotEqualTo(exchangeAnswerSetDTO2);
        exchangeAnswerSetDTO2.setId(exchangeAnswerSetDTO1.getId());
        assertThat(exchangeAnswerSetDTO1).isEqualTo(exchangeAnswerSetDTO2);
        exchangeAnswerSetDTO2.setId(2L);
        assertThat(exchangeAnswerSetDTO1).isNotEqualTo(exchangeAnswerSetDTO2);
        exchangeAnswerSetDTO1.setId(null);
        assertThat(exchangeAnswerSetDTO1).isNotEqualTo(exchangeAnswerSetDTO2);
    }
}
