package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeAnswerSessionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeAnswerSession.class);
        ExchangeAnswerSession exchangeAnswerSession1 = new ExchangeAnswerSession();
        exchangeAnswerSession1.setId(1L);
        ExchangeAnswerSession exchangeAnswerSession2 = new ExchangeAnswerSession();
        exchangeAnswerSession2.setId(exchangeAnswerSession1.getId());
        assertThat(exchangeAnswerSession1).isEqualTo(exchangeAnswerSession2);
        exchangeAnswerSession2.setId(2L);
        assertThat(exchangeAnswerSession1).isNotEqualTo(exchangeAnswerSession2);
        exchangeAnswerSession1.setId(null);
        assertThat(exchangeAnswerSession1).isNotEqualTo(exchangeAnswerSession2);
    }
}
