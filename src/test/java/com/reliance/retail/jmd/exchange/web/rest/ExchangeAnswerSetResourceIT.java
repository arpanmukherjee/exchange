package com.reliance.retail.jmd.exchange.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.reliance.retail.jmd.exchange.IntegrationTest;
import com.reliance.retail.jmd.exchange.domain.ExchangeAnswerSet;
import com.reliance.retail.jmd.exchange.repository.ExchangeAnswerSetRepository;
import com.reliance.retail.jmd.exchange.service.dto.ExchangeAnswerSetDTO;
import com.reliance.retail.jmd.exchange.service.mapper.ExchangeAnswerSetMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangeAnswerSetResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangeAnswerSetResourceIT {

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/exchange-answer-sets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExchangeAnswerSetRepository exchangeAnswerSetRepository;

    @Autowired
    private ExchangeAnswerSetMapper exchangeAnswerSetMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangeAnswerSetMockMvc;

    private ExchangeAnswerSet exchangeAnswerSet;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeAnswerSet createEntity(EntityManager em) {
        ExchangeAnswerSet exchangeAnswerSet = new ExchangeAnswerSet()
            .tag(DEFAULT_TAG)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return exchangeAnswerSet;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeAnswerSet createUpdatedEntity(EntityManager em) {
        ExchangeAnswerSet exchangeAnswerSet = new ExchangeAnswerSet()
            .tag(UPDATED_TAG)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return exchangeAnswerSet;
    }

    @BeforeEach
    public void initTest() {
        exchangeAnswerSet = createEntity(em);
    }

    @Test
    @Transactional
    void createExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeCreate = exchangeAnswerSetRepository.findAll().size();
        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);
        restExchangeAnswerSetMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangeAnswerSet testExchangeAnswerSet = exchangeAnswerSetList.get(exchangeAnswerSetList.size() - 1);
        assertThat(testExchangeAnswerSet.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testExchangeAnswerSet.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangeAnswerSet.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testExchangeAnswerSet.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testExchangeAnswerSet.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createExchangeAnswerSetWithExistingId() throws Exception {
        // Create the ExchangeAnswerSet with an existing ID
        exchangeAnswerSet.setId(1L);
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        int databaseSizeBeforeCreate = exchangeAnswerSetRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeAnswerSetMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllExchangeAnswerSets() throws Exception {
        // Initialize the database
        exchangeAnswerSetRepository.saveAndFlush(exchangeAnswerSet);

        // Get all the exchangeAnswerSetList
        restExchangeAnswerSetMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeAnswerSet.getId().intValue())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getExchangeAnswerSet() throws Exception {
        // Initialize the database
        exchangeAnswerSetRepository.saveAndFlush(exchangeAnswerSet);

        // Get the exchangeAnswerSet
        restExchangeAnswerSetMockMvc
            .perform(get(ENTITY_API_URL_ID, exchangeAnswerSet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchangeAnswerSet.getId().intValue()))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingExchangeAnswerSet() throws Exception {
        // Get the exchangeAnswerSet
        restExchangeAnswerSetMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchangeAnswerSet() throws Exception {
        // Initialize the database
        exchangeAnswerSetRepository.saveAndFlush(exchangeAnswerSet);

        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();

        // Update the exchangeAnswerSet
        ExchangeAnswerSet updatedExchangeAnswerSet = exchangeAnswerSetRepository.findById(exchangeAnswerSet.getId()).get();
        // Disconnect from session so that the updates on updatedExchangeAnswerSet are not directly saved in db
        em.detach(updatedExchangeAnswerSet);
        updatedExchangeAnswerSet
            .tag(UPDATED_TAG)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(updatedExchangeAnswerSet);

        restExchangeAnswerSetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeAnswerSetDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswerSet testExchangeAnswerSet = exchangeAnswerSetList.get(exchangeAnswerSetList.size() - 1);
        assertThat(testExchangeAnswerSet.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testExchangeAnswerSet.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeAnswerSet.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangeAnswerSet.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangeAnswerSet.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();
        exchangeAnswerSet.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeAnswerSetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeAnswerSetDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();
        exchangeAnswerSet.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();
        exchangeAnswerSet.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSetMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangeAnswerSetWithPatch() throws Exception {
        // Initialize the database
        exchangeAnswerSetRepository.saveAndFlush(exchangeAnswerSet);

        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();

        // Update the exchangeAnswerSet using partial update
        ExchangeAnswerSet partialUpdatedExchangeAnswerSet = new ExchangeAnswerSet();
        partialUpdatedExchangeAnswerSet.setId(exchangeAnswerSet.getId());

        partialUpdatedExchangeAnswerSet.updatedAt(UPDATED_UPDATED_AT).createdBy(UPDATED_CREATED_BY);

        restExchangeAnswerSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeAnswerSet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeAnswerSet))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswerSet testExchangeAnswerSet = exchangeAnswerSetList.get(exchangeAnswerSetList.size() - 1);
        assertThat(testExchangeAnswerSet.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testExchangeAnswerSet.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExchangeAnswerSet.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangeAnswerSet.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangeAnswerSet.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateExchangeAnswerSetWithPatch() throws Exception {
        // Initialize the database
        exchangeAnswerSetRepository.saveAndFlush(exchangeAnswerSet);

        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();

        // Update the exchangeAnswerSet using partial update
        ExchangeAnswerSet partialUpdatedExchangeAnswerSet = new ExchangeAnswerSet();
        partialUpdatedExchangeAnswerSet.setId(exchangeAnswerSet.getId());

        partialUpdatedExchangeAnswerSet
            .tag(UPDATED_TAG)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restExchangeAnswerSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchangeAnswerSet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExchangeAnswerSet))
            )
            .andExpect(status().isOk());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
        ExchangeAnswerSet testExchangeAnswerSet = exchangeAnswerSetList.get(exchangeAnswerSetList.size() - 1);
        assertThat(testExchangeAnswerSet.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testExchangeAnswerSet.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExchangeAnswerSet.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testExchangeAnswerSet.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExchangeAnswerSet.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();
        exchangeAnswerSet.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeAnswerSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangeAnswerSetDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();
        exchangeAnswerSet.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchangeAnswerSet() throws Exception {
        int databaseSizeBeforeUpdate = exchangeAnswerSetRepository.findAll().size();
        exchangeAnswerSet.setId(count.incrementAndGet());

        // Create the ExchangeAnswerSet
        ExchangeAnswerSetDTO exchangeAnswerSetDTO = exchangeAnswerSetMapper.toDto(exchangeAnswerSet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeAnswerSetMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(exchangeAnswerSetDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExchangeAnswerSet in the database
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchangeAnswerSet() throws Exception {
        // Initialize the database
        exchangeAnswerSetRepository.saveAndFlush(exchangeAnswerSet);

        int databaseSizeBeforeDelete = exchangeAnswerSetRepository.findAll().size();

        // Delete the exchangeAnswerSet
        restExchangeAnswerSetMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchangeAnswerSet.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangeAnswerSet> exchangeAnswerSetList = exchangeAnswerSetRepository.findAll();
        assertThat(exchangeAnswerSetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
