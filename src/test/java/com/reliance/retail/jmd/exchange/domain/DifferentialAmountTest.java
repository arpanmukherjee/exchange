package com.reliance.retail.jmd.exchange.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.reliance.retail.jmd.exchange.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DifferentialAmountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DifferentialAmount.class);
        DifferentialAmount differentialAmount1 = new DifferentialAmount();
        differentialAmount1.setId(1L);
        DifferentialAmount differentialAmount2 = new DifferentialAmount();
        differentialAmount2.setId(differentialAmount1.getId());
        assertThat(differentialAmount1).isEqualTo(differentialAmount2);
        differentialAmount2.setId(2L);
        assertThat(differentialAmount1).isNotEqualTo(differentialAmount2);
        differentialAmount1.setId(null);
        assertThat(differentialAmount1).isNotEqualTo(differentialAmount2);
    }
}
