/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import VendorHubMappingDetailComponent from '@/entities/vendor-hub-mapping/vendor-hub-mapping-details.vue';
import VendorHubMappingClass from '@/entities/vendor-hub-mapping/vendor-hub-mapping-details.component';
import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('VendorHubMapping Management Detail Component', () => {
    let wrapper: Wrapper<VendorHubMappingClass>;
    let comp: VendorHubMappingClass;
    let vendorHubMappingServiceStub: SinonStubbedInstance<VendorHubMappingService>;

    beforeEach(() => {
      vendorHubMappingServiceStub = sinon.createStubInstance<VendorHubMappingService>(VendorHubMappingService);

      wrapper = shallowMount<VendorHubMappingClass>(VendorHubMappingDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { vendorHubMappingService: () => vendorHubMappingServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVendorHubMapping = { id: 123 };
        vendorHubMappingServiceStub.find.resolves(foundVendorHubMapping);

        // WHEN
        comp.retrieveVendorHubMapping(123);
        await comp.$nextTick();

        // THEN
        expect(comp.vendorHubMapping).toBe(foundVendorHubMapping);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVendorHubMapping = { id: 123 };
        vendorHubMappingServiceStub.find.resolves(foundVendorHubMapping);

        // WHEN
        comp.beforeRouteEnter({ params: { vendorHubMappingId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.vendorHubMapping).toBe(foundVendorHubMapping);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
