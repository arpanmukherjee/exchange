/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import VendorHubMappingUpdateComponent from '@/entities/vendor-hub-mapping/vendor-hub-mapping-update.vue';
import VendorHubMappingClass from '@/entities/vendor-hub-mapping/vendor-hub-mapping-update.component';
import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';

import PincodeService from '@/entities/pincode/pincode.service';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('VendorHubMapping Management Update Component', () => {
    let wrapper: Wrapper<VendorHubMappingClass>;
    let comp: VendorHubMappingClass;
    let vendorHubMappingServiceStub: SinonStubbedInstance<VendorHubMappingService>;

    beforeEach(() => {
      vendorHubMappingServiceStub = sinon.createStubInstance<VendorHubMappingService>(VendorHubMappingService);

      wrapper = shallowMount<VendorHubMappingClass>(VendorHubMappingUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          vendorHubMappingService: () => vendorHubMappingServiceStub,
          alertService: () => new AlertService(),

          pincodeService: () =>
            sinon.createStubInstance<PincodeService>(PincodeService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeOpsUserService: () =>
            sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorHubService: () =>
            sinon.createStubInstance<VendorHubService>(VendorHubService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.vendorHubMapping = entity;
        vendorHubMappingServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(vendorHubMappingServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.vendorHubMapping = entity;
        vendorHubMappingServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(vendorHubMappingServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVendorHubMapping = { id: 123 };
        vendorHubMappingServiceStub.find.resolves(foundVendorHubMapping);
        vendorHubMappingServiceStub.retrieve.resolves([foundVendorHubMapping]);

        // WHEN
        comp.beforeRouteEnter({ params: { vendorHubMappingId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.vendorHubMapping).toBe(foundVendorHubMapping);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
