/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import VendorHubMappingComponent from '@/entities/vendor-hub-mapping/vendor-hub-mapping.vue';
import VendorHubMappingClass from '@/entities/vendor-hub-mapping/vendor-hub-mapping.component';
import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('VendorHubMapping Management Component', () => {
    let wrapper: Wrapper<VendorHubMappingClass>;
    let comp: VendorHubMappingClass;
    let vendorHubMappingServiceStub: SinonStubbedInstance<VendorHubMappingService>;

    beforeEach(() => {
      vendorHubMappingServiceStub = sinon.createStubInstance<VendorHubMappingService>(VendorHubMappingService);
      vendorHubMappingServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<VendorHubMappingClass>(VendorHubMappingComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          vendorHubMappingService: () => vendorHubMappingServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      vendorHubMappingServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllVendorHubMappings();
      await comp.$nextTick();

      // THEN
      expect(vendorHubMappingServiceStub.retrieve.called).toBeTruthy();
      expect(comp.vendorHubMappings[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      vendorHubMappingServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(vendorHubMappingServiceStub.retrieve.called).toBeTruthy();
      expect(comp.vendorHubMappings[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      vendorHubMappingServiceStub.retrieve.reset();
      vendorHubMappingServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(vendorHubMappingServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.vendorHubMappings[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      vendorHubMappingServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(vendorHubMappingServiceStub.retrieve.callCount).toEqual(1);

      comp.removeVendorHubMapping();
      await comp.$nextTick();

      // THEN
      expect(vendorHubMappingServiceStub.delete.called).toBeTruthy();
      expect(vendorHubMappingServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
