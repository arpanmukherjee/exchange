/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import LedgerUpdateComponent from '@/entities/ledger/ledger-update.vue';
import LedgerClass from '@/entities/ledger/ledger-update.component';
import LedgerService from '@/entities/ledger/ledger.service';

import VendorService from '@/entities/vendor/vendor.service';

import LedgerTransactionService from '@/entities/ledger-transaction/ledger-transaction.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Ledger Management Update Component', () => {
    let wrapper: Wrapper<LedgerClass>;
    let comp: LedgerClass;
    let ledgerServiceStub: SinonStubbedInstance<LedgerService>;

    beforeEach(() => {
      ledgerServiceStub = sinon.createStubInstance<LedgerService>(LedgerService);

      wrapper = shallowMount<LedgerClass>(LedgerUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          ledgerService: () => ledgerServiceStub,
          alertService: () => new AlertService(),

          vendorService: () =>
            sinon.createStubInstance<VendorService>(VendorService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          ledgerTransactionService: () =>
            sinon.createStubInstance<LedgerTransactionService>(LedgerTransactionService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.ledger = entity;
        ledgerServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(ledgerServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.ledger = entity;
        ledgerServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(ledgerServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundLedger = { id: 123 };
        ledgerServiceStub.find.resolves(foundLedger);
        ledgerServiceStub.retrieve.resolves([foundLedger]);

        // WHEN
        comp.beforeRouteEnter({ params: { ledgerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.ledger).toBe(foundLedger);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
