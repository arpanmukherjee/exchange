/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import DocumentUpdateComponent from '@/entities/document/document-update.vue';
import DocumentClass from '@/entities/document/document-update.component';
import DocumentService from '@/entities/document/document.service';

import VendorService from '@/entities/vendor/vendor.service';

import ProductContextService from '@/entities/product-context/product-context.service';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Document Management Update Component', () => {
    let wrapper: Wrapper<DocumentClass>;
    let comp: DocumentClass;
    let documentServiceStub: SinonStubbedInstance<DocumentService>;

    beforeEach(() => {
      documentServiceStub = sinon.createStubInstance<DocumentService>(DocumentService);

      wrapper = shallowMount<DocumentClass>(DocumentUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          documentService: () => documentServiceStub,
          alertService: () => new AlertService(),

          vendorService: () =>
            sinon.createStubInstance<VendorService>(VendorService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          productContextService: () =>
            sinon.createStubInstance<ProductContextService>(ProductContextService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeOrderService: () =>
            sinon.createStubInstance<ExchangeOrderService>(ExchangeOrderService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.document = entity;
        documentServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(documentServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.document = entity;
        documentServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(documentServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDocument = { id: 123 };
        documentServiceStub.find.resolves(foundDocument);
        documentServiceStub.retrieve.resolves([foundDocument]);

        // WHEN
        comp.beforeRouteEnter({ params: { documentId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.document).toBe(foundDocument);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
