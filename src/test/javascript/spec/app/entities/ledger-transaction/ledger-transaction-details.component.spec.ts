/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import LedgerTransactionDetailComponent from '@/entities/ledger-transaction/ledger-transaction-details.vue';
import LedgerTransactionClass from '@/entities/ledger-transaction/ledger-transaction-details.component';
import LedgerTransactionService from '@/entities/ledger-transaction/ledger-transaction.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('LedgerTransaction Management Detail Component', () => {
    let wrapper: Wrapper<LedgerTransactionClass>;
    let comp: LedgerTransactionClass;
    let ledgerTransactionServiceStub: SinonStubbedInstance<LedgerTransactionService>;

    beforeEach(() => {
      ledgerTransactionServiceStub = sinon.createStubInstance<LedgerTransactionService>(LedgerTransactionService);

      wrapper = shallowMount<LedgerTransactionClass>(LedgerTransactionDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { ledgerTransactionService: () => ledgerTransactionServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundLedgerTransaction = { id: 123 };
        ledgerTransactionServiceStub.find.resolves(foundLedgerTransaction);

        // WHEN
        comp.retrieveLedgerTransaction(123);
        await comp.$nextTick();

        // THEN
        expect(comp.ledgerTransaction).toBe(foundLedgerTransaction);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundLedgerTransaction = { id: 123 };
        ledgerTransactionServiceStub.find.resolves(foundLedgerTransaction);

        // WHEN
        comp.beforeRouteEnter({ params: { ledgerTransactionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.ledgerTransaction).toBe(foundLedgerTransaction);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
