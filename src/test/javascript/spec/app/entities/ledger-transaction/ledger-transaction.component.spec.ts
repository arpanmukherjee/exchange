/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import LedgerTransactionComponent from '@/entities/ledger-transaction/ledger-transaction.vue';
import LedgerTransactionClass from '@/entities/ledger-transaction/ledger-transaction.component';
import LedgerTransactionService from '@/entities/ledger-transaction/ledger-transaction.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('LedgerTransaction Management Component', () => {
    let wrapper: Wrapper<LedgerTransactionClass>;
    let comp: LedgerTransactionClass;
    let ledgerTransactionServiceStub: SinonStubbedInstance<LedgerTransactionService>;

    beforeEach(() => {
      ledgerTransactionServiceStub = sinon.createStubInstance<LedgerTransactionService>(LedgerTransactionService);
      ledgerTransactionServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<LedgerTransactionClass>(LedgerTransactionComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          ledgerTransactionService: () => ledgerTransactionServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      ledgerTransactionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllLedgerTransactions();
      await comp.$nextTick();

      // THEN
      expect(ledgerTransactionServiceStub.retrieve.called).toBeTruthy();
      expect(comp.ledgerTransactions[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      ledgerTransactionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(ledgerTransactionServiceStub.retrieve.called).toBeTruthy();
      expect(comp.ledgerTransactions[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      ledgerTransactionServiceStub.retrieve.reset();
      ledgerTransactionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(ledgerTransactionServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.ledgerTransactions[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      ledgerTransactionServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(ledgerTransactionServiceStub.retrieve.callCount).toEqual(1);

      comp.removeLedgerTransaction();
      await comp.$nextTick();

      // THEN
      expect(ledgerTransactionServiceStub.delete.called).toBeTruthy();
      expect(ledgerTransactionServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
