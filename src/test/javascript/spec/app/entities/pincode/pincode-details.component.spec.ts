/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PincodeDetailComponent from '@/entities/pincode/pincode-details.vue';
import PincodeClass from '@/entities/pincode/pincode-details.component';
import PincodeService from '@/entities/pincode/pincode.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Pincode Management Detail Component', () => {
    let wrapper: Wrapper<PincodeClass>;
    let comp: PincodeClass;
    let pincodeServiceStub: SinonStubbedInstance<PincodeService>;

    beforeEach(() => {
      pincodeServiceStub = sinon.createStubInstance<PincodeService>(PincodeService);

      wrapper = shallowMount<PincodeClass>(PincodeDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { pincodeService: () => pincodeServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPincode = { id: 123 };
        pincodeServiceStub.find.resolves(foundPincode);

        // WHEN
        comp.retrievePincode(123);
        await comp.$nextTick();

        // THEN
        expect(comp.pincode).toBe(foundPincode);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPincode = { id: 123 };
        pincodeServiceStub.find.resolves(foundPincode);

        // WHEN
        comp.beforeRouteEnter({ params: { pincodeId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.pincode).toBe(foundPincode);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
