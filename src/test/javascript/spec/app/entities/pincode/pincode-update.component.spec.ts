/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import PincodeUpdateComponent from '@/entities/pincode/pincode-update.vue';
import PincodeClass from '@/entities/pincode/pincode-update.component';
import PincodeService from '@/entities/pincode/pincode.service';

import AddressService from '@/entities/address/address.service';

import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Pincode Management Update Component', () => {
    let wrapper: Wrapper<PincodeClass>;
    let comp: PincodeClass;
    let pincodeServiceStub: SinonStubbedInstance<PincodeService>;

    beforeEach(() => {
      pincodeServiceStub = sinon.createStubInstance<PincodeService>(PincodeService);

      wrapper = shallowMount<PincodeClass>(PincodeUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          pincodeService: () => pincodeServiceStub,
          alertService: () => new AlertService(),

          addressService: () =>
            sinon.createStubInstance<AddressService>(AddressService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorHubMappingService: () =>
            sinon.createStubInstance<VendorHubMappingService>(VendorHubMappingService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.pincode = entity;
        pincodeServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(pincodeServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.pincode = entity;
        pincodeServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(pincodeServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPincode = { id: 123 };
        pincodeServiceStub.find.resolves(foundPincode);
        pincodeServiceStub.retrieve.resolves([foundPincode]);

        // WHEN
        comp.beforeRouteEnter({ params: { pincodeId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.pincode).toBe(foundPincode);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
