/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import VendorHubUpdateComponent from '@/entities/vendor-hub/vendor-hub-update.vue';
import VendorHubClass from '@/entities/vendor-hub/vendor-hub-update.component';
import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';

import AddressService from '@/entities/address/address.service';

import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';

import VendorService from '@/entities/vendor/vendor.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('VendorHub Management Update Component', () => {
    let wrapper: Wrapper<VendorHubClass>;
    let comp: VendorHubClass;
    let vendorHubServiceStub: SinonStubbedInstance<VendorHubService>;

    beforeEach(() => {
      vendorHubServiceStub = sinon.createStubInstance<VendorHubService>(VendorHubService);

      wrapper = shallowMount<VendorHubClass>(VendorHubUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          vendorHubService: () => vendorHubServiceStub,
          alertService: () => new AlertService(),

          addressService: () =>
            sinon.createStubInstance<AddressService>(AddressService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorHubMappingService: () =>
            sinon.createStubInstance<VendorHubMappingService>(VendorHubMappingService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangePriceService: () =>
            sinon.createStubInstance<ExchangePriceService>(ExchangePriceService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeOpsUserService: () =>
            sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorService: () =>
            sinon.createStubInstance<VendorService>(VendorService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.vendorHub = entity;
        vendorHubServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(vendorHubServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.vendorHub = entity;
        vendorHubServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(vendorHubServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVendorHub = { id: 123 };
        vendorHubServiceStub.find.resolves(foundVendorHub);
        vendorHubServiceStub.retrieve.resolves([foundVendorHub]);

        // WHEN
        comp.beforeRouteEnter({ params: { vendorHubId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.vendorHub).toBe(foundVendorHub);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
