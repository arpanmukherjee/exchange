/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import VendorHubDetailComponent from '@/entities/vendor-hub/vendor-hub-details.vue';
import VendorHubClass from '@/entities/vendor-hub/vendor-hub-details.component';
import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('VendorHub Management Detail Component', () => {
    let wrapper: Wrapper<VendorHubClass>;
    let comp: VendorHubClass;
    let vendorHubServiceStub: SinonStubbedInstance<VendorHubService>;

    beforeEach(() => {
      vendorHubServiceStub = sinon.createStubInstance<VendorHubService>(VendorHubService);

      wrapper = shallowMount<VendorHubClass>(VendorHubDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { vendorHubService: () => vendorHubServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVendorHub = { id: 123 };
        vendorHubServiceStub.find.resolves(foundVendorHub);

        // WHEN
        comp.retrieveVendorHub(123);
        await comp.$nextTick();

        // THEN
        expect(comp.vendorHub).toBe(foundVendorHub);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVendorHub = { id: 123 };
        vendorHubServiceStub.find.resolves(foundVendorHub);

        // WHEN
        comp.beforeRouteEnter({ params: { vendorHubId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.vendorHub).toBe(foundVendorHub);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
