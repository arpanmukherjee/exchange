/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ProductContextUpdateComponent from '@/entities/product-context/product-context-update.vue';
import ProductContextClass from '@/entities/product-context/product-context-update.component';
import ProductContextService from '@/entities/product-context/product-context.service';

import DocumentService from '@/entities/document/document.service';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';

import ClientService from '@/entities/client/client.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ProductContext Management Update Component', () => {
    let wrapper: Wrapper<ProductContextClass>;
    let comp: ProductContextClass;
    let productContextServiceStub: SinonStubbedInstance<ProductContextService>;

    beforeEach(() => {
      productContextServiceStub = sinon.createStubInstance<ProductContextService>(ProductContextService);

      wrapper = shallowMount<ProductContextClass>(ProductContextUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          productContextService: () => productContextServiceStub,
          alertService: () => new AlertService(),

          documentService: () =>
            sinon.createStubInstance<DocumentService>(DocumentService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeOrderService: () =>
            sinon.createStubInstance<ExchangeOrderService>(ExchangeOrderService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          clientService: () =>
            sinon.createStubInstance<ClientService>(ClientService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.productContext = entity;
        productContextServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(productContextServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.productContext = entity;
        productContextServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(productContextServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundProductContext = { id: 123 };
        productContextServiceStub.find.resolves(foundProductContext);
        productContextServiceStub.retrieve.resolves([foundProductContext]);

        // WHEN
        comp.beforeRouteEnter({ params: { productContextId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.productContext).toBe(foundProductContext);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
