/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ExchangeOpsUserUpdateComponent from '@/entities/exchange-ops-user/exchange-ops-user-update.vue';
import ExchangeOpsUserClass from '@/entities/exchange-ops-user/exchange-ops-user-update.component';
import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';

import ClientService from '@/entities/client/client.service';

import DeliveryPartnerService from '@/entities/delivery-partner/delivery-partner.service';

import VendorService from '@/entities/vendor/vendor.service';

import VendorHubService from '@/entities/vendor-hub/vendor-hub.service';

import VendorHubMappingService from '@/entities/vendor-hub-mapping/vendor-hub-mapping.service';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';

import DifferentialAmountService from '@/entities/differential-amount/differential-amount.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ExchangeOpsUser Management Update Component', () => {
    let wrapper: Wrapper<ExchangeOpsUserClass>;
    let comp: ExchangeOpsUserClass;
    let exchangeOpsUserServiceStub: SinonStubbedInstance<ExchangeOpsUserService>;

    beforeEach(() => {
      exchangeOpsUserServiceStub = sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService);

      wrapper = shallowMount<ExchangeOpsUserClass>(ExchangeOpsUserUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          exchangeOpsUserService: () => exchangeOpsUserServiceStub,
          alertService: () => new AlertService(),

          clientService: () =>
            sinon.createStubInstance<ClientService>(ClientService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          deliveryPartnerService: () =>
            sinon.createStubInstance<DeliveryPartnerService>(DeliveryPartnerService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorService: () =>
            sinon.createStubInstance<VendorService>(VendorService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorHubService: () =>
            sinon.createStubInstance<VendorHubService>(VendorHubService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          vendorHubMappingService: () =>
            sinon.createStubInstance<VendorHubMappingService>(VendorHubMappingService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangePriceService: () =>
            sinon.createStubInstance<ExchangePriceService>(ExchangePriceService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          differentialAmountService: () =>
            sinon.createStubInstance<DifferentialAmountService>(DifferentialAmountService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.exchangeOpsUser = entity;
        exchangeOpsUserServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeOpsUserServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.exchangeOpsUser = entity;
        exchangeOpsUserServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeOpsUserServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeOpsUser = { id: 123 };
        exchangeOpsUserServiceStub.find.resolves(foundExchangeOpsUser);
        exchangeOpsUserServiceStub.retrieve.resolves([foundExchangeOpsUser]);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeOpsUserId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeOpsUser).toBe(foundExchangeOpsUser);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
