/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ExchangeOpsUserComponent from '@/entities/exchange-ops-user/exchange-ops-user.vue';
import ExchangeOpsUserClass from '@/entities/exchange-ops-user/exchange-ops-user.component';
import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('ExchangeOpsUser Management Component', () => {
    let wrapper: Wrapper<ExchangeOpsUserClass>;
    let comp: ExchangeOpsUserClass;
    let exchangeOpsUserServiceStub: SinonStubbedInstance<ExchangeOpsUserService>;

    beforeEach(() => {
      exchangeOpsUserServiceStub = sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService);
      exchangeOpsUserServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ExchangeOpsUserClass>(ExchangeOpsUserComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          exchangeOpsUserService: () => exchangeOpsUserServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      exchangeOpsUserServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllExchangeOpsUsers();
      await comp.$nextTick();

      // THEN
      expect(exchangeOpsUserServiceStub.retrieve.called).toBeTruthy();
      expect(comp.exchangeOpsUsers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      exchangeOpsUserServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(exchangeOpsUserServiceStub.retrieve.called).toBeTruthy();
      expect(comp.exchangeOpsUsers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      exchangeOpsUserServiceStub.retrieve.reset();
      exchangeOpsUserServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(exchangeOpsUserServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.exchangeOpsUsers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      exchangeOpsUserServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(exchangeOpsUserServiceStub.retrieve.callCount).toEqual(1);

      comp.removeExchangeOpsUser();
      await comp.$nextTick();

      // THEN
      expect(exchangeOpsUserServiceStub.delete.called).toBeTruthy();
      expect(exchangeOpsUserServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
