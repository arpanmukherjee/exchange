/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ExchangeOpsUserDetailComponent from '@/entities/exchange-ops-user/exchange-ops-user-details.vue';
import ExchangeOpsUserClass from '@/entities/exchange-ops-user/exchange-ops-user-details.component';
import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ExchangeOpsUser Management Detail Component', () => {
    let wrapper: Wrapper<ExchangeOpsUserClass>;
    let comp: ExchangeOpsUserClass;
    let exchangeOpsUserServiceStub: SinonStubbedInstance<ExchangeOpsUserService>;

    beforeEach(() => {
      exchangeOpsUserServiceStub = sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService);

      wrapper = shallowMount<ExchangeOpsUserClass>(ExchangeOpsUserDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { exchangeOpsUserService: () => exchangeOpsUserServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundExchangeOpsUser = { id: 123 };
        exchangeOpsUserServiceStub.find.resolves(foundExchangeOpsUser);

        // WHEN
        comp.retrieveExchangeOpsUser(123);
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeOpsUser).toBe(foundExchangeOpsUser);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeOpsUser = { id: 123 };
        exchangeOpsUserServiceStub.find.resolves(foundExchangeOpsUser);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeOpsUserId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeOpsUser).toBe(foundExchangeOpsUser);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
