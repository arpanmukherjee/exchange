/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ExchangeAnswerSessionDetailComponent from '@/entities/exchange-answer-session/exchange-answer-session-details.vue';
import ExchangeAnswerSessionClass from '@/entities/exchange-answer-session/exchange-answer-session-details.component';
import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ExchangeAnswerSession Management Detail Component', () => {
    let wrapper: Wrapper<ExchangeAnswerSessionClass>;
    let comp: ExchangeAnswerSessionClass;
    let exchangeAnswerSessionServiceStub: SinonStubbedInstance<ExchangeAnswerSessionService>;

    beforeEach(() => {
      exchangeAnswerSessionServiceStub = sinon.createStubInstance<ExchangeAnswerSessionService>(ExchangeAnswerSessionService);

      wrapper = shallowMount<ExchangeAnswerSessionClass>(ExchangeAnswerSessionDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { exchangeAnswerSessionService: () => exchangeAnswerSessionServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundExchangeAnswerSession = { id: 123 };
        exchangeAnswerSessionServiceStub.find.resolves(foundExchangeAnswerSession);

        // WHEN
        comp.retrieveExchangeAnswerSession(123);
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswerSession).toBe(foundExchangeAnswerSession);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeAnswerSession = { id: 123 };
        exchangeAnswerSessionServiceStub.find.resolves(foundExchangeAnswerSession);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeAnswerSessionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswerSession).toBe(foundExchangeAnswerSession);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
