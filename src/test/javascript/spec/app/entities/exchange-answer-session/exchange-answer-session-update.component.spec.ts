/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ExchangeAnswerSessionUpdateComponent from '@/entities/exchange-answer-session/exchange-answer-session-update.vue';
import ExchangeAnswerSessionClass from '@/entities/exchange-answer-session/exchange-answer-session-update.component';
import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';

import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';

import ExchangeAnswerService from '@/entities/exchange-answer/exchange-answer.service';

import ClientService from '@/entities/client/client.service';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ExchangeAnswerSession Management Update Component', () => {
    let wrapper: Wrapper<ExchangeAnswerSessionClass>;
    let comp: ExchangeAnswerSessionClass;
    let exchangeAnswerSessionServiceStub: SinonStubbedInstance<ExchangeAnswerSessionService>;

    beforeEach(() => {
      exchangeAnswerSessionServiceStub = sinon.createStubInstance<ExchangeAnswerSessionService>(ExchangeAnswerSessionService);

      wrapper = shallowMount<ExchangeAnswerSessionClass>(ExchangeAnswerSessionUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          exchangeAnswerSessionService: () => exchangeAnswerSessionServiceStub,
          alertService: () => new AlertService(),

          exchangePriceService: () =>
            sinon.createStubInstance<ExchangePriceService>(ExchangePriceService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeAnswerService: () =>
            sinon.createStubInstance<ExchangeAnswerService>(ExchangeAnswerService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          clientService: () =>
            sinon.createStubInstance<ClientService>(ClientService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeOrderService: () =>
            sinon.createStubInstance<ExchangeOrderService>(ExchangeOrderService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.exchangeAnswerSession = entity;
        exchangeAnswerSessionServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeAnswerSessionServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.exchangeAnswerSession = entity;
        exchangeAnswerSessionServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeAnswerSessionServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeAnswerSession = { id: 123 };
        exchangeAnswerSessionServiceStub.find.resolves(foundExchangeAnswerSession);
        exchangeAnswerSessionServiceStub.retrieve.resolves([foundExchangeAnswerSession]);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeAnswerSessionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswerSession).toBe(foundExchangeAnswerSession);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
