/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import DifferentialAmountUpdateComponent from '@/entities/differential-amount/differential-amount-update.vue';
import DifferentialAmountClass from '@/entities/differential-amount/differential-amount-update.component';
import DifferentialAmountService from '@/entities/differential-amount/differential-amount.service';

import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('DifferentialAmount Management Update Component', () => {
    let wrapper: Wrapper<DifferentialAmountClass>;
    let comp: DifferentialAmountClass;
    let differentialAmountServiceStub: SinonStubbedInstance<DifferentialAmountService>;

    beforeEach(() => {
      differentialAmountServiceStub = sinon.createStubInstance<DifferentialAmountService>(DifferentialAmountService);

      wrapper = shallowMount<DifferentialAmountClass>(DifferentialAmountUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          differentialAmountService: () => differentialAmountServiceStub,
          alertService: () => new AlertService(),

          exchangeOrderService: () =>
            sinon.createStubInstance<ExchangeOrderService>(ExchangeOrderService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeOpsUserService: () =>
            sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.differentialAmount = entity;
        differentialAmountServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(differentialAmountServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.differentialAmount = entity;
        differentialAmountServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(differentialAmountServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDifferentialAmount = { id: 123 };
        differentialAmountServiceStub.find.resolves(foundDifferentialAmount);
        differentialAmountServiceStub.retrieve.resolves([foundDifferentialAmount]);

        // WHEN
        comp.beforeRouteEnter({ params: { differentialAmountId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.differentialAmount).toBe(foundDifferentialAmount);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
