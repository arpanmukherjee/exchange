/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import DifferentialAmountDetailComponent from '@/entities/differential-amount/differential-amount-details.vue';
import DifferentialAmountClass from '@/entities/differential-amount/differential-amount-details.component';
import DifferentialAmountService from '@/entities/differential-amount/differential-amount.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('DifferentialAmount Management Detail Component', () => {
    let wrapper: Wrapper<DifferentialAmountClass>;
    let comp: DifferentialAmountClass;
    let differentialAmountServiceStub: SinonStubbedInstance<DifferentialAmountService>;

    beforeEach(() => {
      differentialAmountServiceStub = sinon.createStubInstance<DifferentialAmountService>(DifferentialAmountService);

      wrapper = shallowMount<DifferentialAmountClass>(DifferentialAmountDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { differentialAmountService: () => differentialAmountServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundDifferentialAmount = { id: 123 };
        differentialAmountServiceStub.find.resolves(foundDifferentialAmount);

        // WHEN
        comp.retrieveDifferentialAmount(123);
        await comp.$nextTick();

        // THEN
        expect(comp.differentialAmount).toBe(foundDifferentialAmount);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDifferentialAmount = { id: 123 };
        differentialAmountServiceStub.find.resolves(foundDifferentialAmount);

        // WHEN
        comp.beforeRouteEnter({ params: { differentialAmountId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.differentialAmount).toBe(foundDifferentialAmount);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
