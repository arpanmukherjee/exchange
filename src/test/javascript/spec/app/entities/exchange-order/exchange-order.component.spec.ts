/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ExchangeOrderComponent from '@/entities/exchange-order/exchange-order.vue';
import ExchangeOrderClass from '@/entities/exchange-order/exchange-order.component';
import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('ExchangeOrder Management Component', () => {
    let wrapper: Wrapper<ExchangeOrderClass>;
    let comp: ExchangeOrderClass;
    let exchangeOrderServiceStub: SinonStubbedInstance<ExchangeOrderService>;

    beforeEach(() => {
      exchangeOrderServiceStub = sinon.createStubInstance<ExchangeOrderService>(ExchangeOrderService);
      exchangeOrderServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ExchangeOrderClass>(ExchangeOrderComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          exchangeOrderService: () => exchangeOrderServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      exchangeOrderServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllExchangeOrders();
      await comp.$nextTick();

      // THEN
      expect(exchangeOrderServiceStub.retrieve.called).toBeTruthy();
      expect(comp.exchangeOrders[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      exchangeOrderServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(exchangeOrderServiceStub.retrieve.called).toBeTruthy();
      expect(comp.exchangeOrders[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      exchangeOrderServiceStub.retrieve.reset();
      exchangeOrderServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(exchangeOrderServiceStub.retrieve.callCount).toEqual(2);
      expect(comp.page).toEqual(1);
      expect(comp.exchangeOrders[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      exchangeOrderServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(exchangeOrderServiceStub.retrieve.callCount).toEqual(1);

      comp.removeExchangeOrder();
      await comp.$nextTick();

      // THEN
      expect(exchangeOrderServiceStub.delete.called).toBeTruthy();
      expect(exchangeOrderServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
