/* tslint:disable max-line-length */
import axios from 'axios';
import sinon from 'sinon';
import dayjs from 'dayjs';

import { DATE_FORMAT } from '@/shared/date/filters';
import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';
import { ExchangeOrder } from '@/shared/model/exchange-order.model';
import { ExchangeOrderStatus } from '@/shared/model/enumerations/exchange-order-status.model';
import { CancellationReason } from '@/shared/model/enumerations/cancellation-reason.model';

const error = {
  response: {
    status: null,
    data: {
      type: null,
    },
  },
};

const axiosStub = {
  get: sinon.stub(axios, 'get'),
  post: sinon.stub(axios, 'post'),
  put: sinon.stub(axios, 'put'),
  patch: sinon.stub(axios, 'patch'),
  delete: sinon.stub(axios, 'delete'),
};

describe('Service Tests', () => {
  describe('ExchangeOrder Service', () => {
    let service: ExchangeOrderService;
    let elemDefault;
    let currentDate: Date;

    beforeEach(() => {
      service = new ExchangeOrderService();
      currentDate = new Date();
      elemDefault = new ExchangeOrder(
        123,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        'AAAAAAA',
        ExchangeOrderStatus.Created,
        CancellationReason.CancelledByCustomer,
        currentDate,
        currentDate,
        currentDate,
        currentDate,
        0,
        currentDate,
        0,
        currentDate,
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            expectedPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            customerPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            dcDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            dcPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            vendorDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            createdAt: dayjs(currentDate).format(DATE_FORMAT),
            updatedAt: dayjs(currentDate).format(DATE_FORMAT),
          },
          elemDefault
        );
        axiosStub.get.resolves({ data: returnedFromService });

        return service.find(123).then(res => {
          expect(res).toMatchObject(elemDefault);
        });
      });

      it('should not find an element', async () => {
        axiosStub.get.rejects(error);
        return service
          .find(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should create a ExchangeOrder', async () => {
        const returnedFromService = Object.assign(
          {
            id: 123,
            expectedPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            customerPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            dcDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            dcPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            vendorDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            createdAt: dayjs(currentDate).format(DATE_FORMAT),
            updatedAt: dayjs(currentDate).format(DATE_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            expectedPickupDate: currentDate,
            customerPickupDate: currentDate,
            dcDeliveredDate: currentDate,
            dcPickupDate: currentDate,
            vendorDeliveredDate: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );

        axiosStub.post.resolves({ data: returnedFromService });
        return service.create({}).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not create a ExchangeOrder', async () => {
        axiosStub.post.rejects(error);

        return service
          .create({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should update a ExchangeOrder', async () => {
        const returnedFromService = Object.assign(
          {
            clientOrderRefId: 'BBBBBB',
            customerPhoneNo: 'BBBBBB',
            clientOrderDeliveryRefId: 'BBBBBB',
            deliveryPartnerPhoneNo: 'BBBBBB',
            totalOrderAmount: 1,
            paidAmount: 1,
            codInitialPendingAmount: 1,
            exchangeEstimateAmount: 1,
            exchangeFinalAmountToUser: 1,
            codFinalAmount: 1,
            codAmountPaid: 1,
            codPaymentTxnRefId: 'BBBBBB',
            status: 'BBBBBB',
            cancellationReason: 'BBBBBB',
            expectedPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            customerPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            dcDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            dcPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            exchangeFinalAmountToDc: 1,
            vendorDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            exchangeFinalAmountToVendor: 1,
            createdAt: dayjs(currentDate).format(DATE_FORMAT),
            updatedAt: dayjs(currentDate).format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            expectedPickupDate: currentDate,
            customerPickupDate: currentDate,
            dcDeliveredDate: currentDate,
            dcPickupDate: currentDate,
            vendorDeliveredDate: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );
        axiosStub.put.resolves({ data: returnedFromService });

        return service.update(expected).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not update a ExchangeOrder', async () => {
        axiosStub.put.rejects(error);

        return service
          .update({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should partial update a ExchangeOrder', async () => {
        const patchObject = Object.assign(
          {
            clientOrderRefId: 'BBBBBB',
            clientOrderDeliveryRefId: 'BBBBBB',
            deliveryPartnerPhoneNo: 'BBBBBB',
            totalOrderAmount: 1,
            paidAmount: 1,
            codInitialPendingAmount: 1,
            exchangeEstimateAmount: 1,
            codFinalAmount: 1,
            cancellationReason: 'BBBBBB',
            expectedPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            customerPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            vendorDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            createdAt: dayjs(currentDate).format(DATE_FORMAT),
            updatedAt: dayjs(currentDate).format(DATE_FORMAT),
          },
          new ExchangeOrder()
        );
        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            expectedPickupDate: currentDate,
            customerPickupDate: currentDate,
            dcDeliveredDate: currentDate,
            dcPickupDate: currentDate,
            vendorDeliveredDate: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );
        axiosStub.patch.resolves({ data: returnedFromService });

        return service.partialUpdate(patchObject).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not partial update a ExchangeOrder', async () => {
        axiosStub.patch.rejects(error);

        return service
          .partialUpdate({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should return a list of ExchangeOrder', async () => {
        const returnedFromService = Object.assign(
          {
            clientOrderRefId: 'BBBBBB',
            customerPhoneNo: 'BBBBBB',
            clientOrderDeliveryRefId: 'BBBBBB',
            deliveryPartnerPhoneNo: 'BBBBBB',
            totalOrderAmount: 1,
            paidAmount: 1,
            codInitialPendingAmount: 1,
            exchangeEstimateAmount: 1,
            exchangeFinalAmountToUser: 1,
            codFinalAmount: 1,
            codAmountPaid: 1,
            codPaymentTxnRefId: 'BBBBBB',
            status: 'BBBBBB',
            cancellationReason: 'BBBBBB',
            expectedPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            customerPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            dcDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            dcPickupDate: dayjs(currentDate).format(DATE_FORMAT),
            exchangeFinalAmountToDc: 1,
            vendorDeliveredDate: dayjs(currentDate).format(DATE_FORMAT),
            exchangeFinalAmountToVendor: 1,
            createdAt: dayjs(currentDate).format(DATE_FORMAT),
            updatedAt: dayjs(currentDate).format(DATE_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            expectedPickupDate: currentDate,
            customerPickupDate: currentDate,
            dcDeliveredDate: currentDate,
            dcPickupDate: currentDate,
            vendorDeliveredDate: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );
        axiosStub.get.resolves([returnedFromService]);
        return service.retrieve({ sort: {}, page: 0, size: 10 }).then(res => {
          expect(res).toContainEqual(expected);
        });
      });

      it('should not return a list of ExchangeOrder', async () => {
        axiosStub.get.rejects(error);

        return service
          .retrieve()
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should delete a ExchangeOrder', async () => {
        axiosStub.delete.resolves({ ok: true });
        return service.delete(123).then(res => {
          expect(res.ok).toBeTruthy();
        });
      });

      it('should not delete a ExchangeOrder', async () => {
        axiosStub.delete.rejects(error);

        return service
          .delete(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });
    });
  });
});
