/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ExchangeOrderUpdateComponent from '@/entities/exchange-order/exchange-order-update.vue';
import ExchangeOrderClass from '@/entities/exchange-order/exchange-order-update.component';
import ExchangeOrderService from '@/entities/exchange-order/exchange-order.service';

import AddressService from '@/entities/address/address.service';

import DocumentService from '@/entities/document/document.service';

import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';

import DifferentialAmountService from '@/entities/differential-amount/differential-amount.service';

import ClientService from '@/entities/client/client.service';

import ProductContextService from '@/entities/product-context/product-context.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ExchangeOrder Management Update Component', () => {
    let wrapper: Wrapper<ExchangeOrderClass>;
    let comp: ExchangeOrderClass;
    let exchangeOrderServiceStub: SinonStubbedInstance<ExchangeOrderService>;

    beforeEach(() => {
      exchangeOrderServiceStub = sinon.createStubInstance<ExchangeOrderService>(ExchangeOrderService);

      wrapper = shallowMount<ExchangeOrderClass>(ExchangeOrderUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          exchangeOrderService: () => exchangeOrderServiceStub,
          alertService: () => new AlertService(),

          addressService: () =>
            sinon.createStubInstance<AddressService>(AddressService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          documentService: () =>
            sinon.createStubInstance<DocumentService>(DocumentService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeAnswerSessionService: () =>
            sinon.createStubInstance<ExchangeAnswerSessionService>(ExchangeAnswerSessionService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          differentialAmountService: () =>
            sinon.createStubInstance<DifferentialAmountService>(DifferentialAmountService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          clientService: () =>
            sinon.createStubInstance<ClientService>(ClientService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          productContextService: () =>
            sinon.createStubInstance<ProductContextService>(ProductContextService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.exchangeOrder = entity;
        exchangeOrderServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeOrderServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.exchangeOrder = entity;
        exchangeOrderServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeOrderServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeOrder = { id: 123 };
        exchangeOrderServiceStub.find.resolves(foundExchangeOrder);
        exchangeOrderServiceStub.retrieve.resolves([foundExchangeOrder]);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeOrderId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeOrder).toBe(foundExchangeOrder);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
