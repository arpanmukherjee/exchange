/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ExchangeAnswerSetDetailComponent from '@/entities/exchange-answer-set/exchange-answer-set-details.vue';
import ExchangeAnswerSetClass from '@/entities/exchange-answer-set/exchange-answer-set-details.component';
import ExchangeAnswerSetService from '@/entities/exchange-answer-set/exchange-answer-set.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ExchangeAnswerSet Management Detail Component', () => {
    let wrapper: Wrapper<ExchangeAnswerSetClass>;
    let comp: ExchangeAnswerSetClass;
    let exchangeAnswerSetServiceStub: SinonStubbedInstance<ExchangeAnswerSetService>;

    beforeEach(() => {
      exchangeAnswerSetServiceStub = sinon.createStubInstance<ExchangeAnswerSetService>(ExchangeAnswerSetService);

      wrapper = shallowMount<ExchangeAnswerSetClass>(ExchangeAnswerSetDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { exchangeAnswerSetService: () => exchangeAnswerSetServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundExchangeAnswerSet = { id: 123 };
        exchangeAnswerSetServiceStub.find.resolves(foundExchangeAnswerSet);

        // WHEN
        comp.retrieveExchangeAnswerSet(123);
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswerSet).toBe(foundExchangeAnswerSet);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeAnswerSet = { id: 123 };
        exchangeAnswerSetServiceStub.find.resolves(foundExchangeAnswerSet);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeAnswerSetId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswerSet).toBe(foundExchangeAnswerSet);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
