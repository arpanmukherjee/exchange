/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ExchangePriceDetailComponent from '@/entities/exchange-price/exchange-price-details.vue';
import ExchangePriceClass from '@/entities/exchange-price/exchange-price-details.component';
import ExchangePriceService from '@/entities/exchange-price/exchange-price.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ExchangePrice Management Detail Component', () => {
    let wrapper: Wrapper<ExchangePriceClass>;
    let comp: ExchangePriceClass;
    let exchangePriceServiceStub: SinonStubbedInstance<ExchangePriceService>;

    beforeEach(() => {
      exchangePriceServiceStub = sinon.createStubInstance<ExchangePriceService>(ExchangePriceService);

      wrapper = shallowMount<ExchangePriceClass>(ExchangePriceDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { exchangePriceService: () => exchangePriceServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundExchangePrice = { id: 123 };
        exchangePriceServiceStub.find.resolves(foundExchangePrice);

        // WHEN
        comp.retrieveExchangePrice(123);
        await comp.$nextTick();

        // THEN
        expect(comp.exchangePrice).toBe(foundExchangePrice);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangePrice = { id: 123 };
        exchangePriceServiceStub.find.resolves(foundExchangePrice);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangePriceId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangePrice).toBe(foundExchangePrice);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
