/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ExchangeAnswerDetailComponent from '@/entities/exchange-answer/exchange-answer-details.vue';
import ExchangeAnswerClass from '@/entities/exchange-answer/exchange-answer-details.component';
import ExchangeAnswerService from '@/entities/exchange-answer/exchange-answer.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ExchangeAnswer Management Detail Component', () => {
    let wrapper: Wrapper<ExchangeAnswerClass>;
    let comp: ExchangeAnswerClass;
    let exchangeAnswerServiceStub: SinonStubbedInstance<ExchangeAnswerService>;

    beforeEach(() => {
      exchangeAnswerServiceStub = sinon.createStubInstance<ExchangeAnswerService>(ExchangeAnswerService);

      wrapper = shallowMount<ExchangeAnswerClass>(ExchangeAnswerDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { exchangeAnswerService: () => exchangeAnswerServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundExchangeAnswer = { id: 123 };
        exchangeAnswerServiceStub.find.resolves(foundExchangeAnswer);

        // WHEN
        comp.retrieveExchangeAnswer(123);
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswer).toBe(foundExchangeAnswer);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeAnswer = { id: 123 };
        exchangeAnswerServiceStub.find.resolves(foundExchangeAnswer);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeAnswerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswer).toBe(foundExchangeAnswer);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
