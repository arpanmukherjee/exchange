/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ExchangeAnswerUpdateComponent from '@/entities/exchange-answer/exchange-answer-update.vue';
import ExchangeAnswerClass from '@/entities/exchange-answer/exchange-answer-update.component';
import ExchangeAnswerService from '@/entities/exchange-answer/exchange-answer.service';

import QuestionService from '@/entities/question/question.service';

import OptionService from '@/entities/option/option.service';

import ExchangeAnswerSetService from '@/entities/exchange-answer-set/exchange-answer-set.service';

import ExchangeAnswerSessionService from '@/entities/exchange-answer-session/exchange-answer-session.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ExchangeAnswer Management Update Component', () => {
    let wrapper: Wrapper<ExchangeAnswerClass>;
    let comp: ExchangeAnswerClass;
    let exchangeAnswerServiceStub: SinonStubbedInstance<ExchangeAnswerService>;

    beforeEach(() => {
      exchangeAnswerServiceStub = sinon.createStubInstance<ExchangeAnswerService>(ExchangeAnswerService);

      wrapper = shallowMount<ExchangeAnswerClass>(ExchangeAnswerUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          exchangeAnswerService: () => exchangeAnswerServiceStub,
          alertService: () => new AlertService(),

          questionService: () =>
            sinon.createStubInstance<QuestionService>(QuestionService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          optionService: () =>
            sinon.createStubInstance<OptionService>(OptionService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeAnswerSetService: () =>
            sinon.createStubInstance<ExchangeAnswerSetService>(ExchangeAnswerSetService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          exchangeAnswerSessionService: () =>
            sinon.createStubInstance<ExchangeAnswerSessionService>(ExchangeAnswerSessionService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.exchangeAnswer = entity;
        exchangeAnswerServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeAnswerServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.exchangeAnswer = entity;
        exchangeAnswerServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(exchangeAnswerServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundExchangeAnswer = { id: 123 };
        exchangeAnswerServiceStub.find.resolves(foundExchangeAnswer);
        exchangeAnswerServiceStub.retrieve.resolves([foundExchangeAnswer]);

        // WHEN
        comp.beforeRouteEnter({ params: { exchangeAnswerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.exchangeAnswer).toBe(foundExchangeAnswer);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
