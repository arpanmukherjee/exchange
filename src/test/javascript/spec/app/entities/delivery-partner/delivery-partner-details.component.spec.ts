/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import DeliveryPartnerDetailComponent from '@/entities/delivery-partner/delivery-partner-details.vue';
import DeliveryPartnerClass from '@/entities/delivery-partner/delivery-partner-details.component';
import DeliveryPartnerService from '@/entities/delivery-partner/delivery-partner.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('DeliveryPartner Management Detail Component', () => {
    let wrapper: Wrapper<DeliveryPartnerClass>;
    let comp: DeliveryPartnerClass;
    let deliveryPartnerServiceStub: SinonStubbedInstance<DeliveryPartnerService>;

    beforeEach(() => {
      deliveryPartnerServiceStub = sinon.createStubInstance<DeliveryPartnerService>(DeliveryPartnerService);

      wrapper = shallowMount<DeliveryPartnerClass>(DeliveryPartnerDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { deliveryPartnerService: () => deliveryPartnerServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundDeliveryPartner = { id: 123 };
        deliveryPartnerServiceStub.find.resolves(foundDeliveryPartner);

        // WHEN
        comp.retrieveDeliveryPartner(123);
        await comp.$nextTick();

        // THEN
        expect(comp.deliveryPartner).toBe(foundDeliveryPartner);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDeliveryPartner = { id: 123 };
        deliveryPartnerServiceStub.find.resolves(foundDeliveryPartner);

        // WHEN
        comp.beforeRouteEnter({ params: { deliveryPartnerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.deliveryPartner).toBe(foundDeliveryPartner);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
