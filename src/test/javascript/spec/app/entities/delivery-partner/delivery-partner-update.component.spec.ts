/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import DeliveryPartnerUpdateComponent from '@/entities/delivery-partner/delivery-partner-update.vue';
import DeliveryPartnerClass from '@/entities/delivery-partner/delivery-partner-update.component';
import DeliveryPartnerService from '@/entities/delivery-partner/delivery-partner.service';

import ExchangeOpsUserService from '@/entities/exchange-ops-user/exchange-ops-user.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('DeliveryPartner Management Update Component', () => {
    let wrapper: Wrapper<DeliveryPartnerClass>;
    let comp: DeliveryPartnerClass;
    let deliveryPartnerServiceStub: SinonStubbedInstance<DeliveryPartnerService>;

    beforeEach(() => {
      deliveryPartnerServiceStub = sinon.createStubInstance<DeliveryPartnerService>(DeliveryPartnerService);

      wrapper = shallowMount<DeliveryPartnerClass>(DeliveryPartnerUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          deliveryPartnerService: () => deliveryPartnerServiceStub,
          alertService: () => new AlertService(),

          exchangeOpsUserService: () =>
            sinon.createStubInstance<ExchangeOpsUserService>(ExchangeOpsUserService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.deliveryPartner = entity;
        deliveryPartnerServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(deliveryPartnerServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.deliveryPartner = entity;
        deliveryPartnerServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(deliveryPartnerServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDeliveryPartner = { id: 123 };
        deliveryPartnerServiceStub.find.resolves(foundDeliveryPartner);
        deliveryPartnerServiceStub.retrieve.resolves([foundDeliveryPartner]);

        // WHEN
        comp.beforeRouteEnter({ params: { deliveryPartnerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.deliveryPartner).toBe(foundDeliveryPartner);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
